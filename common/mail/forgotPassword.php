<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->request->hostInfo."/#/login";

;
?>
<div class="password-reset">
    <p>Hello  <?= Html::encode($user->username) ?>,</p>

    <p>Your new password :</p>

    <p><?= Html::encode($new_pass) ?></p>

    <p><?= Html::a($resetLink, $resetLink) ?></p>
</div>

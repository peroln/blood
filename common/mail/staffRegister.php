<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->request->hostInfo."/#/login";

;
?>
<div class="password-reset">

    <p>You have been registered in the web-site <?= Html::a($resetLink, $resetLink) ?>. </p>

    <p>Username:<?= Html::encode($user->username) ?></p>
    <p>Your password:<?= Html::encode($user->password) ?></p>

</div>

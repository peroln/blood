<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $user common\models\Donor */

$resetLink = Yii::$app->request->hostInfo."/#/login";

;
?>
<div class="password-reset">

    <p>New donor was registered in the system. </p>

    <p>Surname:<?= Html::encode($user->last_name) ?></p>
    <p>First Name:<?= Html::encode($user->first_name) ?></p>
    <p>Email Address:<?= Html::encode($user->email) ?></p>
    <p>Telephone No:<?= Html::encode($user->mobile) ?></p>

    <p><?= Html::a($resetLink, $resetLink) ?></p>
</div>

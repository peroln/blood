<?php

namespace common\models;

use Yii;
use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;
/**
 * This is the model class for table "questionnaire_answer".
 *
 * @property integer $id
 * @property integer $questionnaire_id
 * @property integer $title_id
 * @property integer $category_id
 * @property string $answer
 * @property integer $deleted
 *
 * @property QuestionTitle $title
 * @property Questionnaire $questionnaire
 */
class QuestionnaireAnswer extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionnaire_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionnaire_id', 'title_id', 'category_id'], 'required'],
            [['answer'], 'required', 'on' =>'recw'],
            [['questionnaire_id', 'title_id', 'deleted'], 'integer'],
            [['answer'], 'string'],
            [['title_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionTitle::className(), 'targetAttribute' => ['title_id' => 'id']],
            [['questionnaire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionnaire::className(), 'targetAttribute' => ['questionnaire_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionnaire_id' => 'Questionnaire ID',
            'title_id' => 'Title ID',
            'answer' => 'Answer',
            'deleted' => 'Deleted',
        ];
    }

    public function findModel()
    {
        return self::find()->where([
            'questionnaire_id' => $this->questionnaire_id,
            'title_id' => $this->title_id,
        ])->one();
    }

    public function saveModel()
    {
        $item = $this->findModel();
        if ($item) {
            $item->answer = $this->answer;
            if ($item->deleted == self::DELETED) {
                $item->deleted = self::NOT_DELETED;
            }
            if (!$item->save()) {
                return $this->addError('answer','Record not saved');
            }
            return true;
        } else {
            return $this->save();
        }
    }

    public function saveAnswers($categories, $parent_id)
    {
        if (is_array($categories)) {
            foreach ($categories as $category) {

                foreach($category['titles'] as $one) {
                    $model = new QuestionnaireAnswer();
                    $model->questionnaire_id = $parent_id;
                    $model->category_id = $category['id'];
                    $model->title_id = $one['id'];
                    if($one['mandatory'] == 0){
                        $model->setScenario('recw');
                    }
                    if($one['answer']){
                        $model->answer = strval($one['answer']);
                    } elseif($one['type'] == QuestionTitle::TYPE_CHECKBOX){
                        $model->answer = "false";
                    }
                    if (!$model->saveModel()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    public static function deleteAnswers($diffs){
        if ($diffs) {
            foreach ($diffs as $one) {
                //удаление тех записей, которых нет в новом массиве
                $item = self::findOne($one->id);
                if(!$item->remove()){
                    return $item;
                }
            }
        }
        return true;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle()
    {
        return $this->hasOne(QuestionTitle::className(), ['id' => 'title_id']) ->andOnCondition([
            'question_title.deleted' => self::NOT_DELETED,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaire()
    {
        return $this->hasOne(Questionnaire::className(), ['id' => 'questionnaire_id']) ->andOnCondition([
            'questionnaire.deleted' => self::NOT_DELETED,
        ]);
    }


}

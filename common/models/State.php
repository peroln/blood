<?php

namespace common\models;


use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;

/**
 * This is the model class for table "state".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property string $description
 * @property integer $deleted
 * @property integer $capital_id
 *
 * @property Area[] $areas
 * @property Country $country
 */
class State extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;

    const NOT_DELETED = 0;
    const DELETED = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id', 'deleted', 'capital_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
            'description' => 'Description',
            'deleted' => 'Deleted',
            'capital_id' => 'Capital ID',
        ];
    }


    public function beforeValidate()
    {
        if($this->isNewRecord){
            return $this->country_id = 1;
        }
        return true;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['state_id' => 'id'])
            ->andOnCondition([
                'area.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->andOnCondition([
                'country.deleted' => self::NOT_DELETED,
            ]);
    }
}

<?php
namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;

use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class PasswordChangeForm extends Model
{
    use soft;
    use errors;

    public $password;
    public $new_password;

    /**
     * @var User
     */
    private $_user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['password', 'new_password'], 'required'],
            ['password', 'validatePassword'],
            ['new_password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'new_password' => Yii::t('app', 'new password'),
            'password' => Yii::t('app', 'password'),
        ];
    }

    public static function FirstError($errors){
        foreach ($errors as $error){
            return ['error' => $error[0]];
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->_user->validatePassword($this->$attribute)) {
                $this->addError($attribute, Yii::t('app', 'Error. Wrong password.'));
            }
        }
    }

    /**
     * @return boolean
     */
    public function changePassword()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->setPassword($this->new_password);
            return $user->save();
        } else {
            return $this;
        }
    }
}

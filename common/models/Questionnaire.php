<?php

namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;


use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "donor_questionnaire".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $date
 * @property string $questionnaire_number
 * @property integer $donor_id
 * @property integer $location_id
 * @property integer $deleted
 *
 * @property Donor $donor
 * @property Location $location
 * @property QuestionnaireAnswer[] $questionnaireAnswers
 * @property QuestionCategory[] $questionnaireCategories
 *  * @property User $creator
 */
class Questionnaire extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $answers;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionnaire';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'questionnaire_number', 'donor_id',], 'required'],
            [['created_by', 'date', 'donor_id', 'location_id', 'deleted'], 'integer'],
            [['questionnaire_number'], 'string', 'max' => 255],
            [['questionnaire_number'], 'unique'],
            [['donor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Donor::className(), 'targetAttribute' => ['donor_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['answers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'date' => 'Date',
            'questionnaire_number' => 'Questionnaire Number',
            'donor_id' => 'Donor ID',
            'location_id' => 'Location ID',
            'deleted' => 'Deleted',
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Questionnaire' => [
                    'id',
                    'questionnaire_number',
                    'date',
                    'created_by',
                    'creator' => function ($model) {
                        return $model->creator->username;
                    },
                    'donor_id',
                    'location_id',
                    'location' => function ($model) {
                        return $model->location->name;
                    },
                    'deleted',
//                    'answers
                ],
            ]
        );
        return $result;
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'gender' => $this->donor->gender,
            'questionnaire_number' => $this->questionnaire_number,
            'date' => $this->date,
            'created_by' => $this->created_by,
            'creator' => $this->creator->username,
            'donor_id' => $this->donor_id,
            'first_name' => $this->donor->first_name,
            'last_name' => $this->donor->last_name,
            'location_id' => $this->location_id,
            'answers' => QuestionCategory::allAnswers($this->id)
        ];
    }

    public function saveModel()
    {
        $trans = Yii::$app->db->beginTransaction();
        if ($this->isNewRecord) {
            $this->questionnaire_number = Yii::$app->security->generateRandomString(12);
            $this->created_by = Yii::$app->user->id;
            $this->date = time();
            $this->location_id = $this->donor->location_id;
        }

        if (!$this->save()) {
            $trans->rollBack();
            return $this->getErrors();
        }

        if (Yii::$app->request->post('answers')) {
            $this->answers = Yii::$app->request->post('answers');
            $my_answer = new QuestionnaireAnswer;
            if (!$my_answer->saveAnswers($this->answers, $this->id)) {
                $trans->rollBack();
                return $my_answer->getErrors();
            }
        }

//        $donor = $this->donor;
//        $donor->date_guestionnaire = time();

//        if ($this->donor->guestionnaire_complited == 0 || $this->donor->guestionnaire_id != $this->questionnaire_number) {
//            $donor->guestionnaire_complited = 1;
//            $donor->guestionnaire_id = $this->questionnaire_number;
//        }

//        if (!$donor->save()) {
//            $trans->rollBack();
//            return $donor->getErrors();
//        }
        $trans->commit();
        return $this;
    }

    public static function deleteQuestionnaire($donor_id)
    {
        $records = self::findAll(['donor_id' => $donor_id, 'deleted' => self::NOT_DELETED]);
        if ($records) {
            foreach ($records as $one) {
                if (!$one->remove()) {
                    return $one;
                }
            }
        }
        return true;
    }

    public function remove()
    {
        $this->deleted = self::DELETED;
        if ($this->questionnaireAnswers) {
            $result = QuestionnaireAnswer::deleteAnswers($this->questionnaireAnswers);
            if ($result != true && $result->getErrors()) {
                $this->addError(['error' => $result->getErrors()]);
                return $this;
            }

        }
        return $this->save();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaireAnswers()
    {
        return $this->hasMany(QuestionnaireAnswer::className(), ['questionnaire_id' => 'id'])->andOnCondition([
            'questionnaire_answer.deleted' => self::NOT_DELETED,
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'donor_id'])
            ->andOnCondition([
                'donor.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id'])
            ->andOnCondition([
                'location.deleted' => self::NOT_DELETED,
            ]);
    }

    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => self::NOT_DELETED,
            ]);
    }
}

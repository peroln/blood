<?php

namespace common\models;

use Yii;

use common\components\traits\soft;
use common\components\traits\errors;
use common\components\traits\findRecords;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property integer $row_id
 * @property string $table
 * @property string $extension
 * @property string $url
 * @property integer $deleted
 */
class File extends \yii\db\ActiveRecord
{
    use findRecords;
    use errors;
    use soft;
    const TYPE_FILE = 1;
    const TYPE_IMAGE_BASE64 = 2;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_id', 'table', 'extension', 'url'], 'required'],
            [['row_id', 'deleted'], 'integer'],
            [['table', 'extension', 'url'], 'string', 'max' => 255],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'row_id' => 'Row ID',
            'table' => 'Table',
            'extension' => 'Extension',
            'url' => 'Url',
            'deleted' => 'Deleted',
        ];
    }

    public function findModel()
    {
        return self::findOne(['row_id' => $this->row_id, 'table' => $this->table, 'deleted' => self::NOT_DELETED]);
    }

    public static function uploadOne($row_id, $table)
    {
        $file = new self;
        $result = Yii::$app->uploadFile->upload($row_id, $table);
        if (!$result) {
            return $file->addError('error', 'File not saved');
        }

        unset($_FILES);

        $file = new self;
        $file->row_id = $row_id;
        $file->table = $table;
        $item = $file->findModel();
        if ($item) {
            $item->extension = $result->file->extension;
            $item->url = "/" . $result->name . '.' . $result->file->extension;

            if ($item->deleted == self::DELETED) {
                $item->deleted = self::NOT_DELETED;
            }
            if (!$item->save()) {
                $file->addError('error', $item->getErrors());
            }
            return true;
        } else {
            $file->extension = $result->file->extension;
            $file->url = "/" . $result->name . '.' . $result->file->extension;
            if ($file->save()) {
                return true;
            }
        }
        return $file;

    }

    public static function uploadBase($row_id, $table, $name)
    {

        $result = Yii::$app->uploadFile->uploadBase($name, $row_id, $table);

        $file = new self;
        $file->row_id = $row_id;
        $file->table = $table;
        $item = $file->findModel();
        if ($item) {
            $item->extension = $result['extension'];
            $item->url = "/" . $result['name'];

            if ($item->deleted == self::DELETED) {
                $item->deleted = self::NOT_DELETED;
            }
            if (!$item->save()) {
                $file->addError('error', $item->getErrors());
            }
            return true;
        } else {
            $file->extension = $result['extension'];
            $file->url = "/" . $result['name'];
            if ($file->save()) {
                return true;
            }
        }
        return $file;

    }

    public static function deleteOneFile($row_id, $table)
    {
        $file = new self;
        $file->row_id = $row_id;
        $file->table = $table;
        $item = $file->findModel();
        return $item->remove();
    }


    public static function getFilePath($row_id, $table, $type)
    {
        $file = new self;
        $file->row_id = $row_id;
        $file->table = $table;
        $item = $file->findModel();
        if ($item) {
            if ($type == self::TYPE_FILE) {
                return Yii::$app->request->hostInfo . "/files/" . $item->table . "/" . $item->row_id . $item->url;
            } elseif ($type == self::TYPE_IMAGE_BASE64) {
                return Yii::$app->request->hostInfo . "/photo/" . $item->table . "/" . $item->row_id . $item->url;
            }
        }
        return false;
    }

    public function getFileDir($row_id, $table, $type)
    {
        $file = new self;
        $file->row_id = $row_id;
        $file->table = $table;
        $item = $file->findModel();
        if ($item) {
            if ($type == self::TYPE_FILE) {
                return dirname(Yii::getAlias('@app')) . "/files/" . $item->table . "/" . $item->row_id . $item->url;
            } elseif ($type == self::TYPE_IMAGE_BASE64) {
                return dirname(Yii::getAlias('@app')) . "/photo/" . $item->table . "/" . $item->row_id . $item->url;
            }
        }
        return false;
    }

    public function getParent()
    {
        return $this->hasOne(BloodDrive::className(), ['id' => 'row_id'])->andOnCondition([
            'blood_drive.deleted' => self::NOT_DELETED,
//            'blood_drive.disabled' => Donor::NOT_DISABLED,
        ]);
    }
}

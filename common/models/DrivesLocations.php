<?php

namespace common\models;

use common\components\traits\errors;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "drives_locations".
 *
 * @property integer $drive_id
 * @property integer $location_id
 *
 * @property BloodDrive $drive
 * @property Location $location
 */
class DrivesLocations extends \yii\db\ActiveRecord
{
    use errors;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drives_locations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drive_id', 'location_id'], 'required'],
            [['drive_id', 'location_id'], 'integer'],
            [['drive_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['drive_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'drive_id' => 'Drive ID',
            'location_id' => 'Location ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrive()
    {
        return $this->hasMany(BloodDrive::className(), ['id' => 'drive_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasMany(Location::className(), ['id' => 'location_id']);
    }

    /**
     * @param integer
     * @return mixed
     */
    static function findLocations($drive_id)
    {
        $query = (new Query())
            ->select([
                'drives_locations.location_id',
                'location.address',
                'location.another_address',
                'location.city_id',
                'location.area_id',
                'location.state_id',
                'location.office_phone',
                'location.another_phone',
                'location.email',
                'location.notes',
            ])
            ->from('drives_locations')
            ->leftJoin('location', 'location.id = drives_locations.location_id')
            ->where(['drives_locations.drive_id' => $drive_id])
            ->all();
        return $query;
    }
}

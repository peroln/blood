<?php

namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;

use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "blood_drive".
 *
 * @property integer $id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property string $contact_person
 * @property string $notes
 * @property string $created_at
 * @property integer $created_by
 * @property integer $disable
 * @property integer $deleted
 * @property integer $screen_report
 *
 * @property Bag[] $bags
 * @property BloodInfo $bloodInfo
 * @property DrivesBags[] $drivesBags
 * @property Bag[] $bags0
 * @property DrivesLocations[] $drivesLocations
 * @property Location[] $locations
 * @property DrivesStaffs[] $drivesStaffs
 * @property User[] $staff
 */
class BloodDrive extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;
    public $screen_report;
    public $staffs;
    public $locations;
    public $bags;


    const NOT_DISABLED = 0;
    const DISABLED = 1;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const NOT_BOUGHT= 0;
    const BOUGHT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_drive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', ], 'required'],
            [['created_by', 'disable', 'deleted'], 'integer'],
            [['name', 'start_date', 'end_date', 'contact_person', 'notes', 'created_at'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'contact_person' => 'Contact Person',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'disable' => 'Disable',
            'deleted' => 'Deleted',
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\BloodDrive' => [
                    'id',
                    'name',
                    'start_date',
                    'end_date',
                    'contact_person',
                    'notes',
                    'created_at',
                    'created_by',
                    'disable',
                    'deleted',
                    'bags' => $this->getDrivesBags(),
                    'locations' => $this->getDrivesLocations(),
                    'staffs' => $this->getDrivesStaffs(),

                ],
            ]
        );
        return $result;
    }
    public function one_fields()
    {

        return [
//            'id' => $this->id,
//            'donation_date' => $this->donation_date,
//            'donor_id' => $this->donor_id,
//            'donor' => $this->donor->first_name. " " .$this->donor->last_name,
//            'location_id' => $this->location_id,
//            'screening_agency_number' => $this->screening_agency_number,
//            'blood_bag_number' => $this->blood_bag_number,
//            'notes_bag' => $this->notes_bag,
//            'sent_for_screening' => $this->sent_for_screening,
//            'date_sent' => $this->date_sent,
//            'screening_status' => $this->screening_status,
//            'date_screen' => $this->date_screen,
//            'screen_result' => $this->screen_result,
//            'date_expiry' => $this->date_expiry,
//            'notes_screening' => $this->notes_screening,
//            'created_by' => $this->created_by,
//            'bought' => $this->bought,
//            'disable' => $this->disable,
//            'deleted' => $this->deleted,
//            'screen_report' => File::getFilePath($this->id, $this->tableName(),File::TYPE_FILE)
            'id'=> $this->id,
            'name'=> $this->name,
            'start_date'=> $this->start_date,
            'end_date'=> $this->end_date,
            'contact_person'=> $this->contact_person,
            'notes'=> $this->notes,
            'created_at'=> $this->created_at,
            'created_by'=> $this->created_by,
            'disable'=> $this->disable,
            'deleted'=> $this->deleted,
            'bags' =>  DrivesBags::findBag($this->id),
            'locations' => DrivesLocations::findLocations($this->id),
            'staffs' => DrivesStaffs::findStaffs($this->id),
        ];
    }

    public function saveModel()
    {
        if($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
            $this->created_at = date('Y-m-d');
        }
//            $this->blood_bag_number = Yii::$app->security->generateRandomString(12);
//            $this->location_id = $this->donor->location_id;
            if($this->save()){

                if($bag_id = Yii::$app->request->post('bag_id')){
                    $bag = new DrivesBags();
                    $bag->bag_id = $bag_id;
                    $bag->drive_id = $this->id;
                    if(!$bag->save()){
                        throw new Exception('was unable to save the model $bag');
                    }
                }
                if($location_id = Yii::$app->request->post('location_id')){
                    $location = new DrivesLocations();
                    $location->location_id = $location_id;
                    $location->drive_id = $this->id;
                    if(!$location->save()){
                        throw new Exception('was unable to save the model $location');
                    }
                }
                if($staff_id = Yii::$app->request->post('staf_id')){
                    $staff = new DrivesStaffs();
                    $staff->staff_id = $staff_id;
                    $staff->drive_id = $this->id;
                    if(!$staff->save()){
                        throw new Exception('was unable to save the model $staff');
                    }
                }
                return true;
            }

//        if($this->sent_for_screening){
//            $this->setScenario('screening');
//        }


        return $this->getErrors();
    }

    public function afterSave()
    {
        if($_FILES['file']){
            $result = File::uploadOne($this->id, $this->tableName());
            if($result!= true && $result->getErrors()){
                $this->addError(['error' => $result->getErrors()]);
                return $this;
            }
        }
        return true;
    }

    public function bought()
    {
        $this->bought = self::BOUGHT;
        return $this->save();
    }


    public function remove()
    {
        $this->deleted = self::DELETED;
        if (File::getFilePath($this->id, $this->tableName(),File::TYPE_FILE)) {
            if (!File::deleteOneFile($this->id, $this->tableName())) {
                return false;
            }
        }
        return $this->save();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBags()
    {
        return $this->hasMany(Bag::className(), ['drives_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodInfo()
    {
        return $this->hasOne(BloodInfo::className(), ['blood_drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id'])
            ->andOnCondition([
                'location.deleted' => self::NOT_DELETED,
            ]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesBags()
    {
        return $this->hasMany(DrivesBags::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBags0()
    {
        return $this->hasMany(Bag::className(), ['id' => 'bag_id'])->viaTable('drives_bags', ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesLocations()
    {
        return $this->hasMany(DrivesLocations::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['id' => 'location_id'])->viaTable('drives_locations', ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesStaffs()
    {
        return $this->hasMany(DrivesStaffs::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(User::className(), ['id' => 'staff_id'])->viaTable('drives_staffs', ['drive_id' => 'id']);
    }
}

<?php

namespace common\models\auth;


use common\components\traits\errors;
use common\components\traits\soft;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "auth_assignment".
 *
 * @property string $item_name
 * @property integer $user_id
 * @property integer $created_at
 *
 * @property User $user
 * @property AuthItem $itemName
 */
class AuthAssignment extends ActiveRecord
{

    use soft;
    use errors;

    public $error;

    const TYPE_ROLE = 1;
    const  TYPE_PERMISSION = 2;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['user_id', 'created_at'], 'integer'],
//            [['user_id', 'item_name'], 'validateModel'],
            [['item_name'], 'string', 'max' => 64],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['item_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['item_name' => 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => 'Item Name',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemName()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }

//    public function checkAccess(){
//        return self::findOne([
//            'item_name' => $this->item_name,
//            'user_id' => $this->user_id,
//            'deleted' => $this->user_id,
//        ]);
//    }

    public function saveModel(){

        $item = self::findOne([
            'item_name' => $this->item_name,
            'user_id' => $this->user_id,
        ]);
        if (!$item){
            if (!$this->save()) {
                return false;
            }
        }
        return true;

    }

    public static function savePermissions($item_arr, $user_id)
    {
        if(is_array($item_arr)) {
            foreach ($item_arr as $one) {
                if (!Yii::$app->authManager->getPermission($one)) {
                    return false;
                }
                $model = new self;
                $model->item_name = $one;
                $model->user_id = $user_id;
                if (!$model->saveModel()) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function deletePermissions($diffs,$user_id){
        if ($diffs) {
            foreach ($diffs as $one) {
                //удаление тех записей, которых нет в новом массиве
               if(!self::deleteOne($one, $user_id)){
                   return false;
               }
            }
        }
        return true;
    }

    public static function deleteOne($one, $user_id)
    {
        $item = self::findOne([
            'item_name' => $one,
            'user_id' => $user_id,
        ]);;
        if ($item) {
            return $item->delete();
        }
        return true;
    }

    public static function saveRole($item_name, $user_id)
    {
        if (!Yii::$app->authManager->getRole($item_name)) {
            return false;
        }
        $model = new self;
        $model->item_name = $item_name;
        $model->user_id = $user_id;
        return $model->saveModel();
    }

}

<?php

namespace common\models;


use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;

/**
 * This is the model class for table "donor".
 *
 * @property integer $id
 * @property string $donor_number
 * @property integer $location_id
 * @property integer $date
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $title
 * @property string $other_title
 * @property string $first_name
 * @property string $last_name
 * @property integer $gender
 * @property integer $date_birth
 * @property string $occupation
 * @property string $home_address
 * @property string $email
 * @property double $mobile
 * @property double $phone
 * @property string $type_donor
 * @property integer $blood_group
 * @property integer $status
 * @property string $hiv
 * @property string $hcv
 * @property integer $hbsag
 * @property integer $syphillis
 * @property integer $guestionnaire_complited
 * @property integer $date_guestionnaire
 * @property string $guestionnaire_id
 * @property integer $health_status
 * @property string $notes
 * @property integer $disable
 * @property integer $deleted
 *
 * @property Questionnaire [] $questionnaire
 * @property BloodDrive[] $bloodDrives
 * @property Location $location
 * @property User $creator
 * @property User $updater
 */
class Donor extends \yii\db\ActiveRecord
{

    use soft;
    use findRecords;
    use errors;
    use logs;

    public $photo;
    public $error;

    const NOT_DISABLED = 0;
    const DISABLED = 1;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $image_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'created_by', 'first_name', 'last_name', 'date_birth', 'mobile',
                ], 'required'],
//            [['guestionnaire_id', 'health_status', 'date_guestionnaire'], 'required', 'on' => 'all_field'],
            [['location_id',  'created_by', 'updated_by', 'date_birth', 'disable', 'deleted'], 'integer'],
            [['mobile', 'phone'], 'number'],
            ['email', 'email'],
            [['first_name', 'donor_number', 'last_name', 'occupation', 'home_address', ], 'string', 'max' => 255],
            [['other_title'], 'string', 'max' => 55],
            [['email', 'mobile',], 'unique'],
            ['gender', 'in', 'range' => ['male', 'female']],
            ['title', 'in', 'range' => ['mr', 'ms', 'other']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            ['disable', 'default', 'value' => self::NOT_DISABLED],
            ['disable', 'in', 'range' => [self::NOT_DISABLED, self::DISABLED]],
            [['image_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['photo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location_id' => 'Location ID',
            'photo' => 'Photo',
            'title' => 'Title',
            'other_title' => 'Other Title',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'gender' => 'Gender',
            'date_birth' => 'Date Of Birth',
            'occupation' => 'Occupation',
            'donor_number' => 'Donor Number',
            'home_address' => 'Home Address',
            'email' => 'Email Address',
            'mobile' => 'Mobile Telephone Number',
            'phone' => 'Other Telephone Number',
            'disable' => 'Disable',
            'deleted' => 'Deleted',
            'created_by' =>'Created By',
        ];
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'location_id' => $this->location_id,
            'title' => $this->title,
            'other_title' => $this->other_title,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'gender' => $this->gender,
            'date_birth' => $this->date_birth,
            'occupation' => $this->occupation,
            'home_address' => $this->home_address,
            'donor_number' => $this->donor_number,
            'email' => $this->email,
            'mobile' => $this->mobile,
            'phone' => $this->phone,
            'created_by' => $this->created_by,
            'creator' => $this->creator->surname,
            'updater' => $this->updater->surname,
            'disable' => $this->disable,
            'deleted' => $this->deleted,
            'photo' => File::getFilePath($this->id, $this->tableName(), File::TYPE_IMAGE_BASE64),
        ];
    }

//    public function all_fields()
//    {
//        return [
//            'id' => $this->id,
//            'donor_number' => $this->donor_number,
////            'location_id' => $this->location_id,
////            'date' => $this->date,
//            'title' => $this->title,
//            'other_title' => $this->other_title,
//            'first_name' => $this->first_name,
//            'last_name' => $this->last_name,
////            'gender' => $this->gender,
////            'date_birth' => $this->date_birth,
////            'occupation' => $this->occupation,
////            'home_address' => $this->home_address,
//            'email' => $this->email,
//            'mobile' => $this->mobile,
//            'phone' => $this->phone,
////            'type_donor' => $this->type_donor,
////            'blood_group' => $this->blood_group,
////            'status' => $this->status,
////            'hiv' => $this->hiv,
////            'hbsag' => $this->hbsag,
////            'hcv' => $this->hcv,
////            'syphillis' => $this->syphillis,
////            'guestionnaire_complited' => $this->guestionnaire_complited,
////            'date_guestionnaire' => $this->date_guestionnaire,
////            'guestionnaire_id' => $this->guestionnaire_id,
////            'health_status' => $this->health_status,
////            'notes' => $this->notes,
//            'disable' => $this->disable,
//            'deleted' => $this->deleted,
////            'photo' => File::getFilePath($this->id, $this->tableName(), File::TYPE_IMAGE_BASE64),
//        ];
//    }


    public function saveModel()
    {
        if ($this->isNewRecord) {
            $this->donor_number = Yii::$app->security->generateRandomString(12);
            $this->created_by = Yii::$app->user->id;
//            $this->date = time();
            //TODO: status
//            $this->health_status = 1;
        }
//        $this->updated_by = Yii::$app->user->id;
//        if ($this->guestionnaire_complited == 1) {
//            $this->setScenario('all_field');
//        } else {
//            $this->guestionnaire_id = null;
//            $this->date_guestionnaire = null;

            //TODO: status
//        }
//        $this->health_status = 1;
        return $this->save();
    }

    public function afterSave()
    {
        if ($this->image_file) {
            $result = File::uploadBase($this->id, $this->tableName(), $this->image_file);
            if ($result != true && $result->getErrors()) {
                $this->addError(['error' => $result->getErrors()]);
                return $this;
            }
            $this->image_file = false;
        }
        return true;

//        $model = new User();
//        $model->created_by = Yii::$app->user->id;
//        $model->username = $this->email;
//        $model->phone = $this->phone;
//        $model->role = User::ROLE_STAFF;

    }

    public function remove()
    {
        $this->deleted = self::DELETED;
        if (File::getFilePath($this->id, $this->tableName(), File::TYPE_FILE)) {
            if (!File::deleteOneFile($this->id, $this->tableName())) {
                return false;
            }
        }
        if($this->questionnaire){
            $result = Questionnaire::deleteQuestionnaire($this->id);
            if ($result != true && $result->getErrors()) {
                $this->addError(['error' => $result->getErrors()]);
                return $this;
            }
        }

        return $this->save();
    }


    public function getQuestionnaire()
    {
        return $this->hasMany(Questionnaire::className(), ['donor_id' => 'id'])
            ->andOnCondition([
                'questionnaire.deleted' => self::NOT_DELETED,
            ]);
    }
//    public function getQuestionnaire()
//    {
//        return $this->hasOne(Questionnaire::className(), [ 'questionnaire_number' => 'guestionnaire_id'])
//            ->andOnCondition([
//                'questionnaire.deleted' => self::NOT_DELETED,
//            ]);
//    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDrives()
    {
        return $this->hasMany(BloodDrive::className(), ['donor_id' => 'id'])
            ->andOnCondition([
                'blood_drive.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id'])
            ->andOnCondition([
                'deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => self::NOT_DISABLED,
            ]);
    }
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => self::NOT_DISABLED,
            ]);
    }
}

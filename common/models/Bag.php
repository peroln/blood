<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\findRecords;
use common\components\traits\logs;
use common\components\traits\soft;
use Yii;

/**
 * This is the model class for table "Bag".
 *
 * @property integer $id
 * @property integer $donor_id
 * @property integer $questionnaire_id
 * @property string $bar_code
 * @property integer $drives_id
 * @property integer $location_id
 * @property integer $screening_agency
 * @property string $date_sent
 * @property integer $screening_status
 * @property string $screen_date
 * @property string $screen_result
 * @property string $blood_group
 * @property integer $weight
 * @property integer $pressure
 * @property string $hiv
 * @property integer $syphilis
 * @property integer $hep_a
 * @property integer $hep_b
 * @property string $Expiry Date
 * @property string $screen_notes
 * @property integer $screen_sent
 * @property string $created_at
 * @property integer $created_by
 * @property string $update_at
 * @property integer $update_by
 * @property integer $deleted

 * @property Donor $donor
 * @property BloodDrive $drives
 * @property Questionnaire $questionnaire
 */
class Bag extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;
    const NOT_DELETED = 0;
    const DELETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['donor_id', 'questionnaire_id', 'bar_code', 'location_id', 'created_by'], 'required'],
            [['donor_id', 'questionnaire_id', 'drives_id', 'location_id', 'screening_agency', 'screening_status', 'weight', 'pressure', 'syphilis', 'hep_a', 'hep_b', 'screen_sent', 'created_by', 'update_by', 'deleted'], 'integer'],
            [['created_at'], 'safe'],
            [['bar_code', 'date_sent', 'screen_date', 'screen_result', 'blood_group', 'hiv', 'expire_data', 'screen_notes', 'update_at'], 'string', 'max' => 255],
            [['bar_code'], 'unique'],
            [['questionnaire_id'], 'unique'],
            [['donor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Donor::className(), 'targetAttribute' => ['donor_id' => 'id']],
            [['drives_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['drives_id' => 'id']],
            [['questionnaire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionnaire::className(), 'targetAttribute' => ['questionnaire_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'donor_id' => 'Donor ID',
            'questionnaire_id' => 'Questionnaire ID',
            'bar_code' => 'Bar Code',
            'drives_id' => 'Drives ID',
            'location_id' => 'Location ID',
            'screening_agency' => 'Screening Agency',
            'date_sent' => 'Date Sent',
            'screening_status' => 'Screening Status',
            'screen_date' => 'Screen Date',
            'screen_result' => 'Screen Result',
            'blood_group' => 'Blood Group',
            'weight' => 'Weight',
            'pressure' => 'Pressure',
            'hiv' => 'Hiv',
            'syphilis' => 'Syphilis',
            'hep_a' => 'Hep A',
            'hep_b' => 'Hep B',
            'Expiry Date' => 'Expiry  Date',
            'screen_notes' => 'Screen Notes',
            'screen_sent' => 'Screen Sent',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'update_at' => 'Update At',
            'update_by' => 'Update By',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrives()
    {
        return $this->hasOne(BloodDrive::className(), ['id' => 'drives_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaire()
    {
        return $this->hasOne(Questionnaire::className(), ['id' => 'questionnaire_id']);
    }

    /**
     * Save the Bag model and add needed params value.
     *
     *
     * @return true or false bool
     *
     */
    public function saveModel()
    {

        if($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
            $this->created_at = date('Y-m-d');
            $this->bar_code = Yii::$app->security->generateRandomString(12);
        }
        else{
            $this->update_at = date('Y-m-d');
            $this->update_by = Yii::$app->user->id;
        }
        return $this->save();
    }
}

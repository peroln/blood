<?php

namespace common\models;

use Yii;
use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;


use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "question_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_by
 * @property integer $deleted
 *
 * @property User $createdBy
 * @property QuestionTitle[] $questionTitles
 */
class QuestionCategory extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    public $titles;
    public $title_id;
    public $answer;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_by'], 'required'],
            [['created_by', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['title_id','answer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'deleted' => 'Deleted',
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\QuestionCategory' => [
                    'id',
                    'name',
                    'titles' => function ($model) {
                        return $model->questionTitles;
                    },
                    'deleted',
                ],
            ]
        );
        return $result;
    }

    public static function allAnswers($questionnaire_id){
        $all_category = self::findAll([ 'deleted' => self::NOT_DELETED]);
        $arr =[];
        foreach($all_category as $key =>$one){
            $category = self::findOne($one->id);
            $arr[$key] = [
                'id' => $one->id,
                'name' => $one->name
            ];
            $titles =  $category->questionTitles;
            foreach($titles as $val => $title){
                $arr[$key]['titles'][$val] =[
                    'id' => $title->id,
                    'label' => $title->label,
                    'type' =>$title->type,
                    'condition' =>$title->condition,
                    'mandatory' =>$title->mandatory,
                    'previous' =>$title->previous,
                    'answer' => $category->getAnswers($title->id, $questionnaire_id)->answer
                ];

            }
        }

        return $arr;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => User::NOT_DISABLED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionTitles()
    {
        return $this->hasMany(QuestionTitle::className(), ['category_id' => 'id'])
            ->andOnCondition([
                'question_title.deleted' => self::NOT_DELETED,
            ])->orderBy('previous ASC');
    }

    public function getAnswers($title_id, $questionnaire_id)
    {
        return QuestionnaireAnswer::find()->select('answer')->where(['questionnaire_id' => $questionnaire_id,'category_id' => $this->id, 'title_id' =>$title_id])
            ->andOnCondition([
                'questionnaire_answer.deleted' => self::NOT_DELETED,
            ])->one();
    }
}

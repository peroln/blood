<?php

namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "question_title".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $label
 * @property integer $type
 * @property string $condition
 * @property integer $previous
 * @property integer $mandatory
 * @property string $deleted
 *
 * @property QuestionCategory $category
 * @property QuestionTitle $before
 * @property QuestionTitle $after
 */
class QuestionTitle extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    public $error;

    const TYPE_TEXT = 1;
    const TYPE_RADIO = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_DATE = 4;

    const NOT_DELETED = 0;
    const DELETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_title';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'type', 'previous'], 'required', 'except' => 'first'],
            [['label', 'type'], 'required', 'on' => 'first'],
            [['category_id', 'type', 'previous', 'deleted', 'mandatory'], 'integer'],
            [['label', 'condition'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionCategory::className(), 'targetAttribute' => ['category_id' => 'id']],

            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['type'], 'in', 'range' => [self::TYPE_TEXT, self::TYPE_RADIO, self::TYPE_CHECKBOX, self::TYPE_DATE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
//            'category_id' => 'Category ID',
            'label' => 'Label',
            'type' => 'Type',
            'condition' => 'Condition',
            'previous' => 'Previous',
            'deleted' => 'Deleted',
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\QuestionTitle' => [
                    'id',
                    'category_id',
                    'label',
                    'type',
                    'condition',
                    'mandatory',
                    'previous',
                    'category' => function ($model) {
                        return $model->category->name;
                    },
                    'deleted',
                ],
            ]
        );
        return $result;
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'category' => $this->category->name,
            'label' => $this->label,
            'type' => $this->type,
            'condition' => $this->condition,
            'mandatory' => $this->mandatory,
            'previous' => $this->previous,
            'deleted' => $this->deleted,
        ];

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(QuestionCategory::className(), ['id' => 'category_id'])
            ->andOnCondition([
                'question_category.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBefore()
    {
        return $this->hasOne(self::className(), ['id' => 'previous'])
            ->andOnCondition([
                'question_title.deleted' => self::NOT_DELETED,
            ]);
    }
}

<?php

namespace common\models;

use common\models\BloodDrive;
use common\models\City;
use common\models\Donor;
use common\models\Questionnaire;
use common\models\search\StateSearch;
use common\models\search\AreaSearch;
use common\models\search\CitySearch;
use common\models\User;
use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $address
 * @property string $another_address
 * @property string $office_phone
 * @property string $another_phone
 * @property string $email
 * @property string $notes
 * @property string $contact_name
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_another_phone
 * @property string $contact_notes
 * @property integer $area_id
 * @property integer $state_id
 * @property string $created_at
 * @property string $designation
 * @property integer $created_by
 * @property integer $update_at
 * @property integer $update_by
 * @property integer $deleted
 *
 * @property BloodDrive[] $bloodDrives
 * @property Donor[] $donors
 * @property City $city
 * @property Questionnaire[] $questionnaires
 * @property User[] $users
 */
class Location extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;

    const NOT_DELETED = 0;
    const DELETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city_id'], 'required'],
            [['city_id', 'area_id', 'state_id', 'created_by', 'update_at', 'update_by', 'deleted'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'address', 'another_address', 'email', 'notes', 'contact_name', 'contact_email', 'contact_phone', 'contact_another_phone', 'designation', 'contact_notes'], 'string', 'max' => 255],
            [['office_phone', 'another_phone'], 'string', 'max' => 20],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'address' => 'Address',
            'another_address' => 'Another Address',
            'office_phone' => 'Office Phone',
            'another_phone' => 'Another Phone',
            'email' => 'Email',
            'notes' => 'Notes',
            'contact_name' => 'Contact Name',
            'contact_email' => 'Contact Adress',
            'contact_phone' => 'Contact Phone',
            'contact_another_phone' => 'Contact Another Phone',
            'contact_notes' => 'Contact Note',
            'area_id' => 'Area ID',
            'state_id' => 'State ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'update_at' => 'Update At',
            'update_by' => 'Update By',
            'deleted' => 'Deleted',
            'designation' => 'Designation',
        ];
    }

    //найти все записи по штату этой локации

    public function getAllLocations(){
        $model = new StateSearch();
        $state = $model->searchAll()['models'];
        $model = new AreaSearch();
        $area = $model->searchAll(['state_id' =>$this->city->area->state_id])['models'];
        $model = new CitySearch();
        $sity = $model->searchAll([ 'area_id' => $this->city->area_id])['models'];
        return [
//            'state_id' => $this->city->area->state_id,
//            'area_id' => $this->city->area_id,
//            'city_id' => $this->city->id,
//            'location_id' => $this->id,
            'states' => $state,
            'areas' =>  $area,
            "cities" =>  $sity,
            'id' => $this->id,
            'name' => $this->name,
            'notes' => $this->notes,
            'office_phone' => $this->office_phone,
            'contact_name' => $this->contact_name,
            'contact_email' => $this->contact_email,
            'contact_phone' => $this->contact_phone,
            'contact_another_phone' => $this->contact_another_phone,
            'contact_notes' => $this->contact_notes,
            'email' => $this->email,
            'address' => $this->address,
            'another_address' => $this->another_address,
            'another_phone' => $this->another_phone,
            'designation' => $this->designation,
//            'city_id' => $this->city_id,
//            'area_id' => $this->area_id,
//            'state_id' => $this->state_id,
        ];
    }






    public function one_fields()
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'notes' => $this->notes,
            'office_phone' => $this->office_phone,
            'contact_name' => $this->contact_name,
            'contact_email' => $this->contact_email,
            'contact_phone' => $this->contact_phone,
            'contact_another_phone' => $this->contact_another_phone,
            'contact_notes' => $this->contact_notes,
            'email' => $this->email,
            'address' => $this->address,
            'another_address' => $this->another_address,
            'another_phone' => $this->another_phone,
            'designation' => $this->designation,
            'city_id' => $this->city_id,
            'area_id' => $this->area_id,
            'state_id' => $this->state_id,
            'all_locations' => $this->getAllLocations()
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDrives()
    {
        return $this->hasMany(BloodDrive::className(), ['location_id' => 'id'])
            ->andOnCondition([
                'blood_drive.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonors()
    {
        return $this->hasMany(Donor::className(), ['location_id' => 'id'])
            ->andOnCondition([
                'donor.deleted' => self::NOT_DELETED,
                'donor.disable' => Donor::NOT_DISABLED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaires()
    {
        return $this->hasMany(Questionnaire::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['location_id' => 'id']);
    }
}

<?php

namespace common\models;

use common\components\traits\errors;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "drives_bags".
 *
 * @property integer $drive_id
 * @property integer $bag_id
 *
 * @property Bag $bag
 * @property BloodDrive $drive
 */
class DrivesBags extends \yii\db\ActiveRecord
{
    use errors;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drives_bags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drive_id', 'bag_id'], 'required'],
            [ 'bag_id', 'unique', ],
            [['drive_id', 'bag_id'], 'integer'],
            [['bag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bag::className(), 'targetAttribute' => ['bag_id' => 'id']],
            [['drive_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['drive_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'drive_id' => 'Drive ID',
            'bag_id' => 'Bag ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBag()
    {
        return $this->hasMany(Bag::className(), ['id' => 'bag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrive()
    {
        return $this->hasMany(BloodDrive::className(), ['id' => 'drive_id']);
    }

    /**
     * @param integer
     * @return mixed
     */
    static function findBag($drive_id)
    {
        $query = (new Query())
            ->select([
                'drives_bags.bag_id',
                'bag.donor_id',
                'bag.questionnaire_id',
                'bag.bar_code',
                'donor.last_name',
            ])
            ->from('drives_bags')
            ->leftJoin('bag', 'bag.id = drives_bags.bag_id')
            ->leftJoin('donor', 'donor.id = bag.donor_id')
            ->where(['drives_bags.drive_id' => $drive_id])
            ->all();
        return $query;
    }
}

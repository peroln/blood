<?php

namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;


/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $state_id
 * @property string $name
 * @property string $description
 * @property integer $deleted
 * @property integer $popularity
 *
 * @property State $state
 * @property City[] $cities
 */
class Area extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    const NOT_DELETED = 0;
    const DELETED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id', 'name'], 'required'],
            [['state_id', 'deleted', 'popularity'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State ID',
            'name' => 'Name',
            'description' => 'Description',
            'deleted' => 'Deleted',
            'popularity' => 'Popularity',
        ];
    }

    /**
     * @return $this
     */
    public function saveModel()
    {
        if ($this->save()) {
            return $this;
        }
        return $this->getErrors();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id'])
            ->andOnCondition([
                'state.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['area_id' => 'id'])
            ->andOnCondition([
                'city.deleted' => self::NOT_DELETED,
            ]);
    }
}

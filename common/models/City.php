<?php

namespace common\models;


use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;


/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $area_id
 * @property string $name
 * @property string $description
 * @property integer $deleted
 *
 * @property Area $area
 * @property Location[] $locations
 */
class City extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;

    const NOT_DELETED = 0;
    const DELETED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_id', 'name'], 'required'],
            [['area_id', 'deleted'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'name' => 'Name',
            'description' => 'Description',
            'deleted' => 'Deleted',
        ];
    }

//    public function fields()
//    {
//        return [
//            'id' =>$this->id,
//            'name'=> $this->name
//        ];
//    }

    /**
     * @return $this
     */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id'])
            ->andOnCondition([
                'area.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['city_id' => 'id'])
            ->andOnCondition([
            'location.deleted' => self::NOT_DELETED,
        ]);
    }
}

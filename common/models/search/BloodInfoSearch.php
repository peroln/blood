<?php

namespace common\models\search;

use common\models\BloodInfo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;



/**
 * BloodInfoSearch represents the model behind the search form about `app\models\BloodInfo`.
 */
class BloodInfoSearch extends BloodInfo
{

    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'blood_drive_id', 'weight', 'pressure', 'hbsag', 'hcv', 'syphillis', 'created_at', 'updated_by', 'deleted'], 'integer'],
            [['blood_group', 'hiv', 'notes', 'created_by', 'updated_at'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BloodInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'blood_drive_id' => $this->blood_drive_id,
            'weight' => $this->weight,
            'pressure' => $this->pressure,
            'hbsag' => $this->hbsag,
            'hcv' => $this->hcv,
            'syphillis' => $this->syphillis,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'blood_group', $this->blood_group])
            ->andFilterWhere(['like', 'hiv', $this->hiv])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }

}

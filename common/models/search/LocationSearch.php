<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Location;

/**
 * LocationSearch represents the model behind the search form about `common\models\Location`.
 */
class LocationSearch extends Location
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'area_id', 'state_id', 'created_by', 'update_at', 'update_by', 'deleted','page', 'page_size'], 'integer'],
            [['name', 'address', 'designation', 'another_address', 'office_phone', 'another_phone', 'email', 'notes', 'contact_name', 'contact_email', 'contact_phone', 'contact_another_phone', 'contact_notes', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Location::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page ? $this->page : 1;
        } else {
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'area_id' => $this->area_id,
            'state_id' => $this->state_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'update_at' => $this->update_at,
            'update_by' => $this->update_by,
            'deleted' => $this->deleted,

        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'another_address', $this->another_address])
            ->andFilterWhere(['like', 'office_phone', $this->office_phone])
            ->andFilterWhere(['like', 'another_phone', $this->another_phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_another_phone', $this->contact_another_phone])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'contact_notes', $this->contact_notes]);

        return $dataProvider;
    }
}

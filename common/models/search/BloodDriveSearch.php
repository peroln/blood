<?php

namespace common\models\search;

use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BloodDrive;

/**
 * @property string $date_from
 * @property string $date_to
 * @property integer $search_gender

 * @property integer $area_name
 * @property integer $area_id
 * @property integer $city_id
 * @property integer $state_id
 *
 * BloodDriveSearch represents the model behind the search form about `common\models\BloodDrive`.
 */
class BloodDriveSearch extends BloodDrive
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    public $date_from;
    public $date_to;
    public $search_gender;

    public $city_id;
    public $area_id;
    public $area_name;
    public $state_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page', 'page_size', 'id', 'created_by', 'disable', 'deleted'], 'integer'],
            [['name', 'start_date', 'end_date', 'contact_person', 'notes', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     *
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
//        throw new Exception($params);
        $query = BloodDrive::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page ? $this->page : 1;
        } else {
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'disable' => $this->disable,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'start_date', $this->start_date])
            ->andFilterWhere(['like', 'end_date', $this->end_date])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}

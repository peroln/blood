<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Donor;

/**
 * DonorSearch represents the model behind the search form about `common\models\Donor`.
 */
class DonorSearch extends Donor
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page', 'page_size','id', 'location_id',  'created_by', 'title',  'date_birth', 'disable', 'deleted',], 'integer'],
            [[ 'photo', 'other_title', 'first_name', 'donor_number', 'last_name', 'occupation','gender', 'home_address', 'email',], 'safe'],
            [['mobile', 'phone'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Donor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page ? $this->page : 1;
        } else {
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'location_id' => $this->location_id,
            'created_by' => $this->created_by,
            'title' => $this->title,
            'gender' => $this->gender,
            'date_birth' => $this->date_birth,
            'mobile' => $this->mobile,
            'phone' => $this->phone,
            'disable' => $this->disable,
            'deleted' => $this->deleted,
        ]);

        $query
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'donor_number', $this->donor_number])
            ->andFilterWhere(['like', 'other_title', $this->other_title])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'home_address', $this->home_address])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}

<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bag;

/**
 * BagSearch represents the model behind the search form about `app\models\Bag`.
 */
class BagSearch extends Bag
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'donor_id', 'questionnaire_id', 'drives_id', 'location_id', 'screening_agency', 'screening_status', 'weight', 'pressure', 'syphilis', 'hep_a', 'hep_b', 'screen_sent', 'created_by', 'update_by', 'deleted'], 'integer'],
            [['bar_code', 'date_sent', 'screen_date', 'screen_result', 'blood_group', 'hiv', 'expiry_date', 'screen_notes', 'created_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     *
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Bag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page ? $this->page : 1;
        } else {
            $dataProvider->pagination = false;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'donor_id' => $this->donor_id,
            'questionnaire_id' => $this->questionnaire_id,
            'drives_id' => $this->drives_id,
            'location_id' => $this->location_id,
            'screening_agency' => $this->screening_agency,
            'screening_status' => $this->screening_status,
            'weight' => $this->weight,
            'pressure' => $this->pressure,
            'syphilis' => $this->syphilis,
            'hep_a' => $this->hep_a,
            'hep_b' => $this->hep_b,
            'screen_sent' => $this->screen_sent,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'update_by' => $this->update_by,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'bar_code', $this->bar_code])
            ->andFilterWhere(['like', 'date_sent', $this->date_sent])
            ->andFilterWhere(['like', 'screen_date', $this->screen_date])
            ->andFilterWhere(['like', 'screen_result', $this->screen_result])
            ->andFilterWhere(['like', 'blood_group', $this->blood_group])
            ->andFilterWhere(['like', 'hiv', $this->hiv])
            ->andFilterWhere(['like', 'expiry_date', $this->expiry_date])
            ->andFilterWhere(['like', 'screen_notes', $this->screen_notes])
            ->andFilterWhere(['like', 'update_at', $this->update_at]);

        return $dataProvider;
    }
}

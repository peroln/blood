<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    const DEFAULT_COUNT = 20;

    public $role;
    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by',  'status', 'disable', 'deleted', 'page', 'page_size'], 'integer'],
            [['surname', 'email',  'role'], 'safe'],
            [['phone'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = User::find();

        $query->joinWith('authAssignments');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page ? $this->page : 1;
        } else {
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.created_by' => $this->created_by,
            'user.phone' => $this->phone,
            'user.surname' => $this->surname,
            'user.status' => $this->status,
            'user.disable' => $this->disable,
            'user.deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'user.surname', $this->surname])
            ->andFilterWhere(['like', 'user.email', $this->email]);

        $query->andWhere([
            'auth_assignment.item_name' => $this->role,
//            'auth_assignment.item_name' => $this->permission
            ]);

        return $dataProvider;
    }
}

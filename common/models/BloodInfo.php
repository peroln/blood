<?php

namespace common\models;


use Yii;

use common\components\traits\errors;
use common\components\traits\logs;
use common\components\traits\soft;
use common\components\traits\findRecords;
use yii\base\Exception;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "blood_info".
 *
 * @property integer $id
 * @property integer $blood_drive_id
 * @property string $blood_group
 * @property integer $weight
 * @property integer $pressure
 * @property string $hiv
 * @property integer $hbsag
 * @property integer $hcv
 * @property integer $syphillis
 * @property string $notes
 * @property integer $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $deleted
 *
 * @property BloodDrive $bloodDrive
 */

class BloodInfo extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use logs;

    public $error;

    const NOT_DELETED = 0;
    const DELETED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blood_drive_id', 'blood_group', 'weight', 'pressure', 'hiv', 'hbsag', 'hcv', 'syphillis', 'created_at' ], 'required'],
            [['blood_drive_id', 'weight', 'pressure', 'hbsag', 'hcv', 'syphillis' , 'updated_by', 'deleted'], 'integer'],
            [['created_by', ], 'safe'],
            [['blood_group'], 'string', 'max' => 10],
            [['hiv', 'notes','created_at', 'updated_at'], 'string', 'max' => 255],
            [['blood_drive_id'], 'unique'],
            [['blood_drive_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['blood_drive_id' => 'id']],
            [['deleted', 'disable'], 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blood_drive_id' => 'Blood Drive ID',
            'blood_group' => 'Blood Group',
            'weight' => 'Weight',
            'pressure' => 'Pressure',
            'hiv' => 'Hiv',
            'hbsag' => 'Hbsag',
            'hcv' => 'Hcv',
            'syphillis' => 'Syphillis',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted' => 'Deleted',
            'disable' => 'Disable',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDrive()
    {
        return $this->hasOne(BloodDrive::className(), ['id' => 'blood_drive_id'])
            ->andOnCondition([
                'bloodDrive.deleted' => self::NOT_DELETED,
            ]);
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'blood_drive_id' => $this->blood_drive_id,
            'blood_group' => $this->blood_group,
            'weight' => $this->weight,
            'pressure' => $this->pressure,
            'hiv' => $this->hiv,
            'hbsag' => $this->hbsag,
            'hcv' => $this->hcv,
            'syphillis' => $this->syphillis,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'notes' => $this->notes,
            'disable' => $this->disable,
            'deleted' => $this->deleted,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\BloodInfo' => [
                    'id' => $this->id,
                    'blood_drive_id' => $this->blood_drive_id,
                    'blood_group' => $this->blood_group,
                    'weight' => $this->weight,
                    'pressure' => $this->pressure,
                    'hiv' => $this->hiv,
                    'hbsag' => $this->hbsag,
                    'hcv' => $this->hcv,
                    'syphillis' => $this->syphillis,
                    'created_at' => $this->created_at,
                    'created_by' => $this->created_by,
                    'updated_at' => $this->updated_at,
                    'updated_by' => $this->updated_by,
                    'notes' => $this->notes,
                    'disable' => $this->disable,
                    'deleted' => $this->deleted,
                ],
            ]
        );
        return $result;
    }
    public function saveModel()
    {

        if($this->isNewRecord) {

            $this->deleted = 0;
            $this->disable = 0;
            $this->created_by = Yii::$app->user->id;
            $this->created_at = date('Y-m-d');
        }else{
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = date('Y-m-d');
        }

        return $this->save();
    }

}

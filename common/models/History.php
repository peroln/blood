<?php

namespace common\models;


use common\components\traits\soft;
use common\components\traits\errors;
use common\components\traits\findRecords;
use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $date
 * @property integer $created_by
 * @property string $table
 * @property integer $row
 * @property string $status
 * @property string $last_state
 * @property string $current_state
 * @property integer $deleted
 *
 * @property User $creator
 */
class History extends \yii\db\ActiveRecord
{
    use findRecords;
    use errors;
    use soft;

    public $error;

    const NOT_DELETED = 0;
    const DELETED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_by', 'table', 'row', 'status'], 'required'],
            [['date', 'created_by', 'row', 'deleted'], 'integer'],
            [['table', 'status'], 'string', 'max' => 55],
            [['last_state', 'current_state'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
//            ['created_by', 'default', 'value' => Yii::$app->user->id],
//            ['date', 'default', 'value' => time()],
//            ['status', 'default', 'value' => Yii::$app->controller->action->id,
//                'when' => function ($model) {
//                return $model->isNewRecord;
//                }
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'created_by' => 'Creator ID',
            'table' => 'Table',
            'row' => 'Row',
            'status' => 'Status',
            'last_state' => 'Last State',
            'current_state' => 'Current State',
            'deleted' => 'Deleted',
        ];
    }


    public static function saveLog($data){
        $model = new History();
        $model->created_by = \Yii::$app->user->id;
        $model->date = time();
        $model->table = soft::lastNameClass($data->className());
        $model->status = Yii::$app->controller->action->id;
        $model->row = $data->id;
        $model->save();
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => User::NOT_DISABLED,
            ]);
    }


}

<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\search\UserSearch;

class CMail extends Model
{

    public static function StaffRegister($user)
    {
        $HTML = \Yii::$app->view->render('@common/mail/staffRegister', [
            'user' => $user,
        ]);

        if (!Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($user->email)
            ->setSubject('registeration')
            ->setHtmlBody($HTML)
            ->send()
        ) {
            return false;
        }
        $HTML = \Yii::$app->view->render('@common/mail/massageToAdmin', [
            'user' => $user,
        ]);
        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setSubject('New staff registered')
            ->setHtmlBody($HTML)
            ->send();
        return true;
    }

    public static function UserRegister($user)
    {
        $model = new UserSearch();
//        $admins = $model->searchAll(['deleted' => User::NOT_DELETED, 'disabled' => User::NOT_DISABLED]);
        $HTML = \Yii::$app->view->render('@common/mail/donorRegister', [
            'user' => $user,
        ]);
//        if ($admins) {
//            foreach ($admins['models'] as $admin) {
                if (!Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo(Yii::$app->params['supportEmail'])
                    ->setSubject('New donor registered')
                    ->setHtmlBody($HTML)
                    ->send()
                ) {
                    return false;
                }
//            }
            return true;
//        }


    }


    public static function ForgotPassword($user, $new_pass)
    {

        $HTML = \Yii::$app->view->render('@common/mail/forgotPassword', [
            'user' => $user,
            'new_pass' => $new_pass
        ]);

        $res = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($user->email)
            ->setSubject('Reset Password')
            ->setHtmlBody($HTML)
            ->send();
        return $res;
    }


}
<?php
namespace common\components;

use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use common\models\User;

class UserRoleRule extends Rule
{
    public $name = 'userRole';
    public function execute($user, $item, $params)
    {
        //Получаем массив пользователя из базы
        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));
        if ($user) {
            $role = $user->getRole(); //Значение из поля role базы данных
            switch($item->name) {
                case 'donor':
                    return $role ==  User::ROLE_DONOR;
                    break;
                case 'staff':
                    return $role ==  User::ROLE_STAFF;
                    break;
                case 'admin':
                    return $role ==  User::ROLE_ADMIN || User::ROLE_STAFF;
                    break;

                case 'register_staff':
                    return true;
                    break;
                case 'edit_staff':
                    return true;
                    break;
                case 'disable_staff':
                    return true;
                    break;
                case 'archiving_staff':
                    return true;
                    break;
                  case 'register_location':
                    return true;
                    break;
                case 'edit_location':
                    return true;
                    break;
                 case 'search_location':
                    return true;
                    break;


                default :
                    return false;
            }
        }
    }
}
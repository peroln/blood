<?php

namespace common\components\traits;

use Yii;

trait findRecords
{
    # search records

    public function searchAll($request =  null)
    {
//        $className = 'common\models\search\\'.soft::lastNameClass(static::className()).'Search()';
//        $className = common\models\search\DonorSearch;
        $this->deleted = parent::NOT_DELETED;
        if ($request && (!$this->load([soft::lastNameClass(static::className()) => $request]) || !$this->validate())) {
            return false;
        }
        $dataProvider = $this->search();
        $models = $dataProvider->getModels();
        return [
            'models' => $models,
            'count_page' => $dataProvider->pagination->pageCount,
            'count_model' => $dataProvider->getTotalCount()
        ];

    }
}
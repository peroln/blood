<?php

namespace common\components\traits;


use common\models\History;
use Yii;

trait logs
{
    public function afterSave()
    {
        return History::saveLog($this);
    }
}
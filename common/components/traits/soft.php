<?php

namespace common\components\traits;

use Yii;

trait soft
{
    # class name

    public static function lastNameClass($class)
    {
        $array = explode('\\', $class);
        return array_pop($array);
    }

    # load

    public function load($data, $formName = null)
    {
        $className = $this::lastNameClass(static::className());

        if (array_key_exists($className, $data)) {
            return parent::load($data, $formName);
        }

        return parent::load([$className => $data], $formName);
    }

    public function remove()
    {
        $className = $this::lastNameClass(static::className());
        $data = [
            'deleted' => parent::DELETED
        ];
        parent::load([$className => $data]);
        return $this->save();
    }

    public function disable()
    {
        $className = $this::lastNameClass(static::className());
        $data = [
            'disable' => $this->disable
        ];
        parent::load([$className => $data]);
        return $this->save();
    }
}
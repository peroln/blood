<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'en-US',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;
            dbname=niarains_blood',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
//            'transport' => [
//                'class' => 'Swift_MailTransport',
//            ],
            'useFileTransport' => false,
            //'useFileTransport' => true, //Put the list in the local mail, e-mail can open the test

//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'tkalenko.inka@gmail.com',
//                'password' => '',
//                'port' => '465',
//                'encryption' => 'ssl',
//
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'uploadFile' => [
            'class' => 'common\components\UploadFile',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            //здесь прописываем роли
//            'defaultRoles' => ['donor','staff','admin'],
        ],
        'i18n' => [
            'translations' => [
                'msg*' => [ // другие заголовки
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',// путь к файлам перевода
                    'sourceLanguage' => 'en-US', // // Язык с которого переводиться (данный язык использован в текстах сообщений).
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/index',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],


];

<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;
            dbname=niarains_blood',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
//            'transport' => [
//                'class' => 'Swift_MailTransport',
//            ],
            'useFileTransport' => false,
            //'useFileTransport' => true, //Put the list in the local mail, e-mail can open the test

//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'tkalenko.inka@gmail.com',
//                'password' => '',
//                'port' => '465',
//                'encryption' => 'ssl',
//
            ],

        ],
//    'bootstrap' => ['gii'],
//    'modules' => [
//        'gii' => [
//            'class' => 'yii\gii\Module',
//        ],
//
//    ],
];

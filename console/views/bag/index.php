<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bag-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bag', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'donor_id',
            'questionnaire_id',
            'bar_code',
            'drives_id',
            // 'location_id',
            // 'screening_agency',
            // 'date_sent',
            // 'screening_status',
            // 'screen_date',
            // 'screen_result',
            // 'blood_group',
            // 'weight',
            // 'pressure',
            // 'hiv',
            // 'syphilis',
            // 'hep_a',
            // 'hep_b',
            // 'expiry_date',
            // 'screen_notes',
            // 'screen_sent',
            // 'created_at',
            // 'created_by',
            // 'update_at',
            // 'update_by',
            // 'deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

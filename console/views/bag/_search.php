<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BagSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bag-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'donor_id') ?>

    <?= $form->field($model, 'questionnaire_id') ?>

    <?= $form->field($model, 'bar_code') ?>

    <?= $form->field($model, 'drives_id') ?>

    <?php // echo $form->field($model, 'location_id') ?>

    <?php // echo $form->field($model, 'screening_agency') ?>

    <?php // echo $form->field($model, 'date_sent') ?>

    <?php // echo $form->field($model, 'screening_status') ?>

    <?php // echo $form->field($model, 'screen_date') ?>

    <?php // echo $form->field($model, 'screen_result') ?>

    <?php // echo $form->field($model, 'blood_group') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'pressure') ?>

    <?php // echo $form->field($model, 'hiv') ?>

    <?php // echo $form->field($model, 'syphilis') ?>

    <?php // echo $form->field($model, 'hep_a') ?>

    <?php // echo $form->field($model, 'hep_b') ?>

    <?php // echo $form->field($model, 'expiry_date') ?>

    <?php // echo $form->field($model, 'screen_notes') ?>

    <?php // echo $form->field($model, 'screen_sent') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

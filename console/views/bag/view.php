<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bag */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bag-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'donor_id',
            'questionnaire_id',
            'bar_code',
            'drives_id',
            'location_id',
            'screening_agency',
            'date_sent',
            'screening_status',
            'screen_date',
            'screen_result',
            'blood_group',
            'weight',
            'pressure',
            'hiv',
            'syphilis',
            'hep_a',
            'hep_b',
            'expiry_date',
            'screen_notes',
            'screen_sent',
            'created_at',
            'created_by',
            'update_at',
            'update_by',
            'deleted',
        ],
    ]) ?>

</div>

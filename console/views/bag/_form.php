<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Bag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'donor_id')->textInput() ?>

    <?= $form->field($model, 'questionnaire_id')->textInput() ?>

    <?= $form->field($model, 'bar_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'drives_id')->textInput() ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'screening_agency')->textInput() ?>

    <?= $form->field($model, 'date_sent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'screening_status')->textInput() ?>

    <?= $form->field($model, 'screen_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'screen_result')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blood_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'pressure')->textInput() ?>

    <?= $form->field($model, 'hiv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'syphilis')->textInput() ?>

    <?= $form->field($model, 'hep_a')->textInput() ?>

    <?= $form->field($model, 'hep_b')->textInput() ?>

    <?= $form->field($model, 'screen_notes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'screen_sent')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'update_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'update_by')->textInput() ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

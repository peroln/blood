<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bag */

$this->title = 'Create Bag';
$this->params['breadcrumbs'][] = ['label' => 'Bags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel console\models\LocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Locations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Location', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'address',
            'another_address',
            'office_phone',
            // 'another_phone',
            // 'city_id',
            // 'email:email',
            // 'notes',
            // 'contact_name',
            // 'contact_adress',
            // 'contact_anathe_adress',
            // 'contact_phone',
            // 'contact_anathe_phone',
            // 'contact_note',
            // 'area_id',
            // 'state_id',
            // 'created_at',
            // 'created_by',
            // 'update_at',
            // 'update_by',
            // 'deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

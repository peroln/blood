<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BloodDrive */

$this->title = 'Create Blood Drive';
$this->params['breadcrumbs'][] = ['label' => 'Blood Drives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-drive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

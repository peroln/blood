<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\components\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные


        //Включаем наш обработчик
        $rule = new UserRoleRule();
        $auth->add($rule);

        //Добавляем роли
        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        //Добавляем роли
        $staff = $auth->createRole('staff');
        $staff->description = 'Staff';
        $staff->ruleName = $rule->name;
        $auth->add($staff);

        //Добавляем роли
        $donor = $auth->createRole('donor');
        $donor->description = 'Donor';
        $donor->ruleName = $rule->name;
        $auth->add($donor);


        //Добавляем роли
        $data_inputer = $auth->createRole('data_inputer');
        $data_inputer->description = 'Data Inputer';
        $data_inputer->ruleName = $rule->name;
        $auth->add($data_inputer);

        //Добавляем роли
        $reader = $auth->createRole('reader');
        $reader->description = 'Reader';
        $reader->ruleName = $rule->name;
        $auth->add($reader);


        $rule_admin = new UserRoleRule();
        $rule_admin->name = "adminAccess";
        $auth->add($rule_admin);



        //Добавляем

        $register_staff = Yii::$app->authManager->createPermission('register_staff');
        $register_staff->description = 'Staff Register';
        $register_staff->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($register_staff);

        $edit_staff = Yii::$app->authManager->createPermission('edit_staff');
        $edit_staff->description = 'Staff Update';
        $edit_staff->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($edit_staff);

        $disable_staff = Yii::$app->authManager->createPermission('disable_staff');
        $disable_staff->description = 'Staff Disable';
        $disable_staff->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($disable_staff);

        $archiving_staff = Yii::$app->authManager->createPermission('archiving_staff');
        $archiving_staff->description = 'Staff Archiving';
        $archiving_staff->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($archiving_staff);


        //Добавляем

        $register_location = Yii::$app->authManager->createPermission('register_location');
        $register_location->description = 'Location Register';
        $register_location->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($register_location);

        $edit_location = Yii::$app->authManager->createPermission('edit_location');
        $edit_location->description = 'Location Update';
        $edit_location->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($edit_location);

        $search_location = Yii::$app->authManager->createPermission('search_location');
        $search_location->description = 'Location Archiving';
        $search_location->ruleName = $rule_admin->name;
        Yii::$app->authManager->add($search_location);

        $disable_location = Yii::$app->authManager->createPermission('disable_location');
        $disable_location->description = 'Location Disable';
        Yii::$app->authManager->add($disable_location);
        $archiving_location = Yii::$app->authManager->createPermission('archiving_location');
        $archiving_location->description = 'Location Archiving';
        Yii::$app->authManager->add($archiving_location);

        $auth->addChild($admin, $register_location);
        $auth->addChild($admin, $edit_location);
        $auth->addChild($admin, $disable_location);
        $auth->addChild($admin, $archiving_location);
        $auth->addChild($admin, $search_location);




        $search_staff = Yii::$app->authManager->createPermission('search_staff');
        $search_staff->description = 'Staff Search';
        Yii::$app->authManager->add($search_staff);
        $auth->addChild($admin, $register_staff);
        $auth->addChild($admin, $edit_staff);
        $auth->addChild($admin, $disable_staff);
        $auth->addChild($admin, $archiving_staff);
        $auth->addChild($admin, $search_staff);

        //Добавляем

        $register_donor = Yii::$app->authManager->createPermission('register_donor');
        $register_donor->description = 'Donor Register';
        Yii::$app->authManager->add($register_donor);

        $edit_donor = Yii::$app->authManager->createPermission('edit_donor');
        $edit_donor->description = 'Donor Update';
        Yii::$app->authManager->add($edit_donor);

        $disable_donor = Yii::$app->authManager->createPermission('disable_donor');
        $disable_donor->description = 'Donor Disable';
        Yii::$app->authManager->add($disable_donor);

        $archiving_donor = Yii::$app->authManager->createPermission('archiving_donor');
        $archiving_donor->description = 'Donor Archiving';
        Yii::$app->authManager->add($archiving_donor);

        $search_donor = Yii::$app->authManager->createPermission('search_donor');
        $search_donor->description = 'Donor Search';
        Yii::$app->authManager->add($search_donor);
        $auth->addChild($staff, $register_donor);
        $auth->addChild($staff, $edit_donor);
        $auth->addChild($staff, $disable_donor);
        $auth->addChild($staff, $archiving_donor);
        $auth->addChild($staff, $search_donor);

        $auth->addChild($admin, $register_donor);
        $auth->addChild($admin, $edit_donor);
        $auth->addChild($admin, $disable_donor);
        $auth->addChild($admin, $archiving_donor);
        $auth->addChild($admin, $search_donor);

        //Добавляем

        $register_bag = Yii::$app->authManager->createPermission('register_bag');
        $register_bag->description = 'Blood Drive Register';
        Yii::$app->authManager->add($register_bag);

        $edit_bag = Yii::$app->authManager->createPermission('edit_bag');
        $edit_bag->description = 'Blood Drive Update';
        Yii::$app->authManager->add($edit_bag);

        $bought_bag = Yii::$app->authManager->createPermission('bought_bag');
        $bought_bag->description = 'Blood Drive Buy';
        Yii::$app->authManager->add($bought_bag);

        $disable_question = Yii::$app->authManager->createPermission('disable_question');
        $disable_question->description = 'Blood Drive Disable';
        Yii::$app->authManager->add($disable_question);

        $archiving_bag = Yii::$app->authManager->createPermission('archiving_bag');
        $archiving_bag->description = 'Blood Drive Archiving';
        Yii::$app->authManager->add($archiving_bag);

        $search_bag = Yii::$app->authManager->createPermission('search_bag');
        $search_bag->description = 'Blood Drive Search';
          Yii::$app->authManager->add($search_bag);


        //Добавляем

        $register_question = Yii::$app->authManager->createPermission('register_question');
        $register_question->description = 'Questionnaire Register';
        Yii::$app->authManager->add($register_question);

        $edit_question = Yii::$app->authManager->createPermission('edit_question');
        $edit_question->description = 'Questionnaire Update';
        Yii::$app->authManager->add($edit_question);


        $archiving_question = Yii::$app->authManager->createPermission('archiving_question');
        $archiving_question->description = 'Questionnaire Archiving';
        Yii::$app->authManager->add($archiving_question);

        $disable_bag = Yii::$app->authManager->createPermission('disable_bag');
        $disable_bag->description = ' Questionnarie  Disable';
        Yii::$app->authManager->add($disable_bag);

        $auth->addChild($staff, $register_bag);
        $auth->addChild($staff, $disable_question);
        $auth->addChild($staff, $archiving_question);
        $auth->addChild($staff, $edit_question);
        $auth->addChild($staff, $register_question);
        $auth->addChild($staff, $edit_bag);
        $auth->addChild($staff, $disable_bag);
        $auth->addChild($staff, $archiving_bag);
        $auth->addChild($staff, $search_bag);

        $auth->addChild($admin, $register_bag);
        $auth->addChild($admin, $disable_question);
        $auth->addChild($admin, $archiving_question);
        $auth->addChild($admin, $edit_question);
        $auth->addChild($admin, $register_question);
        $auth->addChild($admin, $edit_bag);
        $auth->addChild($admin, $disable_bag);
        $auth->addChild($admin, $archiving_bag);
        $auth->addChild($admin, $search_bag);

        // добавляем разрешение "read"
        $read = $auth->createPermission("read");
        $read->description = "Read";
        $auth->add($read);
        $auth->addChild($reader, $read);
        $auth->addChild($admin, $reader);



        // добавляем разрешение "data"
        $data = $auth->createPermission("data");
        $data->description = "Data";
        $auth->add($data);

        $auth->addChild($data_inputer, $data);
        $auth->addChild($admin, $data);
        $auth->addChild($admin, $data_inputer);
        $auth->addChild($data_inputer, $reader);
        $auth->addChild($staff, $data_inputer);
//        $auth->addChild($staff, $reader);
        $auth->addChild($admin, $staff);

    }
}

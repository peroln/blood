<?php

use yii\db\Migration;

class m160925_203416_location extends Migration
{
     public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%location}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
            'address' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

        public function down()
    {
        $this->dropTable('{{%location}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

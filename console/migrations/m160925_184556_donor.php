<?php

use yii\db\Migration;

class m160925_184556_donor extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%donor}}', [
            'id' => $this->primaryKey(),
            'donor_number' => $this->string()->notNull(),
            'location_id' => $this->integer(11)->notNull(),
            'date' => $this->integer(11)->notNull(),
            'creator_id' => $this->integer(11)->notNull(),
            'disable' => $this->boolean()->notNull()->defaultValue(0),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%donor}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m160925_184618_donor_data extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%donor_data}}', [
            'id' => $this->primaryKey(),
            'donor_id' => $this->integer(11),
            'photo' => $this->string(255),
            'title' => $this->string(55)->notNull(),
            'other_title' => $this->string(55),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'gender' =>  $this->boolean()->notNull()->defaultValue(0),
            'date_birth' => $this->integer(11)->notNull(),
            'occupation' => $this->string(255)->notNull(),
            'home_address' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull()->unique(),
            'mobile' => $this->integer(11)->notNull()->unique(),
            'phone' => $this->integer(11),
            'deleted' =>  $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%donor_data}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

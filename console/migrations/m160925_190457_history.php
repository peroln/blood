<?php

use yii\db\Migration;

class m160925_190457_history extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%history}}', [
            'id' => $this->primaryKey(),
            'date' => $this->integer(11)->notNull(),
            'creator_id' =>  $this->integer(11)->notNull(),
            'table' => $this->string(55)->notNull(),
            'row' => $this->integer(11)->notNull(),
            'status' => $this->string(55)->notNull(),
            'coll' => $this->string(55),
            'last_state' =>  $this->string(),
            'current_state' =>  $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%history}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

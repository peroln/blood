<?php

use yii\db\Migration;

class m160925_184641_health_info extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%health_info}}', [
            'id' => $this->primaryKey(),
            'donor_id' => $this->integer(11)->notNull(),
            'type_donor' => $this->string(255)->notNull(),
            'blood_group' => $this->smallInteger(1)->notNull(),
            'status' => $this->smallInteger(1)->notNull(),
            'hiv' => $this->string(255)->notNull(),
            'hbsag' =>  $this->boolean()->notNull(),
            'syphillis' =>  $this->boolean()->notNull(),
            'guestionnaire_complited' =>  $this->boolean()->notNull(),
            'guestionnaire_id' =>  $this->integer(11),
            'heath_status' =>  $this->smallInteger(1),
            'notes' =>  $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%health_info}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

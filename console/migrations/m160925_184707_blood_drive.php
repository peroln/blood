<?php

use yii\db\Migration;

class m160925_184707_blood_drive extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blood_drive}}', [
            'id' => $this->primaryKey(),
            'donation_date' => $this->integer(11)->notNull(),
            'donor_id' => $this->integer(11)->notNull(),
            'location_id' => $this->integer(11)->notNull(),
            'screening_agency_number' => $this->string()->notNull(),
            'blood_bag_number' => $this->string()->notNull(),
            'notes_bag' => $this->string(),
            'sent_for_screening' =>  $this->boolean()->notNull(),
            'date_sent' => $this->integer(11),
            'screening_status' =>  $this->smallInteger(1),
            'date_screen' => $this->integer(11),
            'screen_result' => $this->smallInteger(1),
            'screen_upload' => $this->string(255),
            'date_expiry' => $this->integer(11),
            'notes_screening' =>  $this->string(),
            'creator_id' => $this->integer(11)->notNull(),
            'disable' => $this->boolean()->notNull()->defaultValue(0),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%blood_drive}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

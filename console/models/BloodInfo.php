<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blood_info".
 *
 * @property integer $id
 * @property integer $blood_drive_id
 * @property string $dlood_group
 * @property integer $weight
 * @property integer $pressure
 * @property string $hiv
 * @property integer $hbsag
 * @property integer $hcv
 * @property integer $syphillis
 * @property string $notes
 * @property integer $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $deleted
 *
 * @property BloodDrive $bloodDrive
 */
class BloodInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blood_drive_id', 'dlood_group', 'weight', 'pressure', 'hiv', 'hbsag', 'hcv', 'syphillis', 'created_at', 'deleted'], 'required'],
            [['blood_drive_id', 'weight', 'pressure', 'hbsag', 'hcv', 'syphillis', 'created_at', 'updated_by', 'deleted'], 'integer'],
            [['created_by', 'updated_at'], 'safe'],
            [['dlood_group'], 'string', 'max' => 10],
            [['hiv', 'notes'], 'string', 'max' => 255],
            [['blood_drive_id'], 'unique'],
            [['blood_drive_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['blood_drive_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blood_drive_id' => 'Blood Drive ID',
            'dlood_group' => 'Dlood Group',
            'weight' => 'Weight',
            'pressure' => 'Pressure',
            'hiv' => 'Hiv',
            'hbsag' => 'Hbsag',
            'hcv' => 'Hcv',
            'syphillis' => 'Syphillis',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDrive()
    {
        return $this->hasOne(BloodDrive::className(), ['id' => 'blood_drive_id']);
    }
}

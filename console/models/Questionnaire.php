<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questionnaire".
 *
 * @property integer $id
 * @property string $questionnaire_number
 * @property integer $created_by
 * @property string $created_at
 * @property integer $date
 * @property integer $donor_id
 * @property integer $location_id
 * @property integer $deleted
 *
 * @property Bag $bag
 * @property Donor $donor
 * @property Location $location
 * @property QuestionnaireAnswer[] $questionnaireAnswers
 */
class Questionnaire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionnaire';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionnaire_number', 'created_by', 'date', 'donor_id', 'deleted'], 'required'],
            [['created_by', 'date', 'donor_id', 'location_id', 'deleted'], 'integer'],
            [['created_at'], 'safe'],
            [['questionnaire_number'], 'string', 'max' => 255],
            [['donor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Donor::className(), 'targetAttribute' => ['donor_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionnaire_number' => 'Questionnaire Number',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'date' => 'Date',
            'donor_id' => 'Donor ID',
            'location_id' => 'Location ID',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBag()
    {
        return $this->hasOne(Bag::className(), ['questionnaire_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaireAnswers()
    {
        return $this->hasMany(QuestionnaireAnswer::className(), ['questionnaire_id' => 'id']);
    }
}

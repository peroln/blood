<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blood_drive".
 *
 * @property integer $id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property string $contact_person
 * @property string $notes
 * @property string $created_at
 * @property integer $created_by
 * @property integer $disable
 * @property integer $deleted
 *
 * @property Bag[] $bags
 * @property BloodInfo $bloodInfo
 * @property DrivesBags[] $drivesBags
 * @property Bag[] $bags0
 * @property DrivesLocations[] $drivesLocations
 * @property Location[] $locations
 * @property DrivesStaffs[] $drivesStaffs
 * @property User[] $staff
 */
class BloodDrive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_drive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'created_by'], 'required'],
            [['created_by', 'disable', 'deleted'], 'integer'],
            [['name', 'start_date', 'end_date', 'contact_person', 'notes', 'created_at'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'contact_person' => 'Contact Person',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'disable' => 'Disable',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBags()
    {
        return $this->hasMany(Bag::className(), ['drives_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodInfo()
    {
        return $this->hasOne(BloodInfo::className(), ['blood_drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesBags()
    {
        return $this->hasMany(DrivesBags::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBags0()
    {
        return $this->hasMany(Bag::className(), ['id' => 'bag_id'])->viaTable('drives_bags', ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesLocations()
    {
        return $this->hasMany(DrivesLocations::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['id' => 'location_id'])->viaTable('drives_locations', ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivesStaffs()
    {
        return $this->hasMany(DrivesStaffs::className(), ['drive_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(User::className(), ['id' => 'staff_id'])->viaTable('drives_staffs', ['drive_id' => 'id']);
    }
}

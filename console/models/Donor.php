<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donor".
 *
 * @property integer $id
 * @property string $donor_number
 * @property integer $location_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $title
 * @property string $other_title
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property integer $date_birth
 * @property string $occupation
 * @property string $home_address
 * @property string $email
 * @property double $mobile
 * @property double $phone
 * @property integer $disable
 * @property integer $deleted
 * @property string $created_at
 * @property integer $updated_at
 *
 * @property Bag[] $bags
 * @property Location $location
 * @property Questionnaire[] $questionnaires
 */
class Donor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'created_by', 'updated_by', 'date_birth', 'disable', 'deleted', 'updated_at'], 'integer'],
            [['created_by', 'title', 'gender'], 'required'],
            [['mobile', 'phone'], 'number'],
            [['created_at'], 'safe'],
            [['donor_number', 'first_name', 'last_name', 'occupation', 'home_address', 'email'], 'string', 'max' => 255],
            [['title', 'gender'], 'string', 'max' => 10],
            [['other_title'], 'string', 'max' => 55],
            [['email'], 'unique'],
            [['mobile'], 'unique'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'donor_number' => 'Donor Number',
            'location_id' => 'Location ID',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'title' => 'Title',
            'other_title' => 'Other Title',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'gender' => 'Gender',
            'date_birth' => 'Date Birth',
            'occupation' => 'Occupation',
            'home_address' => 'Home Address',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'phone' => 'Phone',
            'disable' => 'Disable',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBags()
    {
        return $this->hasMany(Bag::className(), ['donor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaires()
    {
        return $this->hasMany(Questionnaire::className(), ['donor_id' => 'id']);
    }
}

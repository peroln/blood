<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drives_staffs".
 *
 * @property integer $drives_id
 * @property integer $staff_id
 *
 * @property BloodDrive $drives
 * @property User $staff
 */
class DrivesStaffs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drives_staffs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drive_id', 'staff_id'], 'required'],
            [['drive_id', 'staff_id'], 'integer'],
            [['drive_id'], 'exist', 'skipOnError' => true, 'targetClass' => BloodDrive::className(), 'targetAttribute' => ['drive_id' => 'id']],
            [['staff_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['staff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'drive_id' => 'Drive ID',
            'staff_id' => 'Staff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrives()
    {
        return $this->hasMany(BloodDrive::className(), ['id' => 'drive_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(User::className(), ['id' => 'staff_id']);
    }

    /**
     * @param integer
     * @return mixed
     */
    static function findStaffs($drive_id)
    {
        $query = (new Query())
            ->select([
                'drives_staffs.staff_id',
                'user.username',
                'user.phone',
                'user.other_telephone_number',
                'user.email',
                'user.notes',

            ])
            ->from('drives_staffs')
            ->leftJoin('user', 'user.id = drives_staffs.staff_id')
            ->where(['drives_staffs.drive_id' => $drive_id])
            ->all();
        return $query;
    }
}

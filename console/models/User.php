<?php
namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\logs;
use common\components\traits\findRecords;

use common\models\auth\AuthAssignment;
use common\models\auth\AuthItem;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $role
 * @property integer $permission
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
// * @property string $location_id
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_by
 * @property integer $disable
 * @property integer $deleted
 * @property string $password write-only password
 *
 *  * @property User $creator
 *  * @property Location $location
 */
class User extends ActiveRecord implements IdentityInterface
{
    use soft;
    use errors;
    use findRecords;
    use logs;

    public $password;
    public $role;
    public $permission;

    const ROLE_DONOR = 'donor';
    const ROLE_STAFF = 'staff';
    const ROLE_ADMIN = 'admin';
    const ROLE_READER = 'reader';
    const ROLE_DATA_INPUTER = 'data_inputer';

    const NOT_DISABLED = 0;
    const DISABLED = 1;

    const NOT_DELETED = 0;
    const DELETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'email',  'created_by', ], 'required', 'except' => ['change_pass']],


            [['password', 'role', 'username'], 'required', 'on' => 'registry'],

            [['password', 'location_id' ,'role', 'gender', 'surname', 'username', 'email'], 'required', 'on' => ['staff_registry']],
//            [['role'], 'required', 'on' => 'register'],

            [['auth_key', 'password_hash',], 'required', 'on' => 'save'],

            [['password_hash', 'password'], 'required', 'on' => 'change_pass'],

            ['password', 'string', 'min' => 6, 'max' => 32],

             [['date_birth','date_employed','date_dis_engaged', 'access_level',], 'integer',],

            [['phone','other_telephone_number'], 'double'],
            [['status', 'disable', 'deleted', 'created_by', 'location_id', 'state_id','access_level' ], 'integer'],
            [['address_1','address_2','town','first_name','middle_name','surname', 'username', 'password_hash', 'password_reset_token', 'lga','email','notes','designation'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],

//            [['location_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],

            [['surname', 'username'], 'trim'],
//            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            [['auth_key', 'password_reset_token'], 'unique'],

            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            ['disable', 'default', 'value' => self::NOT_DISABLED],
            ['disable', 'in', 'range' => [self::NOT_DISABLED, self::DISABLED]],
            ['title', 'in', 'range' => ['mr', 'ms', 'other']],
            ['gender', 'in', 'range' => ['male', 'female']],

            [['permission'], 'safe'],
            [['role'], 'in', 'range' => [self::ROLE_DATA_INPUTER, self::ROLE_READER, self::ROLE_DONOR, self::ROLE_STAFF, self::ROLE_ADMIN]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'phone' => 'Phone',
            'email' => 'Email',
            'status' => 'Status',
            'disable' => 'Disable',
            'deleted' => 'Deleted',
        ];
    }

    public function profile_fields(){

            return [
                'id' => $this->id,
                'username' => $this->username,
                'phone' => $this->phone,
                'email' => $this->email,
                'location_id' => $this->location_id,
                ];
    }


    public function one_fields()
    {

        return [
            'status' => $this->status,
            'designation' => $this->designation,
            'notes' => $this->notes,
            'lga' => $this->lga,
            'middle_name' => $this->middle_name,
            'first_name' => $this->first_name,
            'town' => $this->town,
            'address_2' => $this->address_2,
            'address_1' => $this->address_1,
            'access_level' => $this->access_level,
            'state_id' => $this->state_id,
            'other_telephone_number' => $this->other_telephone_number,
            'date_dis_engaged' => $this->date_dis_engaged,
            'date_employed' => $this->date_employed,
            'date_birth' => $this->date_birth,
            'gender' => $this->gender,
            'id' => $this->id,
            'surname' => $this->surname,
            'username' => $this->username,
            'phone' => $this->phone,
            'email' => $this->email,
            'location_id' => $this->location_id,
            'title' => $this->title,
            'created_by' => $this->created_by,
            'creator' => $this->creator ?
                [
                    'username' => $this->creator->username,
                    'role' => $this->creator->getRole(),
                ]
                : null
            ,
            'role' => $this->getRole(),
//            'permission' => $this->getPermission() ? $this->getPermission() : [],
//            'all_permission' => AuthItem::findAllPermission($this->getRole()),
            'disable' => $this->disable,
            'deleted' => $this->deleted,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\User' => [
                    'id',
                    'surname',
                    'username',
                    'phone',
                    'email',
                    'role' => function ($model) {
                        return $model->getRole();
                    },
                    'disable',
                    'deleted',
                    'location_id'
                ],
            ]
        );
        return $result;
    }


    public function saveModel()
    {
        if ($this->save() && $this->id!=Yii::$app->user->id) {
            $auth = Yii::$app->authManager;
            switch($this->access_level){
                case 1:$authRole = $auth->getRole('admin'); break;
                case 2:$authRole = $auth->getRole('staff'); break;
                case 3:$authRole = $auth->getRole('data_inputer'); break;
//                case 4:$authRole = $auth->getRole('donor'); break;
                default: $authRole = $auth->getRole('reader');
            }

            $auth->revokeAll($this->getId());
            $auth->assign($authRole, $this->getId());

//            $this->savePermission();
//            if ($this->role && $this->role != $this->getRole()) {
//                $this->saveRole();
//            }
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function remove()
    {
        $this->deleted = self::DELETED;
        if (($this->getPermission() && !AuthAssignment::deletePermissions($this->getPermission(), $this->id))
            || ($this->getRole() && !AuthAssignment::deletePermissions([$this->getRole()], $this->id))
        ) {
            return false;
        }
        return $this->save();
    }

    /**
     * register user
     *
     * @return User|null the saved model or null if saving fails
     */

    public function register()
    {
//        throw new Exception($this->role);
        //сценарий для проверки полей формы
        if($this->role == self::ROLE_STAFF){
            $this->setScenario('staff_registry');
        }else{
//            $this->setScenario('signup');
        }
        $this->created_by = Yii::$app->user->id;

        // TODO: change string to random
//        $this->password = '111111';

        if ($this->validate()) {
//            throw new \Exception('Wau');
            //сценарий для проверки сгенерированых полей
            $this->setScenario('save');

            $this->setPassword($this->password);

            $this->generateAuthKey();
            $this->saveModel();

        }
        return $this;
    }

    public function saveRole()
    {
        if ($this->getRole()) {
            if (!$this->checkRole()) {
                $this->addError(['role' => 'Role not checked']);
            }
        }
        if (!AuthAssignment::saveRole($this->role, $this->id)) {
            $this->addError(['role' => 'Role not saved']);
        }
        return $this;
    }

    public function savePermission()
    {
        if ($this->getPermission()) {
            if (!$this->checkPermission()) {
                $this->addError(['permission' => 'Permission not checked']);
            }
        }
        if (!AuthAssignment::savePermissions($this->permission, $this->id)) {
            $this->addError(['permission' => 'Permission not saved']);
        }
        return $this;
    }

    /**удаление старих значений
     * выбор новых значений
     *
     * @return array|bool
     */

    public function checkRole()
    {
        if ($this->getRole()) {
            return AuthAssignment::deleteOne($this->getRole(), $this->id);
        }
        return true;
    }

    public function checkPermission()
    {
        $my_permission = $this->getPermission();

        //сравниваем новый массив с старым
        $diffs = array_diff($my_permission, $this->permission);

        if (!AuthAssignment::deletePermissions($diffs, $this->id)) {
            return false;
        }
        $this->permission = array_diff($this->permission, $my_permission);
        return $this->permission ? $this->permission : true;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        $result = AuthAssignment::find()
            ->joinWith('itemName')
            ->joinWith('user')
            ->where([
                'user.id' => $this->id,
                'auth_item.type' => AuthAssignment::TYPE_ROLE
            ])
            ->one();
        return $result->item_name;
    }


    /**
     * возвращаю permissions для пользователя
     * с меткой checked, в зависимость от использования
     * @return array
     */
//    public function checkPermission()
//    {
//        History::saveModel(self::tableName(),$this->id, self::STATUS_UPDATE);
//        $all_permissions = $results = AuthItem::findAllForStaff();
//        if ($all_permissions) {
//            $user_permissions = $this->getPermission();
//
//            $result = [];
//            foreach ($all_permissions as $all_permission) {
//                $checked = false;
//                foreach ($user_permissions as $user_permission) {
//                    if ($user_permission == $all_permission->name) {
//                        $checked = true;
//                        break;
//                    }
//                }
//                $result[] = [
//                    'name' => $all_permission->name,
//                    'checked' => $checked
//                ];
//            }
//            return $result;
//        }
//
//
//    }

    /**
     *  возвращаю permissions для пользователя
     * @return array|bool
     */
    public function getPermission()
    {
        $results = AuthAssignment::find()
            ->joinWith('itemName')
            ->joinWith('user')
            ->where([
                'user.id' => $this->id,
                'auth_item.type' => AuthAssignment::TYPE_PERMISSION
            ])
            ->all();

        if ($results) {
            foreach ($results as $one) {
                $arr[] = $one->item_name;
            }
            return $arr;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(self::className(), ['id' => 'created_by'])
            ->andOnCondition([
                'user.deleted' => self::NOT_DELETED,
                'user.disable' => self::NOT_DISABLED
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id'])
            ->andOnCondition([
                'location.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(auth\AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDrives()
    {
        return $this->hasMany(BloodDrive::className(), ['created_by' => 'id'])
            ->andOnCondition([
                'blood_drive.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonors()
    {
        return $this->hasMany(Donor::className(), ['created_by' => 'id'])
            ->andOnCondition([
                'donor.deleted' => self::NOT_DELETED,
                'donor.disable' => self::NOT_DISABLED
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['created_by' => 'id'])
            ->andOnCondition([
                'history.deleted' => self::NOT_DELETED,
            ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'deleted' => self::NOT_DELETED,
            'disable' => self::NOT_DISABLED
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne([
            'auth_key' => $token,
            'deleted' => self::NOT_DELETED,
            'disable' => self::NOT_DISABLED
        ]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {

        return static::findOne(['username' => $username, 'deleted' => self::NOT_DELETED, 'disable' => self::NOT_DISABLED]);

    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'deleted' => self::NOT_DELETED,
            'disable' => self::NOT_DISABLED
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bag;

/**
 * BloodDriveSearch represents the model behind the search form about `common\models\Bag`.
 */
class BloodDriveSearch extends Bag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'donor_id', 'questionnaire_id', 'drives_id', 'location_id', 'screening_agency', 'screening_status', 'weight', 'pressure', 'syphilis', 'hep_a', 'hep_b', 'screen_sent', 'created_by', 'update_by', 'deleted'], 'integer'],
            [['bar_code', 'date_sent', 'screen_date', 'screen_result', 'blood_group', 'hiv', 'Expiry Date', 'screen_notes', 'created_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'donor_id' => $this->donor_id,
            'questionnaire_id' => $this->questionnaire_id,
            'drives_id' => $this->drives_id,
            'location_id' => $this->location_id,
            'screening_agency' => $this->screening_agency,
            'screening_status' => $this->screening_status,
            'weight' => $this->weight,
            'pressure' => $this->pressure,
            'syphilis' => $this->syphilis,
            'hep_a' => $this->hep_a,
            'hep_b' => $this->hep_b,
            'screen_sent' => $this->screen_sent,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'update_by' => $this->update_by,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'bar_code', $this->bar_code])
            ->andFilterWhere(['like', 'date_sent', $this->date_sent])
            ->andFilterWhere(['like', 'screen_date', $this->screen_date])
            ->andFilterWhere(['like', 'screen_result', $this->screen_result])
            ->andFilterWhere(['like', 'blood_group', $this->blood_group])
            ->andFilterWhere(['like', 'hiv', $this->hiv])
            ->andFilterWhere(['like', 'Expiry Date', $this->Expiry Date])
            ->andFilterWhere(['like', 'screen_notes', $this->screen_notes])
            ->andFilterWhere(['like', 'update_at', $this->update_at]);

        return $dataProvider;
    }
}

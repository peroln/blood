-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Host: db650815155.db.1and1.com
-- Generation Time: Oct 10, 2016 at 09:24 AM
-- Server version: 5.5.50-0+deb7u2-log
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db650815155`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `popularity` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=823 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `state_id`, `name`, `description`, `deleted`, `popularity`) VALUES
(1, 1, 'Yola', NULL, 0, 0),
(2, 2, 'Uyo', NULL, 0, 0),
(3, 3, 'Awka', NULL, 0, 0),
(4, 4, 'Bauchi', NULL, 0, 0),
(5, 5, 'Yenagoa', NULL, 0, 0),
(6, 6, 'Makurdi', NULL, 0, 0),
(7, 7, 'Maiduguri', NULL, 0, 0),
(8, 8, 'Calabar', NULL, 0, 0),
(9, 9, 'Asaba', NULL, 0, 0),
(10, 10, 'Abakaliki', NULL, 0, 0),
(11, 11, 'Benin City', NULL, 0, 0),
(12, 12, 'Ado Ekiti', NULL, 0, 0),
(13, 13, 'Enugu', NULL, 0, 0),
(14, 14, 'Gombe', NULL, 0, 0),
(15, 15, 'Owerri', NULL, 0, 0),
(16, 16, 'Dutse', NULL, 0, 0),
(17, 17, 'Kaduna', NULL, 0, 0),
(18, 18, 'Kano', NULL, 0, 0),
(19, 19, 'Katsina', NULL, 0, 0),
(20, 20, 'Birnin Kebbi', NULL, 0, 0),
(21, 21, 'Lokoja', NULL, 0, 0),
(22, 22, 'Ilorin', NULL, 0, 0),
(23, 23, 'Ikeja', NULL, 0, 0),
(24, 24, 'Lafia', NULL, 0, 0),
(25, 25, 'Minna', NULL, 0, 0),
(26, 26, 'Abeokuta', NULL, 0, 0),
(27, 27, 'Akure', NULL, 0, 0),
(28, 28, 'Oshogbo', NULL, 0, 0),
(29, 29, 'Ibadan', NULL, 0, 0),
(30, 30, 'Jos', NULL, 0, 0),
(31, 31, 'Port Harcourt', NULL, 0, 0),
(32, 32, 'Sokoto', NULL, 0, 0),
(33, 33, 'Jalingo', NULL, 0, 0),
(34, 34, 'Damaturu', NULL, 0, 0),
(35, 35, 'Gusau', NULL, 0, 0),
(36, 1, 'Demsa', NULL, 0, 0),
(37, 1, 'Fufore', NULL, 0, 0),
(38, 1, 'Ganaye', NULL, 0, 0),
(39, 1, 'Gireri', NULL, 0, 0),
(40, 1, 'Gombi', NULL, 0, 0),
(41, 1, 'Guyuk', NULL, 0, 0),
(42, 1, 'Hong', NULL, 0, 0),
(43, 1, 'Jada', NULL, 0, 0),
(44, 1, 'Lamurde', NULL, 0, 0),
(45, 1, 'Madagali', NULL, 0, 0),
(46, 1, 'Maiha', NULL, 0, 0),
(47, 1, 'Mayo-Belwa', NULL, 0, 0),
(48, 1, 'Michika', NULL, 0, 0),
(49, 1, 'Mubi North', NULL, 0, 0),
(50, 1, 'Mubi South', NULL, 0, 0),
(51, 1, 'Numan', NULL, 0, 0),
(52, 1, 'Shelleng', NULL, 0, 0),
(53, 1, 'Song', NULL, 0, 0),
(54, 1, 'Toungo', NULL, 0, 0),
(55, 1, 'Yola North', NULL, 0, 0),
(56, 1, 'Yola South', NULL, 0, 0),
(57, 2, 'Abak', NULL, 0, 0),
(58, 2, 'Eastern Obolo', NULL, 0, 0),
(59, 2, 'Eket', NULL, 0, 0),
(60, 2, 'Esit Eket', NULL, 0, 0),
(61, 2, 'Essien Udim', NULL, 0, 0),
(62, 2, 'Etim Ekpo', NULL, 0, 0),
(63, 2, 'Etinan', NULL, 0, 0),
(64, 2, 'Ibeno', NULL, 0, 0),
(65, 2, 'Ibesikpo Asutan', NULL, 0, 0),
(66, 2, 'Ibiono Ibom', NULL, 0, 0),
(67, 2, 'Ika', NULL, 0, 0),
(68, 2, 'Ikono', NULL, 0, 0),
(69, 2, 'Ikot Abasi', NULL, 0, 0),
(70, 2, 'Ikot Ekpene', NULL, 0, 0),
(71, 2, 'Ini', NULL, 0, 0),
(72, 2, 'Itu', NULL, 0, 0),
(73, 2, 'Mbo', NULL, 0, 0),
(74, 2, 'Mkpat Enin', NULL, 0, 0),
(75, 2, 'Nsit Atai', NULL, 0, 0),
(76, 2, 'Nsit Ibom', NULL, 0, 0),
(77, 2, 'Nsit Ubium', NULL, 0, 0),
(78, 2, 'Obot Akara', NULL, 0, 0),
(79, 2, 'Okobo', NULL, 0, 0),
(80, 2, 'Onna', NULL, 0, 0),
(81, 2, 'Oron', NULL, 0, 0),
(82, 2, 'Oruk Anam', NULL, 0, 0),
(83, 2, 'Udung Uko', NULL, 0, 0),
(84, 2, 'Ukanafun', NULL, 0, 0),
(85, 2, 'Uruan', NULL, 0, 0),
(86, 2, 'Urue-Offong/Oruko', NULL, 0, 0),
(87, 2, 'Uyo', NULL, 0, 0),
(88, 3, 'Aguata', NULL, 0, 0),
(89, 3, 'Anambra East', NULL, 0, 0),
(90, 3, 'Anambra West', NULL, 0, 0),
(91, 3, 'Anaocha', NULL, 0, 0),
(92, 3, 'Awka North', NULL, 0, 0),
(93, 3, 'Awka South', NULL, 0, 0),
(94, 3, 'Ayamelum', NULL, 0, 0),
(95, 3, 'Dunukofia', NULL, 0, 0),
(96, 3, 'Ekwusigo', NULL, 0, 0),
(97, 3, 'Idemili North', NULL, 0, 0),
(98, 3, 'Idemili South', NULL, 0, 0),
(99, 3, 'Ihiala', NULL, 0, 0),
(100, 3, 'Njikoka', NULL, 0, 0),
(101, 3, 'Nnewi North', NULL, 0, 0),
(102, 3, 'Nnewi South', NULL, 0, 0),
(103, 3, 'Ogbaru', NULL, 0, 0),
(104, 3, 'Onitsha North', NULL, 0, 0),
(105, 3, 'Onitsha South', NULL, 0, 0),
(106, 3, 'Orumba North', NULL, 0, 0),
(107, 3, 'Orumba South', NULL, 0, 0),
(108, 3, 'Oyi', NULL, 0, 0),
(109, 4, 'Alkaleri', NULL, 0, 0),
(110, 4, 'Bauchi', NULL, 0, 0),
(111, 4, 'Bogoro', NULL, 0, 0),
(112, 4, 'Damban', NULL, 0, 0),
(113, 4, 'Darazo', NULL, 0, 0),
(114, 4, 'Dass', NULL, 0, 0),
(115, 4, 'Ganjuwa', NULL, 0, 0),
(116, 4, 'Giade', NULL, 0, 0),
(117, 4, 'Itas/Gadau', NULL, 0, 0),
(118, 4, 'Jama''are', NULL, 0, 0),
(119, 4, 'Katagum', NULL, 0, 0),
(120, 4, 'Kirfi', NULL, 0, 0),
(121, 4, 'Misau', NULL, 0, 0),
(122, 4, 'Ningi', NULL, 0, 0),
(123, 4, 'Shira', NULL, 0, 0),
(124, 4, 'Tafawa-Balewa', NULL, 0, 0),
(125, 4, 'Toro', NULL, 0, 0),
(126, 4, 'Warji', NULL, 0, 0),
(127, 4, 'Zaki', NULL, 0, 0),
(128, 5, 'Brass', NULL, 0, 0),
(129, 5, 'Ekeremor', NULL, 0, 0),
(130, 5, 'Kolokuma/Opokuma', NULL, 0, 0),
(131, 5, 'Nembe', NULL, 0, 0),
(132, 5, 'Ogbia', NULL, 0, 0),
(133, 5, 'Sagbama', NULL, 0, 0),
(134, 5, 'Southern Jaw', NULL, 0, 0),
(135, 5, 'Yenegoa', NULL, 0, 0),
(136, 6, 'Ado', NULL, 0, 0),
(137, 6, 'Agatu', NULL, 0, 0),
(138, 6, 'Apa', NULL, 0, 0),
(139, 6, 'Buruku', NULL, 0, 0),
(140, 6, 'Gboko', NULL, 0, 0),
(141, 6, 'Guma', NULL, 0, 0),
(142, 6, 'Gwer East', NULL, 0, 0),
(143, 6, 'Gwer West', NULL, 0, 0),
(144, 6, 'Katsina-Ala', NULL, 0, 0),
(145, 6, 'Konshisha', NULL, 0, 0),
(146, 6, 'Kwande', NULL, 0, 0),
(147, 6, 'Logo', NULL, 0, 0),
(148, 6, 'Makurdi', NULL, 0, 0),
(149, 6, 'Obi', NULL, 0, 0),
(150, 6, 'Ogbadibo', NULL, 0, 0),
(151, 6, 'Oju', NULL, 0, 0),
(152, 6, 'Okpokwu', NULL, 0, 0),
(153, 6, 'Ohimini', NULL, 0, 0),
(154, 6, 'Oturkpo', NULL, 0, 0),
(155, 6, 'Tarka', NULL, 0, 0),
(156, 6, 'Ukum', NULL, 0, 0),
(157, 6, 'Ushongo', NULL, 0, 0),
(158, 6, 'Vandeikya', NULL, 0, 0),
(159, 7, 'Abadam', NULL, 0, 0),
(160, 7, 'Askira/Uba', NULL, 0, 0),
(161, 7, 'Bama', NULL, 0, 0),
(162, 7, 'Bayo', NULL, 0, 0),
(163, 7, 'Biu', NULL, 0, 0),
(164, 7, 'Chibok', NULL, 0, 0),
(165, 7, 'Damboa', NULL, 0, 0),
(166, 7, 'Dikwa', NULL, 0, 0),
(167, 7, 'Gubio', NULL, 0, 0),
(168, 7, 'Guzamala', NULL, 0, 0),
(169, 7, 'Gwoza', NULL, 0, 0),
(170, 7, 'Hawul', NULL, 0, 0),
(171, 7, 'Jere', NULL, 0, 0),
(172, 7, 'Kaga', NULL, 0, 0),
(173, 7, 'Kala/Balge', NULL, 0, 0),
(174, 7, 'Konduga', NULL, 0, 0),
(175, 7, 'Kukawa', NULL, 0, 0),
(176, 7, 'Kwaya Kusar', NULL, 0, 0),
(177, 7, 'Mafa', NULL, 0, 0),
(178, 7, 'Magumeri', NULL, 0, 0),
(179, 7, 'Maiduguri', NULL, 0, 0),
(180, 7, 'Marte', NULL, 0, 0),
(181, 7, 'Mobbar', NULL, 0, 0),
(182, 7, 'Monguno', NULL, 0, 0),
(183, 7, 'Ngala', NULL, 0, 0),
(184, 7, 'Nganzai', NULL, 0, 0),
(185, 7, 'Shani', NULL, 0, 0),
(186, 8, 'Akpabuyo', NULL, 0, 0),
(187, 8, 'Odukpani', NULL, 0, 0),
(188, 8, 'Akamkpa', NULL, 0, 0),
(189, 8, 'Biase', NULL, 0, 0),
(190, 8, 'Abi', NULL, 0, 0),
(191, 8, 'Ikom', NULL, 0, 0),
(192, 8, 'Yarkur', NULL, 0, 0),
(193, 8, 'Odubra', NULL, 0, 0),
(194, 8, 'Boki', NULL, 0, 0),
(195, 8, 'Ogoja', NULL, 0, 0),
(196, 8, 'Yala', NULL, 0, 0),
(197, 8, 'Obanliku', NULL, 0, 0),
(198, 8, 'Obudu', NULL, 0, 0),
(199, 8, 'Calabar South', NULL, 0, 0),
(200, 8, 'Etung', NULL, 0, 0),
(201, 8, 'Bekwara', NULL, 0, 0),
(202, 8, 'Bakassi', NULL, 0, 0),
(203, 8, 'Calabar Municipality', NULL, 0, 0),
(204, 9, 'Oshimili', NULL, 0, 0),
(205, 9, 'Aniocha', NULL, 0, 0),
(206, 9, 'Aniocha South', NULL, 0, 0),
(207, 9, 'Ika South', NULL, 0, 0),
(208, 9, 'Ika North-East', NULL, 0, 0),
(209, 9, 'Ndokwa West', NULL, 0, 0),
(210, 9, 'Ndokwa East', NULL, 0, 0),
(211, 9, 'Isoko south', NULL, 0, 0),
(212, 9, 'Isoko North', NULL, 0, 0),
(213, 9, 'Bomadi', NULL, 0, 0),
(214, 9, 'Burutu', NULL, 0, 0),
(215, 9, 'Ughelli South', NULL, 0, 0),
(216, 9, 'Ughelli North', NULL, 0, 0),
(217, 9, 'Ethiope West', NULL, 0, 0),
(218, 9, 'Ethiope East', NULL, 0, 0),
(219, 9, 'Sapele', NULL, 0, 0),
(220, 9, 'Okpe', NULL, 0, 0),
(221, 9, 'Warri North', NULL, 0, 0),
(222, 9, 'Warri South', NULL, 0, 0),
(223, 9, 'Uvwie', NULL, 0, 0),
(224, 9, 'Udu', NULL, 0, 0),
(225, 9, 'Warri Central', NULL, 0, 0),
(226, 9, 'Ukwani', NULL, 0, 0),
(227, 9, 'Oshimili North', NULL, 0, 0),
(228, 9, 'Patani', NULL, 0, 0),
(229, 10, 'Afikpo South', NULL, 0, 0),
(230, 10, 'Afikpo North', NULL, 0, 0),
(231, 10, 'Onicha', NULL, 0, 0),
(232, 10, 'Ohaozara', NULL, 0, 0),
(233, 10, 'Abakaliki', NULL, 0, 0),
(234, 10, 'Ishielu', NULL, 0, 0),
(235, 10, 'lkwo', NULL, 0, 0),
(236, 10, 'Ezza', NULL, 0, 0),
(237, 10, 'Ezza South', NULL, 0, 0),
(238, 10, 'Ohaukwu', NULL, 0, 0),
(239, 10, 'Ebonyi', NULL, 0, 0),
(240, 10, 'Ivo', NULL, 0, 0),
(241, 11, 'Esan North-East', NULL, 0, 0),
(242, 11, 'Esan Central', NULL, 0, 0),
(243, 11, 'Esan West', NULL, 0, 0),
(244, 11, 'Egor', NULL, 0, 0),
(245, 11, 'Ukpoba', NULL, 0, 0),
(246, 11, 'Central', NULL, 0, 0),
(247, 11, 'Etsako Central', NULL, 0, 0),
(248, 11, 'Igueben', NULL, 0, 0),
(249, 11, 'Oredo', NULL, 0, 0),
(250, 11, 'Ovia Southwest', NULL, 0, 0),
(251, 11, 'Ovia South-East', NULL, 0, 0),
(252, 11, 'Orhionwon', NULL, 0, 0),
(253, 11, 'Uhunmwonde', NULL, 0, 0),
(254, 11, 'Etsako East', NULL, 0, 0),
(255, 11, 'Esan South-East', NULL, 0, 0),
(256, 12, 'Ado', NULL, 0, 0),
(257, 12, 'Ekiti-East', NULL, 0, 0),
(258, 12, 'Ekiti-West', NULL, 0, 0),
(259, 12, 'Emure/Ise/Orun', NULL, 0, 0),
(260, 12, 'Ekiti South-West', NULL, 0, 0),
(261, 12, 'Ikare', NULL, 0, 0),
(262, 12, 'Irepodun', NULL, 0, 0),
(263, 12, 'Ijero,', NULL, 0, 0),
(264, 12, 'Ido/Osi', NULL, 0, 0),
(265, 12, 'Oye', NULL, 0, 0),
(266, 12, 'Ikole', NULL, 0, 0),
(267, 12, 'Moba', NULL, 0, 0),
(268, 12, 'Gbonyin', NULL, 0, 0),
(269, 12, 'Efon', NULL, 0, 0),
(270, 12, 'Ise/Orun', NULL, 0, 0),
(271, 12, 'Ilejemeje.', NULL, 0, 0),
(272, 13, 'Enugu South,', NULL, 0, 0),
(273, 13, 'Igbo-Eze South', NULL, 0, 0),
(274, 13, 'Enugu North', NULL, 0, 0),
(275, 13, 'Nkanu', NULL, 0, 0),
(276, 13, 'Udi Agwu', NULL, 0, 0),
(277, 13, 'Oji-River', NULL, 0, 0),
(278, 13, 'Ezeagu', NULL, 0, 0),
(279, 13, 'IgboEze North', NULL, 0, 0),
(280, 13, 'Isi-Uzo', NULL, 0, 0),
(281, 13, 'Nsukka', NULL, 0, 0),
(282, 13, 'Igbo-Ekiti', NULL, 0, 0),
(283, 13, 'Uzo-Uwani', NULL, 0, 0),
(284, 13, 'Enugu Eas', NULL, 0, 0),
(285, 13, 'Aninri', NULL, 0, 0),
(286, 13, 'Nkanu East', NULL, 0, 0),
(287, 13, 'Udenu.', NULL, 0, 0),
(288, 14, 'Akko', NULL, 0, 0),
(289, 14, 'Balanga', NULL, 0, 0),
(290, 14, 'Billiri', NULL, 0, 0),
(291, 14, 'Dukku', NULL, 0, 0),
(292, 14, 'Kaltungo', NULL, 0, 0),
(293, 14, 'Kwami', NULL, 0, 0),
(294, 14, 'Shomgom', NULL, 0, 0),
(295, 14, 'Funakaye', NULL, 0, 0),
(296, 14, 'Gombe', NULL, 0, 0),
(297, 14, 'Nafada/Bajoga', NULL, 0, 0),
(298, 14, 'Yamaltu/Delta.', NULL, 0, 0),
(299, 15, 'Aboh-Mbaise', NULL, 0, 0),
(300, 15, 'Ahiazu-Mbaise', NULL, 0, 0),
(301, 15, 'Ehime-Mbano', NULL, 0, 0),
(302, 15, 'Ezinihitte', NULL, 0, 0),
(303, 15, 'Ideato North', NULL, 0, 0),
(304, 15, 'Ideato South', NULL, 0, 0),
(305, 15, 'Ihitte/Uboma', NULL, 0, 0),
(306, 15, 'Ikeduru', NULL, 0, 0),
(307, 15, 'Isiala Mbano', NULL, 0, 0),
(308, 15, 'Isu', NULL, 0, 0),
(309, 15, 'Mbaitoli', NULL, 0, 0),
(310, 15, 'Mbaitoli', NULL, 0, 0),
(311, 15, 'Ngor-Okpala', NULL, 0, 0),
(312, 15, 'Njaba', NULL, 0, 0),
(313, 15, 'Nwangele', NULL, 0, 0),
(314, 15, 'Nkwerre', NULL, 0, 0),
(315, 15, 'Obowo', NULL, 0, 0),
(316, 15, 'Oguta', NULL, 0, 0),
(317, 15, 'Ohaji/Egbema', NULL, 0, 0),
(318, 15, 'Okigwe', NULL, 0, 0),
(319, 15, 'Orlu', NULL, 0, 0),
(320, 15, 'Orsu', NULL, 0, 0),
(321, 15, 'Oru East', NULL, 0, 0),
(322, 15, 'Oru West', NULL, 0, 0),
(323, 15, 'Owerri-Municipal', NULL, 0, 0),
(324, 15, 'Owerri North', NULL, 0, 0),
(325, 15, 'Owerri West', NULL, 0, 0),
(326, 16, 'Auyo', NULL, 0, 0),
(327, 16, 'Babura', NULL, 0, 0),
(328, 16, 'Birni Kudu', NULL, 0, 0),
(329, 16, 'Biriniwa', NULL, 0, 0),
(330, 16, 'Buji', NULL, 0, 0),
(331, 16, 'Dutse', NULL, 0, 0),
(332, 16, 'Gagarawa', NULL, 0, 0),
(333, 16, 'Garki', NULL, 0, 0),
(334, 16, 'Gumel', NULL, 0, 0),
(335, 16, 'Guri', NULL, 0, 0),
(336, 16, 'Gwaram', NULL, 0, 0),
(337, 16, 'Gwiwa', NULL, 0, 0),
(338, 16, 'Hadejia', NULL, 0, 0),
(339, 16, 'Jahun', NULL, 0, 0),
(340, 16, 'Kafin Hausa', NULL, 0, 0),
(341, 16, 'Kaugama Kazaure', NULL, 0, 0),
(342, 16, 'Kiri Kasamma', NULL, 0, 0),
(343, 16, 'Kiyawa', NULL, 0, 0),
(344, 16, 'Maigatari', NULL, 0, 0),
(345, 16, 'Malam Madori', NULL, 0, 0),
(346, 16, 'Miga', NULL, 0, 0),
(347, 16, 'Ringim', NULL, 0, 0),
(348, 16, 'Roni', NULL, 0, 0),
(349, 16, 'Sule-Tankarkar', NULL, 0, 0),
(350, 16, 'Taura', NULL, 0, 0),
(351, 16, 'Yankwashi', NULL, 0, 0),
(352, 17, 'Birni-Gwari', NULL, 0, 0),
(353, 17, 'Chikun', NULL, 0, 0),
(354, 17, 'Giwa', NULL, 0, 0),
(355, 17, 'Igabi', NULL, 0, 0),
(356, 17, 'Ikara', NULL, 0, 0),
(357, 17, 'jaba', NULL, 0, 0),
(358, 17, 'Jema''a', NULL, 0, 0),
(359, 17, 'Kachia', NULL, 0, 0),
(360, 17, 'Kaduna North', NULL, 0, 0),
(361, 17, 'Kaduna South', NULL, 0, 0),
(362, 17, 'Kagarko', NULL, 0, 0),
(363, 17, 'Kajuru', NULL, 0, 0),
(364, 17, 'Kaura', NULL, 0, 0),
(365, 17, 'Kauru', NULL, 0, 0),
(366, 17, 'Kubau', NULL, 0, 0),
(367, 17, 'Kudan', NULL, 0, 0),
(368, 17, 'Lere', NULL, 0, 0),
(369, 17, 'Makarfi', NULL, 0, 0),
(370, 17, 'Sabon-Gari', NULL, 0, 0),
(371, 17, 'Sanga', NULL, 0, 0),
(372, 17, 'Soba', NULL, 0, 0),
(373, 17, 'Zango-Kataf', NULL, 0, 0),
(374, 17, 'Zaria', NULL, 0, 0),
(375, 18, 'Ajingi', NULL, 0, 0),
(376, 18, 'Albasu', NULL, 0, 0),
(377, 18, 'Bagwai', NULL, 0, 0),
(378, 18, 'Bebeji', NULL, 0, 0),
(379, 18, 'Bichi', NULL, 0, 0),
(380, 18, 'Bunkure', NULL, 0, 0),
(381, 18, 'Dala', NULL, 0, 0),
(382, 18, 'Dambatta', NULL, 0, 0),
(383, 18, 'Dawakin Kudu', NULL, 0, 0),
(384, 18, 'Dawakin Tofa', NULL, 0, 0),
(385, 18, 'Doguwa', NULL, 0, 0),
(386, 18, 'Fagge', NULL, 0, 0),
(387, 18, 'Gabasawa', NULL, 0, 0),
(388, 18, 'Garko', NULL, 0, 0),
(389, 18, 'Garum', NULL, 0, 0),
(390, 18, 'Mallam', NULL, 0, 0),
(391, 18, 'Gaya', NULL, 0, 0),
(392, 18, 'Gezawa', NULL, 0, 0),
(393, 18, 'Gwale', NULL, 0, 0),
(394, 18, 'Gwarzo', NULL, 0, 0),
(395, 18, 'Kabo', NULL, 0, 0),
(396, 18, 'Kano Municipal', NULL, 0, 0),
(397, 18, 'Karaye', NULL, 0, 0),
(398, 18, 'Kibiya', NULL, 0, 0),
(399, 18, 'Kiru', NULL, 0, 0),
(400, 18, 'kumbotso', NULL, 0, 0),
(401, 18, 'Kunchi', NULL, 0, 0),
(402, 18, 'Kura', NULL, 0, 0),
(403, 18, 'Madobi', NULL, 0, 0),
(404, 18, 'Makoda', NULL, 0, 0),
(405, 18, 'Minjibir', NULL, 0, 0),
(406, 18, 'Nasarawa', NULL, 0, 0),
(407, 18, 'Rano', NULL, 0, 0),
(408, 18, 'Rimin Gado', NULL, 0, 0),
(409, 18, 'Rogo', NULL, 0, 0),
(410, 18, 'Shanono', NULL, 0, 0),
(411, 18, 'Sumaila', NULL, 0, 0),
(412, 18, 'Takali', NULL, 0, 0),
(413, 18, 'Tarauni', NULL, 0, 0),
(414, 18, 'Tofa', NULL, 0, 0),
(415, 18, 'Tsanyawa', NULL, 0, 0),
(416, 18, 'Tudun Wada', NULL, 0, 0),
(417, 18, 'Ungogo', NULL, 0, 0),
(418, 18, 'Warawa', NULL, 0, 0),
(419, 18, 'Wudil', NULL, 0, 0),
(420, 19, 'Bakori', NULL, 0, 0),
(421, 19, 'Batagarawa', NULL, 0, 0),
(422, 19, 'Batsari', NULL, 0, 0),
(423, 19, 'Baure', NULL, 0, 0),
(424, 19, 'Bindawa', NULL, 0, 0),
(425, 19, 'Charanchi', NULL, 0, 0),
(426, 19, 'Dandume', NULL, 0, 0),
(427, 19, 'Danja', NULL, 0, 0),
(428, 19, 'Dan Musa', NULL, 0, 0),
(429, 19, 'Daura', NULL, 0, 0),
(430, 19, 'Dutsi', NULL, 0, 0),
(431, 19, 'Dutsin-Ma', NULL, 0, 0),
(432, 19, 'Faskari', NULL, 0, 0),
(433, 19, 'Funtua', NULL, 0, 0),
(434, 19, 'Ingawa', NULL, 0, 0),
(435, 19, 'Jibia', NULL, 0, 0),
(436, 19, 'Kafur', NULL, 0, 0),
(437, 19, 'Kaita', NULL, 0, 0),
(438, 19, 'Kankara', NULL, 0, 0),
(439, 19, 'Kankia', NULL, 0, 0),
(440, 19, 'Katsina', NULL, 0, 0),
(441, 19, 'Kurfi', NULL, 0, 0),
(442, 19, 'Kusada', NULL, 0, 0),
(443, 19, 'Mai''Adua', NULL, 0, 0),
(444, 19, 'Malumfashi', NULL, 0, 0),
(445, 19, 'Mani', NULL, 0, 0),
(446, 19, 'Mashi', NULL, 0, 0),
(447, 19, 'Matazuu', NULL, 0, 0),
(448, 19, 'Musawa', NULL, 0, 0),
(449, 19, 'Rimi', NULL, 0, 0),
(450, 19, 'Sabuwa', NULL, 0, 0),
(451, 19, 'Safana', NULL, 0, 0),
(452, 19, 'Sandamu', NULL, 0, 0),
(453, 19, 'Zango', NULL, 0, 0),
(454, 20, 'Aleiro', NULL, 0, 0),
(455, 20, 'Arewa-Dandi', NULL, 0, 0),
(456, 20, 'Argungu', NULL, 0, 0),
(457, 20, 'Augie', NULL, 0, 0),
(458, 20, 'Bagudo', NULL, 0, 0),
(459, 20, 'Birnin Kebbi', NULL, 0, 0),
(460, 20, 'Bunza', NULL, 0, 0),
(461, 20, 'Dandi', NULL, 0, 0),
(462, 20, 'Fakai', NULL, 0, 0),
(463, 20, 'Gwandu', NULL, 0, 0),
(464, 20, 'Jega', NULL, 0, 0),
(465, 20, 'Kalgo', NULL, 0, 0),
(466, 20, 'Koko/Besse', NULL, 0, 0),
(467, 20, 'Maiyama', NULL, 0, 0),
(468, 20, 'Ngaski', NULL, 0, 0),
(469, 20, 'Sakaba', NULL, 0, 0),
(470, 20, 'Shanga', NULL, 0, 0),
(471, 20, 'Suru', NULL, 0, 0),
(472, 20, 'Wasagu/Danko', NULL, 0, 0),
(473, 20, 'Yauri', NULL, 0, 0),
(474, 20, 'Zuru', NULL, 0, 0),
(475, 21, 'Adavi', NULL, 0, 0),
(476, 21, 'Ajaokuta', NULL, 0, 0),
(477, 21, 'Ankpa', NULL, 0, 0),
(478, 21, 'Bassa', NULL, 0, 0),
(479, 21, 'Dekina', NULL, 0, 0),
(480, 21, 'Ibaji', NULL, 0, 0),
(481, 21, 'Idah', NULL, 0, 0),
(482, 21, 'Igalamela-Odolu', NULL, 0, 0),
(483, 21, 'Ijumu', NULL, 0, 0),
(484, 21, 'Kabba/Bunu', NULL, 0, 0),
(485, 21, 'Kogi', NULL, 0, 0),
(486, 21, 'Lokoja', NULL, 0, 0),
(487, 21, 'Mopa-Muro', NULL, 0, 0),
(488, 21, 'Ofu', NULL, 0, 0),
(489, 21, 'Ogori/Mangongo', NULL, 0, 0),
(490, 21, 'Okehi', NULL, 0, 0),
(491, 21, 'Okene', NULL, 0, 0),
(492, 21, 'Olamabolo', NULL, 0, 0),
(493, 21, 'Omala', NULL, 0, 0),
(494, 21, 'Yagba East', NULL, 0, 0),
(495, 21, 'Yagba West', NULL, 0, 0),
(496, 22, 'Asa', NULL, 0, 0),
(497, 22, 'Baruten', NULL, 0, 0),
(498, 22, 'Edu', NULL, 0, 0),
(499, 22, 'Ekiti', NULL, 0, 0),
(500, 22, 'Ifelodun', NULL, 0, 0),
(501, 22, 'Ilorin East', NULL, 0, 0),
(502, 22, 'Ilorin West', NULL, 0, 0),
(503, 22, 'Irepodun', NULL, 0, 0),
(504, 22, 'Isin', NULL, 0, 0),
(505, 22, 'Kaiama', NULL, 0, 0),
(506, 22, 'Moro', NULL, 0, 0),
(507, 22, 'Offa', NULL, 0, 0),
(508, 22, 'Oke-Ero', NULL, 0, 0),
(509, 22, 'Oyun', NULL, 0, 0),
(510, 22, 'Pategi', NULL, 0, 0),
(511, 23, 'Agege', NULL, 0, 0),
(512, 23, 'Ajeromi-Ifelodun', NULL, 0, 0),
(513, 23, 'Alimosho', NULL, 0, 0),
(514, 23, 'Amuwo-Odofin', NULL, 0, 0),
(515, 23, 'Apapa', NULL, 0, 0),
(516, 23, 'Badagry', NULL, 0, 0),
(517, 23, 'Epe', NULL, 0, 0),
(518, 23, 'Eti-Osa', NULL, 0, 0),
(519, 23, 'Ibeju/Lekki', NULL, 0, 0),
(520, 23, 'Ifako-Ijaye', NULL, 0, 0),
(521, 23, 'Ikeja', NULL, 0, 0),
(522, 23, 'Ikorodu', NULL, 0, 0),
(523, 23, 'Kosofe', NULL, 0, 0),
(524, 23, 'Lagos Island', NULL, 0, 0),
(525, 23, 'Lagos Mainland', NULL, 0, 0),
(526, 23, 'Mushin', NULL, 0, 0),
(527, 23, 'Ojo', NULL, 0, 0),
(528, 23, 'Oshodi-Isolo', NULL, 0, 0),
(529, 23, 'Shomolu', NULL, 0, 0),
(530, 23, 'Surulere', NULL, 0, 0),
(531, 24, 'Akwanga', NULL, 0, 0),
(532, 24, 'Awe', NULL, 0, 0),
(533, 24, 'Doma', NULL, 0, 0),
(534, 24, 'Karu', NULL, 0, 0),
(535, 24, 'Keana', NULL, 0, 0),
(536, 24, 'Keffi', NULL, 0, 0),
(537, 24, 'Kokona', NULL, 0, 0),
(538, 24, 'Lafia', NULL, 0, 0),
(539, 24, 'Nasarawa', NULL, 0, 0),
(540, 24, 'Nasarawa-Eggon', NULL, 0, 0),
(541, 24, 'Obi', NULL, 0, 0),
(542, 24, 'Toto', NULL, 0, 0),
(543, 24, 'Wamba', NULL, 0, 0),
(544, 25, 'Agaie', NULL, 0, 0),
(545, 25, 'Agwara', NULL, 0, 0),
(546, 25, 'Bida', NULL, 0, 0),
(547, 25, 'Borgu', NULL, 0, 0),
(548, 25, 'Bosso', NULL, 0, 0),
(549, 25, 'Chanchaga', NULL, 0, 0),
(550, 25, 'Edati', NULL, 0, 0),
(551, 25, 'Gbako', NULL, 0, 0),
(552, 25, 'Gurara', NULL, 0, 0),
(553, 25, 'Katcha', NULL, 0, 0),
(554, 25, 'Kontagora', NULL, 0, 0),
(555, 25, 'Lapai', NULL, 0, 0),
(556, 25, 'Lavun', NULL, 0, 0),
(557, 25, 'Magama', NULL, 0, 0),
(558, 25, 'Mariga', NULL, 0, 0),
(559, 25, 'Mashegu', NULL, 0, 0),
(560, 25, 'Mokwa', NULL, 0, 0),
(561, 25, 'Muya', NULL, 0, 0),
(562, 25, 'Pailoro', NULL, 0, 0),
(563, 25, 'Rafi', NULL, 0, 0),
(564, 25, 'Rijau', NULL, 0, 0),
(565, 25, 'Shiroro', NULL, 0, 0),
(566, 25, 'Suleja', NULL, 0, 0),
(567, 25, 'Tafa', NULL, 0, 0),
(568, 25, 'Wushishi', NULL, 0, 0),
(569, 26, 'Abeokuta North', NULL, 0, 0),
(570, 26, 'Abeokuta South', NULL, 0, 0),
(571, 26, 'Ado-Odo/Ota', NULL, 0, 0),
(572, 26, 'Egbado North', NULL, 0, 0),
(573, 26, 'Egbado South', NULL, 0, 0),
(574, 26, 'Ewekoro', NULL, 0, 0),
(575, 26, 'Ifo', NULL, 0, 0),
(576, 26, 'Ijebu East', NULL, 0, 0),
(577, 26, 'Ijebu North', NULL, 0, 0),
(578, 26, 'Ijebu North East', NULL, 0, 0),
(579, 26, 'Ijebu Ode', NULL, 0, 0),
(580, 26, 'Ikenne', NULL, 0, 0),
(581, 26, 'Imeko-Afon', NULL, 0, 0),
(582, 26, 'Ipokia', NULL, 0, 0),
(583, 26, 'Obafemi-Owode', NULL, 0, 0),
(584, 26, 'Ogun Waterside', NULL, 0, 0),
(585, 26, 'Odeda', NULL, 0, 0),
(586, 26, 'Odogbolu', NULL, 0, 0),
(587, 26, 'Remo North', NULL, 0, 0),
(588, 26, 'Shagamu', NULL, 0, 0),
(589, 27, 'Akoko North East', NULL, 0, 0),
(590, 27, 'Akoko North West', NULL, 0, 0),
(591, 27, 'Akoko South Akure East', NULL, 0, 0),
(592, 27, 'Akoko South West', NULL, 0, 0),
(593, 27, 'Akure North', NULL, 0, 0),
(594, 27, 'Akure South', NULL, 0, 0),
(595, 27, 'Ese-Odo', NULL, 0, 0),
(596, 27, 'Idanre', NULL, 0, 0),
(597, 27, 'Ifedore', NULL, 0, 0),
(598, 27, 'Ilaje', NULL, 0, 0),
(599, 27, 'Ile-Oluji', NULL, 0, 0),
(600, 27, 'Okeigbo', NULL, 0, 0),
(601, 27, 'Irele', NULL, 0, 0),
(602, 27, 'Odigbo', NULL, 0, 0),
(603, 27, 'Okitipupa', NULL, 0, 0),
(604, 27, 'Ondo East', NULL, 0, 0),
(605, 27, 'Ondo West', NULL, 0, 0),
(606, 27, 'Ose', NULL, 0, 0),
(607, 27, 'Owo', NULL, 0, 0),
(608, 28, 'Aiyedade', NULL, 0, 0),
(609, 28, 'Aiyedire', NULL, 0, 0),
(610, 28, 'Atakumosa East', NULL, 0, 0),
(611, 28, 'Atakumosa West', NULL, 0, 0),
(612, 28, 'Boluwaduro', NULL, 0, 0),
(613, 28, 'Boripe', NULL, 0, 0),
(614, 28, 'Ede North', NULL, 0, 0),
(615, 28, 'Ede South', NULL, 0, 0),
(616, 28, 'Egbedore', NULL, 0, 0),
(617, 28, 'Ejigbo', NULL, 0, 0),
(618, 28, 'Ife Central', NULL, 0, 0),
(619, 28, 'Ife East', NULL, 0, 0),
(620, 28, 'Ife North', NULL, 0, 0),
(621, 28, 'Ife South', NULL, 0, 0),
(622, 28, 'Ifedayo', NULL, 0, 0),
(623, 28, 'Ifelodun', NULL, 0, 0),
(624, 28, 'Ila', NULL, 0, 0),
(625, 28, 'Ilesha East', NULL, 0, 0),
(626, 28, 'Ilesha West', NULL, 0, 0),
(627, 28, 'Irepodun', NULL, 0, 0),
(628, 28, 'Irewole', NULL, 0, 0),
(629, 28, 'Isokan', NULL, 0, 0),
(630, 28, 'Iwo', NULL, 0, 0),
(631, 28, 'Obokun', NULL, 0, 0),
(632, 28, 'Odo-Otin', NULL, 0, 0),
(633, 28, 'Ola-Oluwa', NULL, 0, 0),
(634, 28, 'Olorunda', NULL, 0, 0),
(635, 28, 'Oriade', NULL, 0, 0),
(636, 28, 'Orolu', NULL, 0, 0),
(637, 28, 'Osogbo', NULL, 0, 0),
(638, 29, 'Afijio', NULL, 0, 0),
(639, 29, 'Akinyele', NULL, 0, 0),
(640, 29, 'Atiba', NULL, 0, 0),
(641, 29, 'Atigbo', NULL, 0, 0),
(642, 29, 'Egbeda', NULL, 0, 0),
(643, 29, 'IbadanCentral', NULL, 0, 0),
(644, 29, 'Ibadan North', NULL, 0, 0),
(645, 29, 'Ibadan North West', NULL, 0, 0),
(646, 29, 'Ibadan South East', NULL, 0, 0),
(647, 29, 'Ibadan South West', NULL, 0, 0),
(648, 29, 'Ibarapa Central', NULL, 0, 0),
(649, 29, 'Ibarapa East', NULL, 0, 0),
(650, 29, 'Ibarapa North', NULL, 0, 0),
(651, 29, 'Ido', NULL, 0, 0),
(652, 29, 'Irepo', NULL, 0, 0),
(653, 29, 'Iseyin', NULL, 0, 0),
(654, 29, 'Itesiwaju', NULL, 0, 0),
(655, 29, 'Iwajowa', NULL, 0, 0),
(656, 29, 'Kajola', NULL, 0, 0),
(657, 29, 'Lagelu Ogbomosho North', NULL, 0, 0),
(658, 29, 'Ogbmosho South', NULL, 0, 0),
(659, 29, 'Ogo Oluwa', NULL, 0, 0),
(660, 29, 'Olorunsogo', NULL, 0, 0),
(661, 29, 'Oluyole', NULL, 0, 0),
(662, 29, 'Ona-Ara', NULL, 0, 0),
(663, 29, 'Orelope', NULL, 0, 0),
(664, 29, 'Ori Ire', NULL, 0, 0),
(665, 29, 'Oyo East', NULL, 0, 0),
(666, 29, 'Oyo West', NULL, 0, 0),
(667, 29, 'Saki East', NULL, 0, 0),
(668, 29, 'Saki West', NULL, 0, 0),
(669, 29, 'Surulere', NULL, 0, 0),
(670, 30, 'Barikin Ladi', NULL, 0, 0),
(671, 30, 'Bassa', NULL, 0, 0),
(672, 30, 'Bokkos', NULL, 0, 0),
(673, 30, 'Jos East', NULL, 0, 0),
(674, 30, 'Jos North', NULL, 0, 0),
(675, 30, 'Jos South', NULL, 0, 0),
(676, 30, 'Kanam', NULL, 0, 0),
(677, 30, 'Kanke', NULL, 0, 0),
(678, 30, 'Langtang North', NULL, 0, 0),
(679, 30, 'Langtang South', NULL, 0, 0),
(680, 30, 'Mangu', NULL, 0, 0),
(681, 30, 'Mikang', NULL, 0, 0),
(682, 30, 'Pankshin', NULL, 0, 0),
(683, 30, 'Qua''an Pan', NULL, 0, 0),
(684, 30, 'Riyom', NULL, 0, 0),
(685, 30, 'Shendam', NULL, 0, 0),
(686, 30, 'Wase', NULL, 0, 0),
(687, 31, 'Abua/Odual', NULL, 0, 0),
(688, 31, 'Ahoada East', NULL, 0, 0),
(689, 31, 'Ahoada West', NULL, 0, 0),
(690, 31, 'Akuku Toru', NULL, 0, 0),
(691, 31, 'Andoni', NULL, 0, 0),
(692, 31, 'Asari-Toru', NULL, 0, 0),
(693, 31, 'Bonny', NULL, 0, 0),
(694, 31, 'Degema', NULL, 0, 0),
(695, 31, 'Emohua', NULL, 0, 0),
(696, 31, 'Eleme', NULL, 0, 0),
(697, 31, 'Etche', NULL, 0, 0),
(698, 31, 'Gokana', NULL, 0, 0),
(699, 31, 'Ikwerre', NULL, 0, 0),
(700, 31, 'Khana', NULL, 0, 0),
(701, 31, 'Obia/Akpor', NULL, 0, 0),
(702, 31, 'Ogba/Egbema/Ndoni', NULL, 0, 0),
(703, 31, 'Ogu/Bolo', NULL, 0, 0),
(704, 31, 'Okrika', NULL, 0, 0),
(705, 31, 'Omumma', NULL, 0, 0),
(706, 31, 'Opobo/Nkoro', NULL, 0, 0),
(707, 31, 'Oyigbo', NULL, 0, 0),
(708, 31, 'Port-Harcourt', NULL, 0, 0),
(709, 31, 'Tai', NULL, 0, 0),
(710, 32, 'Binji', NULL, 0, 0),
(711, 32, 'Bodinga', NULL, 0, 0),
(712, 32, 'Dange-shnsi', NULL, 0, 0),
(713, 32, 'Gada', NULL, 0, 0),
(714, 32, 'Goronyo', NULL, 0, 0),
(715, 32, 'Gudu', NULL, 0, 0),
(716, 32, 'Gawabawa', NULL, 0, 0),
(717, 32, 'Illela', NULL, 0, 0),
(718, 32, 'Isa', NULL, 0, 0),
(719, 32, 'Kware', NULL, 0, 0),
(720, 32, 'kebbe', NULL, 0, 0),
(721, 32, 'Rabah', NULL, 0, 0),
(722, 32, 'Sabon Birni', NULL, 0, 0),
(723, 32, 'Shagari', NULL, 0, 0),
(724, 32, 'Silame', NULL, 0, 0),
(725, 32, 'Sokoto North', NULL, 0, 0),
(726, 32, 'Sokoto South', NULL, 0, 0),
(727, 32, 'Tambuwal', NULL, 0, 0),
(728, 32, 'Tqngaza', NULL, 0, 0),
(729, 32, 'Tureta', NULL, 0, 0),
(730, 32, 'Wamako', NULL, 0, 0),
(731, 32, 'Wurno', NULL, 0, 0),
(732, 32, 'Yabo', NULL, 0, 0),
(733, 33, 'Ardo-kola', NULL, 0, 0),
(734, 33, 'Bali', NULL, 0, 0),
(735, 33, 'Donga', NULL, 0, 0),
(736, 33, 'Gashaka', NULL, 0, 0),
(737, 33, 'Cassol', NULL, 0, 0),
(738, 33, 'Ibi', NULL, 0, 0),
(739, 33, 'Jalingo', NULL, 0, 0),
(740, 33, 'Karin-Lamido', NULL, 0, 0),
(741, 33, 'Kurmi', NULL, 0, 0),
(742, 33, 'Lau', NULL, 0, 0),
(743, 33, 'Sardauna', NULL, 0, 0),
(744, 33, 'Takum', NULL, 0, 0),
(745, 33, 'Ussa', NULL, 0, 0),
(746, 33, 'Wukari', NULL, 0, 0),
(747, 33, 'Yorro', NULL, 0, 0),
(748, 33, 'Zing', NULL, 0, 0),
(749, 34, 'Ardo-kola', NULL, 0, 0),
(750, 34, 'Bali', NULL, 0, 0),
(751, 34, 'Donga', NULL, 0, 0),
(752, 34, 'Gashaka', NULL, 0, 0),
(753, 34, 'Cassol', NULL, 0, 0),
(754, 34, 'Ibi', NULL, 0, 0),
(755, 34, 'Jalingo', NULL, 0, 0),
(756, 34, 'Karin-Lamido', NULL, 0, 0),
(757, 34, 'Kurmi', NULL, 0, 0),
(758, 34, 'Lau', NULL, 0, 0),
(759, 34, 'Sardauna', NULL, 0, 0),
(760, 34, 'Takum', NULL, 0, 0),
(761, 34, 'Ussa', NULL, 0, 0),
(762, 34, 'Wukari', NULL, 0, 0),
(763, 34, 'Yorro', NULL, 0, 0),
(764, 34, 'Zing', NULL, 0, 0),
(765, 35, 'Anka', NULL, 0, 0),
(766, 35, 'Bakura', NULL, 0, 0),
(767, 35, 'Birnin Magaji', NULL, 0, 0),
(768, 35, 'Bukkuyum', NULL, 0, 0),
(769, 35, 'Bungudu', NULL, 0, 0),
(770, 35, 'Gummi', NULL, 0, 0),
(771, 35, 'Gusau', NULL, 0, 0),
(772, 35, 'Kaura', NULL, 0, 0),
(773, 35, 'Namoda', NULL, 0, 0),
(774, 35, 'Maradun', NULL, 0, 0),
(775, 35, 'Maru', NULL, 0, 0),
(776, 35, 'Shinkafi', NULL, 0, 0),
(777, 35, 'Talata Mafara', NULL, 0, 0),
(778, 35, 'Tsafe', NULL, 0, 0),
(779, 35, 'Zurmi', NULL, 0, 0),
(799, 23, 'Victoria Island', NULL, 0, 0),
(800, 23, 'Ikoyi', NULL, 0, 0),
(801, 23, 'Ajah', NULL, 0, 0),
(802, 23, 'VGC', NULL, 0, 0),
(803, 23, 'Festac', NULL, 0, 0),
(804, 23, 'Yaba', NULL, 0, 0),
(805, 23, 'Ebute Metta', NULL, 0, 0),
(806, 23, 'Oniru', NULL, 0, 0),
(807, 23, 'Ogudu GRA', NULL, 0, 0),
(808, 23, 'Anthony Village', NULL, 0, 0),
(809, 23, 'Magodo', NULL, 0, 0),
(810, 23, 'Isolo', NULL, 0, 0),
(811, 23, 'Victoria Garden City', NULL, 0, 0),
(812, 23, 'Oshodi', NULL, 0, 0),
(813, 23, 'Chevron', NULL, 0, 0),
(814, 23, 'Ijesha', NULL, 0, 0),
(818, 5, 'test', NULL, 1, 0),
(819, 5, 'test two', NULL, 1, 0),
(820, 5, 'test', NULL, 0, 0),
(821, 45, 'test area one', NULL, 0, 0),
(822, 45, 'test area two', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `id` int(11) NOT NULL,
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`id`, `item_name`, `user_id`, `created_at`, `deleted`) VALUES
(89, 'admin', 1, 1475842439, 0),
(94, 'staff', 8, 1475842439, 0),
(95, 'staff', 9, 1475842439, 0),
(96, 'staff', 7, 1475842439, 0),
(97, 'archiving_question', 10, 1475853152, 0),
(98, 'edit_question', 10, 1475853152, 0),
(99, 'archiving_bag', 10, 1475853152, 0),
(100, 'disable_bag', 10, 1475853152, 0),
(101, 'search_bag', 10, 1475853152, 0),
(102, 'edit_bag', 10, 1475853152, 0),
(103, 'register_bag', 10, 1475853152, 0),
(104, 'archiving_donor', 10, 1475853152, 0),
(105, 'disable_donor', 10, 1475853152, 0),
(106, 'register_donor', 10, 1475853152, 0),
(107, 'search_donor', 10, 1475853152, 0),
(108, 'edit_donor', 10, 1475853152, 0),
(109, 'register_question', 10, 1475853152, 0),
(110, 'staff', 10, 1475853152, 0);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Admin', NULL, NULL, 1475842439, 1475842439),
('archiving_bag', 2, 'Blood Drive Archiving', NULL, NULL, 1475842440, 1475842440),
('archiving_donor', 2, 'Donor Archiving', NULL, NULL, 1475842440, 1475842440),
('archiving_location', 2, 'Location Archiving', NULL, NULL, 1475842440, 1475842440),
('archiving_question', 2, ' Questionnaire Archiving', NULL, NULL, 1475842440, 1475842440),
('archiving_staff', 2, 'Staff Archiving', NULL, NULL, 1475842440, 1475842440),
('disable_bag', 2, 'Blood Drive Disable', NULL, NULL, 1475842440, 1475842440),
('disable_donor', 2, 'Donor Disable', NULL, NULL, 1475842440, 1475842440),
('disable_location', 2, 'Location Disable', NULL, NULL, 1475842440, 1475842440),
('disable_staff', 2, 'Staff Disable', NULL, NULL, 1475842440, 1475842440),
('donor', 1, 'Donor', NULL, NULL, 1475842439, 1475842439),
('edit_bag', 2, 'Blood Drive Update', NULL, NULL, 1475842440, 1475842440),
('edit_donor', 2, 'Donor Update', NULL, NULL, 1475842440, 1475842440),
('edit_location', 2, 'Location Update', NULL, NULL, 1475842440, 1475842440),
('edit_question', 2, ' Questionnaire Update', NULL, NULL, 1475842440, 1475842440),
('edit_staff', 2, 'Staff Update', NULL, NULL, 1475842440, 1475842440),
('register_bag', 2, 'Blood Drive Update Register', NULL, NULL, 1475842440, 1475842440),
('register_donor', 2, 'Donor Register', NULL, NULL, 1475842440, 1475842440),
('register_location', 2, 'Location Register', NULL, NULL, 1475842440, 1475842440),
('register_question', 2, 'Questionnaire Register', NULL, NULL, 1475842440, 1475842440),
('register_staff', 2, 'Staff Register', NULL, NULL, 1475842440, 1475842440),
('search_bag', 2, 'Blood Drive Search', NULL, NULL, 1475842440, 1475842440),
('search_donor', 2, 'Donor Search', NULL, NULL, 1475842440, 1475842440),
('search_location', 2, 'Location Search', NULL, NULL, 1475842440, 1475842440),
('search_staff', 2, 'Staff Search', NULL, NULL, 1475842440, 1475842440),
('staff', 1, 'Staff', NULL, NULL, 1475842439, 1475842439);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'archiving_location'),
('admin', 'archiving_staff'),
('admin', 'disable_location'),
('admin', 'disable_staff'),
('admin', 'edit_location'),
('admin', 'edit_staff'),
('admin', 'register_location'),
('admin', 'register_staff'),
('admin', 'search_location'),
('admin', 'search_staff'),
('admin', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('userRole', 'O:30:"common\\components\\UserRoleRule":3:{s:4:"name";s:8:"userRole";s:9:"createdAt";i:1475842439;s:9:"updatedAt";i:1475842439;}', 1475842439, 1475842439);

-- --------------------------------------------------------

--
-- Table structure for table `blood_drive`
--

CREATE TABLE IF NOT EXISTS `blood_drive` (
  `id` int(11) NOT NULL,
  `donation_date` int(11) NOT NULL,
  `donor_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `screening_agency_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blood_bag_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes_bag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_for_screening` tinyint(1) NOT NULL,
  `date_sent` int(11) DEFAULT NULL,
  `screening_status` smallint(1) DEFAULT NULL,
  `date_screen` int(11) DEFAULT NULL,
  `screen_result` smallint(1) DEFAULT NULL,
  `date_expiry` int(11) DEFAULT NULL,
  `notes_screening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=966 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `area_id`, `name`, `description`, `deleted`) VALUES
(1, 1, 'Yola', NULL, 0),
(2, 2, 'Uyo', NULL, 0),
(3, 3, 'Awka', NULL, 0),
(4, 4, 'Bauchi', NULL, 0),
(5, 5, 'Yenagoa', NULL, 0),
(6, 6, 'Makurdi', NULL, 0),
(7, 7, 'Maiduguri', NULL, 0),
(8, 8, 'Calabar', NULL, 0),
(9, 9, 'Asaba', NULL, 0),
(10, 10, 'Abakaliki', NULL, 0),
(11, 11, 'Benin City', NULL, 0),
(12, 12, 'Ado Ekiti', NULL, 0),
(13, 13, 'Enugu', NULL, 0),
(14, 14, 'Gombe', NULL, 0),
(15, 15, 'Owerri', NULL, 0),
(16, 16, 'Dutse', NULL, 0),
(17, 17, 'Kaduna', NULL, 0),
(18, 18, 'Kano', NULL, 0),
(19, 19, 'Katsina', NULL, 0),
(20, 20, 'Birnin Kebbi', NULL, 0),
(21, 21, 'Lokoja', NULL, 0),
(22, 22, 'Ilorin', NULL, 0),
(23, 23, 'Ikeja', NULL, 0),
(24, 24, 'Lafia', NULL, 0),
(25, 25, 'Minna', NULL, 0),
(26, 26, 'Abeokuta', NULL, 0),
(27, 27, 'Akure', NULL, 0),
(28, 28, 'Oshogbo', NULL, 0),
(29, 29, 'Ibadan', NULL, 0),
(30, 30, 'Jos', NULL, 0),
(31, 31, 'Port Harcourt', NULL, 0),
(32, 32, 'Sokoto', NULL, 0),
(33, 33, 'Jalingo', NULL, 0),
(34, 34, 'Damaturu', NULL, 0),
(35, 35, 'Gusau', NULL, 0),
(36, 36, 'Demsa', NULL, 0),
(37, 37, 'Fufore', NULL, 0),
(38, 38, 'Ganaye', NULL, 0),
(39, 39, 'Gireri', NULL, 0),
(40, 40, 'Gombi', NULL, 0),
(41, 41, 'Guyuk', NULL, 0),
(42, 42, 'Hong', NULL, 0),
(43, 43, 'Jada', NULL, 0),
(44, 44, 'Lamurde', NULL, 0),
(45, 45, 'Madagali', NULL, 0),
(46, 46, 'Maiha', NULL, 0),
(47, 47, 'Mayo-Belwa', NULL, 0),
(48, 48, 'Michika', NULL, 0),
(49, 49, 'Mubi North', NULL, 0),
(50, 50, 'Mubi South', NULL, 0),
(51, 51, 'Numan', NULL, 0),
(52, 52, 'Shelleng', NULL, 0),
(53, 53, 'Song', NULL, 0),
(54, 54, 'Toungo', NULL, 0),
(55, 55, 'Yola North', NULL, 0),
(56, 56, 'Yola South', NULL, 0),
(57, 57, 'Abak', NULL, 0),
(58, 58, 'Eastern Obolo', NULL, 0),
(59, 59, 'Eket', NULL, 0),
(60, 60, 'Esit Eket', NULL, 0),
(61, 61, 'Essien Udim', NULL, 0),
(62, 62, 'Etim Ekpo', NULL, 0),
(63, 63, 'Etinan', NULL, 0),
(64, 64, 'Ibeno', NULL, 0),
(65, 65, 'Ibesikpo Asutan', NULL, 0),
(66, 66, 'Ibiono Ibom', NULL, 0),
(67, 67, 'Ika', NULL, 0),
(68, 68, 'Ikono', NULL, 0),
(69, 69, 'Ikot Abasi', NULL, 0),
(70, 70, 'Ikot Ekpene', NULL, 0),
(71, 71, 'Ini', NULL, 0),
(72, 72, 'Itu', NULL, 0),
(73, 73, 'Mbo', NULL, 0),
(74, 74, 'Mkpat Enin', NULL, 0),
(75, 75, 'Nsit Atai', NULL, 0),
(76, 76, 'Nsit Ibom', NULL, 0),
(77, 77, 'Nsit Ubium', NULL, 0),
(78, 78, 'Obot Akara', NULL, 0),
(79, 79, 'Okobo', NULL, 0),
(80, 80, 'Onna', NULL, 0),
(81, 81, 'Oron', NULL, 0),
(82, 82, 'Oruk Anam', NULL, 0),
(83, 83, 'Udung Uko', NULL, 0),
(84, 84, 'Ukanafun', NULL, 0),
(85, 85, 'Uruan', NULL, 0),
(86, 86, 'Urue-Offong/Oruko', NULL, 0),
(87, 87, 'Uyo', NULL, 0),
(88, 88, 'Aguata', NULL, 0),
(89, 89, 'Anambra East', NULL, 0),
(90, 90, 'Anambra West', NULL, 0),
(91, 91, 'Anaocha', NULL, 0),
(92, 92, 'Awka North', NULL, 0),
(93, 93, 'Awka South', NULL, 0),
(94, 94, 'Ayamelum', NULL, 0),
(95, 95, 'Dunukofia', NULL, 0),
(96, 96, 'Ekwusigo', NULL, 0),
(97, 97, 'Idemili North', NULL, 0),
(98, 98, 'Idemili South', NULL, 0),
(99, 99, 'Ihiala', NULL, 0),
(100, 100, 'Njikoka', NULL, 0),
(101, 101, 'Nnewi North', NULL, 0),
(102, 102, 'Nnewi South', NULL, 0),
(103, 103, 'Ogbaru', NULL, 0),
(104, 104, 'Onitsha North', NULL, 0),
(105, 105, 'Onitsha South', NULL, 0),
(106, 106, 'Orumba North', NULL, 0),
(107, 107, 'Orumba South', NULL, 0),
(108, 108, 'Oyi', NULL, 0),
(109, 109, 'Alkaleri', NULL, 0),
(110, 110, 'Bauchi', NULL, 0),
(111, 111, 'Bogoro', NULL, 0),
(112, 112, 'Damban', NULL, 0),
(113, 113, 'Darazo', NULL, 0),
(114, 114, 'Dass', NULL, 0),
(115, 115, 'Ganjuwa', NULL, 0),
(116, 116, 'Giade', NULL, 0),
(117, 117, 'Itas/Gadau', NULL, 0),
(118, 118, 'Jama''are', NULL, 0),
(119, 119, 'Katagum', NULL, 0),
(120, 120, 'Kirfi', NULL, 0),
(121, 121, 'Misau', NULL, 0),
(122, 122, 'Ningi', NULL, 0),
(123, 123, 'Shira', NULL, 0),
(124, 124, 'Tafawa-Balewa', NULL, 0),
(125, 125, 'Toro', NULL, 0),
(126, 126, 'Warji', NULL, 0),
(127, 127, 'Zaki', NULL, 0),
(128, 128, 'Brass', NULL, 0),
(129, 129, 'Ekeremor', NULL, 0),
(130, 130, 'Kolokuma/Opokuma', NULL, 0),
(131, 131, 'Nembe', NULL, 0),
(132, 132, 'Ogbia', NULL, 0),
(133, 133, 'Sagbama', NULL, 0),
(134, 134, 'Southern Jaw', NULL, 0),
(135, 135, 'Yenegoa', NULL, 0),
(136, 136, 'Ado', NULL, 0),
(137, 137, 'Agatu', NULL, 0),
(138, 138, 'Apa', NULL, 0),
(139, 139, 'Buruku', NULL, 0),
(140, 140, 'Gboko', NULL, 0),
(141, 141, 'Guma', NULL, 0),
(142, 142, 'Gwer East', NULL, 0),
(143, 143, 'Gwer West', NULL, 0),
(144, 144, 'Katsina-Ala', NULL, 0),
(145, 145, 'Konshisha', NULL, 0),
(146, 146, 'Kwande', NULL, 0),
(147, 147, 'Logo', NULL, 0),
(148, 148, 'Makurdi', NULL, 0),
(149, 149, 'Obi', NULL, 0),
(150, 150, 'Ogbadibo', NULL, 0),
(151, 151, 'Oju', NULL, 0),
(152, 152, 'Okpokwu', NULL, 0),
(153, 153, 'Ohimini', NULL, 0),
(154, 154, 'Oturkpo', NULL, 0),
(155, 155, 'Tarka', NULL, 0),
(156, 156, 'Ukum', NULL, 0),
(157, 157, 'Ushongo', NULL, 0),
(158, 158, 'Vandeikya', NULL, 0),
(159, 159, 'Abadam', NULL, 0),
(160, 160, 'Askira/Uba', NULL, 0),
(161, 161, 'Bama', NULL, 0),
(162, 162, 'Bayo', NULL, 0),
(163, 163, 'Biu', NULL, 0),
(164, 164, 'Chibok', NULL, 0),
(165, 165, 'Damboa', NULL, 0),
(166, 166, 'Dikwa', NULL, 0),
(167, 167, 'Gubio', NULL, 0),
(168, 168, 'Guzamala', NULL, 0),
(169, 169, 'Gwoza', NULL, 0),
(170, 170, 'Hawul', NULL, 0),
(171, 171, 'Jere', NULL, 0),
(172, 172, 'Kaga', NULL, 0),
(173, 173, 'Kala/Balge', NULL, 0),
(174, 174, 'Konduga', NULL, 0),
(175, 175, 'Kukawa', NULL, 0),
(176, 176, 'Kwaya Kusar', NULL, 0),
(177, 177, 'Mafa', NULL, 0),
(178, 178, 'Magumeri', NULL, 0),
(179, 179, 'Maiduguri', NULL, 0),
(180, 180, 'Marte', NULL, 0),
(181, 181, 'Mobbar', NULL, 0),
(182, 182, 'Monguno', NULL, 0),
(183, 183, 'Ngala', NULL, 0),
(184, 184, 'Nganzai', NULL, 0),
(185, 185, 'Shani', NULL, 0),
(186, 186, 'Akpabuyo', NULL, 0),
(187, 187, 'Odukpani', NULL, 0),
(188, 188, 'Akamkpa', NULL, 0),
(189, 189, 'Biase', NULL, 0),
(190, 190, 'Abi', NULL, 0),
(191, 191, 'Ikom', NULL, 0),
(192, 192, 'Yarkur', NULL, 0),
(193, 193, 'Odubra', NULL, 0),
(194, 194, 'Boki', NULL, 0),
(195, 195, 'Ogoja', NULL, 0),
(196, 196, 'Yala', NULL, 0),
(197, 197, 'Obanliku', NULL, 0),
(198, 198, 'Obudu', NULL, 0),
(199, 199, 'Calabar South', NULL, 0),
(200, 200, 'Etung', NULL, 0),
(201, 201, 'Bekwara', NULL, 0),
(202, 202, 'Bakassi', NULL, 0),
(203, 203, 'Calabar Municipality', NULL, 0),
(204, 204, 'Oshimili', NULL, 0),
(205, 205, 'Aniocha', NULL, 0),
(206, 206, 'Aniocha South', NULL, 0),
(207, 207, 'Ika South', NULL, 0),
(208, 208, 'Ika North-East', NULL, 0),
(209, 209, 'Ndokwa West', NULL, 0),
(210, 210, 'Ndokwa East', NULL, 0),
(211, 211, 'Isoko south', NULL, 0),
(212, 212, 'Isoko North', NULL, 0),
(213, 213, 'Bomadi', NULL, 0),
(214, 214, 'Burutu', NULL, 0),
(215, 215, 'Ughelli South', NULL, 0),
(216, 216, 'Ughelli North', NULL, 0),
(217, 217, 'Ethiope West', NULL, 0),
(218, 218, 'Ethiope East', NULL, 0),
(219, 219, 'Sapele', NULL, 0),
(220, 220, 'Okpe', NULL, 0),
(221, 221, 'Warri North', NULL, 0),
(222, 222, 'Warri South', NULL, 0),
(223, 223, 'Uvwie', NULL, 0),
(224, 224, 'Udu', NULL, 0),
(225, 225, 'Warri Central', NULL, 0),
(226, 226, 'Ukwani', NULL, 0),
(227, 227, 'Oshimili North', NULL, 0),
(228, 228, 'Patani', NULL, 0),
(229, 229, 'Afikpo South', NULL, 0),
(230, 230, 'Afikpo North', NULL, 0),
(231, 231, 'Onicha', NULL, 0),
(232, 232, 'Ohaozara', NULL, 0),
(233, 233, 'Abakaliki', NULL, 0),
(234, 234, 'Ishielu', NULL, 0),
(235, 235, 'lkwo', NULL, 0),
(236, 236, 'Ezza', NULL, 0),
(237, 237, 'Ezza South', NULL, 0),
(238, 238, 'Ohaukwu', NULL, 0),
(239, 239, 'Ebonyi', NULL, 0),
(240, 240, 'Ivo', NULL, 0),
(241, 241, 'Esan North-East', NULL, 0),
(242, 242, 'Esan Central', NULL, 0),
(243, 243, 'Esan West', NULL, 0),
(244, 244, 'Egor', NULL, 0),
(245, 245, 'Ukpoba', NULL, 0),
(246, 246, 'Central', NULL, 0),
(247, 247, 'Etsako Central', NULL, 0),
(248, 248, 'Igueben', NULL, 0),
(249, 249, 'Oredo', NULL, 0),
(250, 250, 'Ovia Southwest', NULL, 0),
(251, 251, 'Ovia South-East', NULL, 0),
(252, 252, 'Orhionwon', NULL, 0),
(253, 253, 'Uhunmwonde', NULL, 0),
(254, 254, 'Etsako East', NULL, 0),
(255, 255, 'Esan South-East', NULL, 0),
(256, 256, 'Ado', NULL, 0),
(257, 257, 'Ekiti-East', NULL, 0),
(258, 258, 'Ekiti-West', NULL, 0),
(259, 259, 'Emure/Ise/Orun', NULL, 0),
(260, 260, 'Ekiti South-West', NULL, 0),
(261, 261, 'Ikare', NULL, 0),
(262, 262, 'Irepodun', NULL, 0),
(263, 263, 'Ijero,', NULL, 0),
(264, 264, 'Ido/Osi', NULL, 0),
(265, 265, 'Oye', NULL, 0),
(266, 266, 'Ikole', NULL, 0),
(267, 267, 'Moba', NULL, 0),
(268, 268, 'Gbonyin', NULL, 0),
(269, 269, 'Efon', NULL, 0),
(270, 270, 'Ise/Orun', NULL, 0),
(271, 271, 'Ilejemeje.', NULL, 0),
(272, 272, 'Enugu South,', NULL, 0),
(273, 273, 'Igbo-Eze South', NULL, 0),
(274, 274, 'Enugu North', NULL, 0),
(275, 275, 'Nkanu', NULL, 0),
(276, 276, 'Udi Agwu', NULL, 0),
(277, 277, 'Oji-River', NULL, 0),
(278, 278, 'Ezeagu', NULL, 0),
(279, 279, 'IgboEze North', NULL, 0),
(280, 280, 'Isi-Uzo', NULL, 0),
(281, 281, 'Nsukka', NULL, 0),
(282, 282, 'Igbo-Ekiti', NULL, 0),
(283, 283, 'Uzo-Uwani', NULL, 0),
(284, 284, 'Enugu Eas', NULL, 0),
(285, 285, 'Aninri', NULL, 0),
(286, 286, 'Nkanu East', NULL, 0),
(287, 287, 'Udenu.', NULL, 0),
(288, 288, 'Akko', NULL, 0),
(289, 289, 'Balanga', NULL, 0),
(290, 290, 'Billiri', NULL, 0),
(291, 291, 'Dukku', NULL, 0),
(292, 292, 'Kaltungo', NULL, 0),
(293, 293, 'Kwami', NULL, 0),
(294, 294, 'Shomgom', NULL, 0),
(295, 295, 'Funakaye', NULL, 0),
(296, 296, 'Gombe', NULL, 0),
(297, 297, 'Nafada/Bajoga', NULL, 0),
(298, 298, 'Yamaltu/Delta.', NULL, 0),
(299, 299, 'Aboh-Mbaise', NULL, 0),
(300, 300, 'Ahiazu-Mbaise', NULL, 0),
(301, 301, 'Ehime-Mbano', NULL, 0),
(302, 302, 'Ezinihitte', NULL, 0),
(303, 303, 'Ideato North', NULL, 0),
(304, 304, 'Ideato South', NULL, 0),
(305, 305, 'Ihitte/Uboma', NULL, 0),
(306, 306, 'Ikeduru', NULL, 0),
(307, 307, 'Isiala Mbano', NULL, 0),
(308, 308, 'Isu', NULL, 0),
(309, 309, 'Mbaitoli', NULL, 0),
(310, 310, 'Mbaitoli', NULL, 0),
(311, 311, 'Ngor-Okpala', NULL, 0),
(312, 312, 'Njaba', NULL, 0),
(313, 313, 'Nwangele', NULL, 0),
(314, 314, 'Nkwerre', NULL, 0),
(315, 315, 'Obowo', NULL, 0),
(316, 316, 'Oguta', NULL, 0),
(317, 317, 'Ohaji/Egbema', NULL, 0),
(318, 318, 'Okigwe', NULL, 0),
(319, 319, 'Orlu', NULL, 0),
(320, 320, 'Orsu', NULL, 0),
(321, 321, 'Oru East', NULL, 0),
(322, 322, 'Oru West', NULL, 0),
(323, 323, 'Owerri-Municipal', NULL, 0),
(324, 324, 'Owerri North', NULL, 0),
(325, 325, 'Owerri West', NULL, 0),
(326, 326, 'Auyo', NULL, 0),
(327, 327, 'Babura', NULL, 0),
(328, 328, 'Birni Kudu', NULL, 0),
(329, 329, 'Biriniwa', NULL, 0),
(330, 330, 'Buji', NULL, 0),
(331, 331, 'Dutse', NULL, 0),
(332, 332, 'Gagarawa', NULL, 0),
(333, 333, 'Garki', NULL, 0),
(334, 334, 'Gumel', NULL, 0),
(335, 335, 'Guri', NULL, 0),
(336, 336, 'Gwaram', NULL, 0),
(337, 337, 'Gwiwa', NULL, 0),
(338, 338, 'Hadejia', NULL, 0),
(339, 339, 'Jahun', NULL, 0),
(340, 340, 'Kafin Hausa', NULL, 0),
(341, 341, 'Kaugama Kazaure', NULL, 0),
(342, 342, 'Kiri Kasamma', NULL, 0),
(343, 343, 'Kiyawa', NULL, 0),
(344, 344, 'Maigatari', NULL, 0),
(345, 345, 'Malam Madori', NULL, 0),
(346, 346, 'Miga', NULL, 0),
(347, 347, 'Ringim', NULL, 0),
(348, 348, 'Roni', NULL, 0),
(349, 349, 'Sule-Tankarkar', NULL, 0),
(350, 350, 'Taura', NULL, 0),
(351, 351, 'Yankwashi', NULL, 0),
(352, 352, 'Birni-Gwari', NULL, 0),
(353, 353, 'Chikun', NULL, 0),
(354, 354, 'Giwa', NULL, 0),
(355, 355, 'Igabi', NULL, 0),
(356, 356, 'Ikara', NULL, 0),
(357, 357, 'jaba', NULL, 0),
(358, 358, 'Jema''a', NULL, 0),
(359, 359, 'Kachia', NULL, 0),
(360, 360, 'Kaduna North', NULL, 0),
(361, 361, 'Kaduna South', NULL, 0),
(362, 362, 'Kagarko', NULL, 0),
(363, 363, 'Kajuru', NULL, 0),
(364, 364, 'Kaura', NULL, 0),
(365, 365, 'Kauru', NULL, 0),
(366, 366, 'Kubau', NULL, 0),
(367, 367, 'Kudan', NULL, 0),
(368, 368, 'Lere', NULL, 0),
(369, 369, 'Makarfi', NULL, 0),
(370, 370, 'Sabon-Gari', NULL, 0),
(371, 371, 'Sanga', NULL, 0),
(372, 372, 'Soba', NULL, 0),
(373, 373, 'Zango-Kataf', NULL, 0),
(374, 374, 'Zaria', NULL, 0),
(375, 375, 'Ajingi', NULL, 0),
(376, 376, 'Albasu', NULL, 0),
(377, 377, 'Bagwai', NULL, 0),
(378, 378, 'Bebeji', NULL, 0),
(379, 379, 'Bichi', NULL, 0),
(380, 380, 'Bunkure', NULL, 0),
(381, 381, 'Dala', NULL, 0),
(382, 382, 'Dambatta', NULL, 0),
(383, 383, 'Dawakin Kudu', NULL, 0),
(384, 384, 'Dawakin Tofa', NULL, 0),
(385, 385, 'Doguwa', NULL, 0),
(386, 386, 'Fagge', NULL, 0),
(387, 387, 'Gabasawa', NULL, 0),
(388, 388, 'Garko', NULL, 0),
(389, 389, 'Garum', NULL, 0),
(390, 390, 'Mallam', NULL, 0),
(391, 391, 'Gaya', NULL, 0),
(392, 392, 'Gezawa', NULL, 0),
(393, 393, 'Gwale', NULL, 0),
(394, 394, 'Gwarzo', NULL, 0),
(395, 395, 'Kabo', NULL, 0),
(396, 396, 'Kano Municipal', NULL, 0),
(397, 397, 'Karaye', NULL, 0),
(398, 398, 'Kibiya', NULL, 0),
(399, 399, 'Kiru', NULL, 0),
(400, 400, 'kumbotso', NULL, 0),
(401, 401, 'Kunchi', NULL, 0),
(402, 402, 'Kura', NULL, 0),
(403, 403, 'Madobi', NULL, 0),
(404, 404, 'Makoda', NULL, 0),
(405, 405, 'Minjibir', NULL, 0),
(406, 406, 'Nasarawa', NULL, 0),
(407, 407, 'Rano', NULL, 0),
(408, 408, 'Rimin Gado', NULL, 0),
(409, 409, 'Rogo', NULL, 0),
(410, 410, 'Shanono', NULL, 0),
(411, 411, 'Sumaila', NULL, 0),
(412, 412, 'Takali', NULL, 0),
(413, 413, 'Tarauni', NULL, 0),
(414, 414, 'Tofa', NULL, 0),
(415, 415, 'Tsanyawa', NULL, 0),
(416, 416, 'Tudun Wada', NULL, 0),
(417, 417, 'Ungogo', NULL, 0),
(418, 418, 'Warawa', NULL, 0),
(419, 419, 'Wudil', NULL, 0),
(420, 420, 'Bakori', NULL, 0),
(421, 421, 'Batagarawa', NULL, 0),
(422, 422, 'Batsari', NULL, 0),
(423, 423, 'Baure', NULL, 0),
(424, 424, 'Bindawa', NULL, 0),
(425, 425, 'Charanchi', NULL, 0),
(426, 426, 'Dandume', NULL, 0),
(427, 427, 'Danja', NULL, 0),
(428, 428, 'Dan Musa', NULL, 0),
(429, 429, 'Daura', NULL, 0),
(430, 430, 'Dutsi', NULL, 0),
(431, 431, 'Dutsin-Ma', NULL, 0),
(432, 432, 'Faskari', NULL, 0),
(433, 433, 'Funtua', NULL, 0),
(434, 434, 'Ingawa', NULL, 0),
(435, 435, 'Jibia', NULL, 0),
(436, 436, 'Kafur', NULL, 0),
(437, 437, 'Kaita', NULL, 0),
(438, 438, 'Kankara', NULL, 0),
(439, 439, 'Kankia', NULL, 0),
(440, 440, 'Katsina', NULL, 0),
(441, 441, 'Kurfi', NULL, 0),
(442, 442, 'Kusada', NULL, 0),
(443, 443, 'Mai''Adua', NULL, 0),
(444, 444, 'Malumfashi', NULL, 0),
(445, 445, 'Mani', NULL, 0),
(446, 446, 'Mashi', NULL, 0),
(447, 447, 'Matazuu', NULL, 0),
(448, 448, 'Musawa', NULL, 0),
(449, 449, 'Rimi', NULL, 0),
(450, 450, 'Sabuwa', NULL, 0),
(451, 451, 'Safana', NULL, 0),
(452, 452, 'Sandamu', NULL, 0),
(453, 453, 'Zango', NULL, 0),
(454, 454, 'Aleiro', NULL, 0),
(455, 455, 'Arewa-Dandi', NULL, 0),
(456, 456, 'Argungu', NULL, 0),
(457, 457, 'Augie', NULL, 0),
(458, 458, 'Bagudo', NULL, 0),
(459, 459, 'Birnin Kebbi', NULL, 0),
(460, 460, 'Bunza', NULL, 0),
(461, 461, 'Dandi', NULL, 0),
(462, 462, 'Fakai', NULL, 0),
(463, 463, 'Gwandu', NULL, 0),
(464, 464, 'Jega', NULL, 0),
(465, 465, 'Kalgo', NULL, 0),
(466, 466, 'Koko/Besse', NULL, 0),
(467, 467, 'Maiyama', NULL, 0),
(468, 468, 'Ngaski', NULL, 0),
(469, 469, 'Sakaba', NULL, 0),
(470, 470, 'Shanga', NULL, 0),
(471, 471, 'Suru', NULL, 0),
(472, 472, 'Wasagu/Danko', NULL, 0),
(473, 473, 'Yauri', NULL, 0),
(474, 474, 'Zuru', NULL, 0),
(475, 475, 'Adavi', NULL, 0),
(476, 476, 'Ajaokuta', NULL, 0),
(477, 477, 'Ankpa', NULL, 0),
(478, 478, 'Bassa', NULL, 0),
(479, 479, 'Dekina', NULL, 0),
(480, 480, 'Ibaji', NULL, 0),
(481, 481, 'Idah', NULL, 0),
(482, 482, 'Igalamela-Odolu', NULL, 0),
(483, 483, 'Ijumu', NULL, 0),
(484, 484, 'Kabba/Bunu', NULL, 0),
(485, 485, 'Kogi', NULL, 0),
(486, 486, 'Lokoja', NULL, 0),
(487, 487, 'Mopa-Muro', NULL, 0),
(488, 488, 'Ofu', NULL, 0),
(489, 489, 'Ogori/Mangongo', NULL, 0),
(490, 490, 'Okehi', NULL, 0),
(491, 491, 'Okene', NULL, 0),
(492, 492, 'Olamabolo', NULL, 0),
(493, 493, 'Omala', NULL, 0),
(494, 494, 'Yagba East', NULL, 0),
(495, 495, 'Yagba West', NULL, 0),
(496, 496, 'Asa', NULL, 0),
(497, 497, 'Baruten', NULL, 0),
(498, 498, 'Edu', NULL, 0),
(499, 499, 'Ekiti', NULL, 0),
(500, 500, 'Ifelodun', NULL, 0),
(501, 501, 'Ilorin East', NULL, 0),
(502, 502, 'Ilorin West', NULL, 0),
(503, 503, 'Irepodun', NULL, 0),
(504, 504, 'Isin', NULL, 0),
(505, 505, 'Kaiama', NULL, 0),
(506, 506, 'Moro', NULL, 0),
(507, 507, 'Offa', NULL, 0),
(508, 508, 'Oke-Ero', NULL, 0),
(509, 509, 'Oyun', NULL, 0),
(510, 510, 'Pategi', NULL, 0),
(511, 511, 'Agege', NULL, 0),
(512, 512, 'Ajeromi-Ifelodun', NULL, 0),
(513, 513, 'Alimosho', NULL, 0),
(514, 514, 'Amuwo-Odofin', NULL, 0),
(515, 515, 'Apapa', NULL, 0),
(516, 516, 'Badagry', NULL, 0),
(517, 517, 'Epe', NULL, 0),
(518, 518, 'Eti-Osa', NULL, 0),
(519, 519, 'Ibeju/Lekki', NULL, 0),
(520, 520, 'Ifako-Ijaye', NULL, 0),
(521, 521, 'Ikeja', NULL, 0),
(522, 522, 'Ikorodu', NULL, 0),
(523, 523, 'Kosofe', NULL, 0),
(524, 524, 'Lagos Island', NULL, 0),
(525, 525, 'Lagos Mainland', NULL, 0),
(526, 526, 'Mushin', NULL, 0),
(527, 527, 'Ojo', NULL, 0),
(528, 528, 'Oshodi-Isolo', NULL, 0),
(529, 529, 'Shomolu', NULL, 0),
(530, 530, 'Surulere', NULL, 0),
(531, 531, 'Akwanga', NULL, 0),
(532, 532, 'Awe', NULL, 0),
(533, 533, 'Doma', NULL, 0),
(534, 534, 'Karu', NULL, 0),
(535, 535, 'Keana', NULL, 0),
(536, 536, 'Keffi', NULL, 0),
(537, 537, 'Kokona', NULL, 0),
(538, 538, 'Lafia', NULL, 0),
(539, 539, 'Nasarawa', NULL, 0),
(540, 540, 'Nasarawa-Eggon', NULL, 0),
(541, 541, 'Obi', NULL, 0),
(542, 542, 'Toto', NULL, 0),
(543, 543, 'Wamba', NULL, 0),
(544, 544, 'Agaie', NULL, 0),
(545, 545, 'Agwara', NULL, 0),
(546, 546, 'Bida', NULL, 0),
(547, 547, 'Borgu', NULL, 0),
(548, 548, 'Bosso', NULL, 0),
(549, 549, 'Chanchaga', NULL, 0),
(550, 550, 'Edati', NULL, 0),
(551, 551, 'Gbako', NULL, 0),
(552, 552, 'Gurara', NULL, 0),
(553, 553, 'Katcha', NULL, 0),
(554, 554, 'Kontagora', NULL, 0),
(555, 555, 'Lapai', NULL, 0),
(556, 556, 'Lavun', NULL, 0),
(557, 557, 'Magama', NULL, 0),
(558, 558, 'Mariga', NULL, 0),
(559, 559, 'Mashegu', NULL, 0),
(560, 560, 'Mokwa', NULL, 0),
(561, 561, 'Muya', NULL, 0),
(562, 562, 'Pailoro', NULL, 0),
(563, 563, 'Rafi', NULL, 0),
(564, 564, 'Rijau', NULL, 0),
(565, 565, 'Shiroro', NULL, 0),
(566, 566, 'Suleja', NULL, 0),
(567, 567, 'Tafa', NULL, 0),
(568, 568, 'Wushishi', NULL, 0),
(569, 569, 'Abeokuta North', NULL, 0),
(570, 570, 'Abeokuta South', NULL, 0),
(571, 571, 'Ado-Odo/Ota', NULL, 0),
(572, 572, 'Egbado North', NULL, 0),
(573, 573, 'Egbado South', NULL, 0),
(574, 574, 'Ewekoro', NULL, 0),
(575, 575, 'Ifo', NULL, 0),
(576, 576, 'Ijebu East', NULL, 0),
(577, 577, 'Ijebu North', NULL, 0),
(578, 578, 'Ijebu North East', NULL, 0),
(579, 579, 'Ijebu Ode', NULL, 0),
(580, 580, 'Ikenne', NULL, 0),
(581, 581, 'Imeko-Afon', NULL, 0),
(582, 582, 'Ipokia', NULL, 0),
(583, 583, 'Obafemi-Owode', NULL, 0),
(584, 584, 'Ogun Waterside', NULL, 0),
(585, 585, 'Odeda', NULL, 0),
(586, 586, 'Odogbolu', NULL, 0),
(587, 587, 'Remo North', NULL, 0),
(588, 588, 'Shagamu', NULL, 0),
(589, 589, 'Akoko North East', NULL, 0),
(590, 590, 'Akoko North West', NULL, 0),
(591, 591, 'Akoko South Akure East', NULL, 0),
(592, 592, 'Akoko South West', NULL, 0),
(593, 593, 'Akure North', NULL, 0),
(594, 594, 'Akure South', NULL, 0),
(595, 595, 'Ese-Odo', NULL, 0),
(596, 596, 'Idanre', NULL, 0),
(597, 597, 'Ifedore', NULL, 0),
(598, 598, 'Ilaje', NULL, 0),
(599, 599, 'Ile-Oluji', NULL, 0),
(600, 600, 'Okeigbo', NULL, 0),
(601, 601, 'Irele', NULL, 0),
(602, 602, 'Odigbo', NULL, 0),
(603, 603, 'Okitipupa', NULL, 0),
(604, 604, 'Ondo East', NULL, 0),
(605, 605, 'Ondo West', NULL, 0),
(606, 606, 'Ose', NULL, 0),
(607, 607, 'Owo', NULL, 0),
(608, 608, 'Aiyedade', NULL, 0),
(609, 609, 'Aiyedire', NULL, 0),
(610, 610, 'Atakumosa East', NULL, 0),
(611, 611, 'Atakumosa West', NULL, 0),
(612, 612, 'Boluwaduro', NULL, 0),
(613, 613, 'Boripe', NULL, 0),
(614, 614, 'Ede North', NULL, 0),
(615, 615, 'Ede South', NULL, 0),
(616, 616, 'Egbedore', NULL, 0),
(617, 617, 'Ejigbo', NULL, 0),
(618, 618, 'Ife Central', NULL, 0),
(619, 619, 'Ife East', NULL, 0),
(620, 620, 'Ife North', NULL, 0),
(621, 621, 'Ife South', NULL, 0),
(622, 622, 'Ifedayo', NULL, 0),
(623, 623, 'Ifelodun', NULL, 0),
(624, 624, 'Ila', NULL, 0),
(625, 625, 'Ilesha East', NULL, 0),
(626, 626, 'Ilesha West', NULL, 0),
(627, 627, 'Irepodun', NULL, 0),
(628, 628, 'Irewole', NULL, 0),
(629, 629, 'Isokan', NULL, 0),
(630, 630, 'Iwo', NULL, 0),
(631, 631, 'Obokun', NULL, 0),
(632, 632, 'Odo-Otin', NULL, 0),
(633, 633, 'Ola-Oluwa', NULL, 0),
(634, 634, 'Olorunda', NULL, 0),
(635, 635, 'Oriade', NULL, 0),
(636, 636, 'Orolu', NULL, 0),
(637, 637, 'Osogbo', NULL, 0),
(638, 638, 'Afijio', NULL, 0),
(639, 639, 'Akinyele', NULL, 0),
(640, 640, 'Atiba', NULL, 0),
(641, 641, 'Atigbo', NULL, 0),
(642, 642, 'Egbeda', NULL, 0),
(643, 643, 'IbadanCentral', NULL, 0),
(644, 644, 'Ibadan North', NULL, 0),
(645, 645, 'Ibadan North West', NULL, 0),
(646, 646, 'Ibadan South East', NULL, 0),
(647, 647, 'Ibadan South West', NULL, 0),
(648, 648, 'Ibarapa Central', NULL, 0),
(649, 649, 'Ibarapa East', NULL, 0),
(650, 650, 'Ibarapa North', NULL, 0),
(651, 651, 'Ido', NULL, 0),
(652, 652, 'Irepo', NULL, 0),
(653, 653, 'Iseyin', NULL, 0),
(654, 654, 'Itesiwaju', NULL, 0),
(655, 655, 'Iwajowa', NULL, 0),
(656, 656, 'Kajola', NULL, 0),
(657, 657, 'Lagelu Ogbomosho North', NULL, 0),
(658, 658, 'Ogbmosho South', NULL, 0),
(659, 659, 'Ogo Oluwa', NULL, 0),
(660, 660, 'Olorunsogo', NULL, 0),
(661, 661, 'Oluyole', NULL, 0),
(662, 662, 'Ona-Ara', NULL, 0),
(663, 663, 'Orelope', NULL, 0),
(664, 664, 'Ori Ire', NULL, 0),
(665, 665, 'Oyo East', NULL, 0),
(666, 666, 'Oyo West', NULL, 0),
(667, 667, 'Saki East', NULL, 0),
(668, 668, 'Saki West', NULL, 0),
(669, 669, 'Surulere', NULL, 0),
(670, 670, 'Barikin Ladi', NULL, 0),
(671, 671, 'Bassa', NULL, 0),
(672, 672, 'Bokkos', NULL, 0),
(673, 673, 'Jos East', NULL, 0),
(674, 674, 'Jos North', NULL, 0),
(675, 675, 'Jos South', NULL, 0),
(676, 676, 'Kanam', NULL, 0),
(677, 677, 'Kanke', NULL, 0),
(678, 678, 'Langtang North', NULL, 0),
(679, 679, 'Langtang South', NULL, 0),
(680, 680, 'Mangu', NULL, 0),
(681, 681, 'Mikang', NULL, 0),
(682, 682, 'Pankshin', NULL, 0),
(683, 683, 'Qua''an Pan', NULL, 0),
(684, 684, 'Riyom', NULL, 0),
(685, 685, 'Shendam', NULL, 0),
(686, 686, 'Wase', NULL, 0),
(687, 687, 'Abua/Odual', NULL, 0),
(688, 688, 'Ahoada East', NULL, 0),
(689, 689, 'Ahoada West', NULL, 0),
(690, 690, 'Akuku Toru', NULL, 0),
(691, 691, 'Andoni', NULL, 0),
(692, 692, 'Asari-Toru', NULL, 0),
(693, 693, 'Bonny', NULL, 0),
(694, 694, 'Degema', NULL, 0),
(695, 695, 'Emohua', NULL, 0),
(696, 696, 'Eleme', NULL, 0),
(697, 697, 'Etche', NULL, 0),
(698, 698, 'Gokana', NULL, 0),
(699, 699, 'Ikwerre', NULL, 0),
(700, 700, 'Khana', NULL, 0),
(701, 701, 'Obia/Akpor', NULL, 0),
(702, 702, 'Ogba/Egbema/Ndoni', NULL, 0),
(703, 703, 'Ogu/Bolo', NULL, 0),
(704, 704, 'Okrika', NULL, 0),
(705, 705, 'Omumma', NULL, 0),
(706, 706, 'Opobo/Nkoro', NULL, 0),
(707, 707, 'Oyigbo', NULL, 0),
(708, 708, 'Port-Harcourt', NULL, 0),
(709, 709, 'Tai', NULL, 0),
(710, 710, 'Binji', NULL, 0),
(711, 711, 'Bodinga', NULL, 0),
(712, 712, 'Dange-shnsi', NULL, 0),
(713, 713, 'Gada', NULL, 0),
(714, 714, 'Goronyo', NULL, 0),
(715, 715, 'Gudu', NULL, 0),
(716, 716, 'Gawabawa', NULL, 0),
(717, 717, 'Illela', NULL, 0),
(718, 718, 'Isa', NULL, 0),
(719, 719, 'Kware', NULL, 0),
(720, 720, 'kebbe', NULL, 0),
(721, 721, 'Rabah', NULL, 0),
(722, 722, 'Sabon Birni', NULL, 0),
(723, 723, 'Shagari', NULL, 0),
(724, 724, 'Silame', NULL, 0),
(725, 725, 'Sokoto North', NULL, 0),
(726, 726, 'Sokoto South', NULL, 0),
(727, 727, 'Tambuwal', NULL, 0),
(728, 728, 'Tqngaza', NULL, 0),
(729, 729, 'Tureta', NULL, 0),
(730, 730, 'Wamako', NULL, 0),
(731, 731, 'Wurno', NULL, 0),
(732, 732, 'Yabo', NULL, 0),
(733, 733, 'Ardo-kola', NULL, 0),
(734, 734, 'Bali', NULL, 0),
(735, 735, 'Donga', NULL, 0),
(736, 736, 'Gashaka', NULL, 0),
(737, 737, 'Cassol', NULL, 0),
(738, 738, 'Ibi', NULL, 0),
(739, 739, 'Jalingo', NULL, 0),
(740, 740, 'Karin-Lamido', NULL, 0),
(741, 741, 'Kurmi', NULL, 0),
(742, 742, 'Lau', NULL, 0),
(743, 743, 'Sardauna', NULL, 0),
(744, 744, 'Takum', NULL, 0),
(745, 745, 'Ussa', NULL, 0),
(746, 746, 'Wukari', NULL, 0),
(747, 747, 'Yorro', NULL, 0),
(748, 748, 'Zing', NULL, 0),
(749, 749, 'Ardo-kola', NULL, 0),
(750, 750, 'Bali', NULL, 0),
(751, 751, 'Donga', NULL, 0),
(752, 752, 'Gashaka', NULL, 0),
(753, 753, 'Cassol', NULL, 0),
(754, 754, 'Ibi', NULL, 0),
(755, 755, 'Jalingo', NULL, 0),
(756, 756, 'Karin-Lamido', NULL, 0),
(757, 757, 'Kurmi', NULL, 0),
(758, 758, 'Lau', NULL, 0),
(759, 759, 'Sardauna', NULL, 0),
(760, 760, 'Takum', NULL, 0),
(761, 761, 'Ussa', NULL, 0),
(762, 762, 'Wukari', NULL, 0),
(763, 763, 'Yorro', NULL, 0),
(764, 764, 'Zing', NULL, 0),
(765, 765, 'Anka', NULL, 0),
(766, 766, 'Bakura', NULL, 0),
(767, 767, 'Birnin Magaji', NULL, 0),
(768, 768, 'Bukkuyum', NULL, 0),
(769, 769, 'Bungudu', NULL, 0),
(770, 770, 'Gummi', NULL, 0),
(771, 771, 'Gusau', NULL, 0),
(772, 772, 'Kaura', NULL, 0),
(773, 773, 'Namoda', NULL, 0),
(774, 774, 'Maradun', NULL, 0),
(775, 775, 'Maru', NULL, 0),
(776, 776, 'Shinkafi', NULL, 0),
(777, 777, 'Talata Mafara', NULL, 0),
(778, 778, 'Tsafe', NULL, 0),
(779, 779, 'Zurmi', NULL, 0),
(781, 1, 'Southern Jaw', NULL, 0),
(784, 1, 'Ushongo', NULL, 0),
(805, 1, 'Victoria Island', NULL, 0),
(806, 519, 'Alpha Beach Road', NULL, 0),
(807, 1, 'Alpha Beach Road', NULL, 0),
(808, 515, 'Lombra', NULL, 0),
(809, 23, 'lombra1', NULL, 0),
(810, 195, 'Kolum', NULL, 0),
(811, 23, 'Kolum', NULL, 0),
(813, 1, 'Ikeja', NULL, 0),
(858, 1, 'Ort', NULL, 0),
(859, 23, 'Ort', NULL, 0),
(860, 518, 'Ajah', NULL, 0),
(861, 518, 'Lekki', NULL, 0),
(862, 511, 'Agbado', NULL, 0),
(863, 23, 'Navy Town', NULL, 0),
(864, 23, 'Alakija', NULL, 0),
(865, 23, 'Aguda', NULL, 0),
(866, 23, 'Orile Iganmu', NULL, 0),
(867, 23, 'Coker Bridge', NULL, 0),
(868, 23, 'Isa Oroki', NULL, 0),
(869, 530, 'Orile Iganmu', NULL, 0),
(870, 530, 'Aguda', NULL, 0),
(871, 23, 'Ajeromi-Ifelodun', NULL, 0),
(872, 23, 'Pako, Aguda', NULL, 0),
(873, 23, 'Surulere', NULL, 0),
(874, 23, 'Lekki', NULL, 0),
(875, 23, 'Ajah', NULL, 0),
(876, 23, 'Ikorodu', NULL, 0),
(877, 23, 'Amuwo Odofin', NULL, 0),
(878, 23, 'Ifako', NULL, 0),
(879, 23, 'Kosofe ', NULL, 0),
(880, 23, 'Agege', NULL, 0),
(881, 23, 'Badagry', NULL, 0),
(882, 23, 'Alapere', NULL, 0),
(883, 23, 'Oworonsoki', NULL, 0),
(884, 23, 'Ketu', NULL, 0),
(885, 530, 'Ijesha', NULL, 0),
(886, 23, 'Epe', NULL, 0),
(887, 522, 'Cele Bus Stop', NULL, 0),
(888, 23, 'Festac Town', NULL, 0),
(889, 522, 'itamaga', NULL, 0),
(890, 23, ' Lekki', NULL, 0),
(891, 23, 'Ibeju Lekki', NULL, 0),
(892, 23, 'Ogba', NULL, 0),
(893, 522, 'Ota Ona', NULL, 0),
(894, 23, 'Ajegunle', NULL, 0),
(895, 23, 'Ikosi, Ketu', NULL, 0),
(896, 23, 'Ogudu', NULL, 0),
(897, 511, 'Dopemu', NULL, 0),
(898, 23, 'Badore', NULL, 0),
(899, 23, 'Ojodu', NULL, 0),
(900, 23, 'Alakuko Ifako', NULL, 0),
(901, 530, 'Ikate', NULL, 0),
(902, 23, 'Iju Ishaga', NULL, 0),
(903, 23, 'Ojokoro', NULL, 0),
(904, 530, 'Ijeshatedo', NULL, 0),
(905, 23, 'Opebi', NULL, 0),
(906, 23, 'Gbagada', NULL, 0),
(907, 522, 'Igbogbo', NULL, 0),
(908, 23, 'Isheri', NULL, 0),
(909, 522, 'Igbogbo Bayeku', NULL, 0),
(910, 530, 'Masha', NULL, 0),
(911, 23, 'Ojota', NULL, 0),
(913, 800, 'Ikoyi', NULL, 0),
(914, 813, 'Chevron', NULL, 0),
(915, 811, 'Victoria Garden City', NULL, 0),
(916, 812, 'Oshodi', NULL, 0),
(917, 810, 'Isolo', NULL, 0),
(918, 811, 'VGC', NULL, 0),
(919, 799, 'Victoria Island', NULL, 0),
(920, 799, 'VI', NULL, 0),
(921, 23, 'Alimosho', NULL, 0),
(922, 23, 'Akowonjo', NULL, 0),
(923, 23, 'Igbogbo', NULL, 0),
(924, 23, 'Ibereko', NULL, 0),
(925, 511, 'erfghjn', NULL, 0),
(926, 518, 'Jakande', NULL, 0),
(927, 515, 'Ajegunle', NULL, 0),
(928, 523, 'Ikosi', NULL, 0),
(929, 514, 'Amuwo Odofin', NULL, 0),
(930, 530, 'Orile', NULL, 0),
(931, 520, 'Ifako', NULL, 0),
(932, 514, 'Festac Town', NULL, 0),
(933, 520, 'Alakuko Ifako', NULL, 0),
(934, 514, 'Badagry', NULL, 0),
(935, 530, 'Coker Bridge', NULL, 0),
(936, 530, 'Off Yisa Oroki Street', NULL, 0),
(937, 518, 'Ibeju Lekki', NULL, 0),
(938, 522, 'Ojodu', NULL, 0),
(939, 518, 'Onikan', NULL, 0),
(940, 514, 'Alakija', NULL, 0),
(941, 518, 'Badore', NULL, 0),
(942, 530, 'Alakija', NULL, 0),
(943, 530, 'Itire', NULL, 0),
(944, 518, ' Lekki', NULL, 0),
(945, 530, 'Pako, Aguda', NULL, 0),
(946, 514, 'Navy Town', NULL, 0),
(947, 530, 'Ajeromi-Ifelodun', NULL, 0),
(948, 518, 'Epe', NULL, 0),
(949, 530, 'Iju Ishaga', NULL, 0),
(950, 23, 'test', NULL, 0),
(951, 23, 'gra', NULL, 0),
(952, 1, 'Lekki', NULL, 0),
(953, 511, 'Alagbado', NULL, 0),
(954, 528, 'Ajao Estate', NULL, 0),
(956, 530, 'Orile-Iganmu', NULL, 0),
(957, 804, 'Ebute Metta', NULL, 0),
(958, 23, 'Mowe', NULL, 0),
(959, 23, 'lagos', NULL, 0),
(960, 23, 'shomolu', NULL, 0),
(961, 2, '22222', NULL, 0),
(962, 1, '22222', NULL, 1),
(963, 1, 'new City1', NULL, 1),
(964, 821, 'first', NULL, 0),
(965, 821, 'second', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `description`, `deleted`) VALUES
(1, 'Nigeria', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE IF NOT EXISTS `donor` (
  `id` int(11) NOT NULL,
  `donor_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `title` tinyint(1) DEFAULT NULL,
  `other_title` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0',
  `date_birth` int(11) DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` double(11,0) DEFAULT NULL,
  `phone` double(11,0) DEFAULT NULL,
  `type_donor` tinyint(1) DEFAULT NULL,
  `blood_group` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `hiv` tinyint(1) DEFAULT NULL,
  `hbsag` tinyint(1) DEFAULT NULL,
  `syphillis` tinyint(1) DEFAULT NULL,
  `guestionnaire_complited` tinyint(1) DEFAULT NULL,
  `guestionnaire_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `health_status` tinyint(1) DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disable` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `hcv` tinyint(1) DEFAULT NULL,
  `date_guestionnaire` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL,
  `row_id` int(11) NOT NULL,
  `table` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `table` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `row` int(11) NOT NULL,
  `status` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `coll` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `date`, `created_by`, `table`, `row`, `status`, `coll`, `last_state`, `current_state`, `deleted`) VALUES
(1, 1475770266, 1, 'User', 2, 'create', NULL, NULL, NULL, 0),
(2, 1475770466, 1, 'User', 3, 'create', NULL, NULL, NULL, 0),
(3, 1475834735, 1, 'User', 2, 'disable', NULL, NULL, NULL, 0),
(4, 1475834738, 1, 'User', 2, 'disable', NULL, NULL, NULL, 0),
(5, 1475837144, 1, 'User', 4, 'create', NULL, NULL, NULL, 0),
(6, 1475837605, 1, 'User', 5, 'create', NULL, NULL, NULL, 0),
(7, 1475838267, 1, 'User', 6, 'create', NULL, NULL, NULL, 0),
(8, 1475838273, 1, 'User', 7, 'create', NULL, NULL, NULL, 0),
(9, 1475839259, 1, 'User', 8, 'create', NULL, NULL, NULL, 0),
(10, 1475839326, 1, 'User', 8, 'update', NULL, NULL, NULL, 0),
(11, 1475839738, 1, 'User', 8, 'update', NULL, NULL, NULL, 0),
(12, 1475841415, 1, 'User', 9, 'create', NULL, NULL, NULL, 0),
(13, 1475846247, 1, 'State', 1, 'update', NULL, NULL, NULL, 0),
(14, 1475846278, 1, 'State', 44, 'create', NULL, NULL, NULL, 0),
(15, 1475846284, 1, 'State', 44, 'update', NULL, NULL, NULL, 0),
(16, 1475846373, 1, 'State', 44, 'archived', NULL, NULL, NULL, 0),
(17, 1475846503, 1, 'Area', 818, 'create', NULL, NULL, NULL, 0),
(18, 1475846531, 1, 'Area', 819, 'create', NULL, NULL, NULL, 0),
(19, 1475846565, 1, 'Area', 819, 'archived', NULL, NULL, NULL, 0),
(20, 1475846566, 1, 'Area', 818, 'archived', NULL, NULL, NULL, 0),
(21, 1475846676, 1, 'Area', 820, 'create', NULL, NULL, NULL, 0),
(22, 1475846926, 1, 'State', 45, 'create', NULL, NULL, NULL, 0),
(23, 1475846934, 1, 'State', 45, 'update', NULL, NULL, NULL, 0),
(24, 1475846972, 1, 'Area', 821, 'create', NULL, NULL, NULL, 0),
(25, 1475846986, 1, 'Area', 822, 'create', NULL, NULL, NULL, 0),
(26, 1475846994, 1, 'Area', 821, 'update', NULL, NULL, NULL, 0),
(27, 1475846995, 1, 'Area', 822, 'update', NULL, NULL, NULL, 0),
(28, 1475847035, 1, 'City', 964, 'create', NULL, NULL, NULL, 0),
(29, 1475847044, 1, 'City', 965, 'create', NULL, NULL, NULL, 0),
(30, 1475850063, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(31, 1475850069, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(32, 1475851191, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(33, 1475851198, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(34, 1475852165, 1, 'Location', 2, 'create', NULL, NULL, NULL, 0),
(35, 1475853152, 1, 'User', 10, 'create', NULL, NULL, NULL, 0),
(36, 1475853225, 1, 'User', 10, 'update', NULL, NULL, NULL, 0),
(37, 1475853401, 1, 'User', 10, 'update', NULL, NULL, NULL, 0),
(38, 1475853513, 1, 'User', 10, 'update', NULL, NULL, NULL, 0),
(39, 1475853598, 1, 'Location', 1, 'archived', NULL, NULL, NULL, 0),
(40, 1475855775, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(41, 1475855776, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(42, 1475855784, 1, 'User', 10, 'disable', NULL, NULL, NULL, 0),
(43, 1475855785, 1, 'User', 10, 'disable', NULL, NULL, NULL, 0),
(44, 1475855815, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(45, 1475855817, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(46, 1475855818, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(47, 1475855819, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(48, 1475855821, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(49, 1475855821, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(50, 1475855822, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(51, 1475855823, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(52, 1475855823, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(53, 1475855861, 1, 'User', 10, 'disable', NULL, NULL, NULL, 0),
(54, 1475855863, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(55, 1475855864, 1, 'User', 7, 'disable', NULL, NULL, NULL, 0),
(56, 1475855864, 1, 'User', 10, 'disable', NULL, NULL, NULL, 0),
(57, 1475855881, 1, 'Location', 2, 'update', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `city_id`, `name`, `address`, `deleted`) VALUES
(1, 1, 'loc_1', 'erhxtyuio', 1),
(2, 965, 'qwerty 1', 'str. F 22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1474838741),
('m130524_201442_init', 1474838746),
('m160925_184556_donor', 1474838746),
('m160925_184618_donor_data', 1474838746),
('m160925_184641_health_info', 1474838746),
('m160925_184707_blood_drive', 1474838746),
('m160925_190457_history', 1474838747),
('m160925_203416_location', 1474838747);

-- --------------------------------------------------------

--
-- Table structure for table `question_category`
--

CREATE TABLE IF NOT EXISTS `question_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted` tinyint(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_category`
--

INSERT INTO `question_category` (`id`, `name`, `created_by`, `deleted`) VALUES
(1, 'Begin Questions', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question_title`
--

CREATE TABLE IF NOT EXISTS `question_title` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `condition` varchar(255) DEFAULT NULL,
  `previous` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_title`
--

INSERT INTO `question_title` (`id`, `category_id`, `label`, `type`, `condition`, `previous`, `deleted`) VALUES
(1, 1, 'Have you ever donated blood before?', 2, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire`
--

CREATE TABLE IF NOT EXISTS `questionnaire` (
  `id` int(11) NOT NULL,
  `questionnaire_number` varchar(255) NOT NULL,
  `created_by` tinyint(11) NOT NULL,
  `date` int(11) NOT NULL,
  `donor_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire_answer`
--

CREATE TABLE IF NOT EXISTS `questionnaire_answer` (
  `id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `capital_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `country_id`, `name`, `description`, `deleted`, `capital_id`) VALUES
(1, 1, 'Adamawa', NULL, 0, 0),
(2, 1, 'Akwa Ibom', NULL, 0, 0),
(3, 1, 'Anambra', NULL, 0, 0),
(4, 1, 'Bauchi', NULL, 0, 0),
(5, 1, 'Bayelsa', NULL, 0, 0),
(6, 1, 'Benue', NULL, 0, 0),
(7, 1, 'Bornu', NULL, 0, 0),
(8, 1, 'Cross River', NULL, 0, 0),
(9, 1, 'Delta', NULL, 0, 0),
(10, 1, 'Ebonyi', NULL, 0, 0),
(11, 1, 'Edo', NULL, 0, 0),
(12, 1, 'Ekiti', NULL, 0, 0),
(13, 1, 'Enugu', NULL, 0, 0),
(14, 1, 'Gombe', NULL, 0, 0),
(15, 1, 'Imo', NULL, 0, 0),
(16, 1, 'Jigawa', NULL, 0, 0),
(17, 1, 'Kaduna', NULL, 0, 0),
(18, 1, 'Kano', NULL, 0, 0),
(19, 1, 'Katsina', NULL, 0, 0),
(20, 1, 'Kebbi', NULL, 0, 0),
(21, 1, 'Kogi', NULL, 0, 0),
(22, 1, 'Kwara', NULL, 0, 0),
(23, 1, 'Lagos', NULL, 0, 0),
(24, 1, 'Nasarawa', NULL, 0, 0),
(25, 1, 'Niger', NULL, 0, 0),
(26, 1, 'Ogun', NULL, 0, 0),
(27, 1, 'Ondo', NULL, 0, 0),
(28, 1, 'Osun', NULL, 0, 0),
(29, 1, 'Oyo', NULL, 0, 0),
(30, 1, 'Plateau', NULL, 0, 0),
(31, 1, 'Rivers', NULL, 0, 0),
(32, 1, 'Sokoto', NULL, 0, 0),
(33, 1, 'Taraba', NULL, 0, 0),
(34, 1, 'Yobe', NULL, 0, 0),
(35, 1, 'Zamfara', NULL, 0, 0),
(36, 1, 'new stae', NULL, 1, 0),
(37, 1, 'sdfsdf', NULL, 1, 0),
(38, 1, 'sdfsdf', NULL, 1, 0),
(39, 1, 'sdfsdf', NULL, 1, 0),
(44, 1, 'Test', NULL, 1, 0),
(45, 1, 'Testing', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` double(11,0) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '0',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `created_by`, `username`, `phone`, `email`, `location_id`, `auth_key`, `password_hash`, `password_reset_token`, `status`, `disable`, `deleted`) VALUES
(1, NULL, 'admin', 11111111111, 'tkalenko.in@mail.ru', NULL, 'FE7Ngv4RItMWHoOI-0E5zlSIWdw27BYn', '$2y$13$n5CUpWX6AZzP7Tak6X9Ipuk87coFA6KW0jDVlS70a8mGUMgq5V08e', NULL, 0, 0, 0),
(7, 1, 'staff_inna', 1111112222, 'sccds@s.com', 1, 'Mst32GCqwFx9CW0FPfoE-vgI6aMDMD95', '$2y$13$0v.YRhzUPwffgCai80W3xuitf1e2w0qR8FDa1PtaHdqt2KtKnd/ka', NULL, 0, 0, 0),
(8, 1, 'Test three 3', 4444444444, 'ladatkodarija@rambler.ru', NULL, 'bw0hzogviIADFjvkSW-1-jjGMA7tGPhA', '$2y$13$6bFzDey/QoBSsF1s8h4BiOpkoItVGYEHmhH4UqZEsf.rxIdXlPxrO', NULL, 0, 0, 0),
(9, 1, 'test empty', 5555555555, 'jjjj@eee.rrr', NULL, 'Tq_j28MTybjUpks2YWVNt5qbxSPN83-C', '$2y$13$zjKWpaQBDrZJTGAX1Lxgx.flo9k4SxEapQDLbVj2xaLOuvQ1TCFMe', NULL, 0, 0, 0),
(10, 1, 'Test staff', 4444444444, 'ladatkodarijaqqqq@rambler.ru', 2, 'AfSSYPRwgfDj_EisTpRBKnTTF68mhgR3', '$2y$13$vpxrKD13aeA0IBKqI5UtHusJysiPKWpB8BNw5OOXE9n7C43YrdWW6', NULL, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nameIndex` (`name`) USING BTREE,
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_name` (`item_name`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `blood_drive`
--
ALTER TABLE `blood_drive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `blood_drive_ibfk_4` (`donor_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nameIndex` (`name`) USING BTREE,
  ADD KEY `area_id` (`area_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nameIndex` (`name`) USING BTREE;

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `creator_id` (`created_by`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `row_id` (`row_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `question_category`
--
ALTER TABLE `question_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `question_title`
--
ALTER TABLE `question_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `previous` (`previous`);

--
-- Indexes for table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donor_id` (`donor_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `questionnaire_answer`
--
ALTER TABLE `questionnaire_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questionnaire_id` (`questionnaire_id`),
  ADD KEY `title_id` (`title_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nameIndex` (`name`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `location_id` (`location_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=823;
--
-- AUTO_INCREMENT for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `blood_drive`
--
ALTER TABLE `blood_drive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=966;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `donor`
--
ALTER TABLE `donor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `question_category`
--
ALTER TABLE `question_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `question_title`
--
ALTER TABLE `question_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questionnaire_answer`
--
ALTER TABLE `questionnaire_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `area_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `auth_assignment_ibfk_3` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blood_drive`
--
ALTER TABLE `blood_drive`
  ADD CONSTRAINT `blood_drive_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `blood_drive_ibfk_4` FOREIGN KEY (`donor_id`) REFERENCES `donor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `blood_drive_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `donor`
--
ALTER TABLE `donor`
  ADD CONSTRAINT `donor_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `donor_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `location_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `question_category`
--
ALTER TABLE `question_category`
  ADD CONSTRAINT `question_category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `question_title`
--
ALTER TABLE `question_title`
  ADD CONSTRAINT `question_title_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `question_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD CONSTRAINT `questionnaire_ibfk_1` FOREIGN KEY (`donor_id`) REFERENCES `donor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questionnaire_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questionnaire_answer`
--
ALTER TABLE `questionnaire_answer`
  ADD CONSTRAINT `questionnaire_answer_ibfk_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questionnaire_answer_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `question_title` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questionnaire_answer_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `question_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

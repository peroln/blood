<?php
/* @var $model common\models\search\BloodDriveSearch */
/* @var $result common\models\BloodDrive */
?>
<div>
    <h1>Report</h1>
    <?php
    if (($model->date_from && $model->date_to) || $model->search_gender || $model->area_id) {
        ?>
        <h4> Search criteria</h4>
        <?php
        if ($model->date_from && $model->date_to) {
            ?>
            <p> Data Range: <?= $model->date_from ?>
                - <?= $model->date_to ?></p>
            <?php
        }
        if ($model->search_gender == 1 || $model->search_gender == 2) {
            ?>
            <p> Gender:<?= $model->search_gender == 1 ? "MALE" : "Female" ?></p>
            <?php
        }
        if ($model->area_name) {
            ?>
            <p> Area: <?= $model->area_name ?></p>
            <?php
        }
    }
    ?>

    <h3> Blood Drives</h3>

    <table>
        <thead>
        <tr>
            <th>№</th>
            <th>Drive</th>
            <th>Donor info</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $key=>$result) {
            ?>
            <tr>
                <td>
                    <?= $key + 1 ?>
                </td>
                <td>
                    <?= $result->blood_bag_number ?>
                </td>
                <td>
                    <p> Name: <?= $result->donor->first_name . ' ' . $result->donor->last_name ?> </p>
                    <p> Email: <?= $result->donor->email ?></p>
                    <p> Contact: <?= $result->donor->phone ?></p>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
<?php
?>

    <h1 class="donor-title"> Donor <?= $model->donor->first_name . ' ' . $model->donor->last_name ?></h1>
    <p> Questionnaire number: <?= $model->questionnaire_number ?></p>
    <p> Date: <?= date("d-m-Y", $model->date) ?></p>
    <p> Location: <?= $model->location->name ?></p>

<?php $gender = $model->donor->gender ?>
<?php
if ($answers) {
    $n = 1;
    foreach ($answers as $i => $one_category) {?>


        <?php
        $t = 0;
        foreach ($one_category['titles'] as $j => $one_title) {
            if (($gender == $one_title['condition']) || ($one_title['condition'] == null)) {
                if(!$t){?>
                <h3 class="category-title"><?= $n . '. ' . $one_category['name'] ?></h3>

                    <?php $t++; $n++;} ?>
                <div class="category-block">
                    <div class="label-title">  <?= $j + 1 . '. ' . $one_title['label'] ?> </div>
                    <div class="answer-title">  <?= $one_title['answer'] == 'false' ? 'no' : 'yes' ?> </div>
                </div>
                <?php
            }
        }
    }

}
?>
/*
Navicat MySQL Data Transfer

Source Server         : home
Source Server Version : 50541
Source Host           : localhost:3306
Source Database       : life-blood

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2016-10-06 18:25:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `popularity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nameIndex` (`name`) USING BTREE,
  KEY `state_id` (`state_id`),
  CONSTRAINT `area_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=818 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', '1', 'Yola', null, '0', '0');
INSERT INTO `area` VALUES ('2', '2', 'Uyo', null, '0', '0');
INSERT INTO `area` VALUES ('3', '3', 'Awka', null, '0', '0');
INSERT INTO `area` VALUES ('4', '4', 'Bauchi', null, '0', '0');
INSERT INTO `area` VALUES ('5', '5', 'Yenagoa', null, '0', '0');
INSERT INTO `area` VALUES ('6', '6', 'Makurdi', null, '0', '0');
INSERT INTO `area` VALUES ('7', '7', 'Maiduguri', null, '0', '0');
INSERT INTO `area` VALUES ('8', '8', 'Calabar', null, '0', '0');
INSERT INTO `area` VALUES ('9', '9', 'Asaba', null, '0', '0');
INSERT INTO `area` VALUES ('10', '10', 'Abakaliki', null, '0', '0');
INSERT INTO `area` VALUES ('11', '11', 'Benin City', null, '0', '0');
INSERT INTO `area` VALUES ('12', '12', 'Ado Ekiti', null, '0', '0');
INSERT INTO `area` VALUES ('13', '13', 'Enugu', null, '0', '0');
INSERT INTO `area` VALUES ('14', '14', 'Gombe', null, '0', '0');
INSERT INTO `area` VALUES ('15', '15', 'Owerri', null, '0', '0');
INSERT INTO `area` VALUES ('16', '16', 'Dutse', null, '0', '0');
INSERT INTO `area` VALUES ('17', '17', 'Kaduna', null, '0', '0');
INSERT INTO `area` VALUES ('18', '18', 'Kano', null, '0', '0');
INSERT INTO `area` VALUES ('19', '19', 'Katsina', null, '0', '0');
INSERT INTO `area` VALUES ('20', '20', 'Birnin Kebbi', null, '0', '0');
INSERT INTO `area` VALUES ('21', '21', 'Lokoja', null, '0', '0');
INSERT INTO `area` VALUES ('22', '22', 'Ilorin', null, '0', '0');
INSERT INTO `area` VALUES ('23', '23', 'Ikeja', null, '0', '0');
INSERT INTO `area` VALUES ('24', '24', 'Lafia', null, '0', '0');
INSERT INTO `area` VALUES ('25', '25', 'Minna', null, '0', '0');
INSERT INTO `area` VALUES ('26', '26', 'Abeokuta', null, '0', '0');
INSERT INTO `area` VALUES ('27', '27', 'Akure', null, '0', '0');
INSERT INTO `area` VALUES ('28', '28', 'Oshogbo', null, '0', '0');
INSERT INTO `area` VALUES ('29', '29', 'Ibadan', null, '0', '0');
INSERT INTO `area` VALUES ('30', '30', 'Jos', null, '0', '0');
INSERT INTO `area` VALUES ('31', '31', 'Port Harcourt', null, '0', '0');
INSERT INTO `area` VALUES ('32', '32', 'Sokoto', null, '0', '0');
INSERT INTO `area` VALUES ('33', '33', 'Jalingo', null, '0', '0');
INSERT INTO `area` VALUES ('34', '34', 'Damaturu', null, '0', '0');
INSERT INTO `area` VALUES ('35', '35', 'Gusau', null, '0', '0');
INSERT INTO `area` VALUES ('36', '1', 'Demsa', null, '0', '0');
INSERT INTO `area` VALUES ('37', '1', 'Fufore', null, '0', '0');
INSERT INTO `area` VALUES ('38', '1', 'Ganaye', null, '0', '0');
INSERT INTO `area` VALUES ('39', '1', 'Gireri', null, '0', '0');
INSERT INTO `area` VALUES ('40', '1', 'Gombi', null, '0', '0');
INSERT INTO `area` VALUES ('41', '1', 'Guyuk', null, '0', '0');
INSERT INTO `area` VALUES ('42', '1', 'Hong', null, '0', '0');
INSERT INTO `area` VALUES ('43', '1', 'Jada', null, '0', '0');
INSERT INTO `area` VALUES ('44', '1', 'Lamurde', null, '0', '0');
INSERT INTO `area` VALUES ('45', '1', 'Madagali', null, '0', '0');
INSERT INTO `area` VALUES ('46', '1', 'Maiha', null, '0', '0');
INSERT INTO `area` VALUES ('47', '1', 'Mayo-Belwa', null, '0', '0');
INSERT INTO `area` VALUES ('48', '1', 'Michika', null, '0', '0');
INSERT INTO `area` VALUES ('49', '1', 'Mubi North', null, '0', '0');
INSERT INTO `area` VALUES ('50', '1', 'Mubi South', null, '0', '0');
INSERT INTO `area` VALUES ('51', '1', 'Numan', null, '0', '0');
INSERT INTO `area` VALUES ('52', '1', 'Shelleng', null, '0', '0');
INSERT INTO `area` VALUES ('53', '1', 'Song', null, '0', '0');
INSERT INTO `area` VALUES ('54', '1', 'Toungo', null, '0', '0');
INSERT INTO `area` VALUES ('55', '1', 'Yola North', null, '0', '0');
INSERT INTO `area` VALUES ('56', '1', 'Yola South', null, '0', '0');
INSERT INTO `area` VALUES ('57', '2', 'Abak', null, '0', '0');
INSERT INTO `area` VALUES ('58', '2', 'Eastern Obolo', null, '0', '0');
INSERT INTO `area` VALUES ('59', '2', 'Eket', null, '0', '0');
INSERT INTO `area` VALUES ('60', '2', 'Esit Eket', null, '0', '0');
INSERT INTO `area` VALUES ('61', '2', 'Essien Udim', null, '0', '0');
INSERT INTO `area` VALUES ('62', '2', 'Etim Ekpo', null, '0', '0');
INSERT INTO `area` VALUES ('63', '2', 'Etinan', null, '0', '0');
INSERT INTO `area` VALUES ('64', '2', 'Ibeno', null, '0', '0');
INSERT INTO `area` VALUES ('65', '2', 'Ibesikpo Asutan', null, '0', '0');
INSERT INTO `area` VALUES ('66', '2', 'Ibiono Ibom', null, '0', '0');
INSERT INTO `area` VALUES ('67', '2', 'Ika', null, '0', '0');
INSERT INTO `area` VALUES ('68', '2', 'Ikono', null, '0', '0');
INSERT INTO `area` VALUES ('69', '2', 'Ikot Abasi', null, '0', '0');
INSERT INTO `area` VALUES ('70', '2', 'Ikot Ekpene', null, '0', '0');
INSERT INTO `area` VALUES ('71', '2', 'Ini', null, '0', '0');
INSERT INTO `area` VALUES ('72', '2', 'Itu', null, '0', '0');
INSERT INTO `area` VALUES ('73', '2', 'Mbo', null, '0', '0');
INSERT INTO `area` VALUES ('74', '2', 'Mkpat Enin', null, '0', '0');
INSERT INTO `area` VALUES ('75', '2', 'Nsit Atai', null, '0', '0');
INSERT INTO `area` VALUES ('76', '2', 'Nsit Ibom', null, '0', '0');
INSERT INTO `area` VALUES ('77', '2', 'Nsit Ubium', null, '0', '0');
INSERT INTO `area` VALUES ('78', '2', 'Obot Akara', null, '0', '0');
INSERT INTO `area` VALUES ('79', '2', 'Okobo', null, '0', '0');
INSERT INTO `area` VALUES ('80', '2', 'Onna', null, '0', '0');
INSERT INTO `area` VALUES ('81', '2', 'Oron', null, '0', '0');
INSERT INTO `area` VALUES ('82', '2', 'Oruk Anam', null, '0', '0');
INSERT INTO `area` VALUES ('83', '2', 'Udung Uko', null, '0', '0');
INSERT INTO `area` VALUES ('84', '2', 'Ukanafun', null, '0', '0');
INSERT INTO `area` VALUES ('85', '2', 'Uruan', null, '0', '0');
INSERT INTO `area` VALUES ('86', '2', 'Urue-Offong/Oruko', null, '0', '0');
INSERT INTO `area` VALUES ('87', '2', 'Uyo', null, '0', '0');
INSERT INTO `area` VALUES ('88', '3', 'Aguata', null, '0', '0');
INSERT INTO `area` VALUES ('89', '3', 'Anambra East', null, '0', '0');
INSERT INTO `area` VALUES ('90', '3', 'Anambra West', null, '0', '0');
INSERT INTO `area` VALUES ('91', '3', 'Anaocha', null, '0', '0');
INSERT INTO `area` VALUES ('92', '3', 'Awka North', null, '0', '0');
INSERT INTO `area` VALUES ('93', '3', 'Awka South', null, '0', '0');
INSERT INTO `area` VALUES ('94', '3', 'Ayamelum', null, '0', '0');
INSERT INTO `area` VALUES ('95', '3', 'Dunukofia', null, '0', '0');
INSERT INTO `area` VALUES ('96', '3', 'Ekwusigo', null, '0', '0');
INSERT INTO `area` VALUES ('97', '3', 'Idemili North', null, '0', '0');
INSERT INTO `area` VALUES ('98', '3', 'Idemili South', null, '0', '0');
INSERT INTO `area` VALUES ('99', '3', 'Ihiala', null, '0', '0');
INSERT INTO `area` VALUES ('100', '3', 'Njikoka', null, '0', '0');
INSERT INTO `area` VALUES ('101', '3', 'Nnewi North', null, '0', '0');
INSERT INTO `area` VALUES ('102', '3', 'Nnewi South', null, '0', '0');
INSERT INTO `area` VALUES ('103', '3', 'Ogbaru', null, '0', '0');
INSERT INTO `area` VALUES ('104', '3', 'Onitsha North', null, '0', '0');
INSERT INTO `area` VALUES ('105', '3', 'Onitsha South', null, '0', '0');
INSERT INTO `area` VALUES ('106', '3', 'Orumba North', null, '0', '0');
INSERT INTO `area` VALUES ('107', '3', 'Orumba South', null, '0', '0');
INSERT INTO `area` VALUES ('108', '3', 'Oyi', null, '0', '0');
INSERT INTO `area` VALUES ('109', '4', 'Alkaleri', null, '0', '0');
INSERT INTO `area` VALUES ('110', '4', 'Bauchi', null, '0', '0');
INSERT INTO `area` VALUES ('111', '4', 'Bogoro', null, '0', '0');
INSERT INTO `area` VALUES ('112', '4', 'Damban', null, '0', '0');
INSERT INTO `area` VALUES ('113', '4', 'Darazo', null, '0', '0');
INSERT INTO `area` VALUES ('114', '4', 'Dass', null, '0', '0');
INSERT INTO `area` VALUES ('115', '4', 'Ganjuwa', null, '0', '0');
INSERT INTO `area` VALUES ('116', '4', 'Giade', null, '0', '0');
INSERT INTO `area` VALUES ('117', '4', 'Itas/Gadau', null, '0', '0');
INSERT INTO `area` VALUES ('118', '4', 'Jama\'are', null, '0', '0');
INSERT INTO `area` VALUES ('119', '4', 'Katagum', null, '0', '0');
INSERT INTO `area` VALUES ('120', '4', 'Kirfi', null, '0', '0');
INSERT INTO `area` VALUES ('121', '4', 'Misau', null, '0', '0');
INSERT INTO `area` VALUES ('122', '4', 'Ningi', null, '0', '0');
INSERT INTO `area` VALUES ('123', '4', 'Shira', null, '0', '0');
INSERT INTO `area` VALUES ('124', '4', 'Tafawa-Balewa', null, '0', '0');
INSERT INTO `area` VALUES ('125', '4', 'Toro', null, '0', '0');
INSERT INTO `area` VALUES ('126', '4', 'Warji', null, '0', '0');
INSERT INTO `area` VALUES ('127', '4', 'Zaki', null, '0', '0');
INSERT INTO `area` VALUES ('128', '5', 'Brass', null, '0', '0');
INSERT INTO `area` VALUES ('129', '5', 'Ekeremor', null, '0', '0');
INSERT INTO `area` VALUES ('130', '5', 'Kolokuma/Opokuma', null, '0', '0');
INSERT INTO `area` VALUES ('131', '5', 'Nembe', null, '0', '0');
INSERT INTO `area` VALUES ('132', '5', 'Ogbia', null, '0', '0');
INSERT INTO `area` VALUES ('133', '5', 'Sagbama', null, '0', '0');
INSERT INTO `area` VALUES ('134', '5', 'Southern Jaw', null, '0', '0');
INSERT INTO `area` VALUES ('135', '5', 'Yenegoa', null, '0', '0');
INSERT INTO `area` VALUES ('136', '6', 'Ado', null, '0', '0');
INSERT INTO `area` VALUES ('137', '6', 'Agatu', null, '0', '0');
INSERT INTO `area` VALUES ('138', '6', 'Apa', null, '0', '0');
INSERT INTO `area` VALUES ('139', '6', 'Buruku', null, '0', '0');
INSERT INTO `area` VALUES ('140', '6', 'Gboko', null, '0', '0');
INSERT INTO `area` VALUES ('141', '6', 'Guma', null, '0', '0');
INSERT INTO `area` VALUES ('142', '6', 'Gwer East', null, '0', '0');
INSERT INTO `area` VALUES ('143', '6', 'Gwer West', null, '0', '0');
INSERT INTO `area` VALUES ('144', '6', 'Katsina-Ala', null, '0', '0');
INSERT INTO `area` VALUES ('145', '6', 'Konshisha', null, '0', '0');
INSERT INTO `area` VALUES ('146', '6', 'Kwande', null, '0', '0');
INSERT INTO `area` VALUES ('147', '6', 'Logo', null, '0', '0');
INSERT INTO `area` VALUES ('148', '6', 'Makurdi', null, '0', '0');
INSERT INTO `area` VALUES ('149', '6', 'Obi', null, '0', '0');
INSERT INTO `area` VALUES ('150', '6', 'Ogbadibo', null, '0', '0');
INSERT INTO `area` VALUES ('151', '6', 'Oju', null, '0', '0');
INSERT INTO `area` VALUES ('152', '6', 'Okpokwu', null, '0', '0');
INSERT INTO `area` VALUES ('153', '6', 'Ohimini', null, '0', '0');
INSERT INTO `area` VALUES ('154', '6', 'Oturkpo', null, '0', '0');
INSERT INTO `area` VALUES ('155', '6', 'Tarka', null, '0', '0');
INSERT INTO `area` VALUES ('156', '6', 'Ukum', null, '0', '0');
INSERT INTO `area` VALUES ('157', '6', 'Ushongo', null, '0', '0');
INSERT INTO `area` VALUES ('158', '6', 'Vandeikya', null, '0', '0');
INSERT INTO `area` VALUES ('159', '7', 'Abadam', null, '0', '0');
INSERT INTO `area` VALUES ('160', '7', 'Askira/Uba', null, '0', '0');
INSERT INTO `area` VALUES ('161', '7', 'Bama', null, '0', '0');
INSERT INTO `area` VALUES ('162', '7', 'Bayo', null, '0', '0');
INSERT INTO `area` VALUES ('163', '7', 'Biu', null, '0', '0');
INSERT INTO `area` VALUES ('164', '7', 'Chibok', null, '0', '0');
INSERT INTO `area` VALUES ('165', '7', 'Damboa', null, '0', '0');
INSERT INTO `area` VALUES ('166', '7', 'Dikwa', null, '0', '0');
INSERT INTO `area` VALUES ('167', '7', 'Gubio', null, '0', '0');
INSERT INTO `area` VALUES ('168', '7', 'Guzamala', null, '0', '0');
INSERT INTO `area` VALUES ('169', '7', 'Gwoza', null, '0', '0');
INSERT INTO `area` VALUES ('170', '7', 'Hawul', null, '0', '0');
INSERT INTO `area` VALUES ('171', '7', 'Jere', null, '0', '0');
INSERT INTO `area` VALUES ('172', '7', 'Kaga', null, '0', '0');
INSERT INTO `area` VALUES ('173', '7', 'Kala/Balge', null, '0', '0');
INSERT INTO `area` VALUES ('174', '7', 'Konduga', null, '0', '0');
INSERT INTO `area` VALUES ('175', '7', 'Kukawa', null, '0', '0');
INSERT INTO `area` VALUES ('176', '7', 'Kwaya Kusar', null, '0', '0');
INSERT INTO `area` VALUES ('177', '7', 'Mafa', null, '0', '0');
INSERT INTO `area` VALUES ('178', '7', 'Magumeri', null, '0', '0');
INSERT INTO `area` VALUES ('179', '7', 'Maiduguri', null, '0', '0');
INSERT INTO `area` VALUES ('180', '7', 'Marte', null, '0', '0');
INSERT INTO `area` VALUES ('181', '7', 'Mobbar', null, '0', '0');
INSERT INTO `area` VALUES ('182', '7', 'Monguno', null, '0', '0');
INSERT INTO `area` VALUES ('183', '7', 'Ngala', null, '0', '0');
INSERT INTO `area` VALUES ('184', '7', 'Nganzai', null, '0', '0');
INSERT INTO `area` VALUES ('185', '7', 'Shani', null, '0', '0');
INSERT INTO `area` VALUES ('186', '8', 'Akpabuyo', null, '0', '0');
INSERT INTO `area` VALUES ('187', '8', 'Odukpani', null, '0', '0');
INSERT INTO `area` VALUES ('188', '8', 'Akamkpa', null, '0', '0');
INSERT INTO `area` VALUES ('189', '8', 'Biase', null, '0', '0');
INSERT INTO `area` VALUES ('190', '8', 'Abi', null, '0', '0');
INSERT INTO `area` VALUES ('191', '8', 'Ikom', null, '0', '0');
INSERT INTO `area` VALUES ('192', '8', 'Yarkur', null, '0', '0');
INSERT INTO `area` VALUES ('193', '8', 'Odubra', null, '0', '0');
INSERT INTO `area` VALUES ('194', '8', 'Boki', null, '0', '0');
INSERT INTO `area` VALUES ('195', '8', 'Ogoja', null, '0', '0');
INSERT INTO `area` VALUES ('196', '8', 'Yala', null, '0', '0');
INSERT INTO `area` VALUES ('197', '8', 'Obanliku', null, '0', '0');
INSERT INTO `area` VALUES ('198', '8', 'Obudu', null, '0', '0');
INSERT INTO `area` VALUES ('199', '8', 'Calabar South', null, '0', '0');
INSERT INTO `area` VALUES ('200', '8', 'Etung', null, '0', '0');
INSERT INTO `area` VALUES ('201', '8', 'Bekwara', null, '0', '0');
INSERT INTO `area` VALUES ('202', '8', 'Bakassi', null, '0', '0');
INSERT INTO `area` VALUES ('203', '8', 'Calabar Municipality', null, '0', '0');
INSERT INTO `area` VALUES ('204', '9', 'Oshimili', null, '0', '0');
INSERT INTO `area` VALUES ('205', '9', 'Aniocha', null, '0', '0');
INSERT INTO `area` VALUES ('206', '9', 'Aniocha South', null, '0', '0');
INSERT INTO `area` VALUES ('207', '9', 'Ika South', null, '0', '0');
INSERT INTO `area` VALUES ('208', '9', 'Ika North-East', null, '0', '0');
INSERT INTO `area` VALUES ('209', '9', 'Ndokwa West', null, '0', '0');
INSERT INTO `area` VALUES ('210', '9', 'Ndokwa East', null, '0', '0');
INSERT INTO `area` VALUES ('211', '9', 'Isoko south', null, '0', '0');
INSERT INTO `area` VALUES ('212', '9', 'Isoko North', null, '0', '0');
INSERT INTO `area` VALUES ('213', '9', 'Bomadi', null, '0', '0');
INSERT INTO `area` VALUES ('214', '9', 'Burutu', null, '0', '0');
INSERT INTO `area` VALUES ('215', '9', 'Ughelli South', null, '0', '0');
INSERT INTO `area` VALUES ('216', '9', 'Ughelli North', null, '0', '0');
INSERT INTO `area` VALUES ('217', '9', 'Ethiope West', null, '0', '0');
INSERT INTO `area` VALUES ('218', '9', 'Ethiope East', null, '0', '0');
INSERT INTO `area` VALUES ('219', '9', 'Sapele', null, '0', '0');
INSERT INTO `area` VALUES ('220', '9', 'Okpe', null, '0', '0');
INSERT INTO `area` VALUES ('221', '9', 'Warri North', null, '0', '0');
INSERT INTO `area` VALUES ('222', '9', 'Warri South', null, '0', '0');
INSERT INTO `area` VALUES ('223', '9', 'Uvwie', null, '0', '0');
INSERT INTO `area` VALUES ('224', '9', 'Udu', null, '0', '0');
INSERT INTO `area` VALUES ('225', '9', 'Warri Central', null, '0', '0');
INSERT INTO `area` VALUES ('226', '9', 'Ukwani', null, '0', '0');
INSERT INTO `area` VALUES ('227', '9', 'Oshimili North', null, '0', '0');
INSERT INTO `area` VALUES ('228', '9', 'Patani', null, '0', '0');
INSERT INTO `area` VALUES ('229', '10', 'Afikpo South', null, '0', '0');
INSERT INTO `area` VALUES ('230', '10', 'Afikpo North', null, '0', '0');
INSERT INTO `area` VALUES ('231', '10', 'Onicha', null, '0', '0');
INSERT INTO `area` VALUES ('232', '10', 'Ohaozara', null, '0', '0');
INSERT INTO `area` VALUES ('233', '10', 'Abakaliki', null, '0', '0');
INSERT INTO `area` VALUES ('234', '10', 'Ishielu', null, '0', '0');
INSERT INTO `area` VALUES ('235', '10', 'lkwo', null, '0', '0');
INSERT INTO `area` VALUES ('236', '10', 'Ezza', null, '0', '0');
INSERT INTO `area` VALUES ('237', '10', 'Ezza South', null, '0', '0');
INSERT INTO `area` VALUES ('238', '10', 'Ohaukwu', null, '0', '0');
INSERT INTO `area` VALUES ('239', '10', 'Ebonyi', null, '0', '0');
INSERT INTO `area` VALUES ('240', '10', 'Ivo', null, '0', '0');
INSERT INTO `area` VALUES ('241', '11', 'Esan North-East', null, '0', '0');
INSERT INTO `area` VALUES ('242', '11', 'Esan Central', null, '0', '0');
INSERT INTO `area` VALUES ('243', '11', 'Esan West', null, '0', '0');
INSERT INTO `area` VALUES ('244', '11', 'Egor', null, '0', '0');
INSERT INTO `area` VALUES ('245', '11', 'Ukpoba', null, '0', '0');
INSERT INTO `area` VALUES ('246', '11', 'Central', null, '0', '0');
INSERT INTO `area` VALUES ('247', '11', 'Etsako Central', null, '0', '0');
INSERT INTO `area` VALUES ('248', '11', 'Igueben', null, '0', '0');
INSERT INTO `area` VALUES ('249', '11', 'Oredo', null, '0', '0');
INSERT INTO `area` VALUES ('250', '11', 'Ovia Southwest', null, '0', '0');
INSERT INTO `area` VALUES ('251', '11', 'Ovia South-East', null, '0', '0');
INSERT INTO `area` VALUES ('252', '11', 'Orhionwon', null, '0', '0');
INSERT INTO `area` VALUES ('253', '11', 'Uhunmwonde', null, '0', '0');
INSERT INTO `area` VALUES ('254', '11', 'Etsako East', null, '0', '0');
INSERT INTO `area` VALUES ('255', '11', 'Esan South-East', null, '0', '0');
INSERT INTO `area` VALUES ('256', '12', 'Ado', null, '0', '0');
INSERT INTO `area` VALUES ('257', '12', 'Ekiti-East', null, '0', '0');
INSERT INTO `area` VALUES ('258', '12', 'Ekiti-West', null, '0', '0');
INSERT INTO `area` VALUES ('259', '12', 'Emure/Ise/Orun', null, '0', '0');
INSERT INTO `area` VALUES ('260', '12', 'Ekiti South-West', null, '0', '0');
INSERT INTO `area` VALUES ('261', '12', 'Ikare', null, '0', '0');
INSERT INTO `area` VALUES ('262', '12', 'Irepodun', null, '0', '0');
INSERT INTO `area` VALUES ('263', '12', 'Ijero,', null, '0', '0');
INSERT INTO `area` VALUES ('264', '12', 'Ido/Osi', null, '0', '0');
INSERT INTO `area` VALUES ('265', '12', 'Oye', null, '0', '0');
INSERT INTO `area` VALUES ('266', '12', 'Ikole', null, '0', '0');
INSERT INTO `area` VALUES ('267', '12', 'Moba', null, '0', '0');
INSERT INTO `area` VALUES ('268', '12', 'Gbonyin', null, '0', '0');
INSERT INTO `area` VALUES ('269', '12', 'Efon', null, '0', '0');
INSERT INTO `area` VALUES ('270', '12', 'Ise/Orun', null, '0', '0');
INSERT INTO `area` VALUES ('271', '12', 'Ilejemeje.', null, '0', '0');
INSERT INTO `area` VALUES ('272', '13', 'Enugu South,', null, '0', '0');
INSERT INTO `area` VALUES ('273', '13', 'Igbo-Eze South', null, '0', '0');
INSERT INTO `area` VALUES ('274', '13', 'Enugu North', null, '0', '0');
INSERT INTO `area` VALUES ('275', '13', 'Nkanu', null, '0', '0');
INSERT INTO `area` VALUES ('276', '13', 'Udi Agwu', null, '0', '0');
INSERT INTO `area` VALUES ('277', '13', 'Oji-River', null, '0', '0');
INSERT INTO `area` VALUES ('278', '13', 'Ezeagu', null, '0', '0');
INSERT INTO `area` VALUES ('279', '13', 'IgboEze North', null, '0', '0');
INSERT INTO `area` VALUES ('280', '13', 'Isi-Uzo', null, '0', '0');
INSERT INTO `area` VALUES ('281', '13', 'Nsukka', null, '0', '0');
INSERT INTO `area` VALUES ('282', '13', 'Igbo-Ekiti', null, '0', '0');
INSERT INTO `area` VALUES ('283', '13', 'Uzo-Uwani', null, '0', '0');
INSERT INTO `area` VALUES ('284', '13', 'Enugu Eas', null, '0', '0');
INSERT INTO `area` VALUES ('285', '13', 'Aninri', null, '0', '0');
INSERT INTO `area` VALUES ('286', '13', 'Nkanu East', null, '0', '0');
INSERT INTO `area` VALUES ('287', '13', 'Udenu.', null, '0', '0');
INSERT INTO `area` VALUES ('288', '14', 'Akko', null, '0', '0');
INSERT INTO `area` VALUES ('289', '14', 'Balanga', null, '0', '0');
INSERT INTO `area` VALUES ('290', '14', 'Billiri', null, '0', '0');
INSERT INTO `area` VALUES ('291', '14', 'Dukku', null, '0', '0');
INSERT INTO `area` VALUES ('292', '14', 'Kaltungo', null, '0', '0');
INSERT INTO `area` VALUES ('293', '14', 'Kwami', null, '0', '0');
INSERT INTO `area` VALUES ('294', '14', 'Shomgom', null, '0', '0');
INSERT INTO `area` VALUES ('295', '14', 'Funakaye', null, '0', '0');
INSERT INTO `area` VALUES ('296', '14', 'Gombe', null, '0', '0');
INSERT INTO `area` VALUES ('297', '14', 'Nafada/Bajoga', null, '0', '0');
INSERT INTO `area` VALUES ('298', '14', 'Yamaltu/Delta.', null, '0', '0');
INSERT INTO `area` VALUES ('299', '15', 'Aboh-Mbaise', null, '0', '0');
INSERT INTO `area` VALUES ('300', '15', 'Ahiazu-Mbaise', null, '0', '0');
INSERT INTO `area` VALUES ('301', '15', 'Ehime-Mbano', null, '0', '0');
INSERT INTO `area` VALUES ('302', '15', 'Ezinihitte', null, '0', '0');
INSERT INTO `area` VALUES ('303', '15', 'Ideato North', null, '0', '0');
INSERT INTO `area` VALUES ('304', '15', 'Ideato South', null, '0', '0');
INSERT INTO `area` VALUES ('305', '15', 'Ihitte/Uboma', null, '0', '0');
INSERT INTO `area` VALUES ('306', '15', 'Ikeduru', null, '0', '0');
INSERT INTO `area` VALUES ('307', '15', 'Isiala Mbano', null, '0', '0');
INSERT INTO `area` VALUES ('308', '15', 'Isu', null, '0', '0');
INSERT INTO `area` VALUES ('309', '15', 'Mbaitoli', null, '0', '0');
INSERT INTO `area` VALUES ('310', '15', 'Mbaitoli', null, '0', '0');
INSERT INTO `area` VALUES ('311', '15', 'Ngor-Okpala', null, '0', '0');
INSERT INTO `area` VALUES ('312', '15', 'Njaba', null, '0', '0');
INSERT INTO `area` VALUES ('313', '15', 'Nwangele', null, '0', '0');
INSERT INTO `area` VALUES ('314', '15', 'Nkwerre', null, '0', '0');
INSERT INTO `area` VALUES ('315', '15', 'Obowo', null, '0', '0');
INSERT INTO `area` VALUES ('316', '15', 'Oguta', null, '0', '0');
INSERT INTO `area` VALUES ('317', '15', 'Ohaji/Egbema', null, '0', '0');
INSERT INTO `area` VALUES ('318', '15', 'Okigwe', null, '0', '0');
INSERT INTO `area` VALUES ('319', '15', 'Orlu', null, '0', '0');
INSERT INTO `area` VALUES ('320', '15', 'Orsu', null, '0', '0');
INSERT INTO `area` VALUES ('321', '15', 'Oru East', null, '0', '0');
INSERT INTO `area` VALUES ('322', '15', 'Oru West', null, '0', '0');
INSERT INTO `area` VALUES ('323', '15', 'Owerri-Municipal', null, '0', '0');
INSERT INTO `area` VALUES ('324', '15', 'Owerri North', null, '0', '0');
INSERT INTO `area` VALUES ('325', '15', 'Owerri West', null, '0', '0');
INSERT INTO `area` VALUES ('326', '16', 'Auyo', null, '0', '0');
INSERT INTO `area` VALUES ('327', '16', 'Babura', null, '0', '0');
INSERT INTO `area` VALUES ('328', '16', 'Birni Kudu', null, '0', '0');
INSERT INTO `area` VALUES ('329', '16', 'Biriniwa', null, '0', '0');
INSERT INTO `area` VALUES ('330', '16', 'Buji', null, '0', '0');
INSERT INTO `area` VALUES ('331', '16', 'Dutse', null, '0', '0');
INSERT INTO `area` VALUES ('332', '16', 'Gagarawa', null, '0', '0');
INSERT INTO `area` VALUES ('333', '16', 'Garki', null, '0', '0');
INSERT INTO `area` VALUES ('334', '16', 'Gumel', null, '0', '0');
INSERT INTO `area` VALUES ('335', '16', 'Guri', null, '0', '0');
INSERT INTO `area` VALUES ('336', '16', 'Gwaram', null, '0', '0');
INSERT INTO `area` VALUES ('337', '16', 'Gwiwa', null, '0', '0');
INSERT INTO `area` VALUES ('338', '16', 'Hadejia', null, '0', '0');
INSERT INTO `area` VALUES ('339', '16', 'Jahun', null, '0', '0');
INSERT INTO `area` VALUES ('340', '16', 'Kafin Hausa', null, '0', '0');
INSERT INTO `area` VALUES ('341', '16', 'Kaugama Kazaure', null, '0', '0');
INSERT INTO `area` VALUES ('342', '16', 'Kiri Kasamma', null, '0', '0');
INSERT INTO `area` VALUES ('343', '16', 'Kiyawa', null, '0', '0');
INSERT INTO `area` VALUES ('344', '16', 'Maigatari', null, '0', '0');
INSERT INTO `area` VALUES ('345', '16', 'Malam Madori', null, '0', '0');
INSERT INTO `area` VALUES ('346', '16', 'Miga', null, '0', '0');
INSERT INTO `area` VALUES ('347', '16', 'Ringim', null, '0', '0');
INSERT INTO `area` VALUES ('348', '16', 'Roni', null, '0', '0');
INSERT INTO `area` VALUES ('349', '16', 'Sule-Tankarkar', null, '0', '0');
INSERT INTO `area` VALUES ('350', '16', 'Taura', null, '0', '0');
INSERT INTO `area` VALUES ('351', '16', 'Yankwashi', null, '0', '0');
INSERT INTO `area` VALUES ('352', '17', 'Birni-Gwari', null, '0', '0');
INSERT INTO `area` VALUES ('353', '17', 'Chikun', null, '0', '0');
INSERT INTO `area` VALUES ('354', '17', 'Giwa', null, '0', '0');
INSERT INTO `area` VALUES ('355', '17', 'Igabi', null, '0', '0');
INSERT INTO `area` VALUES ('356', '17', 'Ikara', null, '0', '0');
INSERT INTO `area` VALUES ('357', '17', 'jaba', null, '0', '0');
INSERT INTO `area` VALUES ('358', '17', 'Jema\'a', null, '0', '0');
INSERT INTO `area` VALUES ('359', '17', 'Kachia', null, '0', '0');
INSERT INTO `area` VALUES ('360', '17', 'Kaduna North', null, '0', '0');
INSERT INTO `area` VALUES ('361', '17', 'Kaduna South', null, '0', '0');
INSERT INTO `area` VALUES ('362', '17', 'Kagarko', null, '0', '0');
INSERT INTO `area` VALUES ('363', '17', 'Kajuru', null, '0', '0');
INSERT INTO `area` VALUES ('364', '17', 'Kaura', null, '0', '0');
INSERT INTO `area` VALUES ('365', '17', 'Kauru', null, '0', '0');
INSERT INTO `area` VALUES ('366', '17', 'Kubau', null, '0', '0');
INSERT INTO `area` VALUES ('367', '17', 'Kudan', null, '0', '0');
INSERT INTO `area` VALUES ('368', '17', 'Lere', null, '0', '0');
INSERT INTO `area` VALUES ('369', '17', 'Makarfi', null, '0', '0');
INSERT INTO `area` VALUES ('370', '17', 'Sabon-Gari', null, '0', '0');
INSERT INTO `area` VALUES ('371', '17', 'Sanga', null, '0', '0');
INSERT INTO `area` VALUES ('372', '17', 'Soba', null, '0', '0');
INSERT INTO `area` VALUES ('373', '17', 'Zango-Kataf', null, '0', '0');
INSERT INTO `area` VALUES ('374', '17', 'Zaria', null, '0', '0');
INSERT INTO `area` VALUES ('375', '18', 'Ajingi', null, '0', '0');
INSERT INTO `area` VALUES ('376', '18', 'Albasu', null, '0', '0');
INSERT INTO `area` VALUES ('377', '18', 'Bagwai', null, '0', '0');
INSERT INTO `area` VALUES ('378', '18', 'Bebeji', null, '0', '0');
INSERT INTO `area` VALUES ('379', '18', 'Bichi', null, '0', '0');
INSERT INTO `area` VALUES ('380', '18', 'Bunkure', null, '0', '0');
INSERT INTO `area` VALUES ('381', '18', 'Dala', null, '0', '0');
INSERT INTO `area` VALUES ('382', '18', 'Dambatta', null, '0', '0');
INSERT INTO `area` VALUES ('383', '18', 'Dawakin Kudu', null, '0', '0');
INSERT INTO `area` VALUES ('384', '18', 'Dawakin Tofa', null, '0', '0');
INSERT INTO `area` VALUES ('385', '18', 'Doguwa', null, '0', '0');
INSERT INTO `area` VALUES ('386', '18', 'Fagge', null, '0', '0');
INSERT INTO `area` VALUES ('387', '18', 'Gabasawa', null, '0', '0');
INSERT INTO `area` VALUES ('388', '18', 'Garko', null, '0', '0');
INSERT INTO `area` VALUES ('389', '18', 'Garum', null, '0', '0');
INSERT INTO `area` VALUES ('390', '18', 'Mallam', null, '0', '0');
INSERT INTO `area` VALUES ('391', '18', 'Gaya', null, '0', '0');
INSERT INTO `area` VALUES ('392', '18', 'Gezawa', null, '0', '0');
INSERT INTO `area` VALUES ('393', '18', 'Gwale', null, '0', '0');
INSERT INTO `area` VALUES ('394', '18', 'Gwarzo', null, '0', '0');
INSERT INTO `area` VALUES ('395', '18', 'Kabo', null, '0', '0');
INSERT INTO `area` VALUES ('396', '18', 'Kano Municipal', null, '0', '0');
INSERT INTO `area` VALUES ('397', '18', 'Karaye', null, '0', '0');
INSERT INTO `area` VALUES ('398', '18', 'Kibiya', null, '0', '0');
INSERT INTO `area` VALUES ('399', '18', 'Kiru', null, '0', '0');
INSERT INTO `area` VALUES ('400', '18', 'kumbotso', null, '0', '0');
INSERT INTO `area` VALUES ('401', '18', 'Kunchi', null, '0', '0');
INSERT INTO `area` VALUES ('402', '18', 'Kura', null, '0', '0');
INSERT INTO `area` VALUES ('403', '18', 'Madobi', null, '0', '0');
INSERT INTO `area` VALUES ('404', '18', 'Makoda', null, '0', '0');
INSERT INTO `area` VALUES ('405', '18', 'Minjibir', null, '0', '0');
INSERT INTO `area` VALUES ('406', '18', 'Nasarawa', null, '0', '0');
INSERT INTO `area` VALUES ('407', '18', 'Rano', null, '0', '0');
INSERT INTO `area` VALUES ('408', '18', 'Rimin Gado', null, '0', '0');
INSERT INTO `area` VALUES ('409', '18', 'Rogo', null, '0', '0');
INSERT INTO `area` VALUES ('410', '18', 'Shanono', null, '0', '0');
INSERT INTO `area` VALUES ('411', '18', 'Sumaila', null, '0', '0');
INSERT INTO `area` VALUES ('412', '18', 'Takali', null, '0', '0');
INSERT INTO `area` VALUES ('413', '18', 'Tarauni', null, '0', '0');
INSERT INTO `area` VALUES ('414', '18', 'Tofa', null, '0', '0');
INSERT INTO `area` VALUES ('415', '18', 'Tsanyawa', null, '0', '0');
INSERT INTO `area` VALUES ('416', '18', 'Tudun Wada', null, '0', '0');
INSERT INTO `area` VALUES ('417', '18', 'Ungogo', null, '0', '0');
INSERT INTO `area` VALUES ('418', '18', 'Warawa', null, '0', '0');
INSERT INTO `area` VALUES ('419', '18', 'Wudil', null, '0', '0');
INSERT INTO `area` VALUES ('420', '19', 'Bakori', null, '0', '0');
INSERT INTO `area` VALUES ('421', '19', 'Batagarawa', null, '0', '0');
INSERT INTO `area` VALUES ('422', '19', 'Batsari', null, '0', '0');
INSERT INTO `area` VALUES ('423', '19', 'Baure', null, '0', '0');
INSERT INTO `area` VALUES ('424', '19', 'Bindawa', null, '0', '0');
INSERT INTO `area` VALUES ('425', '19', 'Charanchi', null, '0', '0');
INSERT INTO `area` VALUES ('426', '19', 'Dandume', null, '0', '0');
INSERT INTO `area` VALUES ('427', '19', 'Danja', null, '0', '0');
INSERT INTO `area` VALUES ('428', '19', 'Dan Musa', null, '0', '0');
INSERT INTO `area` VALUES ('429', '19', 'Daura', null, '0', '0');
INSERT INTO `area` VALUES ('430', '19', 'Dutsi', null, '0', '0');
INSERT INTO `area` VALUES ('431', '19', 'Dutsin-Ma', null, '0', '0');
INSERT INTO `area` VALUES ('432', '19', 'Faskari', null, '0', '0');
INSERT INTO `area` VALUES ('433', '19', 'Funtua', null, '0', '0');
INSERT INTO `area` VALUES ('434', '19', 'Ingawa', null, '0', '0');
INSERT INTO `area` VALUES ('435', '19', 'Jibia', null, '0', '0');
INSERT INTO `area` VALUES ('436', '19', 'Kafur', null, '0', '0');
INSERT INTO `area` VALUES ('437', '19', 'Kaita', null, '0', '0');
INSERT INTO `area` VALUES ('438', '19', 'Kankara', null, '0', '0');
INSERT INTO `area` VALUES ('439', '19', 'Kankia', null, '0', '0');
INSERT INTO `area` VALUES ('440', '19', 'Katsina', null, '0', '0');
INSERT INTO `area` VALUES ('441', '19', 'Kurfi', null, '0', '0');
INSERT INTO `area` VALUES ('442', '19', 'Kusada', null, '0', '0');
INSERT INTO `area` VALUES ('443', '19', 'Mai\'Adua', null, '0', '0');
INSERT INTO `area` VALUES ('444', '19', 'Malumfashi', null, '0', '0');
INSERT INTO `area` VALUES ('445', '19', 'Mani', null, '0', '0');
INSERT INTO `area` VALUES ('446', '19', 'Mashi', null, '0', '0');
INSERT INTO `area` VALUES ('447', '19', 'Matazuu', null, '0', '0');
INSERT INTO `area` VALUES ('448', '19', 'Musawa', null, '0', '0');
INSERT INTO `area` VALUES ('449', '19', 'Rimi', null, '0', '0');
INSERT INTO `area` VALUES ('450', '19', 'Sabuwa', null, '0', '0');
INSERT INTO `area` VALUES ('451', '19', 'Safana', null, '0', '0');
INSERT INTO `area` VALUES ('452', '19', 'Sandamu', null, '0', '0');
INSERT INTO `area` VALUES ('453', '19', 'Zango', null, '0', '0');
INSERT INTO `area` VALUES ('454', '20', 'Aleiro', null, '0', '0');
INSERT INTO `area` VALUES ('455', '20', 'Arewa-Dandi', null, '0', '0');
INSERT INTO `area` VALUES ('456', '20', 'Argungu', null, '0', '0');
INSERT INTO `area` VALUES ('457', '20', 'Augie', null, '0', '0');
INSERT INTO `area` VALUES ('458', '20', 'Bagudo', null, '0', '0');
INSERT INTO `area` VALUES ('459', '20', 'Birnin Kebbi', null, '0', '0');
INSERT INTO `area` VALUES ('460', '20', 'Bunza', null, '0', '0');
INSERT INTO `area` VALUES ('461', '20', 'Dandi', null, '0', '0');
INSERT INTO `area` VALUES ('462', '20', 'Fakai', null, '0', '0');
INSERT INTO `area` VALUES ('463', '20', 'Gwandu', null, '0', '0');
INSERT INTO `area` VALUES ('464', '20', 'Jega', null, '0', '0');
INSERT INTO `area` VALUES ('465', '20', 'Kalgo', null, '0', '0');
INSERT INTO `area` VALUES ('466', '20', 'Koko/Besse', null, '0', '0');
INSERT INTO `area` VALUES ('467', '20', 'Maiyama', null, '0', '0');
INSERT INTO `area` VALUES ('468', '20', 'Ngaski', null, '0', '0');
INSERT INTO `area` VALUES ('469', '20', 'Sakaba', null, '0', '0');
INSERT INTO `area` VALUES ('470', '20', 'Shanga', null, '0', '0');
INSERT INTO `area` VALUES ('471', '20', 'Suru', null, '0', '0');
INSERT INTO `area` VALUES ('472', '20', 'Wasagu/Danko', null, '0', '0');
INSERT INTO `area` VALUES ('473', '20', 'Yauri', null, '0', '0');
INSERT INTO `area` VALUES ('474', '20', 'Zuru', null, '0', '0');
INSERT INTO `area` VALUES ('475', '21', 'Adavi', null, '0', '0');
INSERT INTO `area` VALUES ('476', '21', 'Ajaokuta', null, '0', '0');
INSERT INTO `area` VALUES ('477', '21', 'Ankpa', null, '0', '0');
INSERT INTO `area` VALUES ('478', '21', 'Bassa', null, '0', '0');
INSERT INTO `area` VALUES ('479', '21', 'Dekina', null, '0', '0');
INSERT INTO `area` VALUES ('480', '21', 'Ibaji', null, '0', '0');
INSERT INTO `area` VALUES ('481', '21', 'Idah', null, '0', '0');
INSERT INTO `area` VALUES ('482', '21', 'Igalamela-Odolu', null, '0', '0');
INSERT INTO `area` VALUES ('483', '21', 'Ijumu', null, '0', '0');
INSERT INTO `area` VALUES ('484', '21', 'Kabba/Bunu', null, '0', '0');
INSERT INTO `area` VALUES ('485', '21', 'Kogi', null, '0', '0');
INSERT INTO `area` VALUES ('486', '21', 'Lokoja', null, '0', '0');
INSERT INTO `area` VALUES ('487', '21', 'Mopa-Muro', null, '0', '0');
INSERT INTO `area` VALUES ('488', '21', 'Ofu', null, '0', '0');
INSERT INTO `area` VALUES ('489', '21', 'Ogori/Mangongo', null, '0', '0');
INSERT INTO `area` VALUES ('490', '21', 'Okehi', null, '0', '0');
INSERT INTO `area` VALUES ('491', '21', 'Okene', null, '0', '0');
INSERT INTO `area` VALUES ('492', '21', 'Olamabolo', null, '0', '0');
INSERT INTO `area` VALUES ('493', '21', 'Omala', null, '0', '0');
INSERT INTO `area` VALUES ('494', '21', 'Yagba East', null, '0', '0');
INSERT INTO `area` VALUES ('495', '21', 'Yagba West', null, '0', '0');
INSERT INTO `area` VALUES ('496', '22', 'Asa', null, '0', '0');
INSERT INTO `area` VALUES ('497', '22', 'Baruten', null, '0', '0');
INSERT INTO `area` VALUES ('498', '22', 'Edu', null, '0', '0');
INSERT INTO `area` VALUES ('499', '22', 'Ekiti', null, '0', '0');
INSERT INTO `area` VALUES ('500', '22', 'Ifelodun', null, '0', '0');
INSERT INTO `area` VALUES ('501', '22', 'Ilorin East', null, '0', '0');
INSERT INTO `area` VALUES ('502', '22', 'Ilorin West', null, '0', '0');
INSERT INTO `area` VALUES ('503', '22', 'Irepodun', null, '0', '0');
INSERT INTO `area` VALUES ('504', '22', 'Isin', null, '0', '0');
INSERT INTO `area` VALUES ('505', '22', 'Kaiama', null, '0', '0');
INSERT INTO `area` VALUES ('506', '22', 'Moro', null, '0', '0');
INSERT INTO `area` VALUES ('507', '22', 'Offa', null, '0', '0');
INSERT INTO `area` VALUES ('508', '22', 'Oke-Ero', null, '0', '0');
INSERT INTO `area` VALUES ('509', '22', 'Oyun', null, '0', '0');
INSERT INTO `area` VALUES ('510', '22', 'Pategi', null, '0', '0');
INSERT INTO `area` VALUES ('511', '23', 'Agege', null, '0', '0');
INSERT INTO `area` VALUES ('512', '23', 'Ajeromi-Ifelodun', null, '0', '0');
INSERT INTO `area` VALUES ('513', '23', 'Alimosho', null, '0', '0');
INSERT INTO `area` VALUES ('514', '23', 'Amuwo-Odofin', null, '0', '0');
INSERT INTO `area` VALUES ('515', '23', 'Apapa', null, '0', '0');
INSERT INTO `area` VALUES ('516', '23', 'Badagry', null, '0', '0');
INSERT INTO `area` VALUES ('517', '23', 'Epe', null, '0', '0');
INSERT INTO `area` VALUES ('518', '23', 'Eti-Osa', null, '0', '0');
INSERT INTO `area` VALUES ('519', '23', 'Ibeju/Lekki', null, '0', '0');
INSERT INTO `area` VALUES ('520', '23', 'Ifako-Ijaye', null, '0', '0');
INSERT INTO `area` VALUES ('521', '23', 'Ikeja', null, '0', '0');
INSERT INTO `area` VALUES ('522', '23', 'Ikorodu', null, '0', '0');
INSERT INTO `area` VALUES ('523', '23', 'Kosofe', null, '0', '0');
INSERT INTO `area` VALUES ('524', '23', 'Lagos Island', null, '0', '0');
INSERT INTO `area` VALUES ('525', '23', 'Lagos Mainland', null, '0', '0');
INSERT INTO `area` VALUES ('526', '23', 'Mushin', null, '0', '0');
INSERT INTO `area` VALUES ('527', '23', 'Ojo', null, '0', '0');
INSERT INTO `area` VALUES ('528', '23', 'Oshodi-Isolo', null, '0', '0');
INSERT INTO `area` VALUES ('529', '23', 'Shomolu', null, '0', '0');
INSERT INTO `area` VALUES ('530', '23', 'Surulere', null, '0', '0');
INSERT INTO `area` VALUES ('531', '24', 'Akwanga', null, '0', '0');
INSERT INTO `area` VALUES ('532', '24', 'Awe', null, '0', '0');
INSERT INTO `area` VALUES ('533', '24', 'Doma', null, '0', '0');
INSERT INTO `area` VALUES ('534', '24', 'Karu', null, '0', '0');
INSERT INTO `area` VALUES ('535', '24', 'Keana', null, '0', '0');
INSERT INTO `area` VALUES ('536', '24', 'Keffi', null, '0', '0');
INSERT INTO `area` VALUES ('537', '24', 'Kokona', null, '0', '0');
INSERT INTO `area` VALUES ('538', '24', 'Lafia', null, '0', '0');
INSERT INTO `area` VALUES ('539', '24', 'Nasarawa', null, '0', '0');
INSERT INTO `area` VALUES ('540', '24', 'Nasarawa-Eggon', null, '0', '0');
INSERT INTO `area` VALUES ('541', '24', 'Obi', null, '0', '0');
INSERT INTO `area` VALUES ('542', '24', 'Toto', null, '0', '0');
INSERT INTO `area` VALUES ('543', '24', 'Wamba', null, '0', '0');
INSERT INTO `area` VALUES ('544', '25', 'Agaie', null, '0', '0');
INSERT INTO `area` VALUES ('545', '25', 'Agwara', null, '0', '0');
INSERT INTO `area` VALUES ('546', '25', 'Bida', null, '0', '0');
INSERT INTO `area` VALUES ('547', '25', 'Borgu', null, '0', '0');
INSERT INTO `area` VALUES ('548', '25', 'Bosso', null, '0', '0');
INSERT INTO `area` VALUES ('549', '25', 'Chanchaga', null, '0', '0');
INSERT INTO `area` VALUES ('550', '25', 'Edati', null, '0', '0');
INSERT INTO `area` VALUES ('551', '25', 'Gbako', null, '0', '0');
INSERT INTO `area` VALUES ('552', '25', 'Gurara', null, '0', '0');
INSERT INTO `area` VALUES ('553', '25', 'Katcha', null, '0', '0');
INSERT INTO `area` VALUES ('554', '25', 'Kontagora', null, '0', '0');
INSERT INTO `area` VALUES ('555', '25', 'Lapai', null, '0', '0');
INSERT INTO `area` VALUES ('556', '25', 'Lavun', null, '0', '0');
INSERT INTO `area` VALUES ('557', '25', 'Magama', null, '0', '0');
INSERT INTO `area` VALUES ('558', '25', 'Mariga', null, '0', '0');
INSERT INTO `area` VALUES ('559', '25', 'Mashegu', null, '0', '0');
INSERT INTO `area` VALUES ('560', '25', 'Mokwa', null, '0', '0');
INSERT INTO `area` VALUES ('561', '25', 'Muya', null, '0', '0');
INSERT INTO `area` VALUES ('562', '25', 'Pailoro', null, '0', '0');
INSERT INTO `area` VALUES ('563', '25', 'Rafi', null, '0', '0');
INSERT INTO `area` VALUES ('564', '25', 'Rijau', null, '0', '0');
INSERT INTO `area` VALUES ('565', '25', 'Shiroro', null, '0', '0');
INSERT INTO `area` VALUES ('566', '25', 'Suleja', null, '0', '0');
INSERT INTO `area` VALUES ('567', '25', 'Tafa', null, '0', '0');
INSERT INTO `area` VALUES ('568', '25', 'Wushishi', null, '0', '0');
INSERT INTO `area` VALUES ('569', '26', 'Abeokuta North', null, '0', '0');
INSERT INTO `area` VALUES ('570', '26', 'Abeokuta South', null, '0', '0');
INSERT INTO `area` VALUES ('571', '26', 'Ado-Odo/Ota', null, '0', '0');
INSERT INTO `area` VALUES ('572', '26', 'Egbado North', null, '0', '0');
INSERT INTO `area` VALUES ('573', '26', 'Egbado South', null, '0', '0');
INSERT INTO `area` VALUES ('574', '26', 'Ewekoro', null, '0', '0');
INSERT INTO `area` VALUES ('575', '26', 'Ifo', null, '0', '0');
INSERT INTO `area` VALUES ('576', '26', 'Ijebu East', null, '0', '0');
INSERT INTO `area` VALUES ('577', '26', 'Ijebu North', null, '0', '0');
INSERT INTO `area` VALUES ('578', '26', 'Ijebu North East', null, '0', '0');
INSERT INTO `area` VALUES ('579', '26', 'Ijebu Ode', null, '0', '0');
INSERT INTO `area` VALUES ('580', '26', 'Ikenne', null, '0', '0');
INSERT INTO `area` VALUES ('581', '26', 'Imeko-Afon', null, '0', '0');
INSERT INTO `area` VALUES ('582', '26', 'Ipokia', null, '0', '0');
INSERT INTO `area` VALUES ('583', '26', 'Obafemi-Owode', null, '0', '0');
INSERT INTO `area` VALUES ('584', '26', 'Ogun Waterside', null, '0', '0');
INSERT INTO `area` VALUES ('585', '26', 'Odeda', null, '0', '0');
INSERT INTO `area` VALUES ('586', '26', 'Odogbolu', null, '0', '0');
INSERT INTO `area` VALUES ('587', '26', 'Remo North', null, '0', '0');
INSERT INTO `area` VALUES ('588', '26', 'Shagamu', null, '0', '0');
INSERT INTO `area` VALUES ('589', '27', 'Akoko North East', null, '0', '0');
INSERT INTO `area` VALUES ('590', '27', 'Akoko North West', null, '0', '0');
INSERT INTO `area` VALUES ('591', '27', 'Akoko South Akure East', null, '0', '0');
INSERT INTO `area` VALUES ('592', '27', 'Akoko South West', null, '0', '0');
INSERT INTO `area` VALUES ('593', '27', 'Akure North', null, '0', '0');
INSERT INTO `area` VALUES ('594', '27', 'Akure South', null, '0', '0');
INSERT INTO `area` VALUES ('595', '27', 'Ese-Odo', null, '0', '0');
INSERT INTO `area` VALUES ('596', '27', 'Idanre', null, '0', '0');
INSERT INTO `area` VALUES ('597', '27', 'Ifedore', null, '0', '0');
INSERT INTO `area` VALUES ('598', '27', 'Ilaje', null, '0', '0');
INSERT INTO `area` VALUES ('599', '27', 'Ile-Oluji', null, '0', '0');
INSERT INTO `area` VALUES ('600', '27', 'Okeigbo', null, '0', '0');
INSERT INTO `area` VALUES ('601', '27', 'Irele', null, '0', '0');
INSERT INTO `area` VALUES ('602', '27', 'Odigbo', null, '0', '0');
INSERT INTO `area` VALUES ('603', '27', 'Okitipupa', null, '0', '0');
INSERT INTO `area` VALUES ('604', '27', 'Ondo East', null, '0', '0');
INSERT INTO `area` VALUES ('605', '27', 'Ondo West', null, '0', '0');
INSERT INTO `area` VALUES ('606', '27', 'Ose', null, '0', '0');
INSERT INTO `area` VALUES ('607', '27', 'Owo', null, '0', '0');
INSERT INTO `area` VALUES ('608', '28', 'Aiyedade', null, '0', '0');
INSERT INTO `area` VALUES ('609', '28', 'Aiyedire', null, '0', '0');
INSERT INTO `area` VALUES ('610', '28', 'Atakumosa East', null, '0', '0');
INSERT INTO `area` VALUES ('611', '28', 'Atakumosa West', null, '0', '0');
INSERT INTO `area` VALUES ('612', '28', 'Boluwaduro', null, '0', '0');
INSERT INTO `area` VALUES ('613', '28', 'Boripe', null, '0', '0');
INSERT INTO `area` VALUES ('614', '28', 'Ede North', null, '0', '0');
INSERT INTO `area` VALUES ('615', '28', 'Ede South', null, '0', '0');
INSERT INTO `area` VALUES ('616', '28', 'Egbedore', null, '0', '0');
INSERT INTO `area` VALUES ('617', '28', 'Ejigbo', null, '0', '0');
INSERT INTO `area` VALUES ('618', '28', 'Ife Central', null, '0', '0');
INSERT INTO `area` VALUES ('619', '28', 'Ife East', null, '0', '0');
INSERT INTO `area` VALUES ('620', '28', 'Ife North', null, '0', '0');
INSERT INTO `area` VALUES ('621', '28', 'Ife South', null, '0', '0');
INSERT INTO `area` VALUES ('622', '28', 'Ifedayo', null, '0', '0');
INSERT INTO `area` VALUES ('623', '28', 'Ifelodun', null, '0', '0');
INSERT INTO `area` VALUES ('624', '28', 'Ila', null, '0', '0');
INSERT INTO `area` VALUES ('625', '28', 'Ilesha East', null, '0', '0');
INSERT INTO `area` VALUES ('626', '28', 'Ilesha West', null, '0', '0');
INSERT INTO `area` VALUES ('627', '28', 'Irepodun', null, '0', '0');
INSERT INTO `area` VALUES ('628', '28', 'Irewole', null, '0', '0');
INSERT INTO `area` VALUES ('629', '28', 'Isokan', null, '0', '0');
INSERT INTO `area` VALUES ('630', '28', 'Iwo', null, '0', '0');
INSERT INTO `area` VALUES ('631', '28', 'Obokun', null, '0', '0');
INSERT INTO `area` VALUES ('632', '28', 'Odo-Otin', null, '0', '0');
INSERT INTO `area` VALUES ('633', '28', 'Ola-Oluwa', null, '0', '0');
INSERT INTO `area` VALUES ('634', '28', 'Olorunda', null, '0', '0');
INSERT INTO `area` VALUES ('635', '28', 'Oriade', null, '0', '0');
INSERT INTO `area` VALUES ('636', '28', 'Orolu', null, '0', '0');
INSERT INTO `area` VALUES ('637', '28', 'Osogbo', null, '0', '0');
INSERT INTO `area` VALUES ('638', '29', 'Afijio', null, '0', '0');
INSERT INTO `area` VALUES ('639', '29', 'Akinyele', null, '0', '0');
INSERT INTO `area` VALUES ('640', '29', 'Atiba', null, '0', '0');
INSERT INTO `area` VALUES ('641', '29', 'Atigbo', null, '0', '0');
INSERT INTO `area` VALUES ('642', '29', 'Egbeda', null, '0', '0');
INSERT INTO `area` VALUES ('643', '29', 'IbadanCentral', null, '0', '0');
INSERT INTO `area` VALUES ('644', '29', 'Ibadan North', null, '0', '0');
INSERT INTO `area` VALUES ('645', '29', 'Ibadan North West', null, '0', '0');
INSERT INTO `area` VALUES ('646', '29', 'Ibadan South East', null, '0', '0');
INSERT INTO `area` VALUES ('647', '29', 'Ibadan South West', null, '0', '0');
INSERT INTO `area` VALUES ('648', '29', 'Ibarapa Central', null, '0', '0');
INSERT INTO `area` VALUES ('649', '29', 'Ibarapa East', null, '0', '0');
INSERT INTO `area` VALUES ('650', '29', 'Ibarapa North', null, '0', '0');
INSERT INTO `area` VALUES ('651', '29', 'Ido', null, '0', '0');
INSERT INTO `area` VALUES ('652', '29', 'Irepo', null, '0', '0');
INSERT INTO `area` VALUES ('653', '29', 'Iseyin', null, '0', '0');
INSERT INTO `area` VALUES ('654', '29', 'Itesiwaju', null, '0', '0');
INSERT INTO `area` VALUES ('655', '29', 'Iwajowa', null, '0', '0');
INSERT INTO `area` VALUES ('656', '29', 'Kajola', null, '0', '0');
INSERT INTO `area` VALUES ('657', '29', 'Lagelu Ogbomosho North', null, '0', '0');
INSERT INTO `area` VALUES ('658', '29', 'Ogbmosho South', null, '0', '0');
INSERT INTO `area` VALUES ('659', '29', 'Ogo Oluwa', null, '0', '0');
INSERT INTO `area` VALUES ('660', '29', 'Olorunsogo', null, '0', '0');
INSERT INTO `area` VALUES ('661', '29', 'Oluyole', null, '0', '0');
INSERT INTO `area` VALUES ('662', '29', 'Ona-Ara', null, '0', '0');
INSERT INTO `area` VALUES ('663', '29', 'Orelope', null, '0', '0');
INSERT INTO `area` VALUES ('664', '29', 'Ori Ire', null, '0', '0');
INSERT INTO `area` VALUES ('665', '29', 'Oyo East', null, '0', '0');
INSERT INTO `area` VALUES ('666', '29', 'Oyo West', null, '0', '0');
INSERT INTO `area` VALUES ('667', '29', 'Saki East', null, '0', '0');
INSERT INTO `area` VALUES ('668', '29', 'Saki West', null, '0', '0');
INSERT INTO `area` VALUES ('669', '29', 'Surulere', null, '0', '0');
INSERT INTO `area` VALUES ('670', '30', 'Barikin Ladi', null, '0', '0');
INSERT INTO `area` VALUES ('671', '30', 'Bassa', null, '0', '0');
INSERT INTO `area` VALUES ('672', '30', 'Bokkos', null, '0', '0');
INSERT INTO `area` VALUES ('673', '30', 'Jos East', null, '0', '0');
INSERT INTO `area` VALUES ('674', '30', 'Jos North', null, '0', '0');
INSERT INTO `area` VALUES ('675', '30', 'Jos South', null, '0', '0');
INSERT INTO `area` VALUES ('676', '30', 'Kanam', null, '0', '0');
INSERT INTO `area` VALUES ('677', '30', 'Kanke', null, '0', '0');
INSERT INTO `area` VALUES ('678', '30', 'Langtang North', null, '0', '0');
INSERT INTO `area` VALUES ('679', '30', 'Langtang South', null, '0', '0');
INSERT INTO `area` VALUES ('680', '30', 'Mangu', null, '0', '0');
INSERT INTO `area` VALUES ('681', '30', 'Mikang', null, '0', '0');
INSERT INTO `area` VALUES ('682', '30', 'Pankshin', null, '0', '0');
INSERT INTO `area` VALUES ('683', '30', 'Qua\'an Pan', null, '0', '0');
INSERT INTO `area` VALUES ('684', '30', 'Riyom', null, '0', '0');
INSERT INTO `area` VALUES ('685', '30', 'Shendam', null, '0', '0');
INSERT INTO `area` VALUES ('686', '30', 'Wase', null, '0', '0');
INSERT INTO `area` VALUES ('687', '31', 'Abua/Odual', null, '0', '0');
INSERT INTO `area` VALUES ('688', '31', 'Ahoada East', null, '0', '0');
INSERT INTO `area` VALUES ('689', '31', 'Ahoada West', null, '0', '0');
INSERT INTO `area` VALUES ('690', '31', 'Akuku Toru', null, '0', '0');
INSERT INTO `area` VALUES ('691', '31', 'Andoni', null, '0', '0');
INSERT INTO `area` VALUES ('692', '31', 'Asari-Toru', null, '0', '0');
INSERT INTO `area` VALUES ('693', '31', 'Bonny', null, '0', '0');
INSERT INTO `area` VALUES ('694', '31', 'Degema', null, '0', '0');
INSERT INTO `area` VALUES ('695', '31', 'Emohua', null, '0', '0');
INSERT INTO `area` VALUES ('696', '31', 'Eleme', null, '0', '0');
INSERT INTO `area` VALUES ('697', '31', 'Etche', null, '0', '0');
INSERT INTO `area` VALUES ('698', '31', 'Gokana', null, '0', '0');
INSERT INTO `area` VALUES ('699', '31', 'Ikwerre', null, '0', '0');
INSERT INTO `area` VALUES ('700', '31', 'Khana', null, '0', '0');
INSERT INTO `area` VALUES ('701', '31', 'Obia/Akpor', null, '0', '0');
INSERT INTO `area` VALUES ('702', '31', 'Ogba/Egbema/Ndoni', null, '0', '0');
INSERT INTO `area` VALUES ('703', '31', 'Ogu/Bolo', null, '0', '0');
INSERT INTO `area` VALUES ('704', '31', 'Okrika', null, '0', '0');
INSERT INTO `area` VALUES ('705', '31', 'Omumma', null, '0', '0');
INSERT INTO `area` VALUES ('706', '31', 'Opobo/Nkoro', null, '0', '0');
INSERT INTO `area` VALUES ('707', '31', 'Oyigbo', null, '0', '0');
INSERT INTO `area` VALUES ('708', '31', 'Port-Harcourt', null, '0', '0');
INSERT INTO `area` VALUES ('709', '31', 'Tai', null, '0', '0');
INSERT INTO `area` VALUES ('710', '32', 'Binji', null, '0', '0');
INSERT INTO `area` VALUES ('711', '32', 'Bodinga', null, '0', '0');
INSERT INTO `area` VALUES ('712', '32', 'Dange-shnsi', null, '0', '0');
INSERT INTO `area` VALUES ('713', '32', 'Gada', null, '0', '0');
INSERT INTO `area` VALUES ('714', '32', 'Goronyo', null, '0', '0');
INSERT INTO `area` VALUES ('715', '32', 'Gudu', null, '0', '0');
INSERT INTO `area` VALUES ('716', '32', 'Gawabawa', null, '0', '0');
INSERT INTO `area` VALUES ('717', '32', 'Illela', null, '0', '0');
INSERT INTO `area` VALUES ('718', '32', 'Isa', null, '0', '0');
INSERT INTO `area` VALUES ('719', '32', 'Kware', null, '0', '0');
INSERT INTO `area` VALUES ('720', '32', 'kebbe', null, '0', '0');
INSERT INTO `area` VALUES ('721', '32', 'Rabah', null, '0', '0');
INSERT INTO `area` VALUES ('722', '32', 'Sabon Birni', null, '0', '0');
INSERT INTO `area` VALUES ('723', '32', 'Shagari', null, '0', '0');
INSERT INTO `area` VALUES ('724', '32', 'Silame', null, '0', '0');
INSERT INTO `area` VALUES ('725', '32', 'Sokoto North', null, '0', '0');
INSERT INTO `area` VALUES ('726', '32', 'Sokoto South', null, '0', '0');
INSERT INTO `area` VALUES ('727', '32', 'Tambuwal', null, '0', '0');
INSERT INTO `area` VALUES ('728', '32', 'Tqngaza', null, '0', '0');
INSERT INTO `area` VALUES ('729', '32', 'Tureta', null, '0', '0');
INSERT INTO `area` VALUES ('730', '32', 'Wamako', null, '0', '0');
INSERT INTO `area` VALUES ('731', '32', 'Wurno', null, '0', '0');
INSERT INTO `area` VALUES ('732', '32', 'Yabo', null, '0', '0');
INSERT INTO `area` VALUES ('733', '33', 'Ardo-kola', null, '0', '0');
INSERT INTO `area` VALUES ('734', '33', 'Bali', null, '0', '0');
INSERT INTO `area` VALUES ('735', '33', 'Donga', null, '0', '0');
INSERT INTO `area` VALUES ('736', '33', 'Gashaka', null, '0', '0');
INSERT INTO `area` VALUES ('737', '33', 'Cassol', null, '0', '0');
INSERT INTO `area` VALUES ('738', '33', 'Ibi', null, '0', '0');
INSERT INTO `area` VALUES ('739', '33', 'Jalingo', null, '0', '0');
INSERT INTO `area` VALUES ('740', '33', 'Karin-Lamido', null, '0', '0');
INSERT INTO `area` VALUES ('741', '33', 'Kurmi', null, '0', '0');
INSERT INTO `area` VALUES ('742', '33', 'Lau', null, '0', '0');
INSERT INTO `area` VALUES ('743', '33', 'Sardauna', null, '0', '0');
INSERT INTO `area` VALUES ('744', '33', 'Takum', null, '0', '0');
INSERT INTO `area` VALUES ('745', '33', 'Ussa', null, '0', '0');
INSERT INTO `area` VALUES ('746', '33', 'Wukari', null, '0', '0');
INSERT INTO `area` VALUES ('747', '33', 'Yorro', null, '0', '0');
INSERT INTO `area` VALUES ('748', '33', 'Zing', null, '0', '0');
INSERT INTO `area` VALUES ('749', '34', 'Ardo-kola', null, '0', '0');
INSERT INTO `area` VALUES ('750', '34', 'Bali', null, '0', '0');
INSERT INTO `area` VALUES ('751', '34', 'Donga', null, '0', '0');
INSERT INTO `area` VALUES ('752', '34', 'Gashaka', null, '0', '0');
INSERT INTO `area` VALUES ('753', '34', 'Cassol', null, '0', '0');
INSERT INTO `area` VALUES ('754', '34', 'Ibi', null, '0', '0');
INSERT INTO `area` VALUES ('755', '34', 'Jalingo', null, '0', '0');
INSERT INTO `area` VALUES ('756', '34', 'Karin-Lamido', null, '0', '0');
INSERT INTO `area` VALUES ('757', '34', 'Kurmi', null, '0', '0');
INSERT INTO `area` VALUES ('758', '34', 'Lau', null, '0', '0');
INSERT INTO `area` VALUES ('759', '34', 'Sardauna', null, '0', '0');
INSERT INTO `area` VALUES ('760', '34', 'Takum', null, '0', '0');
INSERT INTO `area` VALUES ('761', '34', 'Ussa', null, '0', '0');
INSERT INTO `area` VALUES ('762', '34', 'Wukari', null, '0', '0');
INSERT INTO `area` VALUES ('763', '34', 'Yorro', null, '0', '0');
INSERT INTO `area` VALUES ('764', '34', 'Zing', null, '0', '0');
INSERT INTO `area` VALUES ('765', '35', 'Anka', null, '0', '0');
INSERT INTO `area` VALUES ('766', '35', 'Bakura', null, '0', '0');
INSERT INTO `area` VALUES ('767', '35', 'Birnin Magaji', null, '0', '0');
INSERT INTO `area` VALUES ('768', '35', 'Bukkuyum', null, '0', '0');
INSERT INTO `area` VALUES ('769', '35', 'Bungudu', null, '0', '0');
INSERT INTO `area` VALUES ('770', '35', 'Gummi', null, '0', '0');
INSERT INTO `area` VALUES ('771', '35', 'Gusau', null, '0', '0');
INSERT INTO `area` VALUES ('772', '35', 'Kaura', null, '0', '0');
INSERT INTO `area` VALUES ('773', '35', 'Namoda', null, '0', '0');
INSERT INTO `area` VALUES ('774', '35', 'Maradun', null, '0', '0');
INSERT INTO `area` VALUES ('775', '35', 'Maru', null, '0', '0');
INSERT INTO `area` VALUES ('776', '35', 'Shinkafi', null, '0', '0');
INSERT INTO `area` VALUES ('777', '35', 'Talata Mafara', null, '0', '0');
INSERT INTO `area` VALUES ('778', '35', 'Tsafe', null, '0', '0');
INSERT INTO `area` VALUES ('779', '35', 'Zurmi', null, '0', '0');
INSERT INTO `area` VALUES ('799', '23', 'Victoria Island', null, '0', '0');
INSERT INTO `area` VALUES ('800', '23', 'Ikoyi', null, '0', '0');
INSERT INTO `area` VALUES ('801', '23', 'Ajah', null, '0', '0');
INSERT INTO `area` VALUES ('802', '23', 'VGC', null, '0', '0');
INSERT INTO `area` VALUES ('803', '23', 'Festac', null, '0', '0');
INSERT INTO `area` VALUES ('804', '23', 'Yaba', null, '0', '0');
INSERT INTO `area` VALUES ('805', '23', 'Ebute Metta', null, '0', '0');
INSERT INTO `area` VALUES ('806', '23', 'Oniru', null, '0', '0');
INSERT INTO `area` VALUES ('807', '23', 'Ogudu GRA', null, '0', '0');
INSERT INTO `area` VALUES ('808', '23', 'Anthony Village', null, '0', '0');
INSERT INTO `area` VALUES ('809', '23', 'Magodo', null, '0', '0');
INSERT INTO `area` VALUES ('810', '23', 'Isolo', null, '0', '0');
INSERT INTO `area` VALUES ('811', '23', 'Victoria Garden City', null, '0', '0');
INSERT INTO `area` VALUES ('812', '23', 'Oshodi', null, '0', '0');
INSERT INTO `area` VALUES ('813', '23', 'Chevron', null, '0', '0');
INSERT INTO `area` VALUES ('814', '23', 'Ijesha', null, '0', '0');



-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `item_name` (`item_name`),
  CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_ibfk_3` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('1', 'admin', '1', '1474894341', '0');


-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('admin', '1', 'Admin', 'userRole', null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('archiving_bag', '2', 'Archiving Bag', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('archiving_donor', '2', 'Archiving Donor', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('archiving_location', '2', 'Archiving Location', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('archiving_staff', '2', 'Archiving Staff', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('disable_bag', '2', 'Disable Bag', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('disable_donor', '2', 'Disable Donor', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('disable_location', '2', 'Disable Location', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('disable_staff', '2', 'Disable Staff', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('donor', '1', 'Donor', 'userRole', null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('edit_bag', '2', 'Edit Bag', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('edit_donor', '2', 'Edit Donor', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('edit_location', '2', 'Edit Location', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('edit_staff', '2', 'Edit Staff', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('register_bag', '2', 'Register Bag', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('register_donor', '2', 'Register Donor', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('register_location', '2', 'Register Location', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('register_staff', '2', 'Register Staff', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('search_bag', '2', 'Search Bag', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('search_donor', '2', 'Search Donor', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('search_location', '2', 'Search Location', null, null, '1474884278', '1474884278');
INSERT INTO `auth_item` VALUES ('search_staff', '2', 'Search Staff', null, null, '1474884277', '1474884277');
INSERT INTO `auth_item` VALUES ('staff', '1', 'Staff', 'userRole', null, '1474884277', '1474884277');

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('admin', 'archiving_location');
INSERT INTO `auth_item_child` VALUES ('admin', 'archiving_staff');
INSERT INTO `auth_item_child` VALUES ('admin', 'disable_location');
INSERT INTO `auth_item_child` VALUES ('admin', 'disable_staff');
INSERT INTO `auth_item_child` VALUES ('admin', 'edit_location');
INSERT INTO `auth_item_child` VALUES ('admin', 'edit_staff');
INSERT INTO `auth_item_child` VALUES ('admin', 'register_location');
INSERT INTO `auth_item_child` VALUES ('admin', 'register_staff');
INSERT INTO `auth_item_child` VALUES ('admin', 'search_location');
INSERT INTO `auth_item_child` VALUES ('admin', 'search_staff');
INSERT INTO `auth_item_child` VALUES ('admin', 'staff');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------
INSERT INTO `auth_rule` VALUES ('userRole', 'O:30:\"common\\components\\UserRoleRule\":3:{s:4:\"name\";s:8:\"userRole\";s:9:\"createdAt\";i:1474884277;s:9:\"updatedAt\";i:1474884277;}', '1474884277', '1474884277');

-- ----------------------------
-- Table structure for blood_drive
-- ----------------------------
DROP TABLE IF EXISTS `blood_drive`;
CREATE TABLE `blood_drive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `donation_date` int(11) NOT NULL,
  `donor_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `screening_agency_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blood_bag_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes_bag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_for_screening` tinyint(1) NOT NULL,
  `date_sent` int(11) DEFAULT NULL,
  `screening_status` smallint(1) DEFAULT NULL,
  `date_screen` int(11) DEFAULT NULL,
  `screen_result` smallint(1) DEFAULT NULL,
  `date_expiry` int(11) DEFAULT NULL,
  `notes_screening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  KEY `blood_drive_ibfk_4` (`donor_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `blood_drive_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blood_drive_ibfk_4` FOREIGN KEY (`donor_id`) REFERENCES `donor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blood_drive_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nameIndex` (`name`) USING BTREE,
  KEY `area_id` (`area_id`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=964 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '1', 'Yola', null, '0');
INSERT INTO `city` VALUES ('2', '2', 'Uyo', null, '0');
INSERT INTO `city` VALUES ('3', '3', 'Awka', null, '0');
INSERT INTO `city` VALUES ('4', '4', 'Bauchi', null, '0');
INSERT INTO `city` VALUES ('5', '5', 'Yenagoa', null, '0');
INSERT INTO `city` VALUES ('6', '6', 'Makurdi', null, '0');
INSERT INTO `city` VALUES ('7', '7', 'Maiduguri', null, '0');
INSERT INTO `city` VALUES ('8', '8', 'Calabar', null, '0');
INSERT INTO `city` VALUES ('9', '9', 'Asaba', null, '0');
INSERT INTO `city` VALUES ('10', '10', 'Abakaliki', null, '0');
INSERT INTO `city` VALUES ('11', '11', 'Benin City', null, '0');
INSERT INTO `city` VALUES ('12', '12', 'Ado Ekiti', null, '0');
INSERT INTO `city` VALUES ('13', '13', 'Enugu', null, '0');
INSERT INTO `city` VALUES ('14', '14', 'Gombe', null, '0');
INSERT INTO `city` VALUES ('15', '15', 'Owerri', null, '0');
INSERT INTO `city` VALUES ('16', '16', 'Dutse', null, '0');
INSERT INTO `city` VALUES ('17', '17', 'Kaduna', null, '0');
INSERT INTO `city` VALUES ('18', '18', 'Kano', null, '0');
INSERT INTO `city` VALUES ('19', '19', 'Katsina', null, '0');
INSERT INTO `city` VALUES ('20', '20', 'Birnin Kebbi', null, '0');
INSERT INTO `city` VALUES ('21', '21', 'Lokoja', null, '0');
INSERT INTO `city` VALUES ('22', '22', 'Ilorin', null, '0');
INSERT INTO `city` VALUES ('23', '23', 'Ikeja', null, '0');
INSERT INTO `city` VALUES ('24', '24', 'Lafia', null, '0');
INSERT INTO `city` VALUES ('25', '25', 'Minna', null, '0');
INSERT INTO `city` VALUES ('26', '26', 'Abeokuta', null, '0');
INSERT INTO `city` VALUES ('27', '27', 'Akure', null, '0');
INSERT INTO `city` VALUES ('28', '28', 'Oshogbo', null, '0');
INSERT INTO `city` VALUES ('29', '29', 'Ibadan', null, '0');
INSERT INTO `city` VALUES ('30', '30', 'Jos', null, '0');
INSERT INTO `city` VALUES ('31', '31', 'Port Harcourt', null, '0');
INSERT INTO `city` VALUES ('32', '32', 'Sokoto', null, '0');
INSERT INTO `city` VALUES ('33', '33', 'Jalingo', null, '0');
INSERT INTO `city` VALUES ('34', '34', 'Damaturu', null, '0');
INSERT INTO `city` VALUES ('35', '35', 'Gusau', null, '0');
INSERT INTO `city` VALUES ('36', '36', 'Demsa', null, '0');
INSERT INTO `city` VALUES ('37', '37', 'Fufore', null, '0');
INSERT INTO `city` VALUES ('38', '38', 'Ganaye', null, '0');
INSERT INTO `city` VALUES ('39', '39', 'Gireri', null, '0');
INSERT INTO `city` VALUES ('40', '40', 'Gombi', null, '0');
INSERT INTO `city` VALUES ('41', '41', 'Guyuk', null, '0');
INSERT INTO `city` VALUES ('42', '42', 'Hong', null, '0');
INSERT INTO `city` VALUES ('43', '43', 'Jada', null, '0');
INSERT INTO `city` VALUES ('44', '44', 'Lamurde', null, '0');
INSERT INTO `city` VALUES ('45', '45', 'Madagali', null, '0');
INSERT INTO `city` VALUES ('46', '46', 'Maiha', null, '0');
INSERT INTO `city` VALUES ('47', '47', 'Mayo-Belwa', null, '0');
INSERT INTO `city` VALUES ('48', '48', 'Michika', null, '0');
INSERT INTO `city` VALUES ('49', '49', 'Mubi North', null, '0');
INSERT INTO `city` VALUES ('50', '50', 'Mubi South', null, '0');
INSERT INTO `city` VALUES ('51', '51', 'Numan', null, '0');
INSERT INTO `city` VALUES ('52', '52', 'Shelleng', null, '0');
INSERT INTO `city` VALUES ('53', '53', 'Song', null, '0');
INSERT INTO `city` VALUES ('54', '54', 'Toungo', null, '0');
INSERT INTO `city` VALUES ('55', '55', 'Yola North', null, '0');
INSERT INTO `city` VALUES ('56', '56', 'Yola South', null, '0');
INSERT INTO `city` VALUES ('57', '57', 'Abak', null, '0');
INSERT INTO `city` VALUES ('58', '58', 'Eastern Obolo', null, '0');
INSERT INTO `city` VALUES ('59', '59', 'Eket', null, '0');
INSERT INTO `city` VALUES ('60', '60', 'Esit Eket', null, '0');
INSERT INTO `city` VALUES ('61', '61', 'Essien Udim', null, '0');
INSERT INTO `city` VALUES ('62', '62', 'Etim Ekpo', null, '0');
INSERT INTO `city` VALUES ('63', '63', 'Etinan', null, '0');
INSERT INTO `city` VALUES ('64', '64', 'Ibeno', null, '0');
INSERT INTO `city` VALUES ('65', '65', 'Ibesikpo Asutan', null, '0');
INSERT INTO `city` VALUES ('66', '66', 'Ibiono Ibom', null, '0');
INSERT INTO `city` VALUES ('67', '67', 'Ika', null, '0');
INSERT INTO `city` VALUES ('68', '68', 'Ikono', null, '0');
INSERT INTO `city` VALUES ('69', '69', 'Ikot Abasi', null, '0');
INSERT INTO `city` VALUES ('70', '70', 'Ikot Ekpene', null, '0');
INSERT INTO `city` VALUES ('71', '71', 'Ini', null, '0');
INSERT INTO `city` VALUES ('72', '72', 'Itu', null, '0');
INSERT INTO `city` VALUES ('73', '73', 'Mbo', null, '0');
INSERT INTO `city` VALUES ('74', '74', 'Mkpat Enin', null, '0');
INSERT INTO `city` VALUES ('75', '75', 'Nsit Atai', null, '0');
INSERT INTO `city` VALUES ('76', '76', 'Nsit Ibom', null, '0');
INSERT INTO `city` VALUES ('77', '77', 'Nsit Ubium', null, '0');
INSERT INTO `city` VALUES ('78', '78', 'Obot Akara', null, '0');
INSERT INTO `city` VALUES ('79', '79', 'Okobo', null, '0');
INSERT INTO `city` VALUES ('80', '80', 'Onna', null, '0');
INSERT INTO `city` VALUES ('81', '81', 'Oron', null, '0');
INSERT INTO `city` VALUES ('82', '82', 'Oruk Anam', null, '0');
INSERT INTO `city` VALUES ('83', '83', 'Udung Uko', null, '0');
INSERT INTO `city` VALUES ('84', '84', 'Ukanafun', null, '0');
INSERT INTO `city` VALUES ('85', '85', 'Uruan', null, '0');
INSERT INTO `city` VALUES ('86', '86', 'Urue-Offong/Oruko', null, '0');
INSERT INTO `city` VALUES ('87', '87', 'Uyo', null, '0');
INSERT INTO `city` VALUES ('88', '88', 'Aguata', null, '0');
INSERT INTO `city` VALUES ('89', '89', 'Anambra East', null, '0');
INSERT INTO `city` VALUES ('90', '90', 'Anambra West', null, '0');
INSERT INTO `city` VALUES ('91', '91', 'Anaocha', null, '0');
INSERT INTO `city` VALUES ('92', '92', 'Awka North', null, '0');
INSERT INTO `city` VALUES ('93', '93', 'Awka South', null, '0');
INSERT INTO `city` VALUES ('94', '94', 'Ayamelum', null, '0');
INSERT INTO `city` VALUES ('95', '95', 'Dunukofia', null, '0');
INSERT INTO `city` VALUES ('96', '96', 'Ekwusigo', null, '0');
INSERT INTO `city` VALUES ('97', '97', 'Idemili North', null, '0');
INSERT INTO `city` VALUES ('98', '98', 'Idemili South', null, '0');
INSERT INTO `city` VALUES ('99', '99', 'Ihiala', null, '0');
INSERT INTO `city` VALUES ('100', '100', 'Njikoka', null, '0');
INSERT INTO `city` VALUES ('101', '101', 'Nnewi North', null, '0');
INSERT INTO `city` VALUES ('102', '102', 'Nnewi South', null, '0');
INSERT INTO `city` VALUES ('103', '103', 'Ogbaru', null, '0');
INSERT INTO `city` VALUES ('104', '104', 'Onitsha North', null, '0');
INSERT INTO `city` VALUES ('105', '105', 'Onitsha South', null, '0');
INSERT INTO `city` VALUES ('106', '106', 'Orumba North', null, '0');
INSERT INTO `city` VALUES ('107', '107', 'Orumba South', null, '0');
INSERT INTO `city` VALUES ('108', '108', 'Oyi', null, '0');
INSERT INTO `city` VALUES ('109', '109', 'Alkaleri', null, '0');
INSERT INTO `city` VALUES ('110', '110', 'Bauchi', null, '0');
INSERT INTO `city` VALUES ('111', '111', 'Bogoro', null, '0');
INSERT INTO `city` VALUES ('112', '112', 'Damban', null, '0');
INSERT INTO `city` VALUES ('113', '113', 'Darazo', null, '0');
INSERT INTO `city` VALUES ('114', '114', 'Dass', null, '0');
INSERT INTO `city` VALUES ('115', '115', 'Ganjuwa', null, '0');
INSERT INTO `city` VALUES ('116', '116', 'Giade', null, '0');
INSERT INTO `city` VALUES ('117', '117', 'Itas/Gadau', null, '0');
INSERT INTO `city` VALUES ('118', '118', 'Jama\'are', null, '0');
INSERT INTO `city` VALUES ('119', '119', 'Katagum', null, '0');
INSERT INTO `city` VALUES ('120', '120', 'Kirfi', null, '0');
INSERT INTO `city` VALUES ('121', '121', 'Misau', null, '0');
INSERT INTO `city` VALUES ('122', '122', 'Ningi', null, '0');
INSERT INTO `city` VALUES ('123', '123', 'Shira', null, '0');
INSERT INTO `city` VALUES ('124', '124', 'Tafawa-Balewa', null, '0');
INSERT INTO `city` VALUES ('125', '125', 'Toro', null, '0');
INSERT INTO `city` VALUES ('126', '126', 'Warji', null, '0');
INSERT INTO `city` VALUES ('127', '127', 'Zaki', null, '0');
INSERT INTO `city` VALUES ('128', '128', 'Brass', null, '0');
INSERT INTO `city` VALUES ('129', '129', 'Ekeremor', null, '0');
INSERT INTO `city` VALUES ('130', '130', 'Kolokuma/Opokuma', null, '0');
INSERT INTO `city` VALUES ('131', '131', 'Nembe', null, '0');
INSERT INTO `city` VALUES ('132', '132', 'Ogbia', null, '0');
INSERT INTO `city` VALUES ('133', '133', 'Sagbama', null, '0');
INSERT INTO `city` VALUES ('134', '134', 'Southern Jaw', null, '0');
INSERT INTO `city` VALUES ('135', '135', 'Yenegoa', null, '0');
INSERT INTO `city` VALUES ('136', '136', 'Ado', null, '0');
INSERT INTO `city` VALUES ('137', '137', 'Agatu', null, '0');
INSERT INTO `city` VALUES ('138', '138', 'Apa', null, '0');
INSERT INTO `city` VALUES ('139', '139', 'Buruku', null, '0');
INSERT INTO `city` VALUES ('140', '140', 'Gboko', null, '0');
INSERT INTO `city` VALUES ('141', '141', 'Guma', null, '0');
INSERT INTO `city` VALUES ('142', '142', 'Gwer East', null, '0');
INSERT INTO `city` VALUES ('143', '143', 'Gwer West', null, '0');
INSERT INTO `city` VALUES ('144', '144', 'Katsina-Ala', null, '0');
INSERT INTO `city` VALUES ('145', '145', 'Konshisha', null, '0');
INSERT INTO `city` VALUES ('146', '146', 'Kwande', null, '0');
INSERT INTO `city` VALUES ('147', '147', 'Logo', null, '0');
INSERT INTO `city` VALUES ('148', '148', 'Makurdi', null, '0');
INSERT INTO `city` VALUES ('149', '149', 'Obi', null, '0');
INSERT INTO `city` VALUES ('150', '150', 'Ogbadibo', null, '0');
INSERT INTO `city` VALUES ('151', '151', 'Oju', null, '0');
INSERT INTO `city` VALUES ('152', '152', 'Okpokwu', null, '0');
INSERT INTO `city` VALUES ('153', '153', 'Ohimini', null, '0');
INSERT INTO `city` VALUES ('154', '154', 'Oturkpo', null, '0');
INSERT INTO `city` VALUES ('155', '155', 'Tarka', null, '0');
INSERT INTO `city` VALUES ('156', '156', 'Ukum', null, '0');
INSERT INTO `city` VALUES ('157', '157', 'Ushongo', null, '0');
INSERT INTO `city` VALUES ('158', '158', 'Vandeikya', null, '0');
INSERT INTO `city` VALUES ('159', '159', 'Abadam', null, '0');
INSERT INTO `city` VALUES ('160', '160', 'Askira/Uba', null, '0');
INSERT INTO `city` VALUES ('161', '161', 'Bama', null, '0');
INSERT INTO `city` VALUES ('162', '162', 'Bayo', null, '0');
INSERT INTO `city` VALUES ('163', '163', 'Biu', null, '0');
INSERT INTO `city` VALUES ('164', '164', 'Chibok', null, '0');
INSERT INTO `city` VALUES ('165', '165', 'Damboa', null, '0');
INSERT INTO `city` VALUES ('166', '166', 'Dikwa', null, '0');
INSERT INTO `city` VALUES ('167', '167', 'Gubio', null, '0');
INSERT INTO `city` VALUES ('168', '168', 'Guzamala', null, '0');
INSERT INTO `city` VALUES ('169', '169', 'Gwoza', null, '0');
INSERT INTO `city` VALUES ('170', '170', 'Hawul', null, '0');
INSERT INTO `city` VALUES ('171', '171', 'Jere', null, '0');
INSERT INTO `city` VALUES ('172', '172', 'Kaga', null, '0');
INSERT INTO `city` VALUES ('173', '173', 'Kala/Balge', null, '0');
INSERT INTO `city` VALUES ('174', '174', 'Konduga', null, '0');
INSERT INTO `city` VALUES ('175', '175', 'Kukawa', null, '0');
INSERT INTO `city` VALUES ('176', '176', 'Kwaya Kusar', null, '0');
INSERT INTO `city` VALUES ('177', '177', 'Mafa', null, '0');
INSERT INTO `city` VALUES ('178', '178', 'Magumeri', null, '0');
INSERT INTO `city` VALUES ('179', '179', 'Maiduguri', null, '0');
INSERT INTO `city` VALUES ('180', '180', 'Marte', null, '0');
INSERT INTO `city` VALUES ('181', '181', 'Mobbar', null, '0');
INSERT INTO `city` VALUES ('182', '182', 'Monguno', null, '0');
INSERT INTO `city` VALUES ('183', '183', 'Ngala', null, '0');
INSERT INTO `city` VALUES ('184', '184', 'Nganzai', null, '0');
INSERT INTO `city` VALUES ('185', '185', 'Shani', null, '0');
INSERT INTO `city` VALUES ('186', '186', 'Akpabuyo', null, '0');
INSERT INTO `city` VALUES ('187', '187', 'Odukpani', null, '0');
INSERT INTO `city` VALUES ('188', '188', 'Akamkpa', null, '0');
INSERT INTO `city` VALUES ('189', '189', 'Biase', null, '0');
INSERT INTO `city` VALUES ('190', '190', 'Abi', null, '0');
INSERT INTO `city` VALUES ('191', '191', 'Ikom', null, '0');
INSERT INTO `city` VALUES ('192', '192', 'Yarkur', null, '0');
INSERT INTO `city` VALUES ('193', '193', 'Odubra', null, '0');
INSERT INTO `city` VALUES ('194', '194', 'Boki', null, '0');
INSERT INTO `city` VALUES ('195', '195', 'Ogoja', null, '0');
INSERT INTO `city` VALUES ('196', '196', 'Yala', null, '0');
INSERT INTO `city` VALUES ('197', '197', 'Obanliku', null, '0');
INSERT INTO `city` VALUES ('198', '198', 'Obudu', null, '0');
INSERT INTO `city` VALUES ('199', '199', 'Calabar South', null, '0');
INSERT INTO `city` VALUES ('200', '200', 'Etung', null, '0');
INSERT INTO `city` VALUES ('201', '201', 'Bekwara', null, '0');
INSERT INTO `city` VALUES ('202', '202', 'Bakassi', null, '0');
INSERT INTO `city` VALUES ('203', '203', 'Calabar Municipality', null, '0');
INSERT INTO `city` VALUES ('204', '204', 'Oshimili', null, '0');
INSERT INTO `city` VALUES ('205', '205', 'Aniocha', null, '0');
INSERT INTO `city` VALUES ('206', '206', 'Aniocha South', null, '0');
INSERT INTO `city` VALUES ('207', '207', 'Ika South', null, '0');
INSERT INTO `city` VALUES ('208', '208', 'Ika North-East', null, '0');
INSERT INTO `city` VALUES ('209', '209', 'Ndokwa West', null, '0');
INSERT INTO `city` VALUES ('210', '210', 'Ndokwa East', null, '0');
INSERT INTO `city` VALUES ('211', '211', 'Isoko south', null, '0');
INSERT INTO `city` VALUES ('212', '212', 'Isoko North', null, '0');
INSERT INTO `city` VALUES ('213', '213', 'Bomadi', null, '0');
INSERT INTO `city` VALUES ('214', '214', 'Burutu', null, '0');
INSERT INTO `city` VALUES ('215', '215', 'Ughelli South', null, '0');
INSERT INTO `city` VALUES ('216', '216', 'Ughelli North', null, '0');
INSERT INTO `city` VALUES ('217', '217', 'Ethiope West', null, '0');
INSERT INTO `city` VALUES ('218', '218', 'Ethiope East', null, '0');
INSERT INTO `city` VALUES ('219', '219', 'Sapele', null, '0');
INSERT INTO `city` VALUES ('220', '220', 'Okpe', null, '0');
INSERT INTO `city` VALUES ('221', '221', 'Warri North', null, '0');
INSERT INTO `city` VALUES ('222', '222', 'Warri South', null, '0');
INSERT INTO `city` VALUES ('223', '223', 'Uvwie', null, '0');
INSERT INTO `city` VALUES ('224', '224', 'Udu', null, '0');
INSERT INTO `city` VALUES ('225', '225', 'Warri Central', null, '0');
INSERT INTO `city` VALUES ('226', '226', 'Ukwani', null, '0');
INSERT INTO `city` VALUES ('227', '227', 'Oshimili North', null, '0');
INSERT INTO `city` VALUES ('228', '228', 'Patani', null, '0');
INSERT INTO `city` VALUES ('229', '229', 'Afikpo South', null, '0');
INSERT INTO `city` VALUES ('230', '230', 'Afikpo North', null, '0');
INSERT INTO `city` VALUES ('231', '231', 'Onicha', null, '0');
INSERT INTO `city` VALUES ('232', '232', 'Ohaozara', null, '0');
INSERT INTO `city` VALUES ('233', '233', 'Abakaliki', null, '0');
INSERT INTO `city` VALUES ('234', '234', 'Ishielu', null, '0');
INSERT INTO `city` VALUES ('235', '235', 'lkwo', null, '0');
INSERT INTO `city` VALUES ('236', '236', 'Ezza', null, '0');
INSERT INTO `city` VALUES ('237', '237', 'Ezza South', null, '0');
INSERT INTO `city` VALUES ('238', '238', 'Ohaukwu', null, '0');
INSERT INTO `city` VALUES ('239', '239', 'Ebonyi', null, '0');
INSERT INTO `city` VALUES ('240', '240', 'Ivo', null, '0');
INSERT INTO `city` VALUES ('241', '241', 'Esan North-East', null, '0');
INSERT INTO `city` VALUES ('242', '242', 'Esan Central', null, '0');
INSERT INTO `city` VALUES ('243', '243', 'Esan West', null, '0');
INSERT INTO `city` VALUES ('244', '244', 'Egor', null, '0');
INSERT INTO `city` VALUES ('245', '245', 'Ukpoba', null, '0');
INSERT INTO `city` VALUES ('246', '246', 'Central', null, '0');
INSERT INTO `city` VALUES ('247', '247', 'Etsako Central', null, '0');
INSERT INTO `city` VALUES ('248', '248', 'Igueben', null, '0');
INSERT INTO `city` VALUES ('249', '249', 'Oredo', null, '0');
INSERT INTO `city` VALUES ('250', '250', 'Ovia Southwest', null, '0');
INSERT INTO `city` VALUES ('251', '251', 'Ovia South-East', null, '0');
INSERT INTO `city` VALUES ('252', '252', 'Orhionwon', null, '0');
INSERT INTO `city` VALUES ('253', '253', 'Uhunmwonde', null, '0');
INSERT INTO `city` VALUES ('254', '254', 'Etsako East', null, '0');
INSERT INTO `city` VALUES ('255', '255', 'Esan South-East', null, '0');
INSERT INTO `city` VALUES ('256', '256', 'Ado', null, '0');
INSERT INTO `city` VALUES ('257', '257', 'Ekiti-East', null, '0');
INSERT INTO `city` VALUES ('258', '258', 'Ekiti-West', null, '0');
INSERT INTO `city` VALUES ('259', '259', 'Emure/Ise/Orun', null, '0');
INSERT INTO `city` VALUES ('260', '260', 'Ekiti South-West', null, '0');
INSERT INTO `city` VALUES ('261', '261', 'Ikare', null, '0');
INSERT INTO `city` VALUES ('262', '262', 'Irepodun', null, '0');
INSERT INTO `city` VALUES ('263', '263', 'Ijero,', null, '0');
INSERT INTO `city` VALUES ('264', '264', 'Ido/Osi', null, '0');
INSERT INTO `city` VALUES ('265', '265', 'Oye', null, '0');
INSERT INTO `city` VALUES ('266', '266', 'Ikole', null, '0');
INSERT INTO `city` VALUES ('267', '267', 'Moba', null, '0');
INSERT INTO `city` VALUES ('268', '268', 'Gbonyin', null, '0');
INSERT INTO `city` VALUES ('269', '269', 'Efon', null, '0');
INSERT INTO `city` VALUES ('270', '270', 'Ise/Orun', null, '0');
INSERT INTO `city` VALUES ('271', '271', 'Ilejemeje.', null, '0');
INSERT INTO `city` VALUES ('272', '272', 'Enugu South,', null, '0');
INSERT INTO `city` VALUES ('273', '273', 'Igbo-Eze South', null, '0');
INSERT INTO `city` VALUES ('274', '274', 'Enugu North', null, '0');
INSERT INTO `city` VALUES ('275', '275', 'Nkanu', null, '0');
INSERT INTO `city` VALUES ('276', '276', 'Udi Agwu', null, '0');
INSERT INTO `city` VALUES ('277', '277', 'Oji-River', null, '0');
INSERT INTO `city` VALUES ('278', '278', 'Ezeagu', null, '0');
INSERT INTO `city` VALUES ('279', '279', 'IgboEze North', null, '0');
INSERT INTO `city` VALUES ('280', '280', 'Isi-Uzo', null, '0');
INSERT INTO `city` VALUES ('281', '281', 'Nsukka', null, '0');
INSERT INTO `city` VALUES ('282', '282', 'Igbo-Ekiti', null, '0');
INSERT INTO `city` VALUES ('283', '283', 'Uzo-Uwani', null, '0');
INSERT INTO `city` VALUES ('284', '284', 'Enugu Eas', null, '0');
INSERT INTO `city` VALUES ('285', '285', 'Aninri', null, '0');
INSERT INTO `city` VALUES ('286', '286', 'Nkanu East', null, '0');
INSERT INTO `city` VALUES ('287', '287', 'Udenu.', null, '0');
INSERT INTO `city` VALUES ('288', '288', 'Akko', null, '0');
INSERT INTO `city` VALUES ('289', '289', 'Balanga', null, '0');
INSERT INTO `city` VALUES ('290', '290', 'Billiri', null, '0');
INSERT INTO `city` VALUES ('291', '291', 'Dukku', null, '0');
INSERT INTO `city` VALUES ('292', '292', 'Kaltungo', null, '0');
INSERT INTO `city` VALUES ('293', '293', 'Kwami', null, '0');
INSERT INTO `city` VALUES ('294', '294', 'Shomgom', null, '0');
INSERT INTO `city` VALUES ('295', '295', 'Funakaye', null, '0');
INSERT INTO `city` VALUES ('296', '296', 'Gombe', null, '0');
INSERT INTO `city` VALUES ('297', '297', 'Nafada/Bajoga', null, '0');
INSERT INTO `city` VALUES ('298', '298', 'Yamaltu/Delta.', null, '0');
INSERT INTO `city` VALUES ('299', '299', 'Aboh-Mbaise', null, '0');
INSERT INTO `city` VALUES ('300', '300', 'Ahiazu-Mbaise', null, '0');
INSERT INTO `city` VALUES ('301', '301', 'Ehime-Mbano', null, '0');
INSERT INTO `city` VALUES ('302', '302', 'Ezinihitte', null, '0');
INSERT INTO `city` VALUES ('303', '303', 'Ideato North', null, '0');
INSERT INTO `city` VALUES ('304', '304', 'Ideato South', null, '0');
INSERT INTO `city` VALUES ('305', '305', 'Ihitte/Uboma', null, '0');
INSERT INTO `city` VALUES ('306', '306', 'Ikeduru', null, '0');
INSERT INTO `city` VALUES ('307', '307', 'Isiala Mbano', null, '0');
INSERT INTO `city` VALUES ('308', '308', 'Isu', null, '0');
INSERT INTO `city` VALUES ('309', '309', 'Mbaitoli', null, '0');
INSERT INTO `city` VALUES ('310', '310', 'Mbaitoli', null, '0');
INSERT INTO `city` VALUES ('311', '311', 'Ngor-Okpala', null, '0');
INSERT INTO `city` VALUES ('312', '312', 'Njaba', null, '0');
INSERT INTO `city` VALUES ('313', '313', 'Nwangele', null, '0');
INSERT INTO `city` VALUES ('314', '314', 'Nkwerre', null, '0');
INSERT INTO `city` VALUES ('315', '315', 'Obowo', null, '0');
INSERT INTO `city` VALUES ('316', '316', 'Oguta', null, '0');
INSERT INTO `city` VALUES ('317', '317', 'Ohaji/Egbema', null, '0');
INSERT INTO `city` VALUES ('318', '318', 'Okigwe', null, '0');
INSERT INTO `city` VALUES ('319', '319', 'Orlu', null, '0');
INSERT INTO `city` VALUES ('320', '320', 'Orsu', null, '0');
INSERT INTO `city` VALUES ('321', '321', 'Oru East', null, '0');
INSERT INTO `city` VALUES ('322', '322', 'Oru West', null, '0');
INSERT INTO `city` VALUES ('323', '323', 'Owerri-Municipal', null, '0');
INSERT INTO `city` VALUES ('324', '324', 'Owerri North', null, '0');
INSERT INTO `city` VALUES ('325', '325', 'Owerri West', null, '0');
INSERT INTO `city` VALUES ('326', '326', 'Auyo', null, '0');
INSERT INTO `city` VALUES ('327', '327', 'Babura', null, '0');
INSERT INTO `city` VALUES ('328', '328', 'Birni Kudu', null, '0');
INSERT INTO `city` VALUES ('329', '329', 'Biriniwa', null, '0');
INSERT INTO `city` VALUES ('330', '330', 'Buji', null, '0');
INSERT INTO `city` VALUES ('331', '331', 'Dutse', null, '0');
INSERT INTO `city` VALUES ('332', '332', 'Gagarawa', null, '0');
INSERT INTO `city` VALUES ('333', '333', 'Garki', null, '0');
INSERT INTO `city` VALUES ('334', '334', 'Gumel', null, '0');
INSERT INTO `city` VALUES ('335', '335', 'Guri', null, '0');
INSERT INTO `city` VALUES ('336', '336', 'Gwaram', null, '0');
INSERT INTO `city` VALUES ('337', '337', 'Gwiwa', null, '0');
INSERT INTO `city` VALUES ('338', '338', 'Hadejia', null, '0');
INSERT INTO `city` VALUES ('339', '339', 'Jahun', null, '0');
INSERT INTO `city` VALUES ('340', '340', 'Kafin Hausa', null, '0');
INSERT INTO `city` VALUES ('341', '341', 'Kaugama Kazaure', null, '0');
INSERT INTO `city` VALUES ('342', '342', 'Kiri Kasamma', null, '0');
INSERT INTO `city` VALUES ('343', '343', 'Kiyawa', null, '0');
INSERT INTO `city` VALUES ('344', '344', 'Maigatari', null, '0');
INSERT INTO `city` VALUES ('345', '345', 'Malam Madori', null, '0');
INSERT INTO `city` VALUES ('346', '346', 'Miga', null, '0');
INSERT INTO `city` VALUES ('347', '347', 'Ringim', null, '0');
INSERT INTO `city` VALUES ('348', '348', 'Roni', null, '0');
INSERT INTO `city` VALUES ('349', '349', 'Sule-Tankarkar', null, '0');
INSERT INTO `city` VALUES ('350', '350', 'Taura', null, '0');
INSERT INTO `city` VALUES ('351', '351', 'Yankwashi', null, '0');
INSERT INTO `city` VALUES ('352', '352', 'Birni-Gwari', null, '0');
INSERT INTO `city` VALUES ('353', '353', 'Chikun', null, '0');
INSERT INTO `city` VALUES ('354', '354', 'Giwa', null, '0');
INSERT INTO `city` VALUES ('355', '355', 'Igabi', null, '0');
INSERT INTO `city` VALUES ('356', '356', 'Ikara', null, '0');
INSERT INTO `city` VALUES ('357', '357', 'jaba', null, '0');
INSERT INTO `city` VALUES ('358', '358', 'Jema\'a', null, '0');
INSERT INTO `city` VALUES ('359', '359', 'Kachia', null, '0');
INSERT INTO `city` VALUES ('360', '360', 'Kaduna North', null, '0');
INSERT INTO `city` VALUES ('361', '361', 'Kaduna South', null, '0');
INSERT INTO `city` VALUES ('362', '362', 'Kagarko', null, '0');
INSERT INTO `city` VALUES ('363', '363', 'Kajuru', null, '0');
INSERT INTO `city` VALUES ('364', '364', 'Kaura', null, '0');
INSERT INTO `city` VALUES ('365', '365', 'Kauru', null, '0');
INSERT INTO `city` VALUES ('366', '366', 'Kubau', null, '0');
INSERT INTO `city` VALUES ('367', '367', 'Kudan', null, '0');
INSERT INTO `city` VALUES ('368', '368', 'Lere', null, '0');
INSERT INTO `city` VALUES ('369', '369', 'Makarfi', null, '0');
INSERT INTO `city` VALUES ('370', '370', 'Sabon-Gari', null, '0');
INSERT INTO `city` VALUES ('371', '371', 'Sanga', null, '0');
INSERT INTO `city` VALUES ('372', '372', 'Soba', null, '0');
INSERT INTO `city` VALUES ('373', '373', 'Zango-Kataf', null, '0');
INSERT INTO `city` VALUES ('374', '374', 'Zaria', null, '0');
INSERT INTO `city` VALUES ('375', '375', 'Ajingi', null, '0');
INSERT INTO `city` VALUES ('376', '376', 'Albasu', null, '0');
INSERT INTO `city` VALUES ('377', '377', 'Bagwai', null, '0');
INSERT INTO `city` VALUES ('378', '378', 'Bebeji', null, '0');
INSERT INTO `city` VALUES ('379', '379', 'Bichi', null, '0');
INSERT INTO `city` VALUES ('380', '380', 'Bunkure', null, '0');
INSERT INTO `city` VALUES ('381', '381', 'Dala', null, '0');
INSERT INTO `city` VALUES ('382', '382', 'Dambatta', null, '0');
INSERT INTO `city` VALUES ('383', '383', 'Dawakin Kudu', null, '0');
INSERT INTO `city` VALUES ('384', '384', 'Dawakin Tofa', null, '0');
INSERT INTO `city` VALUES ('385', '385', 'Doguwa', null, '0');
INSERT INTO `city` VALUES ('386', '386', 'Fagge', null, '0');
INSERT INTO `city` VALUES ('387', '387', 'Gabasawa', null, '0');
INSERT INTO `city` VALUES ('388', '388', 'Garko', null, '0');
INSERT INTO `city` VALUES ('389', '389', 'Garum', null, '0');
INSERT INTO `city` VALUES ('390', '390', 'Mallam', null, '0');
INSERT INTO `city` VALUES ('391', '391', 'Gaya', null, '0');
INSERT INTO `city` VALUES ('392', '392', 'Gezawa', null, '0');
INSERT INTO `city` VALUES ('393', '393', 'Gwale', null, '0');
INSERT INTO `city` VALUES ('394', '394', 'Gwarzo', null, '0');
INSERT INTO `city` VALUES ('395', '395', 'Kabo', null, '0');
INSERT INTO `city` VALUES ('396', '396', 'Kano Municipal', null, '0');
INSERT INTO `city` VALUES ('397', '397', 'Karaye', null, '0');
INSERT INTO `city` VALUES ('398', '398', 'Kibiya', null, '0');
INSERT INTO `city` VALUES ('399', '399', 'Kiru', null, '0');
INSERT INTO `city` VALUES ('400', '400', 'kumbotso', null, '0');
INSERT INTO `city` VALUES ('401', '401', 'Kunchi', null, '0');
INSERT INTO `city` VALUES ('402', '402', 'Kura', null, '0');
INSERT INTO `city` VALUES ('403', '403', 'Madobi', null, '0');
INSERT INTO `city` VALUES ('404', '404', 'Makoda', null, '0');
INSERT INTO `city` VALUES ('405', '405', 'Minjibir', null, '0');
INSERT INTO `city` VALUES ('406', '406', 'Nasarawa', null, '0');
INSERT INTO `city` VALUES ('407', '407', 'Rano', null, '0');
INSERT INTO `city` VALUES ('408', '408', 'Rimin Gado', null, '0');
INSERT INTO `city` VALUES ('409', '409', 'Rogo', null, '0');
INSERT INTO `city` VALUES ('410', '410', 'Shanono', null, '0');
INSERT INTO `city` VALUES ('411', '411', 'Sumaila', null, '0');
INSERT INTO `city` VALUES ('412', '412', 'Takali', null, '0');
INSERT INTO `city` VALUES ('413', '413', 'Tarauni', null, '0');
INSERT INTO `city` VALUES ('414', '414', 'Tofa', null, '0');
INSERT INTO `city` VALUES ('415', '415', 'Tsanyawa', null, '0');
INSERT INTO `city` VALUES ('416', '416', 'Tudun Wada', null, '0');
INSERT INTO `city` VALUES ('417', '417', 'Ungogo', null, '0');
INSERT INTO `city` VALUES ('418', '418', 'Warawa', null, '0');
INSERT INTO `city` VALUES ('419', '419', 'Wudil', null, '0');
INSERT INTO `city` VALUES ('420', '420', 'Bakori', null, '0');
INSERT INTO `city` VALUES ('421', '421', 'Batagarawa', null, '0');
INSERT INTO `city` VALUES ('422', '422', 'Batsari', null, '0');
INSERT INTO `city` VALUES ('423', '423', 'Baure', null, '0');
INSERT INTO `city` VALUES ('424', '424', 'Bindawa', null, '0');
INSERT INTO `city` VALUES ('425', '425', 'Charanchi', null, '0');
INSERT INTO `city` VALUES ('426', '426', 'Dandume', null, '0');
INSERT INTO `city` VALUES ('427', '427', 'Danja', null, '0');
INSERT INTO `city` VALUES ('428', '428', 'Dan Musa', null, '0');
INSERT INTO `city` VALUES ('429', '429', 'Daura', null, '0');
INSERT INTO `city` VALUES ('430', '430', 'Dutsi', null, '0');
INSERT INTO `city` VALUES ('431', '431', 'Dutsin-Ma', null, '0');
INSERT INTO `city` VALUES ('432', '432', 'Faskari', null, '0');
INSERT INTO `city` VALUES ('433', '433', 'Funtua', null, '0');
INSERT INTO `city` VALUES ('434', '434', 'Ingawa', null, '0');
INSERT INTO `city` VALUES ('435', '435', 'Jibia', null, '0');
INSERT INTO `city` VALUES ('436', '436', 'Kafur', null, '0');
INSERT INTO `city` VALUES ('437', '437', 'Kaita', null, '0');
INSERT INTO `city` VALUES ('438', '438', 'Kankara', null, '0');
INSERT INTO `city` VALUES ('439', '439', 'Kankia', null, '0');
INSERT INTO `city` VALUES ('440', '440', 'Katsina', null, '0');
INSERT INTO `city` VALUES ('441', '441', 'Kurfi', null, '0');
INSERT INTO `city` VALUES ('442', '442', 'Kusada', null, '0');
INSERT INTO `city` VALUES ('443', '443', 'Mai\'Adua', null, '0');
INSERT INTO `city` VALUES ('444', '444', 'Malumfashi', null, '0');
INSERT INTO `city` VALUES ('445', '445', 'Mani', null, '0');
INSERT INTO `city` VALUES ('446', '446', 'Mashi', null, '0');
INSERT INTO `city` VALUES ('447', '447', 'Matazuu', null, '0');
INSERT INTO `city` VALUES ('448', '448', 'Musawa', null, '0');
INSERT INTO `city` VALUES ('449', '449', 'Rimi', null, '0');
INSERT INTO `city` VALUES ('450', '450', 'Sabuwa', null, '0');
INSERT INTO `city` VALUES ('451', '451', 'Safana', null, '0');
INSERT INTO `city` VALUES ('452', '452', 'Sandamu', null, '0');
INSERT INTO `city` VALUES ('453', '453', 'Zango', null, '0');
INSERT INTO `city` VALUES ('454', '454', 'Aleiro', null, '0');
INSERT INTO `city` VALUES ('455', '455', 'Arewa-Dandi', null, '0');
INSERT INTO `city` VALUES ('456', '456', 'Argungu', null, '0');
INSERT INTO `city` VALUES ('457', '457', 'Augie', null, '0');
INSERT INTO `city` VALUES ('458', '458', 'Bagudo', null, '0');
INSERT INTO `city` VALUES ('459', '459', 'Birnin Kebbi', null, '0');
INSERT INTO `city` VALUES ('460', '460', 'Bunza', null, '0');
INSERT INTO `city` VALUES ('461', '461', 'Dandi', null, '0');
INSERT INTO `city` VALUES ('462', '462', 'Fakai', null, '0');
INSERT INTO `city` VALUES ('463', '463', 'Gwandu', null, '0');
INSERT INTO `city` VALUES ('464', '464', 'Jega', null, '0');
INSERT INTO `city` VALUES ('465', '465', 'Kalgo', null, '0');
INSERT INTO `city` VALUES ('466', '466', 'Koko/Besse', null, '0');
INSERT INTO `city` VALUES ('467', '467', 'Maiyama', null, '0');
INSERT INTO `city` VALUES ('468', '468', 'Ngaski', null, '0');
INSERT INTO `city` VALUES ('469', '469', 'Sakaba', null, '0');
INSERT INTO `city` VALUES ('470', '470', 'Shanga', null, '0');
INSERT INTO `city` VALUES ('471', '471', 'Suru', null, '0');
INSERT INTO `city` VALUES ('472', '472', 'Wasagu/Danko', null, '0');
INSERT INTO `city` VALUES ('473', '473', 'Yauri', null, '0');
INSERT INTO `city` VALUES ('474', '474', 'Zuru', null, '0');
INSERT INTO `city` VALUES ('475', '475', 'Adavi', null, '0');
INSERT INTO `city` VALUES ('476', '476', 'Ajaokuta', null, '0');
INSERT INTO `city` VALUES ('477', '477', 'Ankpa', null, '0');
INSERT INTO `city` VALUES ('478', '478', 'Bassa', null, '0');
INSERT INTO `city` VALUES ('479', '479', 'Dekina', null, '0');
INSERT INTO `city` VALUES ('480', '480', 'Ibaji', null, '0');
INSERT INTO `city` VALUES ('481', '481', 'Idah', null, '0');
INSERT INTO `city` VALUES ('482', '482', 'Igalamela-Odolu', null, '0');
INSERT INTO `city` VALUES ('483', '483', 'Ijumu', null, '0');
INSERT INTO `city` VALUES ('484', '484', 'Kabba/Bunu', null, '0');
INSERT INTO `city` VALUES ('485', '485', 'Kogi', null, '0');
INSERT INTO `city` VALUES ('486', '486', 'Lokoja', null, '0');
INSERT INTO `city` VALUES ('487', '487', 'Mopa-Muro', null, '0');
INSERT INTO `city` VALUES ('488', '488', 'Ofu', null, '0');
INSERT INTO `city` VALUES ('489', '489', 'Ogori/Mangongo', null, '0');
INSERT INTO `city` VALUES ('490', '490', 'Okehi', null, '0');
INSERT INTO `city` VALUES ('491', '491', 'Okene', null, '0');
INSERT INTO `city` VALUES ('492', '492', 'Olamabolo', null, '0');
INSERT INTO `city` VALUES ('493', '493', 'Omala', null, '0');
INSERT INTO `city` VALUES ('494', '494', 'Yagba East', null, '0');
INSERT INTO `city` VALUES ('495', '495', 'Yagba West', null, '0');
INSERT INTO `city` VALUES ('496', '496', 'Asa', null, '0');
INSERT INTO `city` VALUES ('497', '497', 'Baruten', null, '0');
INSERT INTO `city` VALUES ('498', '498', 'Edu', null, '0');
INSERT INTO `city` VALUES ('499', '499', 'Ekiti', null, '0');
INSERT INTO `city` VALUES ('500', '500', 'Ifelodun', null, '0');
INSERT INTO `city` VALUES ('501', '501', 'Ilorin East', null, '0');
INSERT INTO `city` VALUES ('502', '502', 'Ilorin West', null, '0');
INSERT INTO `city` VALUES ('503', '503', 'Irepodun', null, '0');
INSERT INTO `city` VALUES ('504', '504', 'Isin', null, '0');
INSERT INTO `city` VALUES ('505', '505', 'Kaiama', null, '0');
INSERT INTO `city` VALUES ('506', '506', 'Moro', null, '0');
INSERT INTO `city` VALUES ('507', '507', 'Offa', null, '0');
INSERT INTO `city` VALUES ('508', '508', 'Oke-Ero', null, '0');
INSERT INTO `city` VALUES ('509', '509', 'Oyun', null, '0');
INSERT INTO `city` VALUES ('510', '510', 'Pategi', null, '0');
INSERT INTO `city` VALUES ('511', '511', 'Agege', null, '0');
INSERT INTO `city` VALUES ('512', '512', 'Ajeromi-Ifelodun', null, '0');
INSERT INTO `city` VALUES ('513', '513', 'Alimosho', null, '0');
INSERT INTO `city` VALUES ('514', '514', 'Amuwo-Odofin', null, '0');
INSERT INTO `city` VALUES ('515', '515', 'Apapa', null, '0');
INSERT INTO `city` VALUES ('516', '516', 'Badagry', null, '0');
INSERT INTO `city` VALUES ('517', '517', 'Epe', null, '0');
INSERT INTO `city` VALUES ('518', '518', 'Eti-Osa', null, '0');
INSERT INTO `city` VALUES ('519', '519', 'Ibeju/Lekki', null, '0');
INSERT INTO `city` VALUES ('520', '520', 'Ifako-Ijaye', null, '0');
INSERT INTO `city` VALUES ('521', '521', 'Ikeja', null, '0');
INSERT INTO `city` VALUES ('522', '522', 'Ikorodu', null, '0');
INSERT INTO `city` VALUES ('523', '523', 'Kosofe', null, '0');
INSERT INTO `city` VALUES ('524', '524', 'Lagos Island', null, '0');
INSERT INTO `city` VALUES ('525', '525', 'Lagos Mainland', null, '0');
INSERT INTO `city` VALUES ('526', '526', 'Mushin', null, '0');
INSERT INTO `city` VALUES ('527', '527', 'Ojo', null, '0');
INSERT INTO `city` VALUES ('528', '528', 'Oshodi-Isolo', null, '0');
INSERT INTO `city` VALUES ('529', '529', 'Shomolu', null, '0');
INSERT INTO `city` VALUES ('530', '530', 'Surulere', null, '0');
INSERT INTO `city` VALUES ('531', '531', 'Akwanga', null, '0');
INSERT INTO `city` VALUES ('532', '532', 'Awe', null, '0');
INSERT INTO `city` VALUES ('533', '533', 'Doma', null, '0');
INSERT INTO `city` VALUES ('534', '534', 'Karu', null, '0');
INSERT INTO `city` VALUES ('535', '535', 'Keana', null, '0');
INSERT INTO `city` VALUES ('536', '536', 'Keffi', null, '0');
INSERT INTO `city` VALUES ('537', '537', 'Kokona', null, '0');
INSERT INTO `city` VALUES ('538', '538', 'Lafia', null, '0');
INSERT INTO `city` VALUES ('539', '539', 'Nasarawa', null, '0');
INSERT INTO `city` VALUES ('540', '540', 'Nasarawa-Eggon', null, '0');
INSERT INTO `city` VALUES ('541', '541', 'Obi', null, '0');
INSERT INTO `city` VALUES ('542', '542', 'Toto', null, '0');
INSERT INTO `city` VALUES ('543', '543', 'Wamba', null, '0');
INSERT INTO `city` VALUES ('544', '544', 'Agaie', null, '0');
INSERT INTO `city` VALUES ('545', '545', 'Agwara', null, '0');
INSERT INTO `city` VALUES ('546', '546', 'Bida', null, '0');
INSERT INTO `city` VALUES ('547', '547', 'Borgu', null, '0');
INSERT INTO `city` VALUES ('548', '548', 'Bosso', null, '0');
INSERT INTO `city` VALUES ('549', '549', 'Chanchaga', null, '0');
INSERT INTO `city` VALUES ('550', '550', 'Edati', null, '0');
INSERT INTO `city` VALUES ('551', '551', 'Gbako', null, '0');
INSERT INTO `city` VALUES ('552', '552', 'Gurara', null, '0');
INSERT INTO `city` VALUES ('553', '553', 'Katcha', null, '0');
INSERT INTO `city` VALUES ('554', '554', 'Kontagora', null, '0');
INSERT INTO `city` VALUES ('555', '555', 'Lapai', null, '0');
INSERT INTO `city` VALUES ('556', '556', 'Lavun', null, '0');
INSERT INTO `city` VALUES ('557', '557', 'Magama', null, '0');
INSERT INTO `city` VALUES ('558', '558', 'Mariga', null, '0');
INSERT INTO `city` VALUES ('559', '559', 'Mashegu', null, '0');
INSERT INTO `city` VALUES ('560', '560', 'Mokwa', null, '0');
INSERT INTO `city` VALUES ('561', '561', 'Muya', null, '0');
INSERT INTO `city` VALUES ('562', '562', 'Pailoro', null, '0');
INSERT INTO `city` VALUES ('563', '563', 'Rafi', null, '0');
INSERT INTO `city` VALUES ('564', '564', 'Rijau', null, '0');
INSERT INTO `city` VALUES ('565', '565', 'Shiroro', null, '0');
INSERT INTO `city` VALUES ('566', '566', 'Suleja', null, '0');
INSERT INTO `city` VALUES ('567', '567', 'Tafa', null, '0');
INSERT INTO `city` VALUES ('568', '568', 'Wushishi', null, '0');
INSERT INTO `city` VALUES ('569', '569', 'Abeokuta North', null, '0');
INSERT INTO `city` VALUES ('570', '570', 'Abeokuta South', null, '0');
INSERT INTO `city` VALUES ('571', '571', 'Ado-Odo/Ota', null, '0');
INSERT INTO `city` VALUES ('572', '572', 'Egbado North', null, '0');
INSERT INTO `city` VALUES ('573', '573', 'Egbado South', null, '0');
INSERT INTO `city` VALUES ('574', '574', 'Ewekoro', null, '0');
INSERT INTO `city` VALUES ('575', '575', 'Ifo', null, '0');
INSERT INTO `city` VALUES ('576', '576', 'Ijebu East', null, '0');
INSERT INTO `city` VALUES ('577', '577', 'Ijebu North', null, '0');
INSERT INTO `city` VALUES ('578', '578', 'Ijebu North East', null, '0');
INSERT INTO `city` VALUES ('579', '579', 'Ijebu Ode', null, '0');
INSERT INTO `city` VALUES ('580', '580', 'Ikenne', null, '0');
INSERT INTO `city` VALUES ('581', '581', 'Imeko-Afon', null, '0');
INSERT INTO `city` VALUES ('582', '582', 'Ipokia', null, '0');
INSERT INTO `city` VALUES ('583', '583', 'Obafemi-Owode', null, '0');
INSERT INTO `city` VALUES ('584', '584', 'Ogun Waterside', null, '0');
INSERT INTO `city` VALUES ('585', '585', 'Odeda', null, '0');
INSERT INTO `city` VALUES ('586', '586', 'Odogbolu', null, '0');
INSERT INTO `city` VALUES ('587', '587', 'Remo North', null, '0');
INSERT INTO `city` VALUES ('588', '588', 'Shagamu', null, '0');
INSERT INTO `city` VALUES ('589', '589', 'Akoko North East', null, '0');
INSERT INTO `city` VALUES ('590', '590', 'Akoko North West', null, '0');
INSERT INTO `city` VALUES ('591', '591', 'Akoko South Akure East', null, '0');
INSERT INTO `city` VALUES ('592', '592', 'Akoko South West', null, '0');
INSERT INTO `city` VALUES ('593', '593', 'Akure North', null, '0');
INSERT INTO `city` VALUES ('594', '594', 'Akure South', null, '0');
INSERT INTO `city` VALUES ('595', '595', 'Ese-Odo', null, '0');
INSERT INTO `city` VALUES ('596', '596', 'Idanre', null, '0');
INSERT INTO `city` VALUES ('597', '597', 'Ifedore', null, '0');
INSERT INTO `city` VALUES ('598', '598', 'Ilaje', null, '0');
INSERT INTO `city` VALUES ('599', '599', 'Ile-Oluji', null, '0');
INSERT INTO `city` VALUES ('600', '600', 'Okeigbo', null, '0');
INSERT INTO `city` VALUES ('601', '601', 'Irele', null, '0');
INSERT INTO `city` VALUES ('602', '602', 'Odigbo', null, '0');
INSERT INTO `city` VALUES ('603', '603', 'Okitipupa', null, '0');
INSERT INTO `city` VALUES ('604', '604', 'Ondo East', null, '0');
INSERT INTO `city` VALUES ('605', '605', 'Ondo West', null, '0');
INSERT INTO `city` VALUES ('606', '606', 'Ose', null, '0');
INSERT INTO `city` VALUES ('607', '607', 'Owo', null, '0');
INSERT INTO `city` VALUES ('608', '608', 'Aiyedade', null, '0');
INSERT INTO `city` VALUES ('609', '609', 'Aiyedire', null, '0');
INSERT INTO `city` VALUES ('610', '610', 'Atakumosa East', null, '0');
INSERT INTO `city` VALUES ('611', '611', 'Atakumosa West', null, '0');
INSERT INTO `city` VALUES ('612', '612', 'Boluwaduro', null, '0');
INSERT INTO `city` VALUES ('613', '613', 'Boripe', null, '0');
INSERT INTO `city` VALUES ('614', '614', 'Ede North', null, '0');
INSERT INTO `city` VALUES ('615', '615', 'Ede South', null, '0');
INSERT INTO `city` VALUES ('616', '616', 'Egbedore', null, '0');
INSERT INTO `city` VALUES ('617', '617', 'Ejigbo', null, '0');
INSERT INTO `city` VALUES ('618', '618', 'Ife Central', null, '0');
INSERT INTO `city` VALUES ('619', '619', 'Ife East', null, '0');
INSERT INTO `city` VALUES ('620', '620', 'Ife North', null, '0');
INSERT INTO `city` VALUES ('621', '621', 'Ife South', null, '0');
INSERT INTO `city` VALUES ('622', '622', 'Ifedayo', null, '0');
INSERT INTO `city` VALUES ('623', '623', 'Ifelodun', null, '0');
INSERT INTO `city` VALUES ('624', '624', 'Ila', null, '0');
INSERT INTO `city` VALUES ('625', '625', 'Ilesha East', null, '0');
INSERT INTO `city` VALUES ('626', '626', 'Ilesha West', null, '0');
INSERT INTO `city` VALUES ('627', '627', 'Irepodun', null, '0');
INSERT INTO `city` VALUES ('628', '628', 'Irewole', null, '0');
INSERT INTO `city` VALUES ('629', '629', 'Isokan', null, '0');
INSERT INTO `city` VALUES ('630', '630', 'Iwo', null, '0');
INSERT INTO `city` VALUES ('631', '631', 'Obokun', null, '0');
INSERT INTO `city` VALUES ('632', '632', 'Odo-Otin', null, '0');
INSERT INTO `city` VALUES ('633', '633', 'Ola-Oluwa', null, '0');
INSERT INTO `city` VALUES ('634', '634', 'Olorunda', null, '0');
INSERT INTO `city` VALUES ('635', '635', 'Oriade', null, '0');
INSERT INTO `city` VALUES ('636', '636', 'Orolu', null, '0');
INSERT INTO `city` VALUES ('637', '637', 'Osogbo', null, '0');
INSERT INTO `city` VALUES ('638', '638', 'Afijio', null, '0');
INSERT INTO `city` VALUES ('639', '639', 'Akinyele', null, '0');
INSERT INTO `city` VALUES ('640', '640', 'Atiba', null, '0');
INSERT INTO `city` VALUES ('641', '641', 'Atigbo', null, '0');
INSERT INTO `city` VALUES ('642', '642', 'Egbeda', null, '0');
INSERT INTO `city` VALUES ('643', '643', 'IbadanCentral', null, '0');
INSERT INTO `city` VALUES ('644', '644', 'Ibadan North', null, '0');
INSERT INTO `city` VALUES ('645', '645', 'Ibadan North West', null, '0');
INSERT INTO `city` VALUES ('646', '646', 'Ibadan South East', null, '0');
INSERT INTO `city` VALUES ('647', '647', 'Ibadan South West', null, '0');
INSERT INTO `city` VALUES ('648', '648', 'Ibarapa Central', null, '0');
INSERT INTO `city` VALUES ('649', '649', 'Ibarapa East', null, '0');
INSERT INTO `city` VALUES ('650', '650', 'Ibarapa North', null, '0');
INSERT INTO `city` VALUES ('651', '651', 'Ido', null, '0');
INSERT INTO `city` VALUES ('652', '652', 'Irepo', null, '0');
INSERT INTO `city` VALUES ('653', '653', 'Iseyin', null, '0');
INSERT INTO `city` VALUES ('654', '654', 'Itesiwaju', null, '0');
INSERT INTO `city` VALUES ('655', '655', 'Iwajowa', null, '0');
INSERT INTO `city` VALUES ('656', '656', 'Kajola', null, '0');
INSERT INTO `city` VALUES ('657', '657', 'Lagelu Ogbomosho North', null, '0');
INSERT INTO `city` VALUES ('658', '658', 'Ogbmosho South', null, '0');
INSERT INTO `city` VALUES ('659', '659', 'Ogo Oluwa', null, '0');
INSERT INTO `city` VALUES ('660', '660', 'Olorunsogo', null, '0');
INSERT INTO `city` VALUES ('661', '661', 'Oluyole', null, '0');
INSERT INTO `city` VALUES ('662', '662', 'Ona-Ara', null, '0');
INSERT INTO `city` VALUES ('663', '663', 'Orelope', null, '0');
INSERT INTO `city` VALUES ('664', '664', 'Ori Ire', null, '0');
INSERT INTO `city` VALUES ('665', '665', 'Oyo East', null, '0');
INSERT INTO `city` VALUES ('666', '666', 'Oyo West', null, '0');
INSERT INTO `city` VALUES ('667', '667', 'Saki East', null, '0');
INSERT INTO `city` VALUES ('668', '668', 'Saki West', null, '0');
INSERT INTO `city` VALUES ('669', '669', 'Surulere', null, '0');
INSERT INTO `city` VALUES ('670', '670', 'Barikin Ladi', null, '0');
INSERT INTO `city` VALUES ('671', '671', 'Bassa', null, '0');
INSERT INTO `city` VALUES ('672', '672', 'Bokkos', null, '0');
INSERT INTO `city` VALUES ('673', '673', 'Jos East', null, '0');
INSERT INTO `city` VALUES ('674', '674', 'Jos North', null, '0');
INSERT INTO `city` VALUES ('675', '675', 'Jos South', null, '0');
INSERT INTO `city` VALUES ('676', '676', 'Kanam', null, '0');
INSERT INTO `city` VALUES ('677', '677', 'Kanke', null, '0');
INSERT INTO `city` VALUES ('678', '678', 'Langtang North', null, '0');
INSERT INTO `city` VALUES ('679', '679', 'Langtang South', null, '0');
INSERT INTO `city` VALUES ('680', '680', 'Mangu', null, '0');
INSERT INTO `city` VALUES ('681', '681', 'Mikang', null, '0');
INSERT INTO `city` VALUES ('682', '682', 'Pankshin', null, '0');
INSERT INTO `city` VALUES ('683', '683', 'Qua\'an Pan', null, '0');
INSERT INTO `city` VALUES ('684', '684', 'Riyom', null, '0');
INSERT INTO `city` VALUES ('685', '685', 'Shendam', null, '0');
INSERT INTO `city` VALUES ('686', '686', 'Wase', null, '0');
INSERT INTO `city` VALUES ('687', '687', 'Abua/Odual', null, '0');
INSERT INTO `city` VALUES ('688', '688', 'Ahoada East', null, '0');
INSERT INTO `city` VALUES ('689', '689', 'Ahoada West', null, '0');
INSERT INTO `city` VALUES ('690', '690', 'Akuku Toru', null, '0');
INSERT INTO `city` VALUES ('691', '691', 'Andoni', null, '0');
INSERT INTO `city` VALUES ('692', '692', 'Asari-Toru', null, '0');
INSERT INTO `city` VALUES ('693', '693', 'Bonny', null, '0');
INSERT INTO `city` VALUES ('694', '694', 'Degema', null, '0');
INSERT INTO `city` VALUES ('695', '695', 'Emohua', null, '0');
INSERT INTO `city` VALUES ('696', '696', 'Eleme', null, '0');
INSERT INTO `city` VALUES ('697', '697', 'Etche', null, '0');
INSERT INTO `city` VALUES ('698', '698', 'Gokana', null, '0');
INSERT INTO `city` VALUES ('699', '699', 'Ikwerre', null, '0');
INSERT INTO `city` VALUES ('700', '700', 'Khana', null, '0');
INSERT INTO `city` VALUES ('701', '701', 'Obia/Akpor', null, '0');
INSERT INTO `city` VALUES ('702', '702', 'Ogba/Egbema/Ndoni', null, '0');
INSERT INTO `city` VALUES ('703', '703', 'Ogu/Bolo', null, '0');
INSERT INTO `city` VALUES ('704', '704', 'Okrika', null, '0');
INSERT INTO `city` VALUES ('705', '705', 'Omumma', null, '0');
INSERT INTO `city` VALUES ('706', '706', 'Opobo/Nkoro', null, '0');
INSERT INTO `city` VALUES ('707', '707', 'Oyigbo', null, '0');
INSERT INTO `city` VALUES ('708', '708', 'Port-Harcourt', null, '0');
INSERT INTO `city` VALUES ('709', '709', 'Tai', null, '0');
INSERT INTO `city` VALUES ('710', '710', 'Binji', null, '0');
INSERT INTO `city` VALUES ('711', '711', 'Bodinga', null, '0');
INSERT INTO `city` VALUES ('712', '712', 'Dange-shnsi', null, '0');
INSERT INTO `city` VALUES ('713', '713', 'Gada', null, '0');
INSERT INTO `city` VALUES ('714', '714', 'Goronyo', null, '0');
INSERT INTO `city` VALUES ('715', '715', 'Gudu', null, '0');
INSERT INTO `city` VALUES ('716', '716', 'Gawabawa', null, '0');
INSERT INTO `city` VALUES ('717', '717', 'Illela', null, '0');
INSERT INTO `city` VALUES ('718', '718', 'Isa', null, '0');
INSERT INTO `city` VALUES ('719', '719', 'Kware', null, '0');
INSERT INTO `city` VALUES ('720', '720', 'kebbe', null, '0');
INSERT INTO `city` VALUES ('721', '721', 'Rabah', null, '0');
INSERT INTO `city` VALUES ('722', '722', 'Sabon Birni', null, '0');
INSERT INTO `city` VALUES ('723', '723', 'Shagari', null, '0');
INSERT INTO `city` VALUES ('724', '724', 'Silame', null, '0');
INSERT INTO `city` VALUES ('725', '725', 'Sokoto North', null, '0');
INSERT INTO `city` VALUES ('726', '726', 'Sokoto South', null, '0');
INSERT INTO `city` VALUES ('727', '727', 'Tambuwal', null, '0');
INSERT INTO `city` VALUES ('728', '728', 'Tqngaza', null, '0');
INSERT INTO `city` VALUES ('729', '729', 'Tureta', null, '0');
INSERT INTO `city` VALUES ('730', '730', 'Wamako', null, '0');
INSERT INTO `city` VALUES ('731', '731', 'Wurno', null, '0');
INSERT INTO `city` VALUES ('732', '732', 'Yabo', null, '0');
INSERT INTO `city` VALUES ('733', '733', 'Ardo-kola', null, '0');
INSERT INTO `city` VALUES ('734', '734', 'Bali', null, '0');
INSERT INTO `city` VALUES ('735', '735', 'Donga', null, '0');
INSERT INTO `city` VALUES ('736', '736', 'Gashaka', null, '0');
INSERT INTO `city` VALUES ('737', '737', 'Cassol', null, '0');
INSERT INTO `city` VALUES ('738', '738', 'Ibi', null, '0');
INSERT INTO `city` VALUES ('739', '739', 'Jalingo', null, '0');
INSERT INTO `city` VALUES ('740', '740', 'Karin-Lamido', null, '0');
INSERT INTO `city` VALUES ('741', '741', 'Kurmi', null, '0');
INSERT INTO `city` VALUES ('742', '742', 'Lau', null, '0');
INSERT INTO `city` VALUES ('743', '743', 'Sardauna', null, '0');
INSERT INTO `city` VALUES ('744', '744', 'Takum', null, '0');
INSERT INTO `city` VALUES ('745', '745', 'Ussa', null, '0');
INSERT INTO `city` VALUES ('746', '746', 'Wukari', null, '0');
INSERT INTO `city` VALUES ('747', '747', 'Yorro', null, '0');
INSERT INTO `city` VALUES ('748', '748', 'Zing', null, '0');
INSERT INTO `city` VALUES ('749', '749', 'Ardo-kola', null, '0');
INSERT INTO `city` VALUES ('750', '750', 'Bali', null, '0');
INSERT INTO `city` VALUES ('751', '751', 'Donga', null, '0');
INSERT INTO `city` VALUES ('752', '752', 'Gashaka', null, '0');
INSERT INTO `city` VALUES ('753', '753', 'Cassol', null, '0');
INSERT INTO `city` VALUES ('754', '754', 'Ibi', null, '0');
INSERT INTO `city` VALUES ('755', '755', 'Jalingo', null, '0');
INSERT INTO `city` VALUES ('756', '756', 'Karin-Lamido', null, '0');
INSERT INTO `city` VALUES ('757', '757', 'Kurmi', null, '0');
INSERT INTO `city` VALUES ('758', '758', 'Lau', null, '0');
INSERT INTO `city` VALUES ('759', '759', 'Sardauna', null, '0');
INSERT INTO `city` VALUES ('760', '760', 'Takum', null, '0');
INSERT INTO `city` VALUES ('761', '761', 'Ussa', null, '0');
INSERT INTO `city` VALUES ('762', '762', 'Wukari', null, '0');
INSERT INTO `city` VALUES ('763', '763', 'Yorro', null, '0');
INSERT INTO `city` VALUES ('764', '764', 'Zing', null, '0');
INSERT INTO `city` VALUES ('765', '765', 'Anka', null, '0');
INSERT INTO `city` VALUES ('766', '766', 'Bakura', null, '0');
INSERT INTO `city` VALUES ('767', '767', 'Birnin Magaji', null, '0');
INSERT INTO `city` VALUES ('768', '768', 'Bukkuyum', null, '0');
INSERT INTO `city` VALUES ('769', '769', 'Bungudu', null, '0');
INSERT INTO `city` VALUES ('770', '770', 'Gummi', null, '0');
INSERT INTO `city` VALUES ('771', '771', 'Gusau', null, '0');
INSERT INTO `city` VALUES ('772', '772', 'Kaura', null, '0');
INSERT INTO `city` VALUES ('773', '773', 'Namoda', null, '0');
INSERT INTO `city` VALUES ('774', '774', 'Maradun', null, '0');
INSERT INTO `city` VALUES ('775', '775', 'Maru', null, '0');
INSERT INTO `city` VALUES ('776', '776', 'Shinkafi', null, '0');
INSERT INTO `city` VALUES ('777', '777', 'Talata Mafara', null, '0');
INSERT INTO `city` VALUES ('778', '778', 'Tsafe', null, '0');
INSERT INTO `city` VALUES ('779', '779', 'Zurmi', null, '0');
INSERT INTO `city` VALUES ('781', '1', 'Southern Jaw', null, '0');
INSERT INTO `city` VALUES ('784', '1', 'Ushongo', null, '0');
INSERT INTO `city` VALUES ('805', '1', 'Victoria Island', null, '0');
INSERT INTO `city` VALUES ('806', '519', 'Alpha Beach Road', null, '0');
INSERT INTO `city` VALUES ('807', '1', 'Alpha Beach Road', null, '0');
INSERT INTO `city` VALUES ('808', '515', 'Lombra', null, '0');
INSERT INTO `city` VALUES ('809', '23', 'lombra1', null, '0');
INSERT INTO `city` VALUES ('810', '195', 'Kolum', null, '0');
INSERT INTO `city` VALUES ('811', '23', 'Kolum', null, '0');
INSERT INTO `city` VALUES ('813', '1', 'Ikeja', null, '0');
INSERT INTO `city` VALUES ('858', '1', 'Ort', null, '0');
INSERT INTO `city` VALUES ('859', '23', 'Ort', null, '0');
INSERT INTO `city` VALUES ('860', '518', 'Ajah', null, '0');
INSERT INTO `city` VALUES ('861', '518', 'Lekki', null, '0');
INSERT INTO `city` VALUES ('862', '511', 'Agbado', null, '0');
INSERT INTO `city` VALUES ('863', '23', 'Navy Town', null, '0');
INSERT INTO `city` VALUES ('864', '23', 'Alakija', null, '0');
INSERT INTO `city` VALUES ('865', '23', 'Aguda', null, '0');
INSERT INTO `city` VALUES ('866', '23', 'Orile Iganmu', null, '0');
INSERT INTO `city` VALUES ('867', '23', 'Coker Bridge', null, '0');
INSERT INTO `city` VALUES ('868', '23', 'Isa Oroki', null, '0');
INSERT INTO `city` VALUES ('869', '530', 'Orile Iganmu', null, '0');
INSERT INTO `city` VALUES ('870', '530', 'Aguda', null, '0');
INSERT INTO `city` VALUES ('871', '23', 'Ajeromi-Ifelodun', null, '0');
INSERT INTO `city` VALUES ('872', '23', 'Pako, Aguda', null, '0');
INSERT INTO `city` VALUES ('873', '23', 'Surulere', null, '0');
INSERT INTO `city` VALUES ('874', '23', 'Lekki', null, '0');
INSERT INTO `city` VALUES ('875', '23', 'Ajah', null, '0');
INSERT INTO `city` VALUES ('876', '23', 'Ikorodu', null, '0');
INSERT INTO `city` VALUES ('877', '23', 'Amuwo Odofin', null, '0');
INSERT INTO `city` VALUES ('878', '23', 'Ifako', null, '0');
INSERT INTO `city` VALUES ('879', '23', 'Kosofe ', null, '0');
INSERT INTO `city` VALUES ('880', '23', 'Agege', null, '0');
INSERT INTO `city` VALUES ('881', '23', 'Badagry', null, '0');
INSERT INTO `city` VALUES ('882', '23', 'Alapere', null, '0');
INSERT INTO `city` VALUES ('883', '23', 'Oworonsoki', null, '0');
INSERT INTO `city` VALUES ('884', '23', 'Ketu', null, '0');
INSERT INTO `city` VALUES ('885', '530', 'Ijesha', null, '0');
INSERT INTO `city` VALUES ('886', '23', 'Epe', null, '0');
INSERT INTO `city` VALUES ('887', '522', 'Cele Bus Stop', null, '0');
INSERT INTO `city` VALUES ('888', '23', 'Festac Town', null, '0');
INSERT INTO `city` VALUES ('889', '522', 'itamaga', null, '0');
INSERT INTO `city` VALUES ('890', '23', ' Lekki', null, '0');
INSERT INTO `city` VALUES ('891', '23', 'Ibeju Lekki', null, '0');
INSERT INTO `city` VALUES ('892', '23', 'Ogba', null, '0');
INSERT INTO `city` VALUES ('893', '522', 'Ota Ona', null, '0');
INSERT INTO `city` VALUES ('894', '23', 'Ajegunle', null, '0');
INSERT INTO `city` VALUES ('895', '23', 'Ikosi, Ketu', null, '0');
INSERT INTO `city` VALUES ('896', '23', 'Ogudu', null, '0');
INSERT INTO `city` VALUES ('897', '511', 'Dopemu', null, '0');
INSERT INTO `city` VALUES ('898', '23', 'Badore', null, '0');
INSERT INTO `city` VALUES ('899', '23', 'Ojodu', null, '0');
INSERT INTO `city` VALUES ('900', '23', 'Alakuko Ifako', null, '0');
INSERT INTO `city` VALUES ('901', '530', 'Ikate', null, '0');
INSERT INTO `city` VALUES ('902', '23', 'Iju Ishaga', null, '0');
INSERT INTO `city` VALUES ('903', '23', 'Ojokoro', null, '0');
INSERT INTO `city` VALUES ('904', '530', 'Ijeshatedo', null, '0');
INSERT INTO `city` VALUES ('905', '23', 'Opebi', null, '0');
INSERT INTO `city` VALUES ('906', '23', 'Gbagada', null, '0');
INSERT INTO `city` VALUES ('907', '522', 'Igbogbo', null, '0');
INSERT INTO `city` VALUES ('908', '23', 'Isheri', null, '0');
INSERT INTO `city` VALUES ('909', '522', 'Igbogbo Bayeku', null, '0');
INSERT INTO `city` VALUES ('910', '530', 'Masha', null, '0');
INSERT INTO `city` VALUES ('911', '23', 'Ojota', null, '0');
INSERT INTO `city` VALUES ('913', '800', 'Ikoyi', null, '0');
INSERT INTO `city` VALUES ('914', '813', 'Chevron', null, '0');
INSERT INTO `city` VALUES ('915', '811', 'Victoria Garden City', null, '0');
INSERT INTO `city` VALUES ('916', '812', 'Oshodi', null, '0');
INSERT INTO `city` VALUES ('917', '810', 'Isolo', null, '0');
INSERT INTO `city` VALUES ('918', '811', 'VGC', null, '0');
INSERT INTO `city` VALUES ('919', '799', 'Victoria Island', null, '0');
INSERT INTO `city` VALUES ('920', '799', 'VI', null, '0');
INSERT INTO `city` VALUES ('921', '23', 'Alimosho', null, '0');
INSERT INTO `city` VALUES ('922', '23', 'Akowonjo', null, '0');
INSERT INTO `city` VALUES ('923', '23', 'Igbogbo', null, '0');
INSERT INTO `city` VALUES ('924', '23', 'Ibereko', null, '0');
INSERT INTO `city` VALUES ('925', '511', 'erfghjn', null, '0');
INSERT INTO `city` VALUES ('926', '518', 'Jakande', null, '0');
INSERT INTO `city` VALUES ('927', '515', 'Ajegunle', null, '0');
INSERT INTO `city` VALUES ('928', '523', 'Ikosi', null, '0');
INSERT INTO `city` VALUES ('929', '514', 'Amuwo Odofin', null, '0');
INSERT INTO `city` VALUES ('930', '530', 'Orile', null, '0');
INSERT INTO `city` VALUES ('931', '520', 'Ifako', null, '0');
INSERT INTO `city` VALUES ('932', '514', 'Festac Town', null, '0');
INSERT INTO `city` VALUES ('933', '520', 'Alakuko Ifako', null, '0');
INSERT INTO `city` VALUES ('934', '514', 'Badagry', null, '0');
INSERT INTO `city` VALUES ('935', '530', 'Coker Bridge', null, '0');
INSERT INTO `city` VALUES ('936', '530', 'Off Yisa Oroki Street', null, '0');
INSERT INTO `city` VALUES ('937', '518', 'Ibeju Lekki', null, '0');
INSERT INTO `city` VALUES ('938', '522', 'Ojodu', null, '0');
INSERT INTO `city` VALUES ('939', '518', 'Onikan', null, '0');
INSERT INTO `city` VALUES ('940', '514', 'Alakija', null, '0');
INSERT INTO `city` VALUES ('941', '518', 'Badore', null, '0');
INSERT INTO `city` VALUES ('942', '530', 'Alakija', null, '0');
INSERT INTO `city` VALUES ('943', '530', 'Itire', null, '0');
INSERT INTO `city` VALUES ('944', '518', ' Lekki', null, '0');
INSERT INTO `city` VALUES ('945', '530', 'Pako, Aguda', null, '0');
INSERT INTO `city` VALUES ('946', '514', 'Navy Town', null, '0');
INSERT INTO `city` VALUES ('947', '530', 'Ajeromi-Ifelodun', null, '0');
INSERT INTO `city` VALUES ('948', '518', 'Epe', null, '0');
INSERT INTO `city` VALUES ('949', '530', 'Iju Ishaga', null, '0');
INSERT INTO `city` VALUES ('950', '23', 'test', null, '0');
INSERT INTO `city` VALUES ('951', '23', 'gra', null, '0');
INSERT INTO `city` VALUES ('952', '1', 'Lekki', null, '0');
INSERT INTO `city` VALUES ('953', '511', 'Alagbado', null, '0');
INSERT INTO `city` VALUES ('954', '528', 'Ajao Estate', null, '0');
INSERT INTO `city` VALUES ('956', '530', 'Orile-Iganmu', null, '0');
INSERT INTO `city` VALUES ('957', '804', 'Ebute Metta', null, '0');
INSERT INTO `city` VALUES ('958', '23', 'Mowe', null, '0');
INSERT INTO `city` VALUES ('959', '23', 'lagos', null, '0');
INSERT INTO `city` VALUES ('960', '23', 'shomolu', null, '0');
INSERT INTO `city` VALUES ('961', '2', '22222', null, '0');
INSERT INTO `city` VALUES ('962', '1', '22222', null, '1');
INSERT INTO `city` VALUES ('963', '1', 'new City1', null, '1');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nameIndex` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'Nigeria', null, '0');

-- ----------------------------
-- Table structure for donor
-- ----------------------------
DROP TABLE IF EXISTS `donor`;
CREATE TABLE `donor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `donor_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `title` tinyint(1) DEFAULT NULL,
  `other_title` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0',
  `date_birth` int(11) DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` double(11,0) DEFAULT NULL,
  `phone` double(11,0) DEFAULT NULL,
  `type_donor` tinyint(1) DEFAULT NULL,
  `blood_group` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `hiv` tinyint(1) DEFAULT NULL,
  `hbsag` tinyint(1) DEFAULT NULL,
  `syphillis` tinyint(1) DEFAULT NULL,
  `guestionnaire_complited` tinyint(1) DEFAULT NULL,
  `guestionnaire_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `health_status` tinyint(1) DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disable` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `hcv` tinyint(1) DEFAULT NULL,
  `date_guestionnaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `location_id` (`location_id`),
  KEY `creator_id` (`created_by`),
  CONSTRAINT `donor_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `donor_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of donor
-- ----------------------------
-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `table` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `row_id` (`row_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of file
-- ----------------------------

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `table` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `row` int(11) NOT NULL,
  `status` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `coll` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `history_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of history
-- ----------------------------


-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1474838741');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1474838746');
INSERT INTO `migration` VALUES ('m160925_184556_donor', '1474838746');
INSERT INTO `migration` VALUES ('m160925_184618_donor_data', '1474838746');
INSERT INTO `migration` VALUES ('m160925_184641_health_info', '1474838746');
INSERT INTO `migration` VALUES ('m160925_184707_blood_drive', '1474838746');
INSERT INTO `migration` VALUES ('m160925_190457_history', '1474838747');
INSERT INTO `migration` VALUES ('m160925_203416_location', '1474838747');

-- ----------------------------
-- Table structure for questionnaire
-- ----------------------------
DROP TABLE IF EXISTS `questionnaire`;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_number` varchar(255) NOT NULL,
  `created_by` tinyint(11) NOT NULL,
  `date` int(11) NOT NULL,
  `donor_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donor_id` (`donor_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `questionnaire_ibfk_1` FOREIGN KEY (`donor_id`) REFERENCES `donor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questionnaire
-- ----------------------------

-- ----------------------------
-- Table structure for questionnaire_answer
-- ----------------------------
DROP TABLE IF EXISTS `questionnaire_answer`;
CREATE TABLE `questionnaire_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `questionnaire_id` (`questionnaire_id`),
  KEY `title_id` (`title_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `questionnaire_answer_ibfk_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_answer_ibfk_2` FOREIGN KEY (`title_id`) REFERENCES `question_title` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_answer_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `question_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questionnaire_answer
-- ----------------------------

-- ----------------------------
-- Table structure for question_category
-- ----------------------------
DROP TABLE IF EXISTS `question_category`;
CREATE TABLE `question_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `question_category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question_category
-- ----------------------------

-- ----------------------------
-- Table structure for question_title
-- ----------------------------
DROP TABLE IF EXISTS `question_title`;
CREATE TABLE `question_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `condition` varchar(255) DEFAULT NULL,
  `previous` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `previous` (`previous`),
  CONSTRAINT `question_title_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `question_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question_title
-- ----------------------------

-- ----------------------------
-- Table structure for state
-- ----------------------------
DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `capital_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nameIndex` (`name`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of state
-- ----------------------------
INSERT INTO `state` VALUES ('1', '1', 'Adamawa', null, '0', '0');
INSERT INTO `state` VALUES ('2', '1', 'Akwa Ibom', null, '0', '0');
INSERT INTO `state` VALUES ('3', '1', 'Anambra', null, '0', '0');
INSERT INTO `state` VALUES ('4', '1', 'Bauchi', null, '0', '0');
INSERT INTO `state` VALUES ('5', '1', 'Bayelsa', null, '0', '0');
INSERT INTO `state` VALUES ('6', '1', 'Benue', null, '0', '0');
INSERT INTO `state` VALUES ('7', '1', 'Bornu', null, '0', '0');
INSERT INTO `state` VALUES ('8', '1', 'Cross River', null, '0', '0');
INSERT INTO `state` VALUES ('9', '1', 'Delta', null, '0', '0');
INSERT INTO `state` VALUES ('10', '1', 'Ebonyi', null, '0', '0');
INSERT INTO `state` VALUES ('11', '1', 'Edo', null, '0', '0');
INSERT INTO `state` VALUES ('12', '1', 'Ekiti', null, '0', '0');
INSERT INTO `state` VALUES ('13', '1', 'Enugu', null, '0', '0');
INSERT INTO `state` VALUES ('14', '1', 'Gombe', null, '0', '0');
INSERT INTO `state` VALUES ('15', '1', 'Imo', null, '0', '0');
INSERT INTO `state` VALUES ('16', '1', 'Jigawa', null, '0', '0');
INSERT INTO `state` VALUES ('17', '1', 'Kaduna', null, '0', '0');
INSERT INTO `state` VALUES ('18', '1', 'Kano', null, '0', '0');
INSERT INTO `state` VALUES ('19', '1', 'Katsina', null, '0', '0');
INSERT INTO `state` VALUES ('20', '1', 'Kebbi', null, '0', '0');
INSERT INTO `state` VALUES ('21', '1', 'Kogi', null, '0', '0');
INSERT INTO `state` VALUES ('22', '1', 'Kwara', null, '0', '0');
INSERT INTO `state` VALUES ('23', '1', 'Lagos', null, '0', '0');
INSERT INTO `state` VALUES ('24', '1', 'Nasarawa', null, '0', '0');
INSERT INTO `state` VALUES ('25', '1', 'Niger', null, '0', '0');
INSERT INTO `state` VALUES ('26', '1', 'Ogun', null, '0', '0');
INSERT INTO `state` VALUES ('27', '1', 'Ondo', null, '0', '0');
INSERT INTO `state` VALUES ('28', '1', 'Osun', null, '0', '0');
INSERT INTO `state` VALUES ('29', '1', 'Oyo', null, '0', '0');
INSERT INTO `state` VALUES ('30', '1', 'Plateau', null, '0', '0');
INSERT INTO `state` VALUES ('31', '1', 'Rivers', null, '0', '0');
INSERT INTO `state` VALUES ('32', '1', 'Sokoto', null, '0', '0');
INSERT INTO `state` VALUES ('33', '1', 'Taraba', null, '0', '0');
INSERT INTO `state` VALUES ('34', '1', 'Yobe', null, '0', '0');
INSERT INTO `state` VALUES ('35', '1', 'Zamfara', null, '0', '0');
INSERT INTO `state` VALUES ('36', '1', 'new stae', null, '1', '0');
INSERT INTO `state` VALUES ('37', '1', 'sdfsdf', null, '1', '0');
INSERT INTO `state` VALUES ('38', '1', 'sdfsdf', null, '1', '0');
INSERT INTO `state` VALUES ('39', '1', 'sdfsdf', null, '1', '0');



-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` double(11,0) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '0',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `created_by` (`created_by`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', null, 'admin', '11111111111', 'tkalenko.in@mail.ru', null, 'FE7Ngv4RItMWHoOI-0E5zlSIWdw27BYn', '$2y$13$n5CUpWX6AZzP7Tak6X9Ipuk87coFA6KW0jDVlS70a8mGUMgq5V08e', null, '0', '0', '0');

<?php
return [
    'Error. Bad request.' => 'Ошибка. Запрос неверный.',
    "Letter don't sent" => 'Письмо не отправлено',
    "User not found" => 'Пользователь не найден',
    "Invalid login or password" => 'Неверный логин или пароль',
    "Record was added before" => 'Запись была добавлена ранее',
    "Image not uploaded" => 'Изображение не загружено',
    'Changes not saved' => 'Изменения не сохранены',
];
<?php

namespace api\modules\v1\controllers;

use common\models\MyDiscipline;
use Yii;
use common\models\User;
use common\models\Role;
use common\models\MyClass;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [

                [
                    'actions' => [
                        'all',
                        'one',

                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
                [
                    'actions' => [
                        'all',
                        'one',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['staff'],

                ],

                [
                    'actions' => [
                        'create-staff',
                        'update-staff',
                        'disable-staff',
                        'archived-staff',

                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create-staff' => ['post'],
                'update-staff' => ['post'],
                'disable-staff' => ['post'],
                'archived-staff' => ['post'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all User models.
     * user/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        return User::searchModel(Yii::$app->request->get());
    }


    /**
     * user/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $request = Yii::$app->request->get();
        if ($request['id']) {
            $user = $this->findModel($request['id']);
            if($user) {
                $result = $user->fields();

                return $result;
            }

        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * site/signup
     * post
     *
     *
     *
     * @return array|User|null
     */

    public function actionCreateStaff()
    {
        $request = Yii::$app->request->post();
        if ($request && Yii::$app->user->can('register_staff')) {

            $model = new User();
            if ($model->load($request, "")) {
                $trans = Yii::$app->db->beginTransaction();

                $user = $model->signup();

                if ($user['error']) {
                    $trans->rollback();
                    return $user;
                }

                $trans->commit();

                if (!Yii::$app->getUser()->login($model)) {
                    return false;
                }

                $result = Yii::$app->user->identity->fields();
                $result['auth_key'] = Yii::$app->user->identity->getAuthKey();
                return $result;

            }
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Creates a new user model.
     *user/create
     * post
     *
     * phone +
     * email +
     * first_name +
     * second_name
     * last_name +
     * image_file
     *
     * role_id + [1-5]
     *
     *if role = [1,2]
     *
     * school_id +
     * sity_id +
     * class +
     *
     * $request['my_classes'] = [
     * '0' => [
     * 'number' => 5,
     * 'letter' => 'A'
     * ] ,
     * '1' => [
     * 'number' => 3,
     * 'letter' => 'A'
     * ]
     * ];
     * $request['my_discipline'] = [
     * '0' => [
     * 'discipline_id' => '1'
     * 'teacher' => 'sdfc'
     * ]
     * ];
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request->post();
        if ($request) {

            $model = new User();

            if ($model->load($request, "")) {
                $trans = Yii::$app->db->beginTransaction();

                $model->password = '11111111';

                $user = $model->signup();
                if (!$user['error']) {

                    //если заданы класы для юзера
                    if ($request['my_classes']) {
                        $save_class = MyClass::saveClasses($user->id, $request['my_classes']);
                        if ($save_class['error']) {
                            $trans->rollback();
                            return $save_class;
                        }
                    }
                    //если заданы дисциплины для юзера
                    if ($request['my_discipline']) {
                        $save_discipline = MyDiscipline::saveDiscipline($user->id, $request['my_discipline']);
                        if ($save_discipline['error']) {
                            $trans->rollback();
                            return $save_discipline;
                        }
                    }

                    $trans->commit();
                    return $user->fields();
                }

                $trans->rollback();
                return $user;
            }

        }

        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Updates an existing User model.
     * user/update
     * post
     *
     * id
     *
     *
     *  * $request['my_classes'] = [
     * '0' => [
     * 'number' => 5,
     * 'letter' => 'A'
     * ] ,
     * '1' => [
     * 'number' => 3,
     * 'letter' => 'A'
     * ]
     * ];
     * @return mixed
     */
    public function actionUpdate()
    {

        $request = Yii::$app->request->post();
        if ($request['id']) {
            $model = $this->findModel($request['id']);

            unset($request['photo']);

            if ($model->load($request, "")) {
                $trans = Yii::$app->db->beginTransaction();
                $user = $model->saveModel();
                if (!$user['error']) {
////                    $request['my_classes'] = '{"0":{"number":5,"letter":A}, "2":1}';
//                    $request['my_classes'] = [
//                        '0' => [
//                            'number' => 5,
//                            'letter' => 'A'
//                        ] ,
//                        '1' => [
//                            'number' => 3,
//                            'letter' => 'A'
//                        ]
//                    ];
                    //если заданы класы для юзера
//                    if ($request['classes']) {
//                        $save_class = MyClass::saveClasses($user->id, $request['classes']);
//                        if ($save_class['error']) {
//                            $trans->rollback();
//                            return $save_class;
//                        }
//                    }
                    //если заданы дисциплины для юзера
                    if ($request['discipline']) {
                        $save_discipline = MyDiscipline::saveDiscipline($user->id, $request['discipline'], true);
                        if ($save_discipline['error']) {
                            $trans->rollback();
                            return $save_discipline;
                        }
                    }
                    $trans->commit();
                    return $user->fields();
                }
                return $user;
            }
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Deletes an existing User model.
     *
     * user/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request->getBodyParams();
        if ($request['id']) {
            $model = $this->findModel($request['id']);
            $model->deleted = User::DELETED;
            $model->email = $model->email.rand();
            if (!$_FILES['image_file']) {
                unset($request['photo']);
            }

            return $model->saveModel();

        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id, 'deleted' => User::NOT_DELETED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

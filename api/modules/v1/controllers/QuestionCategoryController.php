<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\QuestionCategory;
use common\models\search\QuestionCategorySearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * QuestionCategoryController implements the CRUD actions for QuestionCategory model.
 */
class QuestionCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'all-with-title',
                'one',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'all-with-title',
                'one',
                'create',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
                [
                    'actions' => [
                        'all',
                        'all-with-title'

                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all-with-title' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all QuestionCategory models.
     * question-category/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new QuestionCategorySearch();
        return $model->searchAll(Yii::$app->request->get());
    }

    /**
     * все категории и поля к ним
     * question-category/all-with-title
     *
     * get
     *
     * @return string
     */
    public function actionAllWithTitle()
    {
        $model = new QuestionCategorySearch();
        $result =  $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * question-category/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'));
    }

    /**
     * question-category/create
     * post
     *
     * @return array|QuestionCategory|null
     */

    public function actionCreate()
    {
        $model = new QuestionCategory();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;
            if ($model->save()) {
                return $model;
            }
        }
        return $model->getErrors();
    }





    /**
     * Updates an existing QuestionCategory model.
     * question-category/update
     * post
     *
     * id
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing QuestionCategory model.
     *
     * question-category/Archived
     * delete
     *
     * id
     *
     * @return bool|string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->remove()) {
            $trans->commit();
            return true;
        }
        $trans->rollBack();
        return $model->getErrors();
    }

    /**
     * Finds the QuestionCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuestionCategory::findOne(['id' => $id])) !== null) {
            if ($model->deleted == QuestionCategory::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

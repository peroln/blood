<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Area;
use common\models\search\AreaSearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * AreaController implements the CRUD actions for Area model.
 */
class AreaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all'
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Area models.
     * Area/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new AreaSearch();
        return $model->searchAll(Yii::$app->request->get());
    }

    /**
     * Area/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'));
    }

    /**
     * Creates a new Area model.
     *Area/create
     * post
     * name
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Area();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Area model.
     *Area/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Area model.
     *
     * Archived/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionArchived()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Area model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Area the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Area::findOne(['id' => $id])) !== null) {
            if($model->deleted == Area::NOT_DELETED){
                return $model;
            }else{
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

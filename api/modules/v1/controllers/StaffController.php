<?php

namespace api\modules\v1\controllers;


use Yii;
use common\models\User;
use common\models\auth\AuthItem;
use common\models\search\UserSearch;
use common\models\CMail;


use yii\db\Query;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * UserController implements the CRUD actions for User model.
 */
class StaffController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'disable',
                'archived',
                'get-permission',
                'change-password',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'disable',
                'archived',
                'get-permission',
                'change-password',
            ],
            'rules' => [

                [
                    'actions' => [
                        'one',
                        'all',
                        'create',
                        'update',
                        'disable',
                        'archived',
                        'get-permission',
                        'change-password',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],


            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                '-able' => ['post'],
                'archived' => ['delete'],
                'get-permission' => ['get'],
            ],
        ];

        return $behaviors;
    }

    /**
     * get permission for role
     * staff/get-permission
     * get
     *
     * role
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetPermission()
    {
        return AuthItem::findAllPermission(Yii::$app->request->get('role'));
    }

    /**
     * Lists all User models.
     * user/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
//        $model = new UserSearch();
//        $result = $model->searchAll(Yii::$app->request->get());
//        return $result ? $model->all_fields($result) : $model->getErrors();
        if(Yii::$app->request->get('drive')){
            return  (new Query())->from('user')
                ->select([
                    'user.id AS id',
    'username',
    'title',
    'first_name',
    'middle_name',
    'surname',
    'gender',
    'date_birth',
    'email',
    'location_id',
    'address_1',
    'address_2',
    'city_id',
    'area_id',
    'state_id',
    'phone',
    'other_telephone_number',
    'notes',
    'date_employed',
    'date_dis_engaged',
    'access_level',
    'designation',
    'status',
    'disable',
    'deleted',
    'item_name'
                ])
                ->leftJoin('auth_assignment', 'auth_assignment.user_id = user.id')
                ->where(['auth_assignment.item_name' => 'staff'])
                ->all();

        }
        $model = (new Query())
            ->select([
'user.location_id',
 'auth_assignment.user_id',
 'user.id AS id',
 'auth_assignment.item_name',
                'name',
                'surname',
                'username',
                'user.disable AS disable',
 ])
            ->from('user')
            ->join('LEFT JOIN', 'location', 'location.id = user.location_id')
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = user.id')
            ->where(['auth_assignment.item_name' => 'staff'])
//            ->orderBy(['location.id' => SORT_ASC])
            ->all();
//        return $model;

        if(!$model){
            return 'Нет стафов!';
        }
        foreach ($model as $key => $value){
                $res[$model[$key]['name']] = $model[$key]['name'];
        }
foreach($res as $value){
    $result[] = ['location_name' => $res[$value]];
}

foreach($model as $key => $value){
//    $model[$key]['id'] = $model[$key]['user_id'];
    foreach($result as $key1 =>$value1){
        if($result[$key1]['location_name'] == $model[$key]['name']){
            $result[$key1]['users'][] =  $model[$key];
        }
    }
}
return $result;
    }


    /**
     * user/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $staff = $this->findModel(Yii::$app->request->get('id'));
//        if ($staff->location_id && isset($staff->location)) {


//            $location = $staff->location->getAllLocations();
//        }
        return [
            'staff' => $staff->one_fields(),
//            'all_location' => $location
        ];
    }

    /**
     *
     * host/.../staff/create
     * method post
     *
     * [['surname','email','gender','password'], 'request']
     * ['phone','first_name','middle_name',
     * 'date_birth','address_1','address_2','town','lga',
     * 'state_id', 'other_telephone_number','date_employed','date_dis_engaged',
     * 'notes','designation']
     * ['title', 'in', 'range' => ['mr', 'ms', 'other']]
     * ['gender', 'in', 'range' => ['male', 'female']],
     * ['access_level', 'in', 'range' => [1,2,3,4,5],
     *

     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws \yii\db\Exception
     */

    public function actionCreate()
    {

        if (Yii::$app->user->can('register_staff')) {
            $model = new User();
            if ($model->load(Yii::$app->request->post())) {
//                return $model->password;
//                return $model->status;
                switch($model->access_level){
                    case 1:$model->role = User::ROLE_ADMIN; break;
                    case 2:$model->role = User::ROLE_STAFF; break;
                    case 3:$model->role = User::ROLE_DATA_INPUTER; break;
//                    case 4:$model->role = User::ROLE_DONOR; break;
                    default: $model->role = User::ROLE_READER;
                }
                /*
                 * const ROLE_DONOR = 'donor';
    const ROLE_STAFF = 'staff';
    const ROLE_ADMIN = 'admin';*/

                $trans = Yii::$app->db->beginTransaction();

                if ($model->register() && !$model->getErrors()) {

                    if (!CMail::StaffRegister($model)) {
                        return ['error' => "Letter don't sent"];
                    }
                    $trans->commit();
                    return $model->one_fields();
                }

                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }


    /**
     * Updates an existing User model.
     * staff/update
     * post
     *
     * id
     * username
     * phone
     * email
     * permission
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->can('edit_staff')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->saveModel() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Deletes an existing User model.
     *
     * staff/Archived
     * delete
     *
     * id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {
        if (Yii::$app->user->can('archiving_staff')) {
            $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
            $trans = Yii::$app->db->beginTransaction();
            $model->email = $model->email.rand();
            $model->username = $model->username.rand();
            if ($model->remove()) {
                $trans->commit();
                return true;
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }


    /*
* Изменение пароля пользователя администратором
* Method post
* host/.../staff/change-password?auth_key=.../admin/
* 'id' - user`s id
* 'password' - new password
* */
    public function actionChangePassword()
    {

        $auth_key = Yii::$app->request->get('auth_key');
        $id = Yii::$app->request->post('id');
        $password = Yii::$app->request->post('password');
        if ($password && $id && $auth_key) {
            $usr = User::find()->where(['auth_key' => $auth_key])->one();
            if ($usr->getRole() === User::ROLE_ADMIN) {
                $staf = User::find()->where(['id' => $id])->one();
                $staf->password_hash = Yii::$app->security->generatePasswordHash($password);
                if ($staf->save() && Yii::$app->mailer->compose()
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($staf->email)
                        ->setSubject('New password')
                        ->setHtmlBody('<h2>your new password: ' . $password . '</h2><h3><a href="' . Url::base('https') . '/site/login">' . Yii::$app->request->hostinfo . '</a></h3>')
                        ->send()
                ) {
                    ;

                    return true;
                }
                return 'Password did not save';
            }
            return 'You have not permissions';
        }
    }

    /**
     * disable staff
     *
     * post
     * id
     *

     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDisable()
    {
        if (Yii::$app->user->can('disable_staff')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->disable() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            if ($model->deleted == User::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

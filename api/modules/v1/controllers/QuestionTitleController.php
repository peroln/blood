<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\QuestionTitle;
use common\models\search\QuestionTitleSearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * QuestionTitleController implements the CRUD actions for QuestionTitle model.
 */
class QuestionTitleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'save-order',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'save-order',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'save-order',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
                [
                    'actions' => [
                        'all',

                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'save-order' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all QuestionTitle models.
     * question-title/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new QuestionTitleSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * question-title/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'));
    }

    /**
     * question-title/create
     * post
     *
     * @return array|QuestionTitle|null
     */

    public function actionCreate()
    {
        $model = new QuestionTitle();
        $trans = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $trans->commit();
                return $model->one_fields();
        }
        $trans->rollBack();
        return $model->getErrors();
    }


    /**
     * Updates an existing QuestionTitle model.
     * question-title/update
     * post
     *
     * id
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ( $model->load(Yii::$app->request->post()) && $model->save()) {
            $trans->commit();
            return $model->one_fields();
        }
        $trans->rollBack();
        return $model->getErrors();
    }

    /**
     *
     * question-title/save-order
     * post
     *
     * @return array|bool
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */

    public function actionSaveOrder()
    {
        $categories = Yii::$app->request->post();
        $trans = Yii::$app->db->beginTransaction();
        foreach($categories as $one_category) {
            foreach ($one_category as $prev => $id) {
                $model = $this->findModel($id);

                //если изменился порядок записи
                if($model->previous != $prev) {
                    if (!$model->load(['previous' => $prev]) || !$model->save()) {
                        $trans->rollBack();
                        return $model->getErrors();
                    }
                }
            }
        }
        $trans->commit();
        return true;
    }

    /**
     * Deletes an existing QuestionTitle model.
     *
     * staff/Archived
     * delete
     *
     * id
     *
     * @return bool|string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->remove()) {
            $trans->commit();
            return true;
        }
        $trans->rollBack();
        return $model->getErrors();
    }


    /**
     * Finds the QuestionTitle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionTitle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuestionTitle::findOne(['id' => $id])) !== null) {
            if ($model->deleted == QuestionTitle::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

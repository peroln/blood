<?php
namespace api\modules\v1\controllers;


use common\models\User;
use common\models\LoginForm;
use common\models\CMail;


use Yii;
use yii\behaviors;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'login-key',
                'get-all-locations',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'login',
                'forgot-password',
                'login-key',
                'get-all-locations'
            ],
            'rules' => [
                [
                    'actions' => [
                        'login',
                        'forgot-password'
                    ],
                    'allow' => true,
                    'roles' => ['?'],

                ],

                [
                    'actions' => [
                        'login-key',
                        'get-all-locations'
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['post'],
                'login-key' => ['get'],
                'login' => ['post'],
                'get-all-locations' => ['get'],
                'forgot-password' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionGetAllLocations(){
        $location = User::findOne(Yii::$app->user->id)->location;
        return $location ? $location->getAllLocations() : false;
    }

    /**
     * site/login-key
     * get
     *
     * auth_key
     *
     * @return mixed
     */
    public function actionLoginKey()
    {
        $user = User::findOne(Yii::$app->user->id);
        $result['user'] =
            [
                'id' => $user->id,
                'auth_key' => $user->auth_key,
                'role' => $user->getRole()
            ];

        return $result;
    }

    /**
     * site/login
     *
     * post
     *
     * phone
     * password
     *
     * @return $this|array|bool|null|static
     */

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return false;
        }
        $request = Yii::$app->request->post();
        $model = new LoginForm();
        if ($model->load($request, "")) {
            if ($model->login()) {
                $user = User::findOne(Yii::$app->user->id);
                $result['user']['id'] = $user->id;
                $result['user']['auth_key'] = Yii::$app->user->identity->getAuthKey();
                $result['user']['role'] = $user->getRole();
                return $result;
            } else {
                return ['error' => 'Invalid login or password'];
            }
        }
        return ['error' =>'Error. Bad request.'];
    }

    /**get all schools and sities for registration
     * site/get-inform
     * get
     *
     * @return array
     */

//    public function actionGetLabels()
//    {
//        $request = Yii::$app->request->get();
//        if ($request) {
//            $model = "common\\models\\" . $request['model'];
//            return ['label' => $model::attributeLabels()];
//        }
//        return ['error' => 'Error. Bad request.'];
//    }


    /**
     * site/forgot-password
     *   post
     * phone
     *
     * @return array|string
     */

    public
    function actionForgotPassword()
    {
        $request = Yii::$app->request->post();
        if ($request['email']) {
            $user = User::findOne(['email' =>$request['email'], 'deleted' =>User::NOT_DELETED,'disable' =>User::NOT_DISABLED,]);

            if ($user) {
                $new_pass = Yii::$app->security->generateRandomString(8);
                $user->setScenario('save');
                $user->setPassword($new_pass);

                if ($user->save()) {
                    if (CMail::ForgotPassword($user, $new_pass)) {
                        return ['message' => 'Check your email for further instructions'];
                    } else {
                        return ['error' => "Letter don't sent"];
                    }
                } else {
                    return $user->getErrors();
                }
            } else {
                return ['error' =>  'User not found'];
            }
        } else {
            return ['error' =>  'Error. Bad request.'];
        }
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public
    function actionIndex()
    {
        $model = 'Hello!';
//        CMail::User();
        return $model;
    }

//    public function actionIndex()
//    {
//        return $this->render('test');
//    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

}

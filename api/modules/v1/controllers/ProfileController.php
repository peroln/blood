<?php

namespace api\modules\v1\controllers;


use Yii;
use common\models\User;
use common\models\PasswordChangeForm;


use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'update',
                'change-password',

            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'update',
                'change-password'
            ],
            'rules' => [

                [
                    'actions' => [
                        'one',
                        'update',
                        'change-password'
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'one' => ['get'],
                'update' => ['post'],
                'change-password' => ['post'],
            ],
        ];

        return $behaviors;
    }


    /**
     * profile/one
     * get
     *
     * @return mixed
     */
    public function actionOne()
    {
       $user =  Yii::$app->user->identity;
        if ($user->location_id && isset($user->location)) {
            $location = $user->location->getAllLocations();
        }
        return [
            'user' => $user->profile_fields(),
            'all_location' => $location
        ];

    }


    /**
     * Updates an existing User model.
     * profile/update
     * post
     *
     * username
     * phone
     * email
     * location_id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate()
    {
        $model = User::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && !$model->getErrors()) {
                $trans->commit();
                return $model->profile_fields();
            }
            $trans->rollBack();
        }
        return $model->getErrors();
    }

    /**profile/change-password
     * post
     * password
     * new_password
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionChangePassword()
    {
        $user =   User::findOne(Yii::$app->user->id);
        $model = new PasswordChangeForm($user);
        if ($model->load(Yii::$app->request->post())) {
            if($model->changePassword() &&!$model->getErrors()) {
                return true;
            } else {
                return $model->getErrors();
            }
        } else{
            return  ['error' => 'Error. Bad request.'];
        }
    }

}

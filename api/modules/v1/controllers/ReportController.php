<?php

namespace api\modules\v1\controllers;


use common\components\traits\errors;

use common\models\Area;
use common\models\BloodDrive;
use Faker\Provider\tr_TR\DateTime;
use Yii;
use common\models\Questionnaire;
use common\models\QuestionCategory;
use common\models\search\BloodDriveSearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;


use mPDF;

/**
 * ReportController implements the CRUD actions for City model.
 */
class ReportController extends Controller
{
    use errors;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'fdgdfg',

            ],
        ];
//        $behaviors['access'] = [
//            'class' => AccessControl::className(),
//            'only' => [
//                'generate',
//                'questionnaire',
//            ],
//            'rules' => [
//                [
//                    'actions' => [
//                        'generate',
//                        'questionnaire',
//                    ],
//                    'allow' => true,
//                    'roles' => ['!'],
//
//                ],
//            ],
//        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [

                'generate' => ['get'],
                'questionnaire' => ['get'],
                'blood-drive' => ['get'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all City models.
     * report/generate
     * get
     *
     * @return mixed
     */

    public function actionGenerate()
    {
        $mpdf = new mPDF();
        $stylesheet = file_get_contents(dirname(Yii::getAlias('@app')) . '\reports\style.css');
        $html = file_get_contents(dirname(Yii::getAlias('@app')) . '\reports\template_pdf.html');

        $mpdf->setFooter('{PAGENO}');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output('report.pdf', 'I');
    }

    /**
     * report/questionnaire
     * get
     * id
     *
     * @throws \MpdfException
     */
    public function actionQuestionnaire()
    {
        if (Yii::$app->request->get('id')) {
            $model = Questionnaire::findOne(['id' => Yii::$app->request->get('id'), 'deleted' => Questionnaire::NOT_DELETED]);
            if ($model) {
                $answers = QuestionCategory::allAnswers($model->id);

                $stylesheet = file_get_contents(dirname(Yii::getAlias('@app')) . '\reports\questionnaire_style.css');
                $html = $this->renderFile(Yii::getAlias('@report') . '/questionnare.php', [
                    'model' => $model,
                    'answers' => $answers,
                ]);

                $mpdf = new mPDF();
                $mpdf->setFooter('{PAGENO}');
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHTML($html, 2);
                $mpdf->Output(Yii::getAlias('@report') . '\report.pdf', 'D');
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            throw new NotFoundHttpException('The questionnaire ID can not be blank.');
        }
    }

    /**
     * report/blood-drive
     * get
     *
     * search_data_from
        search_data_to
       search_gender
     * area_id
     *
     * @throws \MpdfException
     */
    public function actionBloodDrive()
    {

        $model = new BloodDriveSearch();
        $model->disable = BloodDrive::NOT_DISABLED;
//
        $results = $model->searchAll(Yii::$app->request->get());

        $model->area_name = Area::findOne($model->area_id)->name;

        if($model->search_gender == "0" || $model->search_gender == "1"){
            $model->search_gender = intval($model->search_gender) + 1;
        }

        $stylesheet = file_get_contents(dirname(Yii::getAlias('@app')) . '\reports\blood_style.css');
        $html = $this->renderFile(Yii::getAlias('@report') . '/blood_drive.php', [
            'model' => $model,
            'results' => $results['models'],
        ]);

        $mpdf = new mPDF();
        $mpdf->setFooter('{PAGENO}');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output(Yii::getAlias('@report') . '\report.pdf', 'D');
    }
}

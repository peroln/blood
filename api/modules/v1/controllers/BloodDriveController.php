<?php

namespace api\modules\v1\controllers;

use common\models\DrivesBags;
use common\models\DrivesLocations;
use common\models\DrivesStaffs;
use Yii;
use common\models\BloodDrive;
use common\models\search\BloodDriveSearch;

use yii\db\Query;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BloodDriveController implements the CRUD actions for BloodDrive model.
 */
class BloodDriveController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
                'bought',
                'archived',
                'index'
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
                'bought',
                'archived',
                'index'
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'disable',
                        'bought',
                        'archived',
                        'index'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'index' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'bought' => ['post'],
                'disable' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all BloodDrive models.
     * BloodDrive/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $searchModel = new BloodDriveSearch();
        $dataProvider = $searchModel->search();
        $models = $dataProvider->getModels();
        return[
            'model' => $models,
            'count_page' => $dataProvider->pagination->pageCount,
            'count_model' => $dataProvider->getTotalCount()
        ];
//        $searchModel = new BloodDriveSearch();
//        return $searchModel->searchAll(Yii::$app->request->get());


//        return BloodDrive::findAll(['deleted' => 0]);

    }

    public function actionIndex()
    {
        $searchModel = new BloodDriveSearch();
        $dataProvider = $searchModel->search();
        $models = $dataProvider->getModels();
        return[
            'model' => $models,
            'count_page' => $dataProvider->pagination->pageCount,
            'count_model' => $dataProvider->getTotalCount()
        ];
    }

    /**
     * BloodDrive/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {

        $model = $this->findModel(Yii::$app->request->get('id'));
       return $model->one_fields();

    }

    /**
     * Creates a new BloodDrive model.
     *BloodDrive/create
     * post
     *  [['name',], 'required'],
    [['disable','staf_id', 'bag_id', 'location_id'],  'integer'],
    [['name', 'start_date', 'end_date', 'contact_person', 'notes',], 'string', 'max' => 255],
    [['name'], 'unique'],
     **
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('register_bag')) {

            $model = new BloodDrive();
            $trans = Yii::$app->db->beginTransaction();
            if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Updates an existing BloodDrive model.
     *BloodDrive/update
     * post
     *
     * id
     * name*
     *

     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->can('edit_bag')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            $trans = Yii::$app->db->beginTransaction();
            if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * blood-drive/bought
     * post
     *
     * id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionBought()
    {
        if (Yii::$app->user->can('bought_bag')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->bought() && !$model->getErrors()) {
                return $model->one_fields();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Deleted an existing BloodDrive model.
     *
     * BloodDrive/archived
     * delete
     *
     * id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {
        if (Yii::$app->user->can('archiving_bag')) {
            $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
            $trans = Yii::$app->db->beginTransaction();
            if ($model->remove()) {
                $trans->commit();
                return true;
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * blood-drive/disable
     * post
     *
     * id
     * disable
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDisable()
    {
        if (Yii::$app->user->can('disable_bag')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->disable() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Finds the BloodDrive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BloodDrive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BloodDrive::findOne(['id' => $id])) !== null) {
            if ($model->deleted == BloodDrive::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


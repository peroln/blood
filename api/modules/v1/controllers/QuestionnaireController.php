<?php

namespace api\modules\v1\controllers;


use Yii;
use common\models\Questionnaire;
use common\models\search\QuestionnaireSearch;

use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * QuestionnaireController implements the CRUD actions for Questionnaire model.
 */
class QuestionnaireController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Questionnaire models.
     * Questionnaire/all
     * get
     *
     * donor_id
     * @return mixed
     */
    public function actionAll()
    {
        $model = new QuestionnaireSearch();
        if(Yii::$app->request->get('donor_id')){
            $result = $model->searchAll(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        }
        $model->addError('donor_id', 'Donor ID can not be blank');
        return $model->getErrors();
    }

    /**
     * Questionnaire/one
     * get
     *
     * donor_id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Questionnaire/create
     * post

    donor_id,
    location_id,
    answers =>
    [
    [
    '25' => "papa"
    ],
    [
    '26' => "mama"
    ]
    ];
     *

     * @return $this|array|bool
     * @throws ForbiddenHttpException
     */

    public function actionCreate()
    {
        if (Yii::$app->user->can('register_question')) {
        $model = new Questionnaire();
        if ($model->load(Yii::$app->request->post())) {
            return $model->saveModel();
        }
        return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }


    /**
     * Updates an existing Questionnaire model.
     * questionnaire/update
     * post
     *
     * id
        donor_id,
        location_id,
        answers =>
        [
        [
        'id' => 1
        'title_id' => 25,
        'answer' => "papa"
        ],
        [
        'title_id' => 25,
        'answer' => "mama"
        ]
        ];
     *
     * @return $this|array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->can('edit_question')) {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(Yii::$app->request->post())) {
            return $model->saveModel();
        }
       return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }

    }

    /**
     * Deletes an existing Questionnaire model.
     *
     * questionnaire/archived
     * delete
     *
     * id
     *
     * @return array|bool|Questionnaire
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {

        if (Yii::$app->user->can('archiving_question')) {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->remove()) {
            $trans->commit();
            return $model;
        }
        $trans->rollBack();
        return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }


    /**
     * Finds the Questionnaire model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questionnaire the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questionnaire::findOne(['id' => $id])) !== null) {
            if ($model->deleted == Questionnaire::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

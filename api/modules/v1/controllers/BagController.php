<?php

namespace api\modules\v1\controllers;

use Yii;


use yii\web\NotFoundHttpException;
use yii\behaviors;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;
use common\models\Bag;
use yii\data\ActiveDataProvider;
use common\models\search\BagSearch;
/**
 * BagController implements the CRUD actions for Bag model.
 */
class BagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],

                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }


    /**
     * Lists all Bag models.
     * @return mixed
     */
    public function actionAll()
    {
        $searchModel = new BagSearch();
        $dataProvider = $searchModel->search();
        $models = $dataProvider->getModels();
        return[
            'model' => $models,
            'count_page' => $dataProvider->pagination->pageCount,
            'count_model' => $dataProvider->getTotalCount()
        ];
//        $searchModel = new BagSearch();
//        return $searchModel->searchAll(Yii::$app->request->get());

    }
    /**
     * Lists one Bag model.
     * @return mixed
     */
    public function actionOne()
    {

        return Bag::findOne(Yii::$app->request->get('id'));
    }

    /**
     * Displays a single Bag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return  $this->findModel($id);

    }

    /**
     * Creates a new Bag model.
     *
     * @return mixed
     *
     * method POST
     * host/.../bag/create
     *
     * [['donor_id', 'questionnaire_id',  'location_id',], 'required'],
    [['donor_id', 'questionnaire_id', 'drives_id', 'location_id', 'screening_agency', 'screening_status', 'weight', 'pressure', 'syphilis', 'hep_a', 'hep_b', 'screen_sent', 'created_by', 'update_by', 'deleted'], 'integer'],
    [['bar_code', 'date_sent', 'screen_date', 'screen_result', 'blood_group', 'hiv', 'expire_data', 'screen_notes', 'update_at'], 'string', 'max' => 255],
    [['bar_code'], 'unique'],
    [['questionnaire_id'], 'unique'],
    *
     */
    public function actionCreate()
    {
        $model = new Bag();

        if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
            return $model;
        } else {

            return $model->getErrors();
        }
    }

    /**
     * Updates an existing Bag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     *
     * method POST
     * host/.../bag/update
     *
     * [[ 'id',], 'required'],
     * [[ 'donor_id', 'questionnaire_id', 'drives_id',], 'integer'],
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
            return $model;
        } else {
            return $model->getErrors();
        }
    }

    /**
     * Deletes an existing Bag model.
     *
     * @param integer $id
     * @return object
     * method DELETE
     * host/.../bag/archived
     * [[ 'id',], 'required'],
     */
    public function actionArchived()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParam('id'));
        $model->deleted = Bag::DELETED;
        $model->save();
        return $model;
    }

    /**
     * Finds the Bag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}

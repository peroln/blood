<?php

namespace api\modules\v1\controllers;


use common\models\BloodDrive;
use common\models\search\BloodDriveSearch;
use Yii;
use common\models\CMail;
use common\models\Donor;
use common\models\search\DonorSearch;

use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * DonorController implements the CRUD actions for Donor model.
 */
class DonorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'disable',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all1' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'disable' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Donor models.
     * Donor/all
     * get
     *
     * @return mixed
     */
//    public function actionAll()
//    {
//        if (!Yii::$app->request->get('blood_drive_id')) {
//            $model = new DonorSearch();
//        } else {
////            $model = new BloodDriveSearch();
//            $model = BloodDrive::findOne(['id' => Yii::$app->request->get('blood_drive_id')]);
//            if(!$model){
//                return 'This is blooddrives "id" is absent';
//            }
//            return ArrayHelper::toArray($model, [
//                'common\models\BloodDrive' => [
//                    'id',
//                    'donation_date',
//                    'donor',
//                    'location',
//                    'screening_agency_number',
//                    'blood_bag_number',
//                    'notes_bag',
//                    'sent_for_screening',
//                    'date_sent',
//                    'screening_status',
//                    'date_screen',
//                    'screen_result',
//                    'date_expiry',
//                    'notes_screening',
//                ]
//            ]);
//        }
//        return $model->searchAll(Yii::$app->request->get());
//    }

    public function actionAll()
    {
        $model = new DonorSearch();
        return $model->searchAll(Yii::$app->request->get());
    }
    /**
     * Donor/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $donor = $this->findModel(Yii::$app->request->get('id'));
        if ($donor->location_id && isset($donor->location)) {
            $location = $donor->location->getAllLocations();
        }
        return [
            'donor' => $donor->one_fields(),
            'all_location' => $location
        ];
    }

    /**
     * donor/create
     * post
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws \yii\db\Exception
     */

    public function actionCreate()
    {
        if (Yii::$app->user->can('register_donor')) {
            $model = new Donor();
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->saveModel() && !$model->getErrors()) {
                    CMail::UserRegister($model);
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }


    /**
     * Updates an existing Donor model.
     * donor/update
     * post
     *
     * id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->can('edit_donor')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->saveModel() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Deletes an existing Donor model.
     *
     * staff/Archived
     * delete
     *
     * id
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionArchived()
    {
        if (Yii::$app->user->can('archiving_donor')) {
            $model = $this->findModel(Yii::$app->request->getBodyParam('id'));
            $trans = Yii::$app->db->beginTransaction();
            if ($model->remove()) {
                $trans->commit();
                return true;
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * disable staff
     *
     * post
     * id
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */

    public function actionDisable()
    {
        if (Yii::$app->user->can('disable_donor')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->disable() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Finds the Donor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Donor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donor::findOne(['id' => $id])) !== null) {
            if ($model->deleted == Donor::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
namespace api\modules\v1\controllers;

use common\models\BloodInfo;
use common\models\search\BloodInfoSearch;
use Yii;
use common\models\BloodDrive;
use common\models\search\BloodDriveSearch;

use yii\db\Query;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BloodDriveController implements the CRUD actions for BloodDrive model.
 */
class BloodInfoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'disable',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'disable',
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'disable' => ['post'],

            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all BloodInfo models.
     * BloodDrive/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new BloodInfoSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * BloodInfo/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();


    }

    /**
     * Creates a new BloodInfo model.
     *host/.../blood-info/create
     * post
     *   [['blood_drive_id', 'blood_group', 'weight', 'pressure', 'hiv', 'hbsag', 'hcv', 'syphillis',], 'required'],
     * [['blood_drive_id', 'weight', 'pressure', 'hbsag', 'hcv', 'syphillis',], 'integer'],
     * [['blood_group'], 'string', 'max' => 10],
     * [['hiv', 'notes'], 'string', 'max' => 255],
     **
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws \yii\db\Exception
     */
    public function actionCreate()

    {

        if (Yii::$app->user->can('register_bag')) {

            $model = new BloodInfo();
            $trans = Yii::$app->db->beginTransaction();
            if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Updates an existing BloodInfo model.
     * host/.../blood-info/update
     * post
     * [['id', 'blood_drive_id', 'blood_group', 'weight', 'pressure', 'hiv', 'hbsag', 'hcv', 'syphillis',], 'required'],
     * [['blood_drive_id', 'weight', 'pressure', 'hbsag', 'hcv', 'syphillis',], 'integer'],
     * [['blood_group'], 'string', 'max' => 10],
     * [['hiv', 'notes'], 'string', 'max' => 255],
     *
     *
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate()
    {
//
        if (Yii::$app->user->can('edit_bag')) {

            $model = $this->findModel(Yii::$app->request->post('id'));
            $trans = Yii::$app->db->beginTransaction();
            if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollBack();
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * blood-info/disable
     * post
     *
     * id
     * disable
     *
     * @return array|bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDisable()
    {
        if (Yii::$app->user->can('disable_bag')) {
            $model = $this->findModel(Yii::$app->request->post('id'));
            if ($model->load(Yii::$app->request->post())) {
                $trans = Yii::$app->db->beginTransaction();
                if ($model->disable() && !$model->getErrors()) {
                    $trans->commit();
                    return $model->one_fields();
                }
                $trans->rollBack();
            }
            return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Finds the BloodDrive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BloodDrive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BloodInfo::findOne(['id' => $id])) !== null) {
            if ($model->deleted == 0) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all BloodInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BloodInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BloodInfo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing BloodInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        $model->deleted = 1;
        $model->saveModel();
        return $model;
    }
}
<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Location;
use common\models\search\LocationSearch;

use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],

                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Location models.
     * Location/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new LocationSearch();
        return $model->searchAll(Yii::$app->request->get());
    }

    /**
     * Location/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Location model.
     * host/.../location/create
     * Method post
     * [['name', 'city_id'], 'required'],
    [['city_id', 'area_id', 'state_id'], 'integer'],
    [['name', 'address', 'another_address', 'email', 'notes', 'contact_name', 'contact_email',  'contact_phone', 'contact_another_phone', 'designation', 'contact_notes'], 'string', 'max' => 255],
    [['office_phone', 'another_phone'], 'string', 'max' => 20],

     * @return array|bool|Location
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('register_location')) {
        $model = new Location();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Updates an existing Location model.
     *Location/update
     * post
     *
     * id
     * name*
     *
    /**
     * @return array|bool|Location
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->can('edit_location')) {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Deletes an existing Location model.
     *
     * Location/archived
     * delete
     *
     * id
     *
     * @return array|bool|Location
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionArchived()
    {
        if (Yii::$app->user->can('archiving_location')) {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove()) {
            return $model;
        }
        return $model->getErrors();
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Finds the Location model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Location the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Location::findOne(['id' => $id])) !== null) {
            if($model->deleted == Location::NOT_DELETED){
                return $model;
            }else{
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

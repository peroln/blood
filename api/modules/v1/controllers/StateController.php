<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\State;
use common\models\search\StateSearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * StateController implements the CRUD actions for State model.
 */
class StateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'archived',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'update',
                'archived',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],

                [
                    'actions' => [
                        'one',
                        'create',
                        'update',
                        'archived',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'archived' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all State models.
     * State/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new StateSearch();
        return $model->searchAll(Yii::$app->request->get());
    }

    /**
     * State/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'));
    }

    /**
     * Creates a new State model.
     *State/create
     * post
     * name
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new State();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing State model.
     *State/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing State model.
     *
     * State/Archived
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionArchived()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Finds the State model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return State the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = State::findOne(['id' => $id])) !== null) {
            if ($model->deleted == State::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

var express =require('express');
var path=require('path');
var log=require('./libs/log')(module);
var config=require('./libs/config');
var mongoose=require('./libs/mongoose');
var AbonentModel = mongoose.model('Abonent');

var fs = require('fs');
var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var router = require('router');
var multer = require('multer');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './app/uploads');
  },
  filename: function (req, file, callback) {
	file.originalname=req.filetime+'-'+file.originalname;
    callback(null, file.originalname);
  }
});
app = express();

	
app.use(favicon(__dirname + '/app/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(router());

app.use(express.static(__dirname + '/app'));


app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});


app.get('/api', function (req, res) {
    res.send('API is running');
});

app.get('/api/abonents', function(req, res) {
	 return AbonentModel.find(function (err, abonents) {
			if (!err) {
				return res.send(abonents);
			} else {
				res.statusCode = 500;
				log.error('Internal error(%d): %s',res.statusCode,err.message);
				return res.send({ error: 'Server error' });
			}
		});
});

app.post('/api/abonents', function(req, res) {
	var abonent = new AbonentModel({
			name: req.body.name,
			address: req.body.address,
			image: req.body.image
		});

		abonent.save(function (err) {
			if (!err) {
				log.info("abonent created");
				return res.send({ status: 'OK', abonent:abonent });
			} else {
				console.log(err);
				if(err.name == 'ValidationError') {
					res.statusCode = 400;
					res.send({ error: 'Validation error' });
				} else {
					res.statusCode = 500;
					res.send({ error: 'Server error' });
				}
				log.error('Internal error(%d): %s',res.statusCode,err.message);
			}
		});
});

app.get('/api/abonents/:id', function(req, res) {
    return AbonentModel.findById(req.params.id, function (err, abonent) {
        if(!abonent) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', abonent:abonent });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.put('/api/abonents/:id', function (req, res){
   return AbonentModel.findById(req.params.id, function (err, abonent) {
        if(!abonent) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        abonent.name = req.body.name;
		abonent.address = req.body.address;
		log.info("'"+req.body.image+"'");
		if(abonent.image!='' && req.body.image=='')
		{
			var imageFile='./public/uploads/'+abonent.image;
					fs.exists(imageFile, (exists) => {
					  if (exists) {
						fs.unlinkSync(imageFile);
						} 
					});
		}
		abonent.image = req.body.image;
        return abonent.save(function (err) {
            if (!err) {
                log.info("abonent updated");
                return res.send({ status: 'OK', abonent:abonent });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
});

app.delete('/api/abonents/:id', function (req, res){
   return AbonentModel.findById(req.params.id, function (err, abonent) {
        if(!abonent) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return abonent.remove(function (err) {
            if (!err) {
                log.info("abonent removed");
				if(abonent.image!='')
				{
					var imageFile='./public/uploads/'+abonent.image;
					fs.exists(imageFile, (exists) => {
					  if (exists) {
						fs.unlinkSync(imageFile);
						} 
					});
					
				}
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
});
var upload = multer({ storage : storage}).single('file');

app.post('/api/upload', function(req, res) {
	req.filetime=Date.now();
	upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end(''+req.filetime);
    });
});
app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});


app.listen(config.get('port'), function(){
    log.info('Express server listening on port ' + config.get('port'));
});
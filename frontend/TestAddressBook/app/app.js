'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.version'
]).
    config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    }])
    .controller('AppController', ['$scope',function($scope) {
        $scope.selectedItem={_id:'',name:'',address:'',image:''};
        var clearItem=function()
        {
            uploadFile.files=[];

            $scope.$apply(function() {
                $scope.selectedItem={_id:'',name:'',address:'',image:''};
            });

        };
        getAbonents();
        grid.addEventListener('selected-items-changed',function() {
            if (grid.selection.selected().length > 0) {
                $scope.$apply(function () {
                    $scope.selectedItem = grid.items[grid.selection.selected()];
                });
                if ($scope.selectedItem.image != '' && $scope.selectedItem.image != null) {
                    uploadFile.files = [new File([""], $scope.selectedItem.image)];
                }
                else {
                    uploadFile.files = [];
                }
            }
            else {
                clearItem();
            }

        });

        uploadFile.addEventListener('upload-success', function(event) {

            $scope.$apply(function(){
                $scope.selectedItem.image=event.detail.xhr.responseText+'-'+event.target.files[0].name;
                console.log($scope.selectedItem.image);
            });

        });
        uploadFile.addEventListener('upload-abort', function(event) {
            $scope.$apply(function(){
                $scope.selectedItem.image='';

            });

        });

        $scope.deleteItem=function()
        {
            if($scope.selectedItem._id!='')
            {
                deleteItem($scope.selectedItem._id);
            }
        };
        $scope.saveItem=function()
        {

            $scope.selectedItem.name=nameInput.value;
            $scope.selectedItem.address=addressInput.value;


            saveItem($scope.selectedItem);
        };
    }]);

function deleteItem(id) {

    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "/api/abonents/" + id, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                getAbonents();
            }
            else
            {
                e.detail={xhr:xhr};
                onError(e);
            }
        }
    }
    xhr.onerror =onError;
    xhr.send(null);

}
function saveItem(selectedItem)
{
    var xhr = new XMLHttpRequest();
    if(selectedItem._id=='')
    {
        xhr.open("POST", "/api/abonents/", true);
    }
    else
    {
        xhr.open("PUT", "/api/abonents/" + selectedItem._id, true);
    }
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                getAbonents();
            }
            else
            {
                e.detail={xhr:xhr};
                onError(e);
            }
        }
    }
    xhr.onerror=onError;
    xhr.body=JSON.stringify({name:selectedItem.name,address:selectedItem.address,image:selectedItem.image});
    xhr.send(xhr.body);

}
function onError(event)
{
    if (event.detail.xhr.status == '400') {
        errorToast.text='Validation error: Name or Address cannot be empty';
        errorToast.open();
    }
    else
    {
        errorToast.text=event.detail.xhr.statusText;
        errorToast.open();
    }

}
function getAbonents()
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/abonents", true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                grid.items=JSON.parse(xhr.responseText);

                grid.columns[2].renderer = function(cell) {

                    cell.element.innerHTML = '';
                    if(cell.data!='' && cell.data!=null)
                    {
                        //console.log(cell.data);
                        var img = document.createElement('img');
                        img.style.width = '24px';
                        img.setAttribute('src', 'uploads/'+cell.data);
                        cell.element.appendChild(img);
                    }
                };

            }
            else
            {
                e.detail={xhr:xhr};
                onError(e);
            }
        }
    }
    xhr.onerror =onError;
    xhr.send(null);
}
var mongoose= require('mongoose');
var log= require('./log')(module);
var config= require('./config');
var db = mongoose.connection;

mongoose.connect(config.get('mongoose:uri'));

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to DB!");
});

var Schema = mongoose.Schema;

// Schemas

var Abonent = new Schema({
    name: { type: String, required: true },
	address: { type: String, required: true },
	image: { type: String, required: false, default:'' }
});

// validation
Abonent.path('name').validate(function (v) {
    return v.length > 1 && v.length < 70;
});


module.exports = mongoose.model('Abonent', Abonent);;
package com.emj365.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.os.CountDownTimer;
import android.os.Environment;
import android.content.Context;
import java.util.UUID;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

public class AudioRecorderAPI extends CordovaPlugin {

  private MediaRecorder myRecorder;
  private String outputFile;
  private CountDownTimer countDowntimer;
  private MediaPlayer mediaPlayer;

  @Override
  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    Context context = cordova.getActivity().getApplicationContext();
    Integer seconds;

    if (action.equals("record")) {
        if (args.length() >= 1) {
              seconds = args.getInt(0);
            } else {
              seconds = 7;
            }
      outputFile = context.getFilesDir().getAbsoluteFile() + "/"
        + UUID.randomUUID().toString() + ".m4a";
      myRecorder = new MediaRecorder();
      myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
      myRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
      myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
      myRecorder.setAudioSamplingRate(44100);
      myRecorder.setAudioChannels(1);
      myRecorder.setAudioEncodingBitRate(32000);
      myRecorder.setOutputFile(outputFile);

      try {
        myRecorder.prepare();
        myRecorder.start();
      } catch (final Exception e) {
        cordova.getThreadPool().execute(new Runnable() {
          public void run() {
            callbackContext.error(e.getMessage());
          }
        });
        return false;
      }

      countDowntimer = new CountDownTimer(seconds * 1000, 1000) {
        public void onTick(long millisUntilFinished) {}
        public void onFinish() {
          stopRecord(callbackContext);
        }
      };
      countDowntimer.start();
      return true;
    }

    if (action.equals("stop")) {
      countDowntimer.cancel();
      stopRecord(callbackContext);
      return true;
    }
    if (action.equals("playback")) {
        String url;
        if (args.length() >= 1) {
          url = args.getString(0);
        }
        else
        {
            cordova.getThreadPool().execute(new Runnable() {
                          public void run() {
                            callbackContext.error("No URL");
                          }
                        });
            return false;
        }
        killMediaPlayer(callbackContext);
        if(url.equals(""))
        {
            mediaPlayer=null;
            cordova.getThreadPool().execute(new Runnable() {
                          public void run() {
                            callbackContext.success("Stoped");
                          }
                        });
            return true;
        }
        mediaPlayer=new MediaPlayer();
       // mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
         try {
                 mediaPlayer.setDataSource(url);
          } catch (IOException e)
          {
             cordova.getThreadPool().execute(new Runnable() {
                               public void run() {
                             callbackContext.error("ioerror");
                             }
                          });
         }
        try {
                 mediaPlayer.prepare();
          } catch (IOException e)
          {
             cordova.getThreadPool().execute(new Runnable() {
                               public void run() {
                             callbackContext.error("ioerror");
                             }
                          });
         }

          mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                 public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                 }
               });

        mediaPlayer.start();
         cordova.getThreadPool().execute(new Runnable() {
              public void run() {
                callbackContext.success("OK");
              }
            });
        return true;
    }

    return false;
  }
    private void killMediaPlayer(final CallbackContext callbackContext) {
        if(mediaPlayer!=null) {
            try {
                mediaPlayer.release();
            }
            catch(Exception e) {
             cordova.getThreadPool().execute(new Runnable() {
                  public void run() {
                callbackContext.error("release error");
                }
             });
            }
        }
    }
  private void stopRecord(final CallbackContext callbackContext) {
    myRecorder.stop();
    myRecorder.release();
    cordova.getThreadPool().execute(new Runnable() {
      public void run() {
        callbackContext.success(outputFile);
      }
    });
  }

}

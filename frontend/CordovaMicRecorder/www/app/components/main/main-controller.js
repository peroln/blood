"use strict";

angular.module("ngapp").controller('MainController', ['$scope', function ($scope) {
    $scope.recording = false;
    $scope.readyForRecord = true;
    $scope.paused = true;
    $scope.playing = false;
    $scope.recordData = [];

    document.addEventListener("deviceready", function(){
        cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.RECORD_AUDIO, checkPermissionCallback, null);
        cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, checkPermission2Callback, null);
        cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.READ_EXTERNAL_STORAGE, checkPermission3Callback, null);
    }, false);
    $scope.removeLast = function (rec) {
        if($scope.playing)
        {
            $scope.stopAudio();
        }
        var index=$scope.recordData.indexOf(rec);
        if (index==($scope.recordData.length-1)) {
            deleteFile(rec.name,rec.path);

            $scope.recordData.splice(-1,1)

        }
    };
    $scope.playAudio = function (path) {
        $scope.stopAudio();

        toastMessage("playAudio():" + path);

        window.plugins.audioRecorderAPI.playback(
            path,
            function(msg){
                toastMessage("playAudio():" + msg);
                $scope.$apply($scope.playing = true);
            },
            function(msg){
                toastMessage("playAudio():" + msg);
                $scope.$apply($scope.playing = false);

            });
    };
    $scope.stopAudio = function () {
        window.plugins.audioRecorderAPI.playback(
            '',
            function(msg){
                toastMessage("playAudio():" + msg);
                $scope.$apply($scope.playing = false);
            },
            function(msg){
                toastMessage("playAudio():" + msg);
            });
    };
    $scope.resumeCaptureAudio = function () {

        $scope.paused = false;
        window.plugins.audioRecorderAPI.record(function(savedFilePath){
         capturePathSuccess($scope,savedFilePath,'')
         }, function(msg){captureErrorMessage(msg,$scope)}, 3600);


    };
    $scope.captureAudio = function () {
        toastMessage('Starting..');

        $scope.recording = true;
        $scope.readyForRecord = false;

        $scope.recordData.forEach(function(rec) {
            deleteFile(rec.name,rec.path);
        });

        $scope.recordData = [];

        $scope.resumeCaptureAudio();
    };

    $scope.stopCaptureAudio = function () {
        $scope.recording = false;
        if ($scope.paused) {
            $scope.readyForRecord = checkUploaded($scope);
        }
        else {
            $scope.pauseCaptureAudio();
        }
    };
    $scope.pushCaptureAudio = function (symbol) {

        window.plugins.audioRecorderAPI.stop(function(savedFilePath){
            capturePathSuccess($scope,savedFilePath,symbol)
        }, function(msg){captureErrorMessage(msg,$scope)});

    };
    $scope.pauseCaptureAudio = function () {

        $scope.paused = true;
        $scope.pushCaptureAudio('');

    };
}]);

function checkPermissionCallback(status) {
    if (!status.hasPermission) {
        var errorCallback = function () {
            toastMessage('Mic permission is not turned on');
        };

        cordova.plugins.permissions.requestPermission(
            cordova.plugins.permissions.RECORD_AUDIO,
            function (status) {
                if (!status.hasPermission) {
                    errorCallback();
                }
                else {
                    toastMessage('Mic permission is on');

                }
            },
            errorCallback);
    }
    else {
        toastMessage('Mic permission is on');

    }
}

function checkPermission2Callback(status) {
    if (!status.hasPermission) {
        var errorCallback = function () {
            toastMessage('Write permission is not turned on');
        };

        cordova.plugins.permissions.requestPermission(
            cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE,
            function (status) {
                if (!status.hasPermission) {
                    errorCallback();
                }
                else {
                    toastMessage('Write permission is on');

                }
            },
            errorCallback);
    }
    else {
        toastMessage('Write permission is on');

    }
}
function checkPermission3Callback(status) {
    if (!status.hasPermission) {
        var errorCallback = function () {
            toastMessage('Read permission is not turned on');
        };

        cordova.plugins.permissions.requestPermission(
            cordova.plugins.permissions.READ_EXTERNAL_STORAGE,
            function (status) {
                if (!status.hasPermission) {
                    errorCallback();
                }
                else {
                    toastMessage('Read permission is on');

                }
            },
            errorCallback);
    }
    else {
        toastMessage('Read permission is on');

    }
}

function capturePathSuccess($scope,savedFilePath,symbol) {
    toastMessage('captureSuccess.. ' + savedFilePath);

    var fileName = savedFilePath.split('/')[savedFilePath.split('/').length - 1];
    $scope.$apply(function () {
        $scope.recordData.push({symbol: symbol, name: fileName, path: savedFilePath, uploaded:false});
    });
    if(!$scope.paused)
    {
        $scope.resumeCaptureAudio();
    }
    uploadNext($scope);
}


function captureErrorMessage(msg, $scope) {
    $scope.$apply(function () {
        $scope.paused = true;
    });

    toastMessage(msg);
}
function uploadNext($scope) {
    for (var i in $scope.recordData) {
        if ($scope.recordData[i].uploaded == false) {
            uploadPath($scope.recordData[i].path, $scope.recordData[i].name, $scope);
            break;
        }
    }

}

function uploadPath(path, name, $scope) {

    var ft = new FileTransfer();
    ft.upload(path,
        "http://93.78.150.96:63342/api/upload",
        function (result) {
            toastMessage(result.bytesSent + ' bytes sent');
            toastMessage('Upload success: ' + result.responseCode);
            $scope.$apply(function () {
                for (var i in $scope.recordData) {
                    if ($scope.recordData[i].path == path)
                    {
                        $scope.recordData[i].uploaded = true;
                        break;
                    }
                }
            });
            uploadNext($scope);
            if (!$scope.recording) {
                $scope.$apply(function () {
                    $scope.readyForRecord = checkUploaded($scope);
                });
            }
        },
        function (error) {
            toastMessage('Error uploading file ' + path + ': ' + error.code);
        },
        {fileName: name});
}

function toastMessage(message) {
    errorToast.text = message;
    errorToast.open();
}
function checkUploaded($scope)
{
    for (var i in $scope.recordData) {
        if ($scope.recordData[i].uploaded == false)
        {
           return false;
        }
    }
    return true;
}
function deleteFile(filename, path) {
    window.resolveLocalFileSystemURL(path, function (dir) {
        dir.getFile(filename, {create: false}, function (fileEntry) {
            fileEntry.remove(
                function() {},
                function(error)
                {toastMessage('Error deleting the file' +JSON.stringify(error));},
                function () {});
        });
    });
}
